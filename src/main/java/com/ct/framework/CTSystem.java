package com.ct.framework;

import com.ct.framework.core.configuration.SystemConfigurationManager;
import com.ct.framework.data.access.DataCommandManager;
import com.ct.framework.data.access.DatabaseManager;
import com.ct.framework.utility.JedisPoolHelper;
import lombok.extern.slf4j.Slf4j;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 初始化.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public final class CTSystem {

    /**
     * 私有构造函数.
     */
    private CTSystem() {
    }

    /**
     * 服务端调用此 初始化方法.
     */
    public static void InitAll() {
        long startTime = System.currentTimeMillis();

        log.info("CT 系统 初始化 开始");

        SystemConfigurationManager.init();

        //初始化sql.xml文件
        DataCommandManager.init();

        //初始化数据库连接
        DatabaseManager.init();

        //初始化缓存
        JedisPoolHelper.init();

        //抽象工厂模式 示例
//        IQueryFactory factory = new MessageQueryFactory();
//        MessageQuery messageQuery = factory.CreateMessageQuery("kafka");
//        QueryEntity queryEntity = new QueryEntity();
//        messageQuery.send(queryEntity);

        //建造者模式 示例
//        Director director = new Director();
//        director.GetServerException().run(new Exception(), "");

        //装饰模式 示例
//        BusinessException bx = new ServerException();
//        bx = new SystemExceptionDecorator(bx);
//        bx.PrintMessage(new Exception(), "");

        long endTime = System.currentTimeMillis();

        log.info("CT 系统 初始化 共耗时:{} Ms", endTime - startTime);

        log.info("CT 系统 初始化 结束");
    }

    /**
     * 客户端调用此 初始化方法.
     */
    public static void InitClient() throws Exception {
        long startTime = System.currentTimeMillis();

        log.info("CT 系统 初始化 开始");

        SystemConfigurationManager.init();

        long endTime = System.currentTimeMillis();

        log.info("CT 系统 初始化 共耗时:{} Ms", endTime - startTime);

        log.info("CT 系统 初始化 结束");
    }
}
