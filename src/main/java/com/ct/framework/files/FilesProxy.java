package com.ct.framework.files;

import com.ct.framework.files.entity.UploadOptions;
import com.ct.framework.files.entity.UploadRequest;
import com.ct.framework.files.entity.UploadResponse;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 文件上传下载代理类
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class FilesProxy implements IFilesPersistent {

    private IFilesPersistent iFilesPersistent;

    public FilesProxy(IFilesPersistent iFilesPersistent) {
        this.iFilesPersistent = iFilesPersistent;
    }

    @Override
    public UploadResponse Save(String fileName, byte[] body, UploadOptions options) {
        return this.iFilesPersistent.Save(fileName, body, options);
    }

    @Override
    public UploadResponse Save(UploadRequest request) {
        return this.iFilesPersistent.Save(request);
    }

    @Override
    public IFilesPersistent GetProxy() {
        return this;
    }
}
