package com.ct.framework.files.entity;

import java.util.List;

public class UploadResponse {
    private String key;
    private String url;
    private String md5;
    private long bytes;
    private String filename;
    private String indexUrl;
    private List<FileEntry> extractFiles;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public long getBytes() {
        return bytes;
    }

    public void setBytes(long bytes) {
        this.bytes = bytes;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getIndexUrl() {
        return indexUrl;
    }

    public void setIndexUrl(String indexUrl) {
        this.indexUrl = indexUrl;
    }

    public List<FileEntry> getExtractFiles() {
        return extractFiles;
    }

    public void setExtractFiles(List<FileEntry> extractFiles) {
        this.extractFiles = extractFiles;
    }
}
