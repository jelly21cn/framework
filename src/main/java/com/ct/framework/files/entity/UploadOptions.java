package com.ct.framework.files.entity;

import java.util.HashMap;
import java.util.Map;

public class UploadOptions {

    private String prefix = "f";
    private boolean extract = false;
    private String key;
    private boolean preserveName = true;
    private String bucket;
    private Map<String, Object> tags = new HashMap<>();

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public boolean isExtract() {
        return extract;
    }

    public void setExtract(boolean extract) {
        this.extract = extract;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isPreserveName() {
        return preserveName;
    }

    public void setPreserveName(boolean preserveName) {
        this.preserveName = preserveName;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public Map<String, Object> getTags() {
        return tags;
    }

    public void setTags(Map<String, Object> tags) {
        this.tags = tags;
    }
}
