package com.ct.framework.files.entity;

import org.springframework.web.multipart.MultipartFile;

public class UploadRequest extends UploadOptions {
    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
