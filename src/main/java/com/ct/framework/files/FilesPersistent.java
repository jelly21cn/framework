package com.ct.framework.files;

import com.ct.framework.files.entity.UploadOptions;
import com.ct.framework.files.entity.UploadRequest;
import com.ct.framework.files.entity.UploadResponse;
import com.ct.framework.core.file.FileFactory;
import com.ct.framework.core.file.IFileHandler;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 文件上传下载.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class FilesPersistent implements IFilesPersistent {

    private IFilesPersistent proxy = null;

    private IFileHandler iFileHandler = FileFactory.DistributedFiles();

    @Override
    public UploadResponse Save(String fileName, byte[] body, UploadOptions options) {
        if (this.isProxy()) {
            return iFileHandler.Save(fileName, body, options);
        }
        return null;
    }

    @Override
    public UploadResponse Save(UploadRequest request) {
        if (this.isProxy()) {
            return iFileHandler.Save(request);
        }
        return null;
    }

    @Override
    public IFilesPersistent GetProxy() {
        this.proxy = new FilesProxy(this);
        return this.proxy;
    }

    private boolean isProxy() {
        if (this.proxy == null) {
            return false;
        } else {
            return true;
        }
    }
}
