package com.ct.framework.files;

import com.ct.framework.files.entity.UploadOptions;
import com.ct.framework.files.entity.UploadRequest;
import com.ct.framework.files.entity.UploadResponse;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 文件上传下载.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public interface IFilesPersistent {

    UploadResponse Save(String fileName, byte[] body, UploadOptions options);

    UploadResponse Save(UploadRequest request);

    //获取自身代理
    IFilesPersistent GetProxy();
}
