package com.ct.framework.files;

import com.ct.framework.files.entity.UploadOptions;
import com.ct.framework.files.entity.UploadRequest;
import com.ct.framework.files.entity.UploadResponse;
import com.ct.framework.core.file.IFileHandler;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: Mongo实现.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class MongoProvider implements IFileHandler {
    @Override
    public UploadResponse Save(String fileName, byte[] body, UploadOptions options) {
        return null;
    }

    @Override
    public UploadResponse Save(UploadRequest request) {
        return null;
    }
}
