package com.ct.framework.contract;

import java.io.Serializable;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 默认的查询体契约规则
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class DefaultQueryContract implements Serializable {

    protected DefaultQueryContract() {
    }

    /**
     * 头部
     */
    private MessageHeader header;

    /**
     * 分页
     */
    private PagingInfo pagingInfo;

    public MessageHeader GetHeader() {
        return header;
    }

    public void SetHeader(MessageHeader header) {
        this.header = header;
    }

    public PagingInfo GetPagingInfo() {
        return pagingInfo;
    }

    public void SetPagingInfo(PagingInfo pagingInfo) {
        this.pagingInfo = pagingInfo;
    }
}
