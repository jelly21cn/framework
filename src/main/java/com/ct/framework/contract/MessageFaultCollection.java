package com.ct.framework.contract;

import java.io.Serializable;
import java.util.ArrayList;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 错误信息
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class MessageFaultCollection extends ArrayList<MessageFault> implements Serializable {
}
