package com.ct.framework.contract;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 事件
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class MessageEvent {

    /**
     * 事件ID
     */
    private String eventID;

    /**
     * 动作
     */
    private String action;

    /**
     * 时间戳
     */
    private long timeStamp;

    /**
     * 处理者
     */
    private String processor;

    /**
     * 业主编号
     */
    private String companyCode;

    /**
     * 描述
     */
    private String description;

    public String GetEventID() {
        return eventID;
    }

    public void SetEventID(String eventID) {
        this.eventID = eventID;
    }

    public String GetAction() {
        return action;
    }

    public void SetAction(String action) {
        this.action = action;
    }

    public long GetTimeStamp() {
        return timeStamp;
    }

    public void SetTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String GetProcessor() {
        return processor;
    }

    public void SetProcessor(String processor) {
        this.processor = processor;
    }

    public String GetCompanyCode() {
        return companyCode;
    }

    public void SetCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String GetDescription() {
        return description;
    }

    public void SetDescription(String description) {
        this.description = description;
    }
}

