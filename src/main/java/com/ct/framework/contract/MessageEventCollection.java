package com.ct.framework.contract;

import java.util.HashMap;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 系统事件.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class MessageEventCollection extends HashMap<String, MessageEvent> {

    /**
     * 返回事件ID
     */
    protected String GetKeyForItem(MessageEvent event) {
        return event.GetEventID();
    }
}
