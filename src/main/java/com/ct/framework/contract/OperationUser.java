package com.ct.framework.contract;

import java.io.Serializable;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 操作用户的信息.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class OperationUser implements Serializable {

    public OperationUser() {
    }

    public OperationUser(String loginName, Integer groupId, Integer organizationId, String companyCode) {
        this.loginName = loginName;
        this.groupId = groupId;
        this.organizationId = organizationId;
        this.companyCode = companyCode;
        this.uniqueUserName = String.format("{0}-{1}-{2}[{3}]", this.loginName, this.groupId, this.organizationId, this.companyCode);
    }

    private String displayName;
    /**
     * 登录名 用户名
     */
    private String loginName;

    /**
     * 组织
     */
    private Integer organizationId;
    /**
     * 多租户时使用
     */
    private String companyCode;

    private Integer userSysNo;

    private Integer groupId;

    /**
     * uniqueUserName
     */
    private String uniqueUserName;

    public String GetUniqueUserName() {
        return this.uniqueUserName;
    }

    public void SetUniqueUserName(String uniqueUserName) {
        this.uniqueUserName = uniqueUserName;
    }

    public String GetDisplayName() {
        return displayName;
    }

    public void SetDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String GetLoginName() {
        return loginName;
    }

    public void SetLoginName(String loginName) {
        this.loginName = loginName;
    }

    public Integer GetOrganizationId() {
        return organizationId;
    }

    public void SetOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    public String GetCompanyCode() {
        return companyCode;
    }

    public void SetCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public Integer GetUserSysNo() {
        return userSysNo;
    }

    public void SetUserSysNo(Integer userSysNo) {
        this.userSysNo = userSysNo;
    }

    public Integer GetGroupId() {
        return groupId;
    }

    public void SetGroupId(Integer groupId) {
        this.groupId = groupId;
    }
}
