package com.ct.framework.contract;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 数据显示契约
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public interface IDataContract<T> {

    /**
     * 读取.
     */
    T GetBody();

    /**
     * 赋值.
     */
    void SetBody(T t);
}
