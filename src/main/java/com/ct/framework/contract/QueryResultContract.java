package com.ct.framework.contract;

import java.io.Serializable;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 查询结果契约体
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class QueryResultContract<T> implements Serializable {

    /**
     * 返回列表
     */
    private T resultList;

    /**
     * 分页
     */
    private PagingInfo pagingInfo;

    /**
     * 错误信息
     */
    private MessageFaultCollection messageFaultCollection;

    public T GetResultList() {
        return resultList;
    }

    public void SetResultList(T resultList) {
        this.resultList = resultList;
    }

    public PagingInfo GetPageInfo() {
        return pagingInfo;
    }

    public void setPageInfo(PagingInfo pagingInfo) {
        this.pagingInfo = pagingInfo;
    }

    public MessageFaultCollection GetFaults() {
        return messageFaultCollection;
    }

    public void SetFaults(MessageFaultCollection messageFaultCollection) {
        this.messageFaultCollection = messageFaultCollection;
    }
}
