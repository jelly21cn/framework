package com.ct.framework.contract;

import java.io.Serializable;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 分页
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class PagingInfo implements Serializable {

    public PagingInfo() {
    }

    public PagingInfo(long totalCount) {
        this.totalCount = totalCount;
    }


    /**
     * 排序方式
     * DESC/ASC
     */
    private SortType sortType;

    /**
     * 排序字段
     */
    private String sortField;

    /**
     * 当前页
     */
    private int startRowIndex;

    /**
     * 每页数量
     */
    private int pageSize;

    /**
     * 总数量
     */
    private long totalCount;


    public SortType GetSortType() {
        return sortType;
    }

    public void SetSortType(SortType sortType) {
        this.sortType = sortType;
    }

    public String GetSortField() {
        return sortField;
    }

    public void SetSortField(String sortField) {
        this.sortField = sortField;
    }

    public int GetStartRowIndex() {
        return startRowIndex;
    }

    public void SetStartRowIndex(int startRowIndex) {
        this.startRowIndex = startRowIndex;
    }

    public int GetPageSize() {
        return pageSize;
    }

    public void SetPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public long GetTotalCount() {
        return totalCount;
    }

    public void SetTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }
}
