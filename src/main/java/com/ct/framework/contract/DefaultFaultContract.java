package com.ct.framework.contract;

import java.io.Serializable;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 默认的错误体契约规则
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class DefaultFaultContract implements Serializable {

    /**
     * 头部
     */
    private MessageHeader header;

    /**
     * 错误码
     */
    private String errorCode;

    /**
     * 错误描述
     */
    private String errorDescription;

    /**
     * 详细错误信息，包含出错的理由，业务上下文描述对象
     */
    private String errorDetail;

    public MessageHeader GetHeader() {
        return header;
    }

    public void SetHeader(MessageHeader header) {
        this.header = header;
    }

    public String GetErrorCode() {
        return errorCode;
    }

    public void SetErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String GetErrorDescription() {
        return errorDescription;
    }

    public void SetErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String GetErrorDetail() {
        return errorDetail;
    }

    public void SetErrorDetail(String errorDetail) {
        this.errorDetail = errorDetail;
    }
}
