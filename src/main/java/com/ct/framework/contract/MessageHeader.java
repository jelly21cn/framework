package com.ct.framework.contract;

import java.io.Serializable;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 头部消息体.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class MessageHeader implements Serializable {
    /**
     * 代码的命名空间
     */
    private String nameSpace;

    /**
     * 动作
     * 例如：Void,Create,Hold,Update,Batch,UnHold,Dispatch,Verify,UnVerify
     */
    private String action;

    /**
     * 版本
     */
    private String version;

    /**
     * 语言
     */
    private String language;

    /**
     * 服务器发送者
     */
    private String sender;

    /**
     * 返回地址
     */
    private String callbackAddress;

    /**
     * 来源系统
     */
    private String fromSystem;

    /**
     * 目标系统
     */
    private String toSystem;

    /**
     * 描述
     */
    private String Description;

    /**
     * 原始发起者
     */
    private String originalSender;

    /**
     * 原始消息号
     */
    private String originalGUID;

    private OperationUser operationUser;

    public String GetNameSpace() {
        return nameSpace;
    }

    public void SetNameSpace(String nameSpace) {
        this.nameSpace = nameSpace;
    }

    public String GetAction() {
        return action;
    }

    public void SetAction(String action) {
        this.action = action;
    }

    public String GetVersion() {
        return version;
    }

    public void SetVersion(String version) {
        this.version = version;
    }

    public String GetLanguage() {
        return language;
    }

    public void SetLanguage(String language) {
        this.language = language;
    }

    public String GetSender() {
        return sender;
    }

    public void SetSender(String sender) {
        this.sender = sender;
    }

    public String GetCallbackAddress() {
        return callbackAddress;
    }

    public void SetCallbackAddress(String callbackAddress) {
        this.callbackAddress = callbackAddress;
    }

    public String GetFromSystem() {
        return fromSystem;
    }

    public void SetFromSystem(String fromSystem) {
        this.fromSystem = fromSystem;
    }

    public String GetToSystem() {
        return toSystem;
    }

    public void SetToSystem(String toSystem) {
        this.toSystem = toSystem;
    }

    public String GetDescription() {
        return Description;
    }

    public void SetDescription(String description) {
        Description = description;
    }

    public String GetOriginalSender() {
        return originalSender;
    }

    public void SetOriginalSender(String originalSender) {
        this.originalSender = originalSender;
    }

    public String GetOriginalGUID() {
        return originalGUID;
    }

    public void SetOriginalGUID(String originalGUID) {
        this.originalGUID = originalGUID;
    }

    public OperationUser GetOperationUser() {
        return operationUser;
    }

    public void SetOperationUser(OperationUser operationUser) {
        this.operationUser = operationUser;
    }
}
