package com.ct.framework.contract;

import java.io.Serializable;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 默认的数据体契约规则.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class DefaultDataContract implements Serializable {

    /**
     * 头部
     */
    private MessageHeader header;

    /**
     * 事件
     */
    private MessageEventCollection events;

    /**
     * 错误
     */
    private MessageFaultCollection faults;

    public MessageHeader GetHeader() {
        return header;
    }

    public void SetHeader(MessageHeader header) {
        this.header = header;
    }

    public MessageEventCollection GetEvents() {
        return this.events;
    }

    public void SetEvents(MessageEventCollection events) {
        this.events = events;
    }

    public MessageFaultCollection GetFaults() {
        return this.faults;
    }

    public void SetFaults(MessageFaultCollection faults) {
        this.faults = faults;
    }
}
