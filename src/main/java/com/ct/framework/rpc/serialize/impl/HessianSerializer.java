package com.ct.framework.rpc.serialize.impl;

import com.caucho.hessian.io.Hessian2Input;
import com.caucho.hessian.io.Hessian2Output;
import com.ct.framework.core.exception.ErrorCode;
import com.ct.framework.core.exception.GlobalException;
import com.ct.framework.rpc.serialize.Serializer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class HessianSerializer extends Serializer {

    @Override
    public <T> byte[] serialize(T obj) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        Hessian2Output ho = new Hessian2Output(os);
        try {
            ho.writeObject(obj);
            ho.flush();
            byte[] result = os.toByteArray();
            return result;
        } catch (IOException e) {
            throw new GlobalException("framework-rpc " + e.getMessage(), ErrorCode.NET_RPC_ERROR);
        } finally {
            try {
                ho.close();
            } catch (IOException e) {
                throw new GlobalException("framework-rpc " + e.getMessage(), ErrorCode.NET_RPC_ERROR);
            }
            try {
                os.close();
            } catch (IOException e) {
                throw new GlobalException("framework-rpc " + e.getMessage(), ErrorCode.NET_RPC_ERROR);
            }
        }

    }

    @Override
    public <T> Object deserialize(byte[] bytes, Class<T> clazz) {
        ByteArrayInputStream is = new ByteArrayInputStream(bytes);
        Hessian2Input hi = new Hessian2Input(is);
        try {
            Object result = hi.readObject();
            return result;
        } catch (IOException e) {
            throw new GlobalException("framework-rpc " + e.getMessage(), ErrorCode.NET_RPC_ERROR);
        } finally {
            try {
                hi.close();
            } catch (Exception e) {
                throw new GlobalException("framework-rpc " + e.getMessage(), ErrorCode.NET_RPC_ERROR);
            }
            try {
                is.close();
            } catch (IOException e) {
                throw new GlobalException("framework-rpc " + e.getMessage(), ErrorCode.NET_RPC_ERROR);
            }
        }
    }

}
