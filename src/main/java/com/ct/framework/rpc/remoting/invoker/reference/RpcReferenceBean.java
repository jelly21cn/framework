package com.ct.framework.rpc.remoting.invoker.reference;

import com.ct.framework.rpc.remoting.invoker.generic.RpcGenericService;
import com.ct.framework.core.exception.ErrorCode;
import com.ct.framework.core.exception.GlobalException;
import com.ct.framework.rpc.remoting.invoker.RpcInvokerFactory;
import com.ct.framework.rpc.remoting.invoker.call.CallType;
import com.ct.framework.rpc.remoting.invoker.call.RpcInvokeCallback;
import com.ct.framework.rpc.remoting.invoker.call.RpcInvokeFuture;
import com.ct.framework.rpc.remoting.invoker.route.LoadBalance;
import com.ct.framework.rpc.remoting.net.Client;
import com.ct.framework.rpc.remoting.net.impl.netty.client.NettyClient;
import com.ct.framework.rpc.remoting.net.params.RpcFutureResponse;
import com.ct.framework.rpc.remoting.net.params.RpcRequest;
import com.ct.framework.rpc.remoting.net.params.RpcResponse;
import com.ct.framework.rpc.remoting.provider.RpcProviderFactory;
import com.ct.framework.rpc.serialize.Serializer;
import com.ct.framework.rpc.serialize.impl.HessianSerializer;
import com.ct.framework.utility.ClassHelper;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public class RpcReferenceBean {

    private Class<? extends Client> client = NettyClient.class;
    private Class<? extends Serializer> serializer = HessianSerializer.class;
    private CallType callType = CallType.SYNC;
    private LoadBalance loadBalance = LoadBalance.ROUND;
    private Class<?> iface = null;
    private String className = null;
    private String version = null;
    private long timeout = 60000;
    private String address = null;
    private String accessToken = null;

    private RpcInvokeCallback invokeCallback = null;

    private RpcInvokerFactory invokerFactory = null;

    public void setClient(Class<? extends Client> client) {
        this.client = client;
    }

    public void setSerializer(Class<? extends Serializer> serializer) {
        this.serializer = serializer;
    }

    public void setCallType(CallType callType) {
        this.callType = callType;
    }

    public void setLoadBalance(LoadBalance loadBalance) {
        this.loadBalance = loadBalance;
    }

    public void setIface(Class<?> iface) {
        this.iface = iface;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public void setInvokeCallback(RpcInvokeCallback invokeCallback) {
        this.invokeCallback = invokeCallback;
    }

    public void setInvokerFactory(RpcInvokerFactory invokerFactory) {
        this.invokerFactory = invokerFactory;
    }

    public Serializer getSerializerInstance() {
        return serializerInstance;
    }

    public long getTimeout() {
        return timeout;
    }

    public RpcInvokerFactory getInvokerFactory() {
        return invokerFactory;
    }

    public Class<?> getIface() {
        return iface;
    }

    private Client clientInstance = null;

    private Serializer serializerInstance = null;

    public RpcReferenceBean initClient() throws Exception {

        if (this.client == null) {
            throw new GlobalException("framework-rpc reference client missing.", ErrorCode.NET_RPC_ERROR);
        }
        if (this.serializer == null) {
            throw new GlobalException("framework-rpc reference serializer missing.", ErrorCode.NET_RPC_ERROR);
        }
        if (this.callType == null) {
            throw new GlobalException("framework-rpc reference callType missing.", ErrorCode.NET_RPC_ERROR);
        }
        if (this.loadBalance == null) {
            throw new GlobalException("framework-rpc reference loadBalance missing.", ErrorCode.NET_RPC_ERROR);
        }
        if (this.iface == null) {
            throw new GlobalException("framework-rpc reference interface missing.", ErrorCode.NET_RPC_ERROR);
        }
        if (this.className == null) {
            throw new GlobalException("framework-rpc reference className missing.", ErrorCode.NET_RPC_ERROR);
        }
        if (this.timeout < 0) {
            this.timeout = 0;
        }
        if (this.invokerFactory == null) {
            this.invokerFactory = RpcInvokerFactory.getInstance();
        }

        this.serializerInstance = serializer.newInstance();
        clientInstance = client.newInstance();
        clientInstance.init(this);

        return this;
    }

    public Object getObject() throws Exception {

        initClient();

        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{iface},
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

                        //String className = method.getDeclaringClass().getName();
                        String ver = version;
                        String methodName = method.getName();
                        Class<?>[] parameterTypes = method.getParameterTypes();
                        Object[] parameters = args;

                        if (className.equals(RpcGenericService.class.getName()) && methodName.equals("invoke")) {

                            Class<?>[] paramTypes = null;
                            if (args[3] != null) {
                                String[] paramTypes_str = (String[]) args[3];
                                if (paramTypes_str.length > 0) {
                                    paramTypes = new Class[paramTypes_str.length];
                                    for (int i = 0; i < paramTypes_str.length; i++) {
                                        paramTypes[i] = ClassHelper.resolveClass(paramTypes_str[i]);
                                    }
                                }
                            }

                            className = (String) args[0];
                            ver = (String) args[1];
                            methodName = (String) args[2];
                            parameterTypes = paramTypes;
                            parameters = (Object[]) args[4];
                        }

                        if (className.equals(Object.class.getName())) {
                            log.info(">>>>>>>>>>> framework-rpc proxy class-method not support [{}#{}]", className, methodName);
                            throw new GlobalException("framework-rpc proxy class-method not support", ErrorCode.NET_RPC_ERROR);
                        }

                        String finalAddress = address;

                        if (finalAddress == null || finalAddress.trim().length() == 0) {
                            if (invokerFactory != null && invokerFactory.getRegister() != null) {
                                // 发现
                                String serviceKey = RpcProviderFactory.makeServiceKey(className, ver);
                                TreeSet<String> addressSet = invokerFactory.getRegister().discovery(serviceKey);
                                // 负载均衡
                                if (addressSet == null || addressSet.size() == 0) {
                                } else if (addressSet.size() == 1) {
                                    finalAddress = addressSet.first();
                                } else {
                                    finalAddress = loadBalance.rpcInvokerRouter.route(serviceKey, addressSet);
                                }

                            }
                        }

                        if (finalAddress == null || finalAddress.trim().length() == 0) {
                            throw new GlobalException("framework-rpc reference bean[" + className + "] address empty", ErrorCode.NET_RPC_ERROR);
                        }

                        RpcRequest rpcRequest = new RpcRequest();
                        rpcRequest.setRequestId(UUID.randomUUID().toString());
                        rpcRequest.setCreateMillisTime(System.currentTimeMillis());
                        rpcRequest.setAccessToken(accessToken);
                        rpcRequest.setClassName(className);
                        rpcRequest.setMethodName(methodName);
                        rpcRequest.setParameterTypes(parameterTypes);
                        rpcRequest.setParameters(parameters);
                        rpcRequest.setVersion(version);

                        if (CallType.SYNC == callType) {
                            RpcFutureResponse futureResponse = new RpcFutureResponse(invokerFactory, rpcRequest, null);
                            try {
                                clientInstance.asyncSend(finalAddress, rpcRequest);

                                RpcResponse rpcResponse = futureResponse.get(timeout, TimeUnit.MILLISECONDS);
                                if (rpcResponse.getErrorMsg() != null) {
                                    throw new GlobalException(rpcResponse.getErrorMsg(), ErrorCode.NET_RPC_ERROR);
                                }
                                return rpcResponse.getResult();
                            } catch (Exception e) {
                                log.info(">>>>>>>>>>> framework-rpc, invoke error, address:{}, rpcRequest{}", finalAddress, rpcRequest);
                                throw new GlobalException(e.getMessage(), ErrorCode.NET_RPC_ERROR);
                            } finally {
                                futureResponse.removeInvokerFuture();
                            }
                        } else if (CallType.FUTURE == callType) {
                            RpcFutureResponse futureResponse = new RpcFutureResponse(invokerFactory, rpcRequest, null);
                            try {
                                RpcInvokeFuture invokeFuture = new RpcInvokeFuture(futureResponse);
                                RpcInvokeFuture.setFuture(invokeFuture);

                                clientInstance.asyncSend(finalAddress, rpcRequest);

                                return null;
                            } catch (Exception e) {
                                log.info(">>>>>>>>>>> framework-rpc, invoke error, address:{}, rpcRequest{}", finalAddress, rpcRequest);

                                futureResponse.removeInvokerFuture();
                                throw new GlobalException(e.getMessage(), ErrorCode.NET_RPC_ERROR);
                            }

                        } else if (CallType.CALLBACK == callType) {

                            RpcInvokeCallback finalInvokeCallback = invokeCallback;
                            RpcInvokeCallback threadInvokeCallback = RpcInvokeCallback.getCallback();
                            if (threadInvokeCallback != null) {
                                finalInvokeCallback = threadInvokeCallback;
                            }
                            if (finalInvokeCallback == null) {
                                throw new GlobalException("framework-rpc rpcInvokeCallback（CallType=" + CallType.CALLBACK.name() + "） cannot be null.", ErrorCode.NET_RPC_ERROR);
                            }

                            RpcFutureResponse futureResponse = new RpcFutureResponse(invokerFactory, rpcRequest, finalInvokeCallback);
                            try {
                                clientInstance.asyncSend(finalAddress, rpcRequest);
                            } catch (Exception e) {
                                log.info(">>>>>>>>>>> framework-rpc, invoke error, address:{}, rpcRequest{}", finalAddress, rpcRequest);

                                futureResponse.removeInvokerFuture();
                                throw new GlobalException(e.getMessage(), ErrorCode.NET_RPC_ERROR);
                            }

                            return null;
                        } else if (CallType.ONEWAY == callType) {
                            clientInstance.asyncSend(finalAddress, rpcRequest);
                            return null;
                        } else {
                            throw new GlobalException("framework-rpc callType[" + callType + "] invalid", ErrorCode.NET_RPC_ERROR);
                        }

                    }
                });
    }
}
