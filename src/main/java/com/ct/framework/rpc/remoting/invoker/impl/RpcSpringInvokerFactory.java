package com.ct.framework.rpc.remoting.invoker.impl;

import com.ct.framework.rpc.remoting.invoker.RpcInvokerFactory;
import com.ct.framework.rpc.remoting.invoker.reference.RpcReferenceBean;
import com.ct.framework.rpc.remoting.provider.RpcProviderFactory;
import com.ct.framework.core.annotation.RpcReference;
import com.ct.framework.core.exception.ErrorCode;
import com.ct.framework.core.exception.GlobalException;
import com.ct.framework.rpc.registry.Register;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessorAdapter;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage: 暂时不启用
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Deprecated
@Slf4j
public class RpcSpringInvokerFactory extends InstantiationAwareBeanPostProcessorAdapter implements InitializingBean, DisposableBean, BeanFactoryAware {

    private Class<? extends Register> serviceRegistryClass;
    private Map<String, String> serviceRegistryParam;

    public void setServiceRegistryClass(Class<? extends Register> serviceRegistryClass) {
        this.serviceRegistryClass = serviceRegistryClass;
    }

    public void setServiceRegistryParam(Map<String, String> serviceRegistryParam) {
        this.serviceRegistryParam = serviceRegistryParam;
    }

    private RpcInvokerFactory rpcInvokerFactory;

    @Override
    public void afterPropertiesSet() throws Exception {
        rpcInvokerFactory = new RpcInvokerFactory(serviceRegistryClass, serviceRegistryParam);
        rpcInvokerFactory.start();
    }

    @Override
    public boolean postProcessAfterInstantiation(final Object bean, final String beanName) throws BeansException {

        final Set<String> serviceKeyList = new HashSet<>();

        ReflectionUtils.doWithFields(bean.getClass(), new ReflectionUtils.FieldCallback() {
            @Override
            public void doWith(Field field) throws IllegalArgumentException, IllegalAccessException {
                if (field.isAnnotationPresent(RpcReference.class)) {
                    Class iface = field.getType();
                    if (!iface.isInterface()) {
                        throw new GlobalException("framework-rpc, reference(rpcReference) must be interface.", ErrorCode.NET_RPC_ERROR);
                    }

                    RpcReference rpcReference = field.getAnnotation(RpcReference.class);

                    RpcReferenceBean referenceBean = new RpcReferenceBean();
                    referenceBean.setClient(rpcReference.client());
                    referenceBean.setSerializer(rpcReference.serializer());
                    referenceBean.setCallType(rpcReference.callType());
                    referenceBean.setLoadBalance(rpcReference.loadBalance());
                    referenceBean.setIface(iface);
                    referenceBean.setVersion(rpcReference.version());
                    referenceBean.setTimeout(rpcReference.timeout());
                    referenceBean.setAddress(rpcReference.address());
                    referenceBean.setAccessToken(rpcReference.accessToken());
                    referenceBean.setInvokeCallback(null);
                    referenceBean.setInvokerFactory(rpcInvokerFactory);

                    Object serviceProxy = null;
                    try {
                        serviceProxy = referenceBean.getObject();
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }

                    field.setAccessible(true);
                    field.set(bean, serviceProxy);

                    log.info(">>>>>>>>>>> framework-rpc, invoker factory init reference bean success. serviceKey = {}, bean.field = {}.{}", RpcProviderFactory.makeServiceKey(iface.getName(), rpcReference.version()), beanName, field.getName());

                    String serviceKey = RpcProviderFactory.makeServiceKey(iface.getName(), rpcReference.version());
                    serviceKeyList.add(serviceKey);

                }
            }
        });

        if (rpcInvokerFactory.getRegister() != null) {
            try {
                rpcInvokerFactory.getRegister().discovery(serviceKeyList);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }

        return super.postProcessAfterInstantiation(bean, beanName);
    }

    @Override
    public void destroy() throws Exception {
        rpcInvokerFactory.stop();
    }

    private BeanFactory beanFactory;

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }
}
