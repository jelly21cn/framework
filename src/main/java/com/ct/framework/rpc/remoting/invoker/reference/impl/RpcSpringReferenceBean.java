package com.ct.framework.rpc.remoting.invoker.reference.impl;

import com.ct.framework.rpc.remoting.invoker.reference.RpcReferenceBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class RpcSpringReferenceBean implements FactoryBean<Object>, InitializingBean {
    private RpcReferenceBean rpcReferenceBean;

    @Override
    public void afterPropertiesSet() {
        this.rpcReferenceBean = new RpcReferenceBean();
    }

    @Override
    public Object getObject() throws Exception {
        return rpcReferenceBean.getObject();
    }

    @Override
    public Class<?> getObjectType() {
        return rpcReferenceBean.getIface();
    }

    @Override
    public boolean isSingleton() {
        return false;
    }
}
