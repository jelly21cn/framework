package com.ct.framework.rpc.remoting.provider;

import com.ct.framework.rpc.serialize.Serializer;
import com.ct.framework.rpc.serialize.impl.HessianSerializer;
import com.ct.framework.core.exception.ErrorCode;
import com.ct.framework.core.exception.GlobalException;
import com.ct.framework.rpc.registry.Register;
import com.ct.framework.rpc.remoting.net.Server;
import com.ct.framework.rpc.remoting.net.impl.netty.server.NettyServer;
import com.ct.framework.rpc.remoting.net.params.BaseCallback;
import com.ct.framework.rpc.remoting.net.params.RpcRequest;
import com.ct.framework.rpc.remoting.net.params.RpcResponse;
import com.ct.framework.utility.net.IPHelper;
import com.ct.framework.utility.net.NetHelper;
import com.ct.framework.utility.string.StringHelper;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public class RpcProviderFactory {
    private Class<? extends Server> server = NettyServer.class;
    private Class<? extends Serializer> serializer = HessianSerializer.class;

    private int corePoolSize = 60;
    private int maxPoolSize = 300;

    private String ip = null;
    private int port = 7080;
    private String registryAddress;
    private String accessToken = null;

    private Class<? extends Register> serviceRegistry = null;
    private Map<String, String> serviceRegistryParam = null;

    public void setServer(Class<? extends Server> server) {
        this.server = server;
    }

    public void setSerializer(Class<? extends Serializer> serializer) {
        this.serializer = serializer;
    }

    public void setCorePoolSize(int corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

    public void setMaxPoolSize(int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setRegistryAddress(String registryAddress) {
        this.registryAddress = registryAddress;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public void setServiceRegistry(Class<? extends Register> serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setServiceRegistryParam(Map<String, String> serviceRegistryParam) {
        this.serviceRegistryParam = serviceRegistryParam;
    }

    public Serializer getSerializerInstance() {
        return serializerInstance;
    }

    public int getPort() {
        return port;
    }

    public int getCorePoolSize() {
        return corePoolSize;
    }

    public int getMaxPoolSize() {
        return maxPoolSize;
    }

    private Server serverInstance;
    private Serializer serializerInstance;
    private Register registerInstance;

    public void start() throws Exception {

        if (this.server == null) {
            throw new GlobalException("framework-rpc provider server missing", ErrorCode.NET_RPC_ERROR);
        }
        if (this.serializer == null) {
            throw new GlobalException("framework-rpc provider serializer missing", ErrorCode.NET_RPC_ERROR);
        }
        if (!(this.corePoolSize > 0 && this.maxPoolSize > 0 && this.maxPoolSize >= this.corePoolSize)) {
            this.corePoolSize = 60;
            this.maxPoolSize = 300;
        }
        if (this.ip == null) {
            this.ip = IPHelper.getIp();
        }
        if (this.port <= 0) {
            this.port = 7080;
        }
        if (this.registryAddress == null || this.registryAddress.trim().length() == 0) {
            this.registryAddress = IPHelper.getIpPort(this.ip, this.port);
        }
        if (NetHelper.isPortUsed(this.port)) {
            throw new GlobalException("framework-rpc provider port[\" + this.port + \"] is used", ErrorCode.NET_RPC_ERROR);
        }

        this.serializerInstance = serializer.newInstance();

        serverInstance = server.newInstance();
        serverInstance.setStartedCallback(new BaseCallback() {
            @Override
            public void run() throws Exception {

                if (serviceRegistry != null) {
                    registerInstance = serviceRegistry.newInstance();
                    registerInstance.start(serviceRegistryParam);
                    if (serviceData.size() > 0) {
                        registerInstance.registry(serviceData.keySet(), registryAddress);
                    }
                }
            }
        });
        serverInstance.setStopedCallback(new BaseCallback() {
            @Override
            public void run() {

                if (registerInstance != null) {
                    if (serviceData.size() > 0) {
                        registerInstance.remove(serviceData.keySet(), registryAddress);
                    }
                    registerInstance.stop();
                    registerInstance = null;
                }
            }
        });
        serverInstance.start(this);
    }

    public void stop() throws Exception {
        serverInstance.stop();
    }

    private Map<String, Object> serviceData = new HashMap<String, Object>();

    public Map<String, Object> getServiceData() {
        return serviceData;
    }

    public static String makeServiceKey(String iface, String version) {
        String serviceKey = iface;
        if (version != null && version.trim().length() > 0) {
            serviceKey += "#".concat(version);
        }
        return serviceKey;
    }

    public void addService(String iface, String version, Object serviceBean) {
        String serviceKey = makeServiceKey(iface, version);
        serviceData.put(serviceKey, serviceBean);

        log.info(">>>>>>>>>>> framework-rpc, provider factory add service success. serviceKey = {}, serviceBean = {}", serviceKey, serviceBean.getClass());
    }

    public RpcResponse invokeService(RpcRequest rpcRequest) {

        RpcResponse rpcResponse = new RpcResponse();
        rpcResponse.setRequestId(rpcRequest.getRequestId());

        String serviceKey = makeServiceKey(rpcRequest.getClassName(), rpcRequest.getVersion());
        Object serviceBean = serviceData.get(serviceKey);

        if (serviceBean == null) {
            rpcResponse.setErrorMsg("The serviceKey[" + serviceKey + "] not found.");
            return rpcResponse;
        }

        if (System.currentTimeMillis() - rpcRequest.getCreateMillisTime() > 3 * 60 * 1000) {
            rpcResponse.setErrorMsg("The timestamp difference between admin and executor exceeds the limit.");
            return rpcResponse;
        }
        if (accessToken != null && accessToken.trim().length() > 0 && !accessToken.trim().equals(rpcRequest.getAccessToken())) {
            rpcResponse.setErrorMsg("The access token[" + rpcRequest.getAccessToken() + "] is wrong.");
            return rpcResponse;
        }

        try {
            Class<?> serviceClass = serviceBean.getClass();
            String methodName = rpcRequest.getMethodName();
            Class<?>[] parameterTypes = rpcRequest.getParameterTypes();
            Object[] parameters = rpcRequest.getParameters();

            Method method = serviceClass.getMethod(methodName, parameterTypes);
            method.setAccessible(true);
            Object result = method.invoke(serviceBean, parameters);

            rpcResponse.setResult(result);
        } catch (Throwable t) {
            log.error("framework-rpc provider invokeService error.", t);
            rpcResponse.setErrorMsg(StringHelper.toString(t));
        }

        return rpcResponse;
    }

}
