package com.ct.framework.rpc.remoting.net;

import com.ct.framework.rpc.remoting.net.params.BaseCallback;
import com.ct.framework.rpc.remoting.provider.RpcProviderFactory;
import lombok.extern.slf4j.Slf4j;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public abstract class Server {

    private BaseCallback startedCallback;
    private BaseCallback stopedCallback;

    public void setStartedCallback(BaseCallback startedCallback) {
        this.startedCallback = startedCallback;
    }

    public void setStopedCallback(BaseCallback stopedCallback) {
        this.stopedCallback = stopedCallback;
    }

    public abstract void start(final RpcProviderFactory rpcProviderFactory) throws Exception;

    public void onStarted() {
        if (startedCallback != null) {
            try {
                startedCallback.run();
            } catch (Exception e) {
                log.error(">>>>>>>>>>> framework-rpc, server startedCallback error.", e);
            }
        }
    }

    public abstract void stop() throws Exception;

    public void onStoped() {
        if (stopedCallback != null) {
            try {
                stopedCallback.run();
            } catch (Exception e) {
                log.error(">>>>>>>>>>> framework-rpc, server stopedCallback error.", e);
            }
        }
    }

}
