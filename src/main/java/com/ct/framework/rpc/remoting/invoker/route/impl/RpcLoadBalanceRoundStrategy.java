package com.ct.framework.rpc.remoting.invoker.route.impl;

import com.ct.framework.rpc.remoting.invoker.route.RpcLoadBalance;

import java.util.Random;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class RpcLoadBalanceRoundStrategy extends RpcLoadBalance {

    private ConcurrentMap<String, Integer> routeCountEachJob = new ConcurrentHashMap<String, Integer>();
    private long CACHE_VALID_TIME = 0;

    private int count(String serviceKey) {
        if (System.currentTimeMillis() > CACHE_VALID_TIME) {
            routeCountEachJob.clear();
            CACHE_VALID_TIME = System.currentTimeMillis() + 24 * 60 * 60 * 1000;
        }

        Integer count = routeCountEachJob.get(serviceKey);
        // 初始化时主动Random一次，缓解首次压力
        count = (count == null || count > 1000000) ? (new Random().nextInt(100)) : ++count;
        routeCountEachJob.put(serviceKey, count);
        return count;
    }

    @Override
    public String route(String serviceKey, TreeSet<String> addressSet) {
        String[] addressArr = addressSet.toArray(new String[addressSet.size()]);
        String finalAddress = addressArr[count(serviceKey) % addressArr.length];
        return finalAddress;
    }

}
