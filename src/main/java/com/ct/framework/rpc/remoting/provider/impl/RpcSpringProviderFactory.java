package com.ct.framework.rpc.remoting.provider.impl;

import com.ct.framework.rpc.remoting.provider.annotation.RpcService;
import com.ct.framework.core.exception.ErrorCode;
import com.ct.framework.core.exception.GlobalException;
import com.ct.framework.rpc.remoting.provider.RpcProviderFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class RpcSpringProviderFactory extends RpcProviderFactory implements ApplicationContextAware, InitializingBean, DisposableBean {

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

        Map<String, Object> serviceBeanMap = applicationContext.getBeansWithAnnotation(RpcService.class);
        if (serviceBeanMap != null && serviceBeanMap.size() > 0) {
            for (Object serviceBean : serviceBeanMap.values()) {
                if (serviceBean.getClass().getInterfaces().length == 0) {
                    throw new GlobalException("framework-rpc, service(rpcService) must inherit interface", ErrorCode.NET_RPC_ERROR);
                }
                RpcService rpcService = serviceBean.getClass().getAnnotation(RpcService.class);
                String iface = serviceBean.getClass().getInterfaces()[0].getName();
                String version = rpcService.version();
                super.addService(iface, version, serviceBean);
            }
        }

    }

    @Override
    public void afterPropertiesSet() throws Exception {
        super.start();
    }

    @Override
    public void destroy() throws Exception {
        super.stop();
    }

}
