package com.ct.framework.rpc.remoting.net.impl.netty.server;

import com.ct.framework.rpc.remoting.net.params.Beat;
import com.ct.framework.rpc.remoting.provider.RpcProviderFactory;
import com.ct.framework.rpc.remoting.net.Server;
import com.ct.framework.rpc.remoting.net.impl.netty.codec.NettyDecoder;
import com.ct.framework.rpc.remoting.net.impl.netty.codec.NettyEncoder;
import com.ct.framework.rpc.remoting.net.params.RpcRequest;
import com.ct.framework.rpc.remoting.net.params.RpcResponse;
import com.ct.framework.utility.ThreadPoolHelper;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public class NettyServer extends Server {

    private Thread thread;

    @Override
    public void start(final RpcProviderFactory rpcProviderFactory) throws Exception {

        thread = new Thread(new Runnable() {
            @Override
            public void run() {

                final ThreadPoolExecutor serverHandlerPool = ThreadPoolHelper.makeServerThreadPool(
                        NettyServer.class.getSimpleName(),
                        rpcProviderFactory.getCorePoolSize(),
                        rpcProviderFactory.getMaxPoolSize());
                EventLoopGroup bossGroup = new NioEventLoopGroup();
                EventLoopGroup workerGroup = new NioEventLoopGroup();

                try {
                    ServerBootstrap bootstrap = new ServerBootstrap();
                    bootstrap.group(bossGroup, workerGroup)
                            .channel(NioServerSocketChannel.class)
                            .childHandler(new ChannelInitializer<SocketChannel>() {
                                @Override
                                public void initChannel(SocketChannel channel) throws Exception {
                                    channel.pipeline()
                                            .addLast(new IdleStateHandler(0, 0, Beat.BEAT_INTERVAL * 3, TimeUnit.SECONDS))     // beat 3N, close if idle
                                            .addLast(new NettyDecoder(RpcRequest.class, rpcProviderFactory.getSerializerInstance()))
                                            .addLast(new NettyEncoder(RpcResponse.class, rpcProviderFactory.getSerializerInstance()))
                                            .addLast(new NettyServerHandler(rpcProviderFactory, serverHandlerPool));
                                }
                            })
                            .childOption(ChannelOption.TCP_NODELAY, true)
                            .childOption(ChannelOption.SO_KEEPALIVE, true);

                    ChannelFuture future = bootstrap.bind(rpcProviderFactory.getPort()).sync();

                    log.info(">>>>>>>>>>> framework-rpc remoting server start success, nettype = {}, port = {}", NettyServer.class.getName(), rpcProviderFactory.getPort());
                    onStarted();

                    future.channel().closeFuture().sync();

                } catch (Exception e) {
                    if (e instanceof InterruptedException) {
                        log.info(">>>>>>>>>>> framework-rpc remoting server stop.");
                    } else {
                        log.error(">>>>>>>>>>> framework-rpc remoting server error.", e);
                    }
                } finally {

                    try {
                        serverHandlerPool.shutdown();
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                    try {
                        workerGroup.shutdownGracefully();
                        bossGroup.shutdownGracefully();
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }

                }
            }
        });
        thread.setDaemon(true);
        thread.start();

    }

    @Override
    public void stop() throws Exception {

        if (thread != null && thread.isAlive()) {
            thread.interrupt();
        }
        onStoped();
        log.info(">>>>>>>>>>> framework-rpc remoting server destroy success.");
    }
}
