package com.ct.framework.rpc.remoting.invoker.route.impl;

import com.ct.framework.rpc.remoting.invoker.route.RpcLoadBalance;

import java.util.Random;
import java.util.TreeSet;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class RpcLoadBalanceRandomStrategy extends RpcLoadBalance {

    private Random random = new Random();

    @Override
    public String route(String serviceKey, TreeSet<String> addressSet) {
        String[] addressArr = addressSet.toArray(new String[addressSet.size()]);

        String finalAddress = addressArr[random.nextInt(addressSet.size())];
        return finalAddress;
    }

}
