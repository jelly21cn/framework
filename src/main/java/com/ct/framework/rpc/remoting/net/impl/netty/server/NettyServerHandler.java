package com.ct.framework.rpc.remoting.net.impl.netty.server;

import com.ct.framework.rpc.remoting.net.params.Beat;
import com.ct.framework.rpc.remoting.provider.RpcProviderFactory;
import com.ct.framework.rpc.remoting.net.params.RpcRequest;
import com.ct.framework.rpc.remoting.net.params.RpcResponse;
import com.ct.framework.utility.string.StringHelper;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleStateEvent;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ThreadPoolExecutor;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public class NettyServerHandler extends SimpleChannelInboundHandler<RpcRequest> {

    private RpcProviderFactory rpcProviderFactory;
    private ThreadPoolExecutor serverHandlerPool;

    public NettyServerHandler(final RpcProviderFactory rpcProviderFactory, final ThreadPoolExecutor serverHandlerPool) {
        this.rpcProviderFactory = rpcProviderFactory;
        this.serverHandlerPool = serverHandlerPool;
    }

    @Override
    public void channelRead0(final ChannelHandlerContext ctx, final RpcRequest rpcRequest) throws Exception {

        if (Beat.BEAT_ID.equalsIgnoreCase(rpcRequest.getRequestId())) {
            log.debug(">>>>>>>>>>> framework-rpc provider netty server read beat-ping.");
            return;
        }

        try {
            serverHandlerPool.execute(new Runnable() {
                @Override
                public void run() {
                    RpcResponse rpcResponse = rpcProviderFactory.invokeService(rpcRequest);
                    ctx.writeAndFlush(rpcResponse);
                }
            });
        } catch (Exception e) {
            RpcResponse rpcResponse = new RpcResponse();
            rpcResponse.setRequestId(rpcRequest.getRequestId());
            rpcResponse.setErrorMsg(StringHelper.toString(e));

            ctx.writeAndFlush(rpcResponse);
        }

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        log.error(">>>>>>>>>>> framework-rpc provider netty server caught exception", cause);
        ctx.close();
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            ctx.channel().close();
            log.debug(">>>>>>>>>>> framework-rpc provider netty server close an idle channel.");
        } else {
            super.userEventTriggered(ctx, evt);
        }
    }

}
