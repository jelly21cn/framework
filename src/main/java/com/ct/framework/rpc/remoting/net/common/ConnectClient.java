package com.ct.framework.rpc.remoting.net.common;

import com.ct.framework.rpc.remoting.invoker.RpcInvokerFactory;
import com.ct.framework.rpc.remoting.invoker.reference.RpcReferenceBean;
import com.ct.framework.rpc.remoting.net.params.BaseCallback;
import com.ct.framework.rpc.serialize.Serializer;
import com.ct.framework.rpc.remoting.net.params.RpcRequest;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public abstract class ConnectClient {
    public abstract void init(String address, final Serializer serializer, final RpcInvokerFactory rpcInvokerFactory) throws Exception;

    public abstract void close();

    public abstract boolean isValidate();

    public abstract void send(RpcRequest rpcRequest) throws Exception;

    public static void asyncSend(RpcRequest rpcRequest, String address, Class<? extends ConnectClient> connectClientImpl, final RpcReferenceBean rpcReferenceBean) throws Exception {

        ConnectClient clientPool = ConnectClient.getPool(address, connectClientImpl, rpcReferenceBean);

        try {
            clientPool.send(rpcRequest);
        } catch (Exception e) {
            throw e;
        }
    }

    private static volatile ConcurrentMap<String, ConnectClient> connectClientMap;
    private static volatile ConcurrentMap<String, Object> connectClientLockMap = new ConcurrentHashMap<>();

    private static ConnectClient getPool(String address, Class<? extends ConnectClient> connectClientImpl, final RpcReferenceBean rpcReferenceBean) throws Exception {

        if (connectClientMap == null) {
            synchronized (ConnectClient.class) {
                if (connectClientMap == null) {
                    connectClientMap = new ConcurrentHashMap<String, ConnectClient>();
                    rpcReferenceBean.getInvokerFactory().addStopCallBack(new BaseCallback() {
                        @Override
                        public void run() throws Exception {
                            if (connectClientMap.size() > 0) {
                                for (String key : connectClientMap.keySet()) {
                                    ConnectClient clientPool = connectClientMap.get(key);
                                    clientPool.close();
                                }
                                connectClientMap.clear();
                            }
                        }
                    });
                }
            }
        }

        ConnectClient connectClient = connectClientMap.get(address);
        if (connectClient != null && connectClient.isValidate()) {
            return connectClient;
        }

        Object clientLock = connectClientLockMap.get(address);
        if (clientLock == null) {
            connectClientLockMap.putIfAbsent(address, new Object());
            clientLock = connectClientLockMap.get(address);
        }

        synchronized (clientLock) {
            connectClient = connectClientMap.get(address);
            if (connectClient != null && connectClient.isValidate()) {
                return connectClient;
            }

            if (connectClient != null) {
                connectClient.close();
                connectClientMap.remove(address);
            }

            ConnectClient connectClient_new = connectClientImpl.newInstance();
            try {
                connectClient_new.init(address, rpcReferenceBean.getSerializerInstance(), rpcReferenceBean.getInvokerFactory());
                connectClientMap.put(address, connectClient_new);
            } catch (Exception e) {
                connectClient_new.close();
                throw e;
            }

            return connectClient_new;
        }

    }
}
