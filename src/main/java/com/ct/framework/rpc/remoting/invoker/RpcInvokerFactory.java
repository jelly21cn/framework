package com.ct.framework.rpc.remoting.invoker;

import com.ct.framework.core.exception.ErrorCode;
import com.ct.framework.core.exception.GlobalException;
import com.ct.framework.rpc.registry.Register;
import com.ct.framework.rpc.registry.impl.LocalRegister;
import com.ct.framework.rpc.remoting.net.params.BaseCallback;
import com.ct.framework.rpc.remoting.net.params.RpcFutureResponse;
import com.ct.framework.rpc.remoting.net.params.RpcResponse;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public class RpcInvokerFactory {
    private static volatile RpcInvokerFactory instance = new RpcInvokerFactory(LocalRegister.class, null);

    public static RpcInvokerFactory getInstance() {
        return instance;
    }


    private Class<? extends Register> serviceRegistryClass;
    private Map<String, String> serviceRegistryParam;

    public RpcInvokerFactory() {
    }

    public RpcInvokerFactory(Class<? extends Register> serviceRegistryClass, Map<String, String> serviceRegistryParam) {
        this.serviceRegistryClass = serviceRegistryClass;
        this.serviceRegistryParam = serviceRegistryParam;
    }

    public void start() throws Exception {
        if (serviceRegistryClass != null) {
            register = serviceRegistryClass.newInstance();
            register.start(serviceRegistryParam);
        }
    }

    public void stop() throws Exception {
        if (register != null) {
            register.stop();
        }

        if (stopCallbackList.size() > 0) {
            for (BaseCallback callback : stopCallbackList) {
                try {
                    callback.run();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }

        stopCallbackThreadPool();
    }

    private Register register;

    public Register getRegister() {
        return register;
    }

    private List<BaseCallback> stopCallbackList = new ArrayList<BaseCallback>();

    public void addStopCallBack(BaseCallback callback) {
        stopCallbackList.add(callback);
    }

    private ConcurrentMap<String, RpcFutureResponse> futureResponsePool = new ConcurrentHashMap<String, RpcFutureResponse>();

    public void setInvokerFuture(String requestId, RpcFutureResponse futureResponse) {
        futureResponsePool.put(requestId, futureResponse);
    }

    public void removeInvokerFuture(String requestId) {
        futureResponsePool.remove(requestId);
    }

    public void notifyInvokerFuture(String requestId, final RpcResponse rpcResponse) {
        final RpcFutureResponse futureResponse = futureResponsePool.get(requestId);
        if (futureResponse == null) {
            return;
        }

        if (futureResponse.getInvokeCallback() != null) {

            try {
                executeResponseCallback(new Runnable() {
                    @Override
                    public void run() {
                        if (rpcResponse.getErrorMsg() != null) {
                            futureResponse.getInvokeCallback().onFailure(
                                    new GlobalException(rpcResponse.getErrorMsg(), ErrorCode.NET_RPC_ERROR)
                            );
                        } else {
                            futureResponse.getInvokeCallback().onSuccess(rpcResponse.getResult());
                        }
                    }
                });
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        } else {
            futureResponse.setResponse(rpcResponse);
        }

        futureResponsePool.remove(requestId);

    }

    private ThreadPoolExecutor responseCallbackThreadPool = null;

    public void executeResponseCallback(Runnable runnable) {

        if (responseCallbackThreadPool == null) {
            synchronized (this) {
                if (responseCallbackThreadPool == null) {
                    responseCallbackThreadPool = new ThreadPoolExecutor(
                            10,
                            100,
                            60L,
                            TimeUnit.SECONDS,
                            new LinkedBlockingQueue<Runnable>(1000),
                            new ThreadFactory() {
                                @Override
                                public Thread newThread(Runnable r) {
                                    return new Thread(r, "framework-rpc, rpcInvokerFactory-responseCallbackThreadPool-" + r.hashCode());
                                }
                            },
                            new RejectedExecutionHandler() {
                                @Override
                                public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                                    throw new GlobalException("framework-rpc Invoke Callback Thread pool is EXHAUSTED", ErrorCode.NET_RPC_ERROR);
                                }
                            });
                }
            }
        }
        responseCallbackThreadPool.execute(runnable);
    }

    public void stopCallbackThreadPool() {
        if (responseCallbackThreadPool != null) {
            responseCallbackThreadPool.shutdown();
        }
    }

}
