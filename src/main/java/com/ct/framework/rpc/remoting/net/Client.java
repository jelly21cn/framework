package com.ct.framework.rpc.remoting.net;

import com.ct.framework.rpc.remoting.invoker.reference.RpcReferenceBean;
import com.ct.framework.rpc.remoting.net.params.RpcRequest;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public abstract class Client {
    protected volatile RpcReferenceBean rpcReferenceBean;

    public void init(RpcReferenceBean rpcReferenceBean) {
        this.rpcReferenceBean = rpcReferenceBean;
    }

    public abstract void asyncSend(String address, RpcRequest rpcRequest) throws Exception;
}
