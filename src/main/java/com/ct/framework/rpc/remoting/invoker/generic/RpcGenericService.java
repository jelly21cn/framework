package com.ct.framework.rpc.remoting.invoker.generic;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public interface RpcGenericService {
    public Object invoke(String iface, String version, String method, String[] parameterTypes, Object[] args);
}