package com.ct.framework.rpc.registry.impl.rpcadmin;

import com.ct.framework.rpc.registry.impl.rpcadmin.model.RpcAdminRegistryDataParamVO;
import com.ct.framework.rpc.registry.impl.rpcadmin.util.json.BasicJson;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage: 注册客户端
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public class RpcAdminRegistryClient {

    private volatile Set<RpcAdminRegistryDataParamVO> registryData = new HashSet<>();
    private volatile ConcurrentMap<String, TreeSet<String>> discoveryData = new ConcurrentHashMap<>();

    private Thread registryThread;
    private Thread discoveryThread;
    private volatile boolean registryThreadStop = false;

    private RpcAdminRegistryBaseClient registryBaseClient;

    public RpcAdminRegistryClient(String adminAddress, String accessToken, String biz, String env) {
        registryBaseClient = new RpcAdminRegistryBaseClient(adminAddress, accessToken, biz, env);
        log.info(">>>>>>>>>>> framework-rpc, rpcAdminRegistryClient init .... [adminAddress={}, accessToken={}, biz={}, env={}]", adminAddress, accessToken, biz, env);

        registryThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!registryThreadStop) {
                    try {
                        if (registryData.size() > 0) {

                            boolean ret = registryBaseClient.registry(new ArrayList<RpcAdminRegistryDataParamVO>(registryData));
                            log.debug(">>>>>>>>>>> framework-rpc, 刷新注册数据 {}, registryData = {}", ret ? "success" : "fail", registryData);
                        }
                    } catch (Exception e) {
                        if (!registryThreadStop) {
                            log.error(">>>>>>>>>>> framework-rpc, registryThread error.", e);
                        }
                    }
                    try {
                        TimeUnit.SECONDS.sleep(10);
                    } catch (Exception e) {
                        if (!registryThreadStop) {
                            log.error(">>>>>>>>>>> framework-rpc, registryThread error.", e);
                        }
                    }
                }
                log.info(">>>>>>>>>>> framework-rpc, registryThread stoped.");
            }
        });
        registryThread.setName("framework-rpc, rpcAdminRegistryClient registryThread.");
        registryThread.setDaemon(true);
        registryThread.start();

        discoveryThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!registryThreadStop) {

                    if (discoveryData.size() == 0) {
                        try {
                            TimeUnit.SECONDS.sleep(3);
                        } catch (Exception e) {
                            if (!registryThreadStop) {
                                log.error(">>>>>>>>>>> framework-rpc, discoveryThread error.", e);
                            }
                        }
                    } else {
                        try {
                            boolean monitorRet = registryBaseClient.monitor(discoveryData.keySet());

                            if (!monitorRet) {
                                TimeUnit.SECONDS.sleep(10);
                            }

                            refreshDiscoveryData(discoveryData.keySet());
                        } catch (Exception e) {
                            if (!registryThreadStop) {
                                log.error(">>>>>>>>>>> framework-rpc, discoveryThread error.", e);
                            }
                        }
                    }

                }
                log.info(">>>>>>>>>>> framework-rpc, discoveryThread stoped.");
            }
        });
        discoveryThread.setName("framework-rpc, registryClient discoveryThread.");
        discoveryThread.setDaemon(true);
        discoveryThread.start();

        log.info(">>>>>>>>>>> framework-rpc, registryClient init success.");
    }


    public void stop() {
        registryThreadStop = true;
        if (registryThread != null) {
            registryThread.interrupt();
        }
        if (discoveryThread != null) {
            discoveryThread.interrupt();
        }
    }

    public boolean registry(List<RpcAdminRegistryDataParamVO> registryDataList) {
        if (registryDataList == null || registryDataList.size() == 0) {
            throw new RuntimeException("framework-rpc registryDataList empty");
        }
        for (RpcAdminRegistryDataParamVO registryParam : registryDataList) {
            if (registryParam.getKey() == null || registryParam.getKey().trim().length() < 4 || registryParam.getKey().trim().length() > 255) {
                throw new RuntimeException("framework-rpc registryDataList#key Invalid[4~255]");
            }
            if (registryParam.getValue() == null || registryParam.getValue().trim().length() < 4 || registryParam.getValue().trim().length() > 255) {
                throw new RuntimeException("framework-rpc registryDataList#value Invalid[4~255]");
            }
        }

        registryData.addAll(registryDataList);

        registryBaseClient.registry(registryDataList);

        return true;
    }

    public boolean remove(List<RpcAdminRegistryDataParamVO> registryDataList) {
        if (registryDataList == null || registryDataList.size() == 0) {
            throw new RuntimeException("framework-rpc registryDataList empty");
        }
        for (RpcAdminRegistryDataParamVO registryParam : registryDataList) {
            if (registryParam.getKey() == null || registryParam.getKey().trim().length() < 4 || registryParam.getKey().trim().length() > 255) {
                throw new RuntimeException("framework-rpc registryDataList#key Invalid[4~255]");
            }
            if (registryParam.getValue() == null || registryParam.getValue().trim().length() < 4 || registryParam.getValue().trim().length() > 255) {
                throw new RuntimeException("framework-rpc registryDataList#value Invalid[4~255]");
            }
        }

        registryData.removeAll(registryDataList);

        registryBaseClient.remove(registryDataList);

        return true;
    }

    public Map<String, TreeSet<String>> discovery(Set<String> keys) {
        if (keys == null || keys.size() == 0) {
            return null;
        }

        Map<String, TreeSet<String>> registryDataTmp = new HashMap<String, TreeSet<String>>();
        for (String key : keys) {
            TreeSet<String> valueSet = discoveryData.get(key);
            if (valueSet != null) {
                registryDataTmp.put(key, valueSet);
            }
        }

        if (keys.size() != registryDataTmp.size()) {

            refreshDiscoveryData(keys);

            for (String key : keys) {
                TreeSet<String> valueSet = discoveryData.get(key);
                if (valueSet != null) {
                    registryDataTmp.put(key, valueSet);
                }
            }

        }

        return registryDataTmp;
    }

    private void refreshDiscoveryData(Set<String> keys) {
        if (keys == null || keys.size() == 0) {
            return;
        }

        Map<String, TreeSet<String>> updatedData = new HashMap<>();

        Map<String, TreeSet<String>> keyValueListData = registryBaseClient.discovery(keys);
        if (keyValueListData != null) {
            for (String keyItem : keyValueListData.keySet()) {
                TreeSet<String> valueSet = new TreeSet<>();
                valueSet.addAll(keyValueListData.get(keyItem));
                boolean updated = true;
                TreeSet<String> oldValSet = discoveryData.get(keyItem);
                if (oldValSet != null && BasicJson.toJson(oldValSet).equals(BasicJson.toJson(valueSet))) {
                    updated = false;
                }
                if (updated) {
                    discoveryData.put(keyItem, valueSet);
                    updatedData.put(keyItem, valueSet);
                }
            }
        }

        if (updatedData.size() > 0) {
            log.info(">>>>>>>>>>> framework-rpc, refresh discovery data finish, discoveryData(updated) = {}", updatedData);
        }
        log.debug(">>>>>>>>>>> framework-rpc, refresh discovery data finish, discoveryData = {}", discoveryData);
    }


    public TreeSet<String> discovery(String key) {
        if (key == null) {
            return null;
        }

        Map<String, TreeSet<String>> keyValueSetTmp = discovery(new HashSet<String>(Arrays.asList(key)));
        if (keyValueSetTmp != null) {
            return keyValueSetTmp.get(key);
        }
        return null;
    }


}
