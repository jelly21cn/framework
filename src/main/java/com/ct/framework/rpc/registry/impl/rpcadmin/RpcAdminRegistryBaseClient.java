package com.ct.framework.rpc.registry.impl.rpcadmin;

import com.ct.framework.rpc.registry.impl.rpcadmin.model.RpcAdminRegistryDataParamVO;
import com.ct.framework.rpc.registry.impl.rpcadmin.model.RpcAdminRegistryParamVO;
import com.ct.framework.rpc.registry.impl.rpcadmin.util.json.BasicJson;
import com.ct.framework.utility.http.BasicHttpUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public class RpcAdminRegistryBaseClient {
    private String adminAddress;
    private String accessToken;
    private String biz;
    private String env;

    private List<String> adminAddressArr;

    public RpcAdminRegistryBaseClient(String adminAddress, String accessToken, String biz, String env) {
        this.adminAddress = adminAddress;
        this.accessToken = accessToken;
        this.biz = biz;
        this.env = env;

        if (adminAddress == null || adminAddress.trim().length() == 0) {
            throw new RuntimeException("framework-rpc adminAddress empty");
        }
        if (biz == null || biz.trim().length() < 4 || biz.trim().length() > 255) {
            throw new RuntimeException("framework-rpc biz empty Invalid[4~255]");
        }
        if (env == null || env.trim().length() < 2 || env.trim().length() > 255) {
            throw new RuntimeException("framework-rpc biz env Invalid[2~255]");
        }

        adminAddressArr = new ArrayList<>();
        if (adminAddress.contains(",")) {
            adminAddressArr.addAll(Arrays.asList(adminAddress.split(",")));
        } else {
            adminAddressArr.add(adminAddress);
        }

    }

    public boolean registry(List<RpcAdminRegistryDataParamVO> registryDataList) {

        if (registryDataList == null || registryDataList.size() == 0) {
            throw new RuntimeException("framework-rpc registryDataList empty");
        }
        for (RpcAdminRegistryDataParamVO registryParam : registryDataList) {
            if (registryParam.getKey() == null || registryParam.getKey().trim().length() < 4 || registryParam.getKey().trim().length() > 255) {
                throw new RuntimeException("framework-rpc registryDataList#key Invalid[4~255]");
            }
            if (registryParam.getValue() == null || registryParam.getValue().trim().length() < 4 || registryParam.getValue().trim().length() > 255) {
                throw new RuntimeException("framework-rpc registryDataList#value Invalid[4~255]");
            }
        }

        String pathUrl = "/api/registry";

        RpcAdminRegistryParamVO registryParamVO = new RpcAdminRegistryParamVO();
        registryParamVO.setAccessToken(this.accessToken);
        registryParamVO.setBiz(this.biz);
        registryParamVO.setEnv(this.env);
        registryParamVO.setRegistryDataList(registryDataList);

        String paramsJson = BasicJson.toJson(registryParamVO);

        Map<String, Object> respObj = requestAndValid(pathUrl, paramsJson, 5);
        return respObj != null ? true : false;
    }

    private Map<String, Object> requestAndValid(String pathUrl, String requestBody, int timeout) {

        for (String adminAddressUrl : adminAddressArr) {
            String finalUrl = adminAddressUrl + pathUrl;

            String responseData = BasicHttpUtil.postBody(finalUrl, requestBody, timeout);
            if (responseData == null) {
                log.error("framework-rpc 注册中心连接失败，5秒后重试");
                return null;
            }

            Map<String, Object> resopnseMap = null;
            try {
                resopnseMap = BasicJson.parseMap(responseData);
            } catch (Exception e) {
                log.error("framework-rpc 注册中心应答信息解析失败，5秒后重试" + e.getMessage());
            }

            if (resopnseMap == null || !resopnseMap.containsKey("code") || !"200".equals(String.valueOf(resopnseMap.get("code")))) {
                log.warn("framework-rpc 连接响应失败, 响应数据={}", responseData);
                return null;
            }
            log.info("framework-rpc 注册中心连接成功");
            return resopnseMap;
        }


        return null;
    }

    /**
     * remove
     *
     * @param registryDataList
     * @return
     */
    public boolean remove(List<RpcAdminRegistryDataParamVO> registryDataList) {
        if (registryDataList == null || registryDataList.size() == 0) {
            throw new RuntimeException("framework-rpc registryDataList empty");
        }
        for (RpcAdminRegistryDataParamVO registryParam : registryDataList) {
            if (registryParam.getKey() == null || registryParam.getKey().trim().length() < 4 || registryParam.getKey().trim().length() > 255) {
                throw new RuntimeException("framework-rpc registryDataList#key Invalid[4~255]");
            }
            if (registryParam.getValue() == null || registryParam.getValue().trim().length() < 4 || registryParam.getValue().trim().length() > 255) {
                throw new RuntimeException("framework-rpc registryDataList#value Invalid[4~255]");
            }
        }

        String pathUrl = "/api/remove";

        RpcAdminRegistryParamVO registryParamVO = new RpcAdminRegistryParamVO();
        registryParamVO.setAccessToken(this.accessToken);
        registryParamVO.setBiz(this.biz);
        registryParamVO.setEnv(this.env);
        registryParamVO.setRegistryDataList(registryDataList);

        String paramsJson = BasicJson.toJson(registryParamVO);

        Map<String, Object> respObj = requestAndValid(pathUrl, paramsJson, 5);
        return respObj != null ? true : false;
    }

    /**
     * discovery
     *
     * @param keys
     * @return
     */
    public Map<String, TreeSet<String>> discovery(Set<String> keys) {
        if (keys == null || keys.size() == 0) {
            throw new RuntimeException("framework-rpc keys empty");
        }

        String pathUrl = "/api/discovery";

        RpcAdminRegistryParamVO registryParamVO = new RpcAdminRegistryParamVO();
        registryParamVO.setAccessToken(this.accessToken);
        registryParamVO.setBiz(this.biz);
        registryParamVO.setEnv(this.env);
        registryParamVO.setKeys(new ArrayList<String>(keys));

        String paramsJson = BasicJson.toJson(registryParamVO);

        Map<String, Object> respObj = requestAndValid(pathUrl, paramsJson, 5);

        if (respObj != null && respObj.containsKey("content")) {
            Map<String, TreeSet<String>> data = (Map<String, TreeSet<String>>) respObj.get("content");
            return data;
        }

        return null;
    }

    /**
     * discovery
     *
     * @param keys
     * @return
     */
    public boolean monitor(Set<String> keys) {
        if (keys == null || keys.size() == 0) {
            throw new RuntimeException("framework-rpc keys empty");
        }

        String pathUrl = "/api/monitor";

        RpcAdminRegistryParamVO registryParamVO = new RpcAdminRegistryParamVO();
        registryParamVO.setAccessToken(this.accessToken);
        registryParamVO.setBiz(this.biz);
        registryParamVO.setEnv(this.env);
        registryParamVO.setKeys(new ArrayList<String>(keys));

        String paramsJson = BasicJson.toJson(registryParamVO);

        Map<String, Object> respObj = requestAndValid(pathUrl, paramsJson, 60);
        return respObj != null ? true : false;
    }

}
