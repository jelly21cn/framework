package com.ct.framework.rpc.registry.impl;

import com.ct.framework.rpc.registry.impl.rpcadmin.model.RpcAdminRegistryDataParamVO;
import com.ct.framework.rpc.registry.Register;
import com.ct.framework.rpc.registry.impl.rpcadmin.RpcAdminRegistryClient;

import java.util.*;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage: 远程注册中心
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class RpcAdminRegister extends Register {

    public static final String ADMIN_ADDRESS = "ADMIN_ADDRESS";
    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String BIZ = "BIZ";
    public static final String ENV = "ENV";

    private RpcAdminRegistryClient rpcAdminRegistryClient;

    public RpcAdminRegistryClient getRpcAdminRegistryClient() {
        return rpcAdminRegistryClient;
    }

    @Override
    public void start(Map<String, String> param) {
        String registryAddress = param.get(ADMIN_ADDRESS);
        String accessToken = param.get(ACCESS_TOKEN);
        String biz = param.get(BIZ);
        String env = param.get(ENV);

        biz = (biz != null && biz.trim().length() > 0) ? biz : "default";
        env = (env != null && env.trim().length() > 0) ? env : "default";

        rpcAdminRegistryClient = new RpcAdminRegistryClient(registryAddress, accessToken, biz, env);
    }

    @Override
    public void stop() {
        if (rpcAdminRegistryClient != null) {
            rpcAdminRegistryClient.stop();
        }
    }

    @Override
    public boolean registry(Set<String> keys, String value) {
        if (keys == null || keys.size() == 0 || value == null || value.trim().length() == 0) {
            return false;
        }

        List<RpcAdminRegistryDataParamVO> registryDataList = new ArrayList<>();
        for (String key : keys) {
            registryDataList.add(new RpcAdminRegistryDataParamVO(key, value));
        }

        return rpcAdminRegistryClient.registry(registryDataList);
    }

    @Override
    public boolean remove(Set<String> keys, String value) {
        if (keys == null || keys.size() == 0 || value == null || value.trim().length() == 0) {
            return false;
        }

        List<RpcAdminRegistryDataParamVO> registryDataList = new ArrayList<>();
        for (String key : keys) {
            registryDataList.add(new RpcAdminRegistryDataParamVO(key, value));
        }

        return rpcAdminRegistryClient.remove(registryDataList);
    }

    @Override
    public Map<String, TreeSet<String>> discovery(Set<String> appkeys) {
        return rpcAdminRegistryClient.discovery(appkeys);
    }

    @Override
    public TreeSet<String> discovery(String appkey) {
        return rpcAdminRegistryClient.discovery(appkey);
    }
}
