package com.ct.framework.rpc.registry.impl.rpcadmin.util.json;

import java.util.List;
import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage: push cookies to model as cookieMap
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public final class BasicJson {

    private BasicJson() {
    }

    /**
     * basicJsonReader.
     */
    private static final BasicJsonReader BASIC_JSON_READER = new BasicJsonReader();

    /**
     * basicJsonwriter.
     */
    private static final BasicJsonwriter BASIC_JSON_WRITER = new BasicJsonwriter();


    /**
     * object to json.
     *
     * @param object object
     * @return String
     */
    public static String toJson(Object object) {
        return BASIC_JSON_WRITER.toJson(object);
    }

    /**
     * parse json to map.
     *
     * @param json json
     * @return only for filed type "null、ArrayList、LinkedHashMap、String、Long、Double、..."
     */
    public static Map<String, Object> parseMap(String json) {
        return BASIC_JSON_READER.parseMap(json);
    }

    /**
     * json to List.
     *
     * @param json json
     * @return List<Object>
     */
    public static List<Object> parseList(String json) {
        return BASIC_JSON_READER.parseList(json);
    }

}
