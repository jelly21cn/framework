package com.ct.framework.query;

import com.ct.framework.core.query.DistributedQuery;
import com.ct.framework.core.query.IQueryFactory;
import com.ct.framework.core.query.MessageQuery;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 分布式队列工厂
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class DistributedQueryFactory implements IQueryFactory {

    @Override
    public MessageQuery CreateMessageQuery(String type) {
        return null;
    }

    @Override
    public DistributedQuery CreateDistributedQuery() {
        return new KafkaProvider();
    }
}
