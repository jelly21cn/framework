package com.ct.framework.query;

import com.ct.framework.core.configuration.CPContext;
import com.ct.framework.core.query.DistributedQuery;
import com.ct.framework.core.query.IQueryFactory;
import com.ct.framework.core.query.MessageQuery;


/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: Kafka的具体实现
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class MessageQueryFactory implements IQueryFactory {

    @Override
    public MessageQuery CreateMessageQuery(String type) {
        if (CPContext.RABBITMQ.equalsIgnoreCase(type)) {
            return new RabbitMQProvider();
        } else {
            return new ActiveMQProvider();
        }
    }

    @Override
    public DistributedQuery CreateDistributedQuery() {
        return null;
    }
}
