package com.ct.framework.query;

import com.ct.framework.core.query.MessageQuery;
import com.ct.framework.core.query.QueryEntity;

import java.util.concurrent.Callable;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: ActiveMQ的具体实现
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class ActiveMQProvider extends MessageQuery {
    @Override
    public void Send(QueryEntity entity) {

    }

    @Override
    public void Receive(Callable<Boolean> func, String queue) {

    }

    @Override
    public void Subscribe(Callable<Boolean> func, String queue) {

    }

    @Override
    public void Commit() {

    }
}
