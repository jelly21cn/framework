package com.ct.framework.cache;

import com.ct.framework.core.cache.ICache;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: Memcache.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class MemcacheProvider implements ICache {

    @Override
    public boolean Add(String key, String value) {
        return false;
    }

    @Override
    public boolean Add(String key, String value, long slidingExpiration) {
        return false;
    }

    @Override
    public boolean Add(String key, String value, long slidingExpiration, String groupName) {
        return false;
    }

    @Override
    public String GetData(String key) {
        return null;
    }

    @Override
    public String GetData(String key, String groupName) {
        return null;
    }

    @Override
    public Boolean Remove(String key) {
        return false;
    }
}
