package com.ct.framework.cache;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 缓存代理类
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class CacheProxy<T> implements ICachePersistent<T> {

    private ICachePersistent<T> iCachePersistent;

    public CacheProxy(ICachePersistent<T> iCachePersistent) {
        this.iCachePersistent = iCachePersistent;
    }

    @Override
    public void AddCache(String content, T entity, long slidingExpiration, String group) {
        this.iCachePersistent.AddCache(content, entity, slidingExpiration, group);
    }

    @Override
    public void AddCache(String content, T entity, long slidingExpiration) {
        this.iCachePersistent.AddCache(content, entity, slidingExpiration);
    }

    @Override
    public T GetCache(String content, Class<?> clazz) {
        return this.iCachePersistent.GetCache(content, clazz);
    }

    @Override
    public T GetCache(String content, String group, Class<?> clazz) {
        return this.iCachePersistent.GetCache(content, group, clazz);
    }

    @Override
    public boolean RemoveCache(String key) {
        return this.iCachePersistent.RemoveCache(key);
    }

    @Override
    public ICachePersistent<T> GetProxy() {
        return this;
    }
}
