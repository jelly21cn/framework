package com.ct.framework.cache;

import com.ct.framework.utility.JedisPoolHelper;
import com.ct.framework.core.cache.ICache;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: Redis.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class RedisProvider implements ICache {

    @Override
    public boolean Add(String key, String value) {
        JedisPoolHelper.safeSet(key, value);
        return true;
    }

    @Override
    public boolean Add(String key, String value, long slidingExpiration) {
        JedisPoolHelper.safeSet(key, slidingExpiration, value);
        return true;
    }

    @Override
    public boolean Add(String key, String value, long slidingExpiration, String groupName) {
        JedisPoolHelper.safeSet(groupName + ":" + key, slidingExpiration, value);
        return true;
    }

    @Override
    public String GetData(String key) {
        return JedisPoolHelper.safeGet(key);
    }

    @Override
    public String GetData(String key, String groupName) {
        return JedisPoolHelper.safeGet(groupName + ":" + key);
    }

    @Override
    public Boolean Remove(String key) {
        JedisPoolHelper.safeDel(key);
        return true;
    }
}
