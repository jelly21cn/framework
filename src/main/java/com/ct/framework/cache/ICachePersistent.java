package com.ct.framework.cache;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: Redis 序列化.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public interface ICachePersistent<T> {
    void AddCache(String content, T entity, long slidingExpiration, String group);

    void AddCache(String content, T entity, long slidingExpiration);

    T GetCache(String content, Class<?> clazz);

    T GetCache(String content, String group, Class<?> clazz);

    /**
     * 清除缓冲
     * @param key
     * @return
     */
    boolean RemoveCache(String key);

    //获取自身代理
    ICachePersistent<T> GetProxy();
}
