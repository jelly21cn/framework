package com.ct.framework.cache;

import com.ct.framework.utility.SerializeHelper;
import com.ct.framework.utility.string.StringHelper;
import com.ct.framework.core.cache.CacherFactory;
import com.ct.framework.core.cache.ICache;

public class CachePersistent<T> implements ICachePersistent<T> {

    private ICachePersistent proxy = null;

    private ICache iCache = CacherFactory.DistributedCache();

    @Override
    public ICachePersistent<T> GetProxy() {
        this.proxy = new CacheProxy(this);
        return this.proxy;
    }

    @Override
    public void AddCache(String content, T entity, long slidingExpiration, String group) {
        if (this.isProxy()) {
            String val = SerializeHelper.JsonSerializer(entity, false);
            iCache.Add(content, val, slidingExpiration, group);
        }
    }

    @Override
    public void AddCache(String content, T entity, long slidingExpiration) {
        if (this.isProxy()) {
            String val = SerializeHelper.JsonSerializer(entity, false);
            iCache.Add(content, val, slidingExpiration);
        }
    }

    @Override
    public T GetCache(String content, Class<?> clazz) {
        if (this.isProxy()) {
            String val = iCache.GetData(content);
            if (StringHelper.isBlank(val)) {
                return null;
            }
            T t = (T) SerializeHelper.JsonDeserialize(val, clazz);
            return t;
        }
        return null;
    }

    @Override
    public T GetCache(String content, String group, Class<?> clazz) {
        if (this.isProxy()) {
            String val = iCache.GetData(content, group);
            if (StringHelper.isBlank(val)) {
                return null;
            }
            T t = (T) SerializeHelper.JsonDeserialize(val, clazz);
            return t;
        }
        return null;
    }

    @Override
    public boolean RemoveCache(String key) {
        if (this.isProxy()) {
            return iCache.Remove(key);
        }
        return false;
    }


    private boolean isProxy() {
        if (this.proxy == null) {
            return false;
        } else {
            return true;
        }
    }
}
