package com.ct.framework.task.handler;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  01/06/2021
 * Usage: .
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2022-10-1             xxx                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public abstract class IJobHandler {
    /**
     * 执行.
     */
    public abstract void execute() throws Exception;

    /**
     * 初始化.
     */
    public void init() throws Exception {
        // do something
    }


    /**
     * 移除.
     */
    public void destroy() throws Exception {
        // do something
    }
}
