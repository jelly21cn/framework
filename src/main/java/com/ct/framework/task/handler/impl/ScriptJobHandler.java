package com.ct.framework.task.handler.impl;

import com.ct.framework.utility.FileHelper;
import com.ct.framework.task.context.TaskContext;
import com.ct.framework.task.context.TaskHelper;
import com.ct.framework.task.glue.GlueTypeEnum;
import com.ct.framework.task.handler.IJobHandler;
import com.ct.framework.task.log.TaskFileAppender;

import java.io.File;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  01/06/2021
 * Usage: .
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2022-10-1             xxx                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class ScriptJobHandler extends IJobHandler {

    private int jobId;
    private long glueUpdatetime;
    private String gluesource;
    private GlueTypeEnum glueType;

    public ScriptJobHandler(int jobId, long glueUpdatetime, String gluesource, GlueTypeEnum glueType) {
        this.jobId = jobId;
        this.glueUpdatetime = glueUpdatetime;
        this.gluesource = gluesource;
        this.glueType = glueType;

        File glueSrcPath = new File(TaskFileAppender.getGlueSrcPath());
        if (glueSrcPath.exists()) {
            File[] glueSrcFileList = glueSrcPath.listFiles();
            if (glueSrcFileList != null && glueSrcFileList.length > 0) {
                for (File glueSrcFileItem : glueSrcFileList) {
                    if (glueSrcFileItem.getName().startsWith(String.valueOf(jobId) + "_")) {
                        glueSrcFileItem.delete();
                    }
                }
            }
        }

    }

    public long getGlueUpdatetime() {
        return glueUpdatetime;
    }

    @Override
    public void execute() throws Exception {

        if (!glueType.isScript()) {
            TaskHelper.handleFail("类型 [" + glueType + "] 无效.");
            return;
        }

        String cmd = glueType.getCmd();

        String scriptFileName = TaskFileAppender.getGlueSrcPath()
                .concat(File.separator)
                .concat(String.valueOf(jobId))
                .concat("_")
                .concat(String.valueOf(glueUpdatetime))
                .concat(glueType.getSuffix());
        File scriptFile = new File(scriptFileName);
        if (!scriptFile.exists()) {
            FileHelper.markScriptFile(scriptFileName, gluesource);
        }

        String logFileName = TaskContext.getTaskContext().getJobLogFileName();

        //0=param、1=分片序号、2=分片总数
        String[] scriptParams = new String[3];
        scriptParams[0] = TaskHelper.getJobParam();
        scriptParams[1] = String.valueOf(TaskContext.getTaskContext().getShardIndex());
        scriptParams[2] = String.valueOf(TaskContext.getTaskContext().getShardTotal());

        // 异步处理
        TaskHelper.log(">>>>>>>>>>> framework-task 脚本文件:" + scriptFileName + " -----------");
        int exitValue = FileHelper.execToFile(cmd, scriptFileName, logFileName, scriptParams);

        if (exitValue == 0) {
            TaskHelper.handleSuccess();
            return;
        } else {
            TaskHelper.handleFail("脚本值 (" + exitValue + ") 异常");
            return;
        }

    }

}
