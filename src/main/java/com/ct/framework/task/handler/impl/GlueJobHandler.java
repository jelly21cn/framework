package com.ct.framework.task.handler.impl;

import com.ct.framework.task.context.TaskHelper;
import com.ct.framework.task.handler.IJobHandler;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  01/06/2021
 * Usage: .
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2022-10-1             xxx                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class GlueJobHandler extends IJobHandler {

    private long glueUpdatetime;
    private IJobHandler jobHandler;

    public GlueJobHandler(IJobHandler jobHandler, long glueUpdatetime) {
        this.jobHandler = jobHandler;
        this.glueUpdatetime = glueUpdatetime;
    }

    public long getGlueUpdatetime() {
        return glueUpdatetime;
    }

    @Override
    public void execute() throws Exception {
        TaskHelper.log(">>>>>>>>>>> framework-task 版本号 :" + glueUpdatetime + " -----------");
        jobHandler.execute();
    }

    @Override
    public void init() throws Exception {
        this.jobHandler.init();
    }

    @Override
    public void destroy() throws Exception {
        this.jobHandler.destroy();
    }
}
