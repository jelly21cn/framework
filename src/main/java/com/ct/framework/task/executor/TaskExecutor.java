package com.ct.framework.task.executor;

import com.ct.framework.utility.net.IPHelper;
import com.ct.framework.utility.net.NetHelper;
import com.ct.framework.core.annotation.Mission;
import com.ct.framework.task.biz.AdminBiz;
import com.ct.framework.task.biz.client.AdminBizClient;
import com.ct.framework.task.handler.IJobHandler;
import com.ct.framework.task.handler.impl.MethodJobHandler;
import com.ct.framework.task.log.TaskFileAppender;
import com.ct.framework.task.server.EmbedServer;
import com.ct.framework.task.thread.JobLogFileCleanThread;
import com.ct.framework.task.thread.JobThread;
import com.ct.framework.task.thread.TriggerCallbackThread;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  01/06/2021
 * Usage: .
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2022-10-1             xxx                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public class TaskExecutor {
    /**
     * 远程配置.
     */
    private String adminAddresses;
    /**
     * 远程配置.
     */
    private String accessToken;
    /**
     * 远程配置.
     */
    private String appname;
    /**
     * 远程配置.
     */
    private String address;
    /**
     * 远程配置.
     */
    private String ip;
    /**
     * 远程配置.
     */
    private int port;
    /**
     * 远程配置.
     */
    private String logPath;
    /**
     * 远程配置.
     */
    private int logRetentionDays;
    /**
     * 任务列表.
     */
    private static ConcurrentMap<Integer, JobThread> jobThreadRepository = new ConcurrentHashMap<Integer, JobThread>();
    /**
     * 远程配置.
     */
    private static List<AdminBiz> adminBizList;

    public static List<AdminBiz> getAdminBizList() {
        return adminBizList;
    }

    private static ConcurrentMap<String, IJobHandler> jobHandlerRepository = new ConcurrentHashMap<String, IJobHandler>();
    private EmbedServer embedServer = null;

    public void setAdminAddresses(String adminAddresses) {
        this.adminAddresses = adminAddresses;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setLogPath(String logPath) {
        this.logPath = logPath;
    }

    public void setLogRetentionDays(int logRetentionDays) {
        this.logRetentionDays = logRetentionDays;
    }

    /**
     * 启动.
     */
    public void start() throws Exception {
        TaskFileAppender.initLogPath(logPath);
        initAdminBizList(adminAddresses, accessToken);
        JobLogFileCleanThread.getInstance().start(logRetentionDays);
        TriggerCallbackThread.getInstance().start();
        initEmbedServer(address, ip, port, appname, accessToken);
    }

    /**
     * 释放资源.
     */
    public void destroy() {
        stopEmbedServer();

        if (jobThreadRepository.size() > 0) {
            for (Map.Entry<Integer, JobThread> item : jobThreadRepository.entrySet()) {
                JobThread oldJobThread = removeJobThread(item.getKey(), "任务被终止.");
                if (oldJobThread != null) {
                    try {
                        oldJobThread.join();
                    } catch (InterruptedException e) {
                        log.error(">>>>>>>>>>> framework-task, 任务线程关闭发生错误, 任务编号:{}", item.getKey(), e);
                    }
                }
            }
            jobThreadRepository.clear();
        }
        //清理资源
        jobHandlerRepository.clear();
        JobLogFileCleanThread.getInstance().toStop();
        TriggerCallbackThread.getInstance().toStop();
    }

    /**
     * 初始化.
     */
    private void initAdminBizList(String adminAddresses, String accessToken) throws Exception {
        if (adminAddresses != null && adminAddresses.trim().length() > 0) {
            for (String address : adminAddresses.trim().split(",")) {
                if (address != null && address.trim().length() > 0) {
                    AdminBiz adminBiz = new AdminBizClient(address.trim(), accessToken);
                    if (adminBizList == null) {
                        adminBizList = new ArrayList<AdminBiz>();
                    }
                    adminBizList.add(adminBiz);
                }
            }
        }
    }

    /**
     * 初始化.
     */
    private void initEmbedServer(String address, String ip, int port, String appname, String accessToken) throws Exception {

        port = port > 0 ? port : NetHelper.findAvailablePort(9999);
        ip = (ip != null && ip.trim().length() > 0) ? ip : IPHelper.getIp();

        if (address == null || address.trim().length() == 0) {
            String ip_port_address = IPHelper.getIpPort(ip, port);
            address = "http://{ip_port}/".replace("{ip_port}", ip_port_address);
        }

        if (accessToken == null || accessToken.trim().length() == 0) {
            log.warn(">>>>>>>>>>> framework-task, accessToken 为空，设置Token.");
        }

        embedServer = new EmbedServer();
        embedServer.start(address, port, appname, accessToken);
    }

    private void stopEmbedServer() {
        if (embedServer != null) {
            try {
                embedServer.stop();
            } catch (Exception e) {
                log.error(">>>>>>>>>>> framework-task " + e.getMessage(), e);
            }
        }
    }

    public static IJobHandler loadJobHandler(String name) {
        return jobHandlerRepository.get(name);
    }

    public static IJobHandler registJobHandler(String name, IJobHandler jobHandler) {
        log.info(">>>>>>>>>>> framework-task 注册任务成功, 名称:{}, 句柄:{}", name, jobHandler);
        return jobHandlerRepository.put(name, jobHandler);
    }

    protected void registJobHandler(Mission mission, Object bean, Method executeMethod) {
        if (mission == null) {
            return;
        }

        String name = mission.value();
        Class<?> clazz = bean.getClass();
        String methodName = executeMethod.getName();
        if (name.trim().length() == 0) {
            throw new RuntimeException(">>>>>>>>>>> framework-task 方法句柄无效 [" + clazz + "#" + methodName + "] .");
        }
        if (loadJobHandler(name) != null) {
            throw new RuntimeException(">>>>>>>>>>> framework-task 句柄[" + name + "] 名称冲突.");
        }

        executeMethod.setAccessible(true);

        Method initMethod = null;
        Method destroyMethod = null;

        if (mission.init().trim().length() > 0) {
            try {
                initMethod = clazz.getDeclaredMethod(mission.init());
                initMethod.setAccessible(true);
            } catch (NoSuchMethodException e) {
                throw new RuntimeException(">>>>>>>>>>> framework-task 方法句柄无效 [" + clazz + "#" + methodName + "] .");
            }
        }
        if (mission.destroy().trim().length() > 0) {
            try {
                destroyMethod = clazz.getDeclaredMethod(mission.destroy());
                destroyMethod.setAccessible(true);
            } catch (NoSuchMethodException e) {
                throw new RuntimeException(">>>>>>>>>>> framework-task 方法句柄无效 [" + clazz + "#" + methodName + "] .");
            }
        }

        registJobHandler(name, new MethodJobHandler(bean, executeMethod, initMethod, destroyMethod));
    }

    public static JobThread registJobThread(int jobId, IJobHandler handler, String removeOldReason) {
        JobThread newJobThread = new JobThread(jobId, handler);
        newJobThread.start();
        log.info(">>>>>>>>>>> framework-task 注册成功, 任务ID:{}, 句柄:{}", new Object[]{jobId, handler});

        JobThread oldJobThread = jobThreadRepository.put(jobId, newJobThread);
        if (oldJobThread != null) {
            oldJobThread.toStop(removeOldReason);
            oldJobThread.interrupt();
        }

        return newJobThread;
    }

    public static JobThread removeJobThread(int jobId, String removeOldReason) {
        JobThread oldJobThread = jobThreadRepository.remove(jobId);
        if (oldJobThread != null) {
            oldJobThread.toStop(removeOldReason);
            oldJobThread.interrupt();

            return oldJobThread;
        }
        return null;
    }

    public static JobThread loadJobThread(int jobId) {
        return jobThreadRepository.get(jobId);
    }
}
