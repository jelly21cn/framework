package com.ct.framework.task.executor.impl;

import com.ct.framework.task.executor.TaskExecutor;
import com.ct.framework.core.annotation.Mission;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  01/06/2021
 * Usage: 无框架模式.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2022-10-1             xxx                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class TaskSimpleExecutor extends TaskExecutor {
    /**
     * 获取注解的所有方法.
     */
    private List<Object> taskBeanList = new ArrayList<>();

    /**
     * 返回列表.
     */
    public List<Object> getTaskBeanList() {
        return taskBeanList;
    }

    /**
     * 赋值列表.
     */
    public void setTaskBeanList(List<Object> taskBeanList) {
        this.taskBeanList = taskBeanList;
    }

    /**
     * 启动任务.
     */
    @Override
    public void start() {
        initJobHandlerMethodRepository(taskBeanList);
        try {
            super.start();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 销毁.
     */
    @Override
    public void destroy() {
        super.destroy();
    }

    /**
     * 获取Mission注解.
     */
    private void initJobHandlerMethodRepository(List<Object> taskBeanList) {
        if (taskBeanList == null || taskBeanList.size() == 0) {
            return;
        }

        for (Object bean : taskBeanList) {
            Method[] methods = bean.getClass().getDeclaredMethods();
            if (methods.length == 0) {
                continue;
            }
            for (Method executeMethod : methods) {
                Mission mission = executeMethod.getAnnotation(Mission.class);
                registJobHandler(mission, bean, executeMethod);
            }
        }
    }
}
