package com.ct.framework.task.thread;

import com.ct.framework.task.executor.TaskExecutor;
import com.ct.framework.utility.SerializeHelper;
import com.ct.framework.task.biz.AdminBiz;
import com.ct.framework.task.biz.model.RegistryParam;
import com.ct.framework.task.biz.model.ReturnT;
import com.ct.framework.task.enums.RegistryConfig;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  01/06/2021
 * Usage: .
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2022-10-1             xxx                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public class ExecutorRegistryThread {
    private static ExecutorRegistryThread instance = new ExecutorRegistryThread();

    public static ExecutorRegistryThread getInstance() {
        return instance;
    }

    private Thread registryThread;
    private volatile boolean toStop = false;

    public void start(final String appname, final String address) {
        if (appname == null || appname.trim().length() == 0) {
            log.warn(">>>>>>>>>>> framework-task, 配置异常, appname 为空.");
            return;
        }
        if (TaskExecutor.getAdminBizList() == null) {
            log.warn(">>>>>>>>>>> framework-task, 配置异常, adminAddresses 为空.");
            return;
        }

        registryThread = new Thread(new Runnable() {
            @Override
            public void run() {

                //注册
                while (!toStop) {
                    try {
                        RegistryParam registryParam = new RegistryParam(RegistryConfig.RegistType.EXECUTOR.name(), appname, address);
                        for (AdminBiz adminBiz : TaskExecutor.getAdminBizList()) {
                            try {
                                ReturnT<String> registryResult = adminBiz.registry(registryParam);
                                if (registryResult != null && ReturnT.SUCCESS_CODE == registryResult.getCode()) {
                                    registryResult = ReturnT.SUCCESS;
                                    log.info(">>>>>>>>>>> framework-task 注册成功, registryParam: {}, registryResult: {}", SerializeHelper.JsonSerializer(registryParam), SerializeHelper.JsonSerializer(registryResult));
                                    break;
                                } else {
                                    log.info(">>>>>>>>>>> framework-task 注册失败, registryParam: {}, registryResult: {}", SerializeHelper.JsonSerializer(registryParam), SerializeHelper.JsonSerializer(registryResult));
                                }
                            } catch (Exception e) {
                                log.info(">>>>>>>>>>> framework-task 注册异常, registryParam: {}", SerializeHelper.JsonSerializer(registryParam), e);
                            }
                        }
                    } catch (Exception e) {
                        if (!toStop) {
                            log.error(">>>>>>>>>>> framework-task " + e.getMessage(), e);
                        }
                    }

                    try {
                        if (!toStop) {
                            TimeUnit.SECONDS.sleep(RegistryConfig.BEAT_TIMEOUT);
                        }
                    } catch (InterruptedException e) {
                        if (!toStop) {
                            log.warn(">>>>>>>>>>> framework-task, 执行中断, 错误信息:{}", e.getMessage());
                        }
                    }
                }

                try {
                    RegistryParam registryParam = new RegistryParam(RegistryConfig.RegistType.EXECUTOR.name(), appname, address);
                    for (AdminBiz adminBiz : TaskExecutor.getAdminBizList()) {
                        try {
                            ReturnT<String> registryResult = adminBiz.registryRemove(registryParam);
                            if (registryResult != null && ReturnT.SUCCESS_CODE == registryResult.getCode()) {
                                registryResult = ReturnT.SUCCESS;
                                log.info(">>>>>>>>>>> framework-task 移除成功, registryParam:{}, registryResult:{}", SerializeHelper.JsonSerializer(registryParam), SerializeHelper.JsonSerializer(registryResult));
                                break;
                            } else {
                                log.info(">>>>>>>>>>> framework-task 移除失败, registryParam:{}, registryResult:{}", SerializeHelper.JsonSerializer(registryParam), SerializeHelper.JsonSerializer(registryResult));
                            }
                        } catch (Exception e) {
                            if (!toStop) {
                                log.info(">>>>>>>>>>> framework-task 移除发生错误, registryParam:{}", SerializeHelper.JsonSerializer(registryParam), e);
                            }
                        }
                    }
                } catch (Exception e) {
                    if (!toStop) {
                        log.error(">>>>>>>>>>> framework-task " + e.getMessage(), e);
                    }
                }
                log.info(">>>>>>>>>>> framework-task, 执行任务的线程已经销毁.");

            }
        });
        registryThread.setDaemon(true);
        registryThread.setName("framework-task, executor ExecutorRegistryThread");
        registryThread.start();
    }

    public void toStop() {
        toStop = true;

        if (registryThread != null) {
            registryThread.interrupt();
            try {
                registryThread.join();
            } catch (InterruptedException e) {
                log.error(">>>>>>>>>>> framework-task " + e.getMessage(), e);
            }
        }

    }

}
