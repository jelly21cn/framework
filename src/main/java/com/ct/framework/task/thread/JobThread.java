package com.ct.framework.task.thread;

import com.ct.framework.task.executor.TaskExecutor;
import com.ct.framework.task.biz.model.HandleCallbackParam;
import com.ct.framework.task.biz.model.ReturnT;
import com.ct.framework.task.biz.model.TriggerParam;
import com.ct.framework.task.context.TaskContext;
import com.ct.framework.task.context.TaskHelper;
import com.ct.framework.task.handler.IJobHandler;
import com.ct.framework.task.log.TaskFileAppender;
import lombok.extern.slf4j.Slf4j;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.*;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  01/06/2021
 * Usage: .
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2022-10-1             xxx                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public class JobThread extends Thread {
    private int jobId;
    private IJobHandler handler;
    private LinkedBlockingQueue<TriggerParam> triggerQueue;
    private Set<Long> triggerLogIdSet;

    private volatile boolean toStop = false;
    private String stopReason;

    private boolean running = false;
    private int idleTimes = 0;

    public JobThread(int jobId, IJobHandler handler) {
        this.jobId = jobId;
        this.handler = handler;
        this.triggerQueue = new LinkedBlockingQueue<TriggerParam>();
        this.triggerLogIdSet = Collections.synchronizedSet(new HashSet<Long>());

        this.setName("framework-task, JobThread-" + jobId + "-" + System.currentTimeMillis());
    }

    public IJobHandler getHandler() {
        return handler;
    }

    /**
     * 放入队列.
     *
     * @param triggerParam
     * @return
     */
    public ReturnT<String> pushTriggerQueue(TriggerParam triggerParam) {
        if (triggerLogIdSet.contains(triggerParam.getLogId())) {
            log.info(">>>>>>>>>>> framework-task 重复触发任务, logId:{}", triggerParam.getLogId());
            return new ReturnT<String>(ReturnT.FAIL_CODE, "重复触发任务, logId:" + triggerParam.getLogId());
        }

        triggerLogIdSet.add(triggerParam.getLogId());
        triggerQueue.add(triggerParam);
        return ReturnT.SUCCESS;
    }

    public void toStop(String stopReason) {
        this.toStop = true;
        this.stopReason = stopReason;
    }

    public boolean isRunningOrHasQueue() {
        return running || triggerQueue.size() > 0;
    }

    @Override
    public void run() {
        try {
            handler.init();
        } catch (Throwable e) {
            log.error(e.getMessage(), e);
        }

        while (!toStop) {
            running = false;
            idleTimes++;

            TriggerParam triggerParam = null;
            try {
                triggerParam = triggerQueue.poll(3L, TimeUnit.SECONDS);
                if (triggerParam != null) {
                    running = true;
                    idleTimes = 0;
                    triggerLogIdSet.remove(triggerParam.getLogId());

                    String logFileName = TaskFileAppender.makeLogFileName(new Date(triggerParam.getLogDateTime()), triggerParam.getLogId());
                    TaskContext taskContext = new TaskContext(
                            triggerParam.getJobId(),
                            triggerParam.getExecutorParams(),
                            logFileName,
                            triggerParam.getBroadcastIndex(),
                            triggerParam.getBroadcastTotal());

                    TaskContext.setTaskContext(taskContext);

                    TaskHelper.log("<br>----------- framework-task 任务开始执行 -----------<br>----------- Param:" + taskContext.getJobParam());

                    if (triggerParam.getExecutorTimeout() > 0) {
                        Thread futureThread = null;
                        try {
                            FutureTask<Boolean> futureTask = new FutureTask<Boolean>(new Callable<Boolean>() {
                                @Override
                                public Boolean call() throws Exception {
                                    TaskContext.setTaskContext(taskContext);

                                    handler.execute();
                                    return true;
                                }
                            });
                            futureThread = new Thread(futureTask);
                            futureThread.start();

                            Boolean tempResult = futureTask.get(triggerParam.getExecutorTimeout(), TimeUnit.SECONDS);
                        } catch (TimeoutException e) {

                            TaskHelper.log("<br>----------- framework-task 任务超时");
                            TaskHelper.log(e);

                            TaskHelper.handleTimeout("任务超时");
                        } finally {
                            futureThread.interrupt();
                        }
                    } else {
                        handler.execute();
                    }

                    if (TaskContext.getTaskContext().getHandleCode() <= 0) {
                        TaskHelper.handleFail("任务的结果丢失.");
                    } else {
                        String tempHandleMsg = TaskContext.getTaskContext().getHandleMsg();
                        tempHandleMsg = (tempHandleMsg != null && tempHandleMsg.length() > 50000)
                                ? tempHandleMsg.substring(0, 50000).concat("...")
                                : tempHandleMsg;
                        TaskContext.getTaskContext().setHandleMsg(tempHandleMsg);
                    }
                    TaskHelper.log("<br>----------- framework-task 任务执行完成 -----------<br>----------- Result: handleCode="
                            + TaskContext.getTaskContext().getHandleCode()
                            + ", handleMsg = "
                            + TaskContext.getTaskContext().getHandleMsg()
                    );

                } else {
                    if (idleTimes > 30) {
                        if (triggerQueue.size() == 0) {
                            TaskExecutor.removeJobThread(jobId, "任务执行超出限制.");
                        }
                    }
                }
            } catch (Throwable e) {
                if (toStop) {
                    TaskHelper.log("<br>----------- JobThread 停止理由:" + stopReason);
                }

                StringWriter stringWriter = new StringWriter();
                e.printStackTrace(new PrintWriter(stringWriter));
                String errorMsg = stringWriter.toString();

                TaskHelper.handleFail(errorMsg);

                TaskHelper.log("<br>----------- JobThread 错误信息:" + errorMsg + "<br>----------- framework-task 任务被异常结束 -----------");
            } finally {
                if (triggerParam != null) {
                    if (!toStop) {
                        TriggerCallbackThread.pushCallBack(new HandleCallbackParam(
                                triggerParam.getLogId(),
                                triggerParam.getLogDateTime(),
                                TaskContext.getTaskContext().getHandleCode(),
                                TaskContext.getTaskContext().getHandleMsg())
                        );
                    } else {
                        TriggerCallbackThread.pushCallBack(new HandleCallbackParam(
                                triggerParam.getLogId(),
                                triggerParam.getLogDateTime(),
                                TaskContext.HANDLE_CODE_FAIL,
                                stopReason + " [任务 运行中, killed]")
                        );
                    }
                }
            }
        }

        while (triggerQueue != null && triggerQueue.size() > 0) {
            TriggerParam triggerParam = triggerQueue.poll();
            if (triggerParam != null) {
                TriggerCallbackThread.pushCallBack(new HandleCallbackParam(
                        triggerParam.getLogId(),
                        triggerParam.getLogDateTime(),
                        TaskContext.HANDLE_CODE_FAIL,
                        stopReason + " [任务 没有执行, killed.]")
                );
            }
        }

        try {
            handler.destroy();
        } catch (Throwable e) {
            log.error(">>>>>>>>>>> framework-task " + e.getMessage(), e);
        }

        log.info(">>>>>>>>>>> framework-task JobThread 已经停止, hashCode:{}", Thread.currentThread());
    }
}
