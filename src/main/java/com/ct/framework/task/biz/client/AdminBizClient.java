package com.ct.framework.task.biz.client;

import com.ct.framework.task.biz.model.HandleCallbackParam;
import com.ct.framework.task.biz.model.RegistryParam;
import com.ct.framework.utility.http.HttpClientUtil;
import com.ct.framework.task.biz.AdminBiz;
import com.ct.framework.task.biz.model.ReturnT;

import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  01/06/2021
 * Usage: admin api test
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2022-10-1             xxx                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class AdminBizClient implements AdminBiz {
    private String addressUrl;
    private String accessToken;
    private int timeout = 3;

    public AdminBizClient() {
    }

    public AdminBizClient(String addressUrl, String accessToken) {
        this.addressUrl = addressUrl;
        this.accessToken = accessToken;
        if (!this.addressUrl.endsWith("/")) {
            this.addressUrl = this.addressUrl + "/";
        }
    }

    @Override
    public ReturnT<String> callback(List<HandleCallbackParam> callbackParamList) {
        return HttpClientUtil.postBody(addressUrl + "task/api/callback", accessToken, timeout, callbackParamList, String.class);
    }

    @Override
    public ReturnT<String> registry(RegistryParam registryParam) {
        return HttpClientUtil.postBody(addressUrl + "task/api/registry", accessToken, timeout, registryParam, String.class);
    }

    @Override
    public ReturnT<String> registryRemove(RegistryParam registryParam) {
        return HttpClientUtil.postBody(addressUrl + "task/api/registryRemove", accessToken, timeout, registryParam, String.class);
    }
}
