package com.ct.framework.task.biz;

import com.ct.framework.task.biz.model.HandleCallbackParam;
import com.ct.framework.task.biz.model.RegistryParam;
import com.ct.framework.task.biz.model.ReturnT;

import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  01/06/2021
 * Usage: 业务基类.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2022-10-1             xxx                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public interface AdminBiz {
    /**
     * 回调.
     *
     * @param callbackParamList 参数列表
     * @return ReturnT
     */
    public ReturnT<String> callback(List<HandleCallbackParam> callbackParamList);

    /**
     * 注册.
     *
     * @param registryParam 注册参数
     * @return ReturnT
     */
    public ReturnT<String> registry(RegistryParam registryParam);

    /**
     * 移除注册.
     *
     * @param registryParam 注册参数
     * @return ReturnT
     */
    public ReturnT<String> registryRemove(RegistryParam registryParam);

}
