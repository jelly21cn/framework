package com.ct.framework.task.biz;

import com.ct.framework.task.biz.model.*;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  01/06/2021
 * Usage: 执行基类.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2022-10-1             xxx                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public interface ExecutorBiz {

    public ReturnT<String> beat();

    public ReturnT<String> idleBeat(IdleBeatParam idleBeatParam);

    public ReturnT<String> run(TriggerParam triggerParam);

    public ReturnT<String> kill(KillParam killParam);

    public ReturnT<LogResult> log(LogParam logParam);
}
