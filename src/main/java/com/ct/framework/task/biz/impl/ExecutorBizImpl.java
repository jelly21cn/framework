package com.ct.framework.task.biz.impl;

import com.ct.framework.task.biz.ExecutorBiz;
import com.ct.framework.task.biz.model.*;
import com.ct.framework.task.enums.ExecutorBlockStrategyEnum;
import com.ct.framework.task.executor.TaskExecutor;
import com.ct.framework.task.glue.GlueFactory;
import com.ct.framework.task.glue.GlueTypeEnum;
import com.ct.framework.task.handler.IJobHandler;
import com.ct.framework.task.handler.impl.GlueJobHandler;
import com.ct.framework.task.handler.impl.ScriptJobHandler;
import com.ct.framework.task.log.TaskFileAppender;
import com.ct.framework.task.thread.JobThread;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  01/06/2021
 * Usage: .
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2022-10-1             xxx                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public class ExecutorBizImpl implements ExecutorBiz {
    @Override
    public ReturnT<String> beat() {
        return ReturnT.SUCCESS;
    }

    @Override
    public ReturnT<String> idleBeat(IdleBeatParam idleBeatParam) {
        boolean isRunningOrHasQueue = false;
        JobThread jobThread = TaskExecutor.loadJobThread(idleBeatParam.getJobId());
        if (jobThread != null && jobThread.isRunningOrHasQueue()) {
            isRunningOrHasQueue = true;
        }

        if (isRunningOrHasQueue) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, "任务线程正在运行 或者 放入队列.");
        }
        return ReturnT.SUCCESS;
    }

    @Override
    public ReturnT<String> run(TriggerParam triggerParam) {
        JobThread jobThread = TaskExecutor.loadJobThread(triggerParam.getJobId());
        IJobHandler jobHandler = jobThread != null ? jobThread.getHandler() : null;
        String removeOldReason = null;

        GlueTypeEnum glueTypeEnum = GlueTypeEnum.match(triggerParam.getGlueType());
        if (GlueTypeEnum.BEAN == glueTypeEnum) {

            //新任务
            IJobHandler newJobHandler = TaskExecutor.loadJobHandler(triggerParam.getExecutorHandler());

            //验证旧任务线程
            if (jobThread != null && jobHandler != newJobHandler) {
                removeOldReason = "关闭旧线程.";
                jobThread = null;
                jobHandler = null;
            }

            if (jobHandler == null) {
                jobHandler = newJobHandler;
                if (jobHandler == null) {
                    return new ReturnT<String>(ReturnT.FAIL_CODE, "任务 [" + triggerParam.getExecutorHandler() + "] 没有找到.");
                }
            }

        } else if (GlueTypeEnum.GLUE_GROOVY == glueTypeEnum) {

            if (jobThread != null && !(jobThread.getHandler() instanceof GlueJobHandler && ((GlueJobHandler) jobThread.getHandler()).getGlueUpdatetime() == triggerParam.getGlueUpdatetime())) {
                removeOldReason = "关闭旧线程.";

                jobThread = null;
                jobHandler = null;
            }

            if (jobHandler == null) {
                try {
                    IJobHandler originJobHandler = GlueFactory.getInstance().loadNewInstance(triggerParam.getGlueSource());
                    jobHandler = new GlueJobHandler(originJobHandler, triggerParam.getGlueUpdatetime());
                } catch (Exception e) {
                    log.error(">>>>>>>>>>> framework-task " + e.getMessage(), e);
                    return new ReturnT<String>(ReturnT.FAIL_CODE, e.getMessage());
                }
            }
        } else if (glueTypeEnum != null && glueTypeEnum.isScript()) {
            if (jobThread != null && !(jobThread.getHandler() instanceof ScriptJobHandler && ((ScriptJobHandler) jobThread.getHandler()).getGlueUpdatetime() == triggerParam.getGlueUpdatetime())) {
                removeOldReason = "关闭旧线程.";

                jobThread = null;
                jobHandler = null;
            }

            if (jobHandler == null) {
                jobHandler = new ScriptJobHandler(triggerParam.getJobId(), triggerParam.getGlueUpdatetime(), triggerParam.getGlueSource(), GlueTypeEnum.match(triggerParam.getGlueType()));
            }
        } else {
            return new ReturnT<String>(ReturnT.FAIL_CODE, "类型 [" + triggerParam.getGlueType() + "] 无效.");
        }

        if (jobThread != null) {
            ExecutorBlockStrategyEnum blockStrategy = ExecutorBlockStrategyEnum.match(triggerParam.getExecutorBlockStrategy(), null);
            if (ExecutorBlockStrategyEnum.DISCARD_LATER == blockStrategy) {
                if (jobThread.isRunningOrHasQueue()) {
                    return new ReturnT<String>(ReturnT.FAIL_CODE, "策略：" + ExecutorBlockStrategyEnum.DISCARD_LATER.getTitle());
                }
            } else if (ExecutorBlockStrategyEnum.COVER_EARLY == blockStrategy) {
                if (jobThread.isRunningOrHasQueue()) {
                    removeOldReason = "策略：" + ExecutorBlockStrategyEnum.COVER_EARLY.getTitle();

                    jobThread = null;
                }
            }
        }

        if (jobThread == null) {
            jobThread = TaskExecutor.registJobThread(triggerParam.getJobId(), jobHandler, removeOldReason);
        }

        ReturnT<String> pushResult = jobThread.pushTriggerQueue(triggerParam);
        return pushResult;
    }

    @Override
    public ReturnT<String> kill(KillParam killParam) {
        JobThread jobThread = TaskExecutor.loadJobThread(killParam.getJobId());
        if (jobThread != null) {
            TaskExecutor.removeJobThread(killParam.getJobId(), "结束任务.");
            return ReturnT.SUCCESS;
        }

        return new ReturnT<String>(ReturnT.SUCCESS_CODE, "任务已经结束.");
    }

    @Override
    public ReturnT<LogResult> log(LogParam logParam) {
        String logFileName = TaskFileAppender.makeLogFileName(new Date(logParam.getLogDateTim()), logParam.getLogId());
        LogResult logResult = TaskFileAppender.readLog(logFileName, logParam.getFromLineNum());
        return new ReturnT<LogResult>(logResult);
    }
}
