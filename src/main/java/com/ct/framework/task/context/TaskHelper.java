package com.ct.framework.task.context;

import com.ct.framework.utility.DateHelper;
import com.ct.framework.task.log.TaskFileAppender;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  01/06/2021
 * Usage: .
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2022-10-1             xxx                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public final class TaskHelper {

    private TaskHelper() {
    }

    /**
     * 当前任务ID.
     *
     * @return long
     */
    public static long getJobId() {
        TaskContext taskContext = TaskContext.getTaskContext();
        if (taskContext == null) {
            return -1;
        }

        return taskContext.getJobId();
    }

    /**
     * 当前任务参数.
     *
     * @return String
     */
    public static String getJobParam() {
        TaskContext taskContext = TaskContext.getTaskContext();
        if (taskContext == null) {
            return null;
        }

        return taskContext.getJobParam();
    }

    /**
     * 日志文件.
     *
     * @return String
     */
    public static String getJobLogFileName() {
        TaskContext taskContext = TaskContext.getTaskContext();
        if (taskContext == null) {
            return null;
        }

        return taskContext.getJobLogFileName();
    }

    /**
     * 当前序号.
     *
     * @return int
     */
    public static int getShardIndex() {
        TaskContext taskContext = TaskContext.getTaskContext();
        if (taskContext == null) {
            return -1;
        }

        return taskContext.getShardIndex();
    }

    /**
     * 总量.
     *
     * @return int
     */
    public static int getShardTotal() {
        TaskContext taskContext = TaskContext.getTaskContext();
        if (taskContext == null) {
            return -1;
        }

        return taskContext.getShardTotal();
    }

    /**
     * 追加日志.
     */
    public static boolean log(String appendLogPattern, Object... appendLogArguments) {
        FormattingTuple ft = MessageFormatter.arrayFormat(appendLogPattern, appendLogArguments);
        String appendLog = ft.getMessage();
        StackTraceElement callInfo = new Throwable().getStackTrace()[1];
        return logDetail(callInfo, appendLog);
    }

    /**
     * 追加日志.
     */
    public static boolean log(Throwable e) {

        StringWriter stringWriter = new StringWriter();
        e.printStackTrace(new PrintWriter(stringWriter));
        String appendLog = stringWriter.toString();

        StackTraceElement callInfo = new Throwable().getStackTrace()[1];
        return logDetail(callInfo, appendLog);
    }

    /**
     * 追加日志.
     */
    private static boolean logDetail(StackTraceElement callInfo, String appendLog) {
        TaskContext taskContext = TaskContext.getTaskContext();
        if (taskContext == null) {
            return false;
        }

        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(DateHelper.FormatDateTime(new Date())).append(" ").append("[" + callInfo.getClassName() + "#" + callInfo.getMethodName() + "]").append("-").append("[" + callInfo.getLineNumber() + "]").append("-").append("[" + Thread.currentThread().getName() + "]").append(" ").append(appendLog != null ? appendLog : "");
        String formatAppendLog = stringBuffer.toString();

        String logFileName = taskContext.getJobLogFileName();

        if (logFileName != null && logFileName.trim().length() > 0) {
            TaskFileAppender.appendLog(logFileName, formatAppendLog);
            return true;
        } else {
            log.info(">>>>>>>>>>> framework-task {}", formatAppendLog);
            return false;
        }
    }

    /**
     * 句柄结果.
     *
     * @return boolean
     */
    public static boolean handleSuccess() {
        return handleResult(TaskContext.HANDLE_CODE_SUCCESS, null);
    }

    /**
     * 句柄结果.
     *
     * @return boolean
     */
    public static boolean handleSuccess(String handleMsg) {
        return handleResult(TaskContext.HANDLE_CODE_SUCCESS, handleMsg);
    }

    /**
     * 句柄结果.
     *
     * @return boolean
     */
    public static boolean handleFail() {
        return handleResult(TaskContext.HANDLE_CODE_FAIL, null);
    }

    /**
     * 句柄结果.
     *
     * @return boolean
     */
    public static boolean handleFail(String handleMsg) {
        return handleResult(TaskContext.HANDLE_CODE_FAIL, handleMsg);
    }

    /**
     * 句柄耗时.
     *
     * @return boolean
     */
    public static boolean handleTimeout() {
        return handleResult(TaskContext.HANDLE_CODE_TIMEOUT, null);
    }

    /**
     * 句柄耗时.
     *
     * @return boolean
     */
    public static boolean handleTimeout(String handleMsg) {
        return handleResult(TaskContext.HANDLE_CODE_TIMEOUT, handleMsg);
    }

    /**
     * 句柄结果.
     *
     * @param handleCode code
     * @param handleMsg  消息
     * @return boolean
     */
    public static boolean handleResult(int handleCode, String handleMsg) {
        TaskContext taskContext = TaskContext.getTaskContext();
        if (taskContext == null) {
            return false;
        }

        taskContext.setHandleCode(handleCode);
        if (handleMsg != null) {
            taskContext.setHandleMsg(handleMsg);
        }
        return true;
    }
}
