package com.ct.framework.service.broker.entity;
/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 客户端RMI配置整体xml配置.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class ServiceBrokerConfigEntity {
    private ServiceLocationList location;

    private EndpointAddressList endpoint;

    public ServiceLocationList getLocation() {
        return location;
    }

    public void setLocation(ServiceLocationList location) {
        this.location = location;
    }

    public EndpointAddressList getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(EndpointAddressList endpoint) {
        this.endpoint = endpoint;
    }
}
