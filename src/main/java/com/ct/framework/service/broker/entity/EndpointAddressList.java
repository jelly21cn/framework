package com.ct.framework.service.broker.entity;

import java.util.List;
/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 客户端RMI远程地址实体列表.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class EndpointAddressList {
    private List<EndpointAddressEntity> endpointList;

    public List<EndpointAddressEntity> getEndpointList() {
        return endpointList;
    }

    public void setEndpointList(List<EndpointAddressEntity> endpointList) {
        this.endpointList = endpointList;
    }
}
