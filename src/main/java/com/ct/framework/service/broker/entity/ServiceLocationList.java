package com.ct.framework.service.broker.entity;

import java.util.List;
/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: .
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class ServiceLocationList {
    private List<ServiceLocationEntity> locationList;

    public List<ServiceLocationEntity> getLocationList() {
        return locationList;
    }

    public void setLocationList(List<ServiceLocationEntity> locationList) {
        this.locationList = locationList;
    }
}
