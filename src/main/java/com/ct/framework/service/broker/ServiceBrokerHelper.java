package com.ct.framework.service.broker;

import com.ct.framework.core.configuration.SystemConfigurationManager;
import com.ct.framework.data.configuration.DataAccessSetting;
import com.ct.framework.rpc.remoting.invoker.call.CallType;
import com.ct.framework.rpc.remoting.invoker.reference.RpcReferenceBean;
import com.ct.framework.rpc.remoting.invoker.route.LoadBalance;
import com.ct.framework.rpc.remoting.net.impl.netty.client.NettyClient;
import com.ct.framework.rpc.serialize.impl.HessianSerializer;
import com.ct.framework.service.broker.entity.*;
import com.ct.framework.service.console.ServiceBrokerUtil;
import com.ct.framework.utility.string.XmlHelper;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 解析ServiceBrokerConfig.xml文件.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class ServiceBrokerHelper {

    private static Map<String, EndpointAddressEntity> EndpointList;

    private static ServiceBrokerConfigEntity serviceBrokerConfigEntity;

    static {
        EndpointList = new HashMap<>();
        UpdateAllServiceBrokerFiles();
    }

    private static void UpdateAllServiceBrokerFiles() {
        //<file name="hr/company/DataCommand_User.xml"/>
        NodeList nodeList = XmlHelper.GetNodeList(DataAccessSetting.getServiceBrokerConfigFile());

        ServiceLocationList serviceLocationList = new ServiceLocationList();
        EndpointAddressList endpointAddressList = new EndpointAddressList();
        for (int i1 = 0; i1 < nodeList.getLength(); i1++) {

            //<ServiceLocation host="localhost" port="8080"></ServiceLocation>
            Node serviceBrokerConfigNode = nodeList.item(i1);

            String nodeName = serviceBrokerConfigNode.getNodeName();

            if ("#text".equals(nodeName)) {
                continue;
            }

            if ("#comment".equals(nodeName)) {
                continue;
            }

            if ("ServiceLocationList".equalsIgnoreCase(nodeName)) {
                NodeList list1 = serviceBrokerConfigNode.getChildNodes();
                List<ServiceLocationEntity> serviceLocationEntityList = new ArrayList<>();
                for (int i3 = 0; i3 < list1.getLength(); i3++) {
                    Node serviceLocationNode = list1.item(i3);
                    String serviceLocationNodeName = serviceLocationNode.getNodeName();
                    if ("#text".equals(serviceLocationNodeName)) {
                        continue;
                    }
                    if ("#comment".equals(serviceLocationNodeName)) {
                        continue;
                    }
                    ServiceLocationEntity serviceLocationEntity = new ServiceLocationEntity();
                    serviceLocationEntity.setServiceHost(serviceLocationNode.getAttributes().getNamedItem("host").getNodeValue());
                    serviceLocationEntity.setServicePort(serviceLocationNode.getAttributes().getNamedItem("port").getNodeValue());
                    serviceLocationEntityList.add(serviceLocationEntity);
                }
                serviceLocationList.setLocationList(serviceLocationEntityList);
            } else if ("EndpointAddressList".equalsIgnoreCase(nodeName)) {
                NodeList list2 = serviceBrokerConfigNode.getChildNodes();
                List<EndpointAddressEntity> endpointAddressEntityList = new ArrayList<>();
                for (int i3 = 0; i3 < list2.getLength(); i3++) {
                    Node endpointAddressNode = list2.item(i3);
                    String endpointAddressNodeName = endpointAddressNode.getNodeName();
                    if ("#text".equals(endpointAddressNodeName)) {
                        continue;
                    }
                    if ("#comment".equals(endpointAddressNodeName)) {
                        continue;
                    }
                    EndpointAddressEntity endpointAddressEntity = new EndpointAddressEntity();
                    endpointAddressEntity.setAddress(endpointAddressNode.getAttributes().getNamedItem("address").getNodeValue());
                    endpointAddressEntity.setConfigName(endpointAddressNode.getAttributes().getNamedItem("config").getNodeValue());
                    endpointAddressEntity.setName(endpointAddressNode.getAttributes().getNamedItem("name").getNodeValue());
                    endpointAddressEntity.setType(endpointAddressNode.getAttributes().getNamedItem("type").getNodeValue());
                    endpointAddressEntityList.add(endpointAddressEntity);

                    //全局EndpointList.
                    EndpointList.put(endpointAddressEntity.getName(), endpointAddressEntity);
                }
                endpointAddressList.setEndpointList(endpointAddressEntityList);
            }
        }

        serviceBrokerConfigEntity = new ServiceBrokerConfigEntity();
        serviceBrokerConfigEntity.setEndpoint(endpointAddressList);
        serviceBrokerConfigEntity.setLocation(serviceLocationList);
    }

    /**
     * 检查配置
     */
    public static Registry CheckConfig(Class<?> clazz) {

        String className = clazz.getName();

        if (!EndpointList.containsKey(className)) {
            return null;
        }

        //EndpointAddressEntity endpointAddressEntity = ServiceBrokerHelper.EndpointList.get(className);
        ServiceLocationEntity serviceLocation = ServiceBrokerHelper.serviceBrokerConfigEntity.getLocation().getLocationList().get(0);

        return ServiceBrokerUtil.BuildBindingByType(serviceLocation);
    }

    public static RpcReferenceBean CheckRpcConfig(Class<?> clazz) {
        String className = clazz.getName();

        if (!EndpointList.containsKey(className)) {
            return null;
        }

        RpcReferenceBean referenceBean = new RpcReferenceBean();
        referenceBean.setClient(NettyClient.class);
        referenceBean.setSerializer(HessianSerializer.class);
        referenceBean.setCallType(CallType.SYNC);
        referenceBean.setLoadBalance(LoadBalance.ROUND);
        referenceBean.setIface(clazz);
        referenceBean.setClassName(clazz.getName());
        referenceBean.setVersion(null);
        referenceBean.setTimeout(60000);
        referenceBean.setAddress(null);
        referenceBean.setAccessToken(null);
        referenceBean.setInvokeCallback(null);
        referenceBean.setInvokerFactory(SystemConfigurationManager.getSingleton().GetRpcInvokerFactory());

        return referenceBean;
    }
}
