package com.ct.framework.service.configuration;

import com.ct.framework.rpc.registry.impl.RpcAdminRegister;
import com.ct.framework.rpc.remoting.net.impl.netty.server.NettyServer;
import com.ct.framework.rpc.remoting.provider.RpcProviderFactory;
import com.ct.framework.rpc.serialize.impl.HessianSerializer;
import com.ct.framework.core.configuration.SystemConfigurationManager;
import lombok.extern.slf4j.Slf4j;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.HashMap;
import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 服务初始化，建立RMI通道.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public final class ServiceActivationBuildProvider {

    private ServiceActivationBuildProvider() {
    }

    /**
     * 初始化.
     */
    public static void Init() {
        List<ServiceActivationElement> serviceActivationElementList = ServiceActivationElementCollection.GetServiceActivationElementList();
        log.info("CT  RMI服务 共: " + serviceActivationElementList.size());
        Registry registry = null;
        try {
            log.info("CT  RMI服务 端口号: " + SystemConfigurationManager.getSingleton().GetServicePort());
            registry = LocateRegistry.createRegistry(Integer.parseInt(SystemConfigurationManager.getSingleton().GetServicePort()));
        } catch (RemoteException e) {
            log.info("CT  RMI服务 启动失败: " + e.getMessage());
            e.printStackTrace();
        }
        int index = 1;
        for (ServiceActivationElement element : serviceActivationElementList) {
            try {
                Class<?> clazz = Class.forName(element.GetService());
                Object service = clazz.newInstance();
                assert registry != null;
                registry.rebind(element.GetNamespace(), (Remote) service);
                log.info("CT  RMI服务【" + index + "、" + element.GetNamespace() + "】启动成功");
            } catch (Exception e) {
                log.info("CT  RMI服务【" + index + "、" + element.GetNamespace() + "】发生错误：" + e.getMessage());
                e.printStackTrace();
            }
            index++;
        }

        log.info("CT  RMI服务 启动完成");
    }

    /**
     * RPC初始化.
     */
    public static void InitRpc() {
        RpcProviderFactory providerFactory = new RpcProviderFactory();
        providerFactory.setServer(NettyServer.class);
        providerFactory.setSerializer(HessianSerializer.class);
        providerFactory.setCorePoolSize(-1);
        providerFactory.setMaxPoolSize(-1);
        providerFactory.setIp(SystemConfigurationManager.getSingleton().GetServiceHost());
        providerFactory.setPort(Integer.parseInt(SystemConfigurationManager.getSingleton().GetServicePort()));
        providerFactory.setAccessToken(null);
        providerFactory.setServiceRegistry(RpcAdminRegister.class);
        providerFactory.setServiceRegistryParam(new HashMap<String, String>() {{
            put(RpcAdminRegister.ADMIN_ADDRESS, SystemConfigurationManager.getSingleton().GetRpcUrl());
            put(RpcAdminRegister.ENV, "default");
            put(RpcAdminRegister.BIZ, SystemConfigurationManager.getSingleton().GetRegionName());
        }});

        // 增加服务
        List<ServiceActivationElement> serviceActivationElementList = ServiceActivationElementCollection.GetServiceActivationElementList();
        log.info("CT  RMI服务 共: " + serviceActivationElementList.size());
        int index = 1;
        for (ServiceActivationElement element : serviceActivationElementList) {
            try {
                Class<?> clazz = Class.forName(element.GetService());
                Object service = clazz.newInstance();
                providerFactory.addService(element.GetNamespace(), null, service);
                log.info("CT  RPC服务【" + index + "、" + element.GetNamespace() + "】开始提交注册中心 ");
            } catch (Exception e) {
                log.info("CT  RPC服务【" + index + "、" + element.GetNamespace() + "】发生错误：" + e.getMessage());
                e.printStackTrace();
            }
            index++;
        }

        try {
            providerFactory.start();
        } catch (Exception e) {
            log.info("CT  HOST 启动负载均衡器发生错误：" + e.getMessage());
            e.printStackTrace();
        }
        log.info("CT  RMI服务 启动完成");
    }
}
