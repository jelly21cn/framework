package com.ct.framework.service.configuration;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 服务节点.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class ServiceActivationElement {

    /**
     * 地址.
     */
    private String relativeAddress;

    /**
     * 命名空间.
     */
    private String namespace;

    /**
     * 服务.
     */
    private String service;

    /**
     * 地址.
     *
     * @return string
     */
    public String GetRelativeAddress() {
        return relativeAddress;
    }

    /**
     * 地址.
     *
     * @param relativeAddress 地址
     */
    public void SetRelativeAddress(String relativeAddress) {
        this.relativeAddress = relativeAddress;
    }

    /**
     * 命名空间.
     *
     * @return string
     */
    public String GetNamespace() {
        return namespace;
    }

    /**
     * 命名空间.
     *
     * @param namespace 命名空间
     */
    public void SetNamespace(String namespace) {
        this.namespace = namespace;
    }

    /**
     * 服务.
     *
     * @return string
     */
    public String GetService() {
        return service;
    }

    /**
     * 服务.
     *
     * @param service 服务
     */
    public void SetService(String service) {
        this.service = service;
    }
}
