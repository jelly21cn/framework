package com.ct.framework.service.configuration;

import com.ct.framework.utility.string.XmlHelper;
import com.ct.framework.data.configuration.DataAccessSetting;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 加载服务映射文件
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public final class ServiceActivationElementCollection {

    private ServiceActivationElementCollection() {
    }

    /**
     * serviceActivationElementMap.
     */
    private static Map<String, ServiceActivationElement> serviceActivationElementMap;
    /**
     * serviceActivationElementList.
     */
    private static List<ServiceActivationElement> serviceActivationElementList;

    /*
     * 静态构造.
     */
    static {
        serviceActivationElementMap = new HashMap<>();
        serviceActivationElementList = new ArrayList<>();
        UpdateAllServiceActivationFiles();
    }

    /**
     * 更新所有节点.
     */
    private static void UpdateAllServiceActivationFiles() {

        NodeList nodeList = XmlHelper.GetNodeList(DataAccessSetting.getServiceHostingEnvironment());

        for (int i1 = 0; i1 < nodeList.getLength(); i1++) {

            Node node = nodeList.item(i1);

            String nodeName = node.getNodeName();

            if ("#text".equals(nodeName)) {
                continue;
            }

            if ("#comment".equals(nodeName)) {
                continue;
            }

            ServiceActivationElement serviceActivationElement = new ServiceActivationElement();
            serviceActivationElement.SetNamespace(node.getAttributes().getNamedItem("namespace").getNodeValue());
            serviceActivationElement.SetRelativeAddress(node.getAttributes().getNamedItem("relativeAddress").getNodeValue());
            serviceActivationElement.SetService(node.getAttributes().getNamedItem("service").getNodeValue());
            serviceActivationElementList.add(serviceActivationElement);
            serviceActivationElementMap.put(serviceActivationElement.GetNamespace(), serviceActivationElement);

        }
    }

    /**
     * 获取Map节点.
     *
     * @return Map
     */
    public static Map<String, ServiceActivationElement> GetServiceActivationElements() {
        return serviceActivationElementMap;
    }

    /**
     * 获取列表.
     *
     * @return List
     */
    public static List<ServiceActivationElement> GetServiceActivationElementList() {
        return serviceActivationElementList;
    }
}
