package com.ct.framework.service.common;

import com.ct.framework.contract.QueryResultContract;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 接口基类
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 * @param <Data> 数据
 * @param <Contract> 契约
 * @param <Criterias> 查询
 *****************************************************************/
public interface InterfaceBase<Data, Contract, Criterias> extends Remote {

    /**
     * 分页查询.
     *
     * @param criterias 查询条件
     * @return QueryResultContract
     */
    QueryResultContract<List<Data>> GetEntityList(Criterias criterias) throws RemoteException;

    /**
     * 写入或者更新.
     *
     * @param contractV10 BO
     * @return Contract
     */
    Contract SaveEntity(Contract contractV10) throws RemoteException;

    /**
     * 读取单个数据.
     *
     * @param id 主键
     * @return Contract
     */
    Contract GetEntity(long id) throws RemoteException;

    /**
     * 删除单个数据.
     *
     * @param id 主键
     * @return boolean
     */
    Contract DeleteEntity(Contract contractV10) throws RemoteException;

}
