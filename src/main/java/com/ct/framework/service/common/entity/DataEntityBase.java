package com.ct.framework.service.common.entity;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 数据展示基类
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class DataEntityBase {
    /**
     * 上下文信息
     */
    private MessageHeaderEntity headerEntity;

    public MessageHeaderEntity GetHeaderEntity() {
        return headerEntity;
    }

    public void SetHeaderEntity(MessageHeaderEntity headerEntity) {
        this.headerEntity = headerEntity;
    }
}
