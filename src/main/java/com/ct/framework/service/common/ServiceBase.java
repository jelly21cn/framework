package com.ct.framework.service.common;

import com.ct.framework.contract.ICallService;
import com.ct.framework.rpc.remoting.provider.annotation.RpcService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 服务基类
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@RpcService
public class ServiceBase extends UnicastRemoteObject implements ICallService {

    /**
     * 构造函数.
     */
    public ServiceBase() throws RemoteException {
    }
}
