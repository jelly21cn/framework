package com.ct.framework.service.common.transform;

import com.ct.framework.contract.PagingInfo;
import com.ct.framework.service.common.entity.PagingInfoEntity;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: .
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class PagingTransform {

    public static PagingInfoEntity ToEntity(PagingInfo data) {
        PagingInfoEntity result = new PagingInfoEntity();

        if (data != null) {
            result.SetPageCurrent(data.GetStartRowIndex());
            result.SetPageSize(data.GetPageSize());
            result.SetSortField(data.GetSortField());
            result.SetSortType(data.GetSortType().name());
        }

        return result;
    }

}
