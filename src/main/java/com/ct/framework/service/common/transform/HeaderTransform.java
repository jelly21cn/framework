package com.ct.framework.service.common.transform;

import com.ct.framework.contract.MessageHeader;
import com.ct.framework.service.common.entity.MessageHeaderEntity;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: .
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class HeaderTransform {
    public static MessageHeaderEntity ToEntity(MessageHeader data) {
        MessageHeaderEntity result = new MessageHeaderEntity();

        if (data != null) {
            result.SetFromSystem(data.GetFromSystem());
            result.SetToSystem("LOCALHOST");
            if (data.GetOperationUser() != null) {
                result.SetGroupId(data.GetOperationUser().GetGroupId());
                result.SetOrganizationId(data.GetOperationUser().GetOrganizationId());
                result.SetLoginName(data.GetOperationUser().GetLoginName());
                result.SetDisplayName(data.GetOperationUser().GetDisplayName());
                result.SetCompanyCode(data.GetOperationUser().GetCompanyCode());
                result.SetUserSysNo(data.GetOperationUser().GetUserSysNo());
            }
        }
        return result;
    }
}
