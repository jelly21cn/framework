package com.ct.framework.service.common.entity;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 查询【条件】基类.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class QueryEntityBase {

    /**
     * 分页信息。
     */
    private PagingInfoEntity pageInfo;

    /**
     * 上下文信息
     */
    private MessageHeaderEntity headerEntity;

    public PagingInfoEntity GetPageInfo() {
        return pageInfo;
    }

    public void SetPageInfo(PagingInfoEntity pageInfo) {
        this.pageInfo = pageInfo;
    }

    public MessageHeaderEntity GetHeaderEntity() {
        return headerEntity;
    }

    public void SetHeaderEntity(MessageHeaderEntity headerEntity) {
        this.headerEntity = headerEntity;
    }
}
