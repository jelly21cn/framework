package com.ct.framework.service.common.transform;

import com.ct.framework.contract.DefaultDataContract;
import com.ct.framework.contract.IDataContract;
import com.ct.framework.contract.PagingInfo;
import com.ct.framework.contract.QueryResultContract;
import com.ct.framework.service.common.entity.DataEntityBase;
import com.ct.framework.service.common.entity.QueryResultEntity;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: OTO基类，用于服务端
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 * @param <Contract> 契约
 * @param <TData> VO
 * @param <TEntity> PO
 *****************************************************************/
public abstract class TransformBase<Contract, TData, TEntity extends DataEntityBase> {

    /**
     * 查询列表结果转换.
     * 从PO转换为BO.
     *
     * @param entityList 查询实体
     * @return QueryResultContract
     */
    public QueryResultContract<List<TData>> ToQueryResultList(QueryResultEntity<List<TEntity>> entityList) {

        QueryResultContract<List<TData>> result = new QueryResultContract<>();

        List<TData> list = new ArrayList<>();

        for (TEntity entity : entityList.getResultList()) {
            list.add(ToData(entity));
        }

        result.SetResultList(list);
        result.setPageInfo(new PagingInfo(entityList.getTotalCount()));

        return result;
    }

    /**
     * 从BO转换为PO.
     *
     * @param <T>          范型
     * @param dataContract 契约
     * @return TEntity PO
     */
    public <T extends DefaultDataContract & IDataContract> TEntity ToEntity(T dataContract) {

        Type superclass = getClass().getGenericSuperclass();

        ParameterizedType parameterizedType = (ParameterizedType) superclass;

        Type[] typeArray = parameterizedType.getActualTypeArguments();

        Class clazz = (Class) typeArray[2];

        TEntity tEntity = null;

        try {
            tEntity = (TEntity) clazz.newInstance();

            tEntity = Transform.CopyFields(dataContract.GetBody(), tEntity);

            tEntity.SetHeaderEntity(HeaderTransform.ToEntity(dataContract.GetHeader()));

        } catch (Exception ex) {
        }
        return tEntity;
    }

    /**
     * 从PO转换为BO.
     *
     * @param tEntity PO
     * @return TData
     */
    public TData ToData(TEntity tEntity) {

        Type superclass = getClass().getGenericSuperclass();

        ParameterizedType parameterizedType = (ParameterizedType) superclass;

        Type[] typeArray = parameterizedType.getActualTypeArguments();

        Class clazz = (Class) typeArray[1];

        TData tData = null;

        try {
            tData = (TData) clazz.newInstance();
            tData = Transform.CopyFields(tEntity, tData);
        } catch (Exception ex) {
        }
        return tData;
    }

    /**
     * 从PO转换为BO.
     *
     * @param <T>     范型
     * @param tEntity PO
     * @return T
     */
    public <T extends IDataContract> T ToDataContract(TEntity tEntity) {

        Type superclass = getClass().getGenericSuperclass();

        ParameterizedType parameterizedType = (ParameterizedType) superclass;

        Type[] typeArray = parameterizedType.getActualTypeArguments();

        Class vClazz = (Class) typeArray[0];
        Class dataClazz = (Class) typeArray[1];

        T t = null;
        TData tData = null;

        try {
            t = (T) vClazz.newInstance();
            tData = (TData) dataClazz.newInstance();

        } catch (Exception ex) {
        }
        Transform.CopyFields(tEntity, tData);
        t.SetBody(tData);
        return t;
    }

    /**
     * 从List<BO>转换为List<PO>.
     *
     * @param <T>      范型
     * @param contract BO
     * @return T
     */
    public <T extends DefaultDataContract & IDataContract<List<TData>>> List<TEntity> ToEntitys(T contract) {

        Type superclass = getClass().getGenericSuperclass();

        ParameterizedType parameterizedType = (ParameterizedType) superclass;

        Type[] typeArray = parameterizedType.getActualTypeArguments();

        Class clazz = (Class) typeArray[2];

        TEntity tEntity;

        List<TEntity> list = new ArrayList<>();

        try {

            DefaultDataContract dataContract = (DefaultDataContract) contract;

            for (TData data : contract.GetBody()) {
                tEntity = (TEntity) clazz.newInstance();
                tEntity = Transform.CopyFields(data, tEntity);
                tEntity.SetHeaderEntity(HeaderTransform.ToEntity(dataContract.GetHeader()));
                list.add(tEntity);
            }

        } catch (Exception ex) {
        }
        return list;
    }

    /**
     * 从List<PO>转换为List<BO>.
     *
     * @param <T>        范型
     * @param entityList PO
     * @return T
     */
    public <T extends IDataContract<List<TData>>> T ToDataContracts(List<TEntity> entityList) {

        Type superclass = getClass().getGenericSuperclass();

        ParameterizedType parameterizedType = (ParameterizedType) superclass;

        Type[] typeArray = parameterizedType.getActualTypeArguments();

        Class clazz = (Class) typeArray[0];

        List<TData> dataList = new ArrayList<>();

        T t = null;

        try {
            t = (T) clazz.newInstance();

            for (TEntity viewEntity : entityList) {
                dataList.add(this.ToData(viewEntity));
            }
            t.SetBody(dataList);
        } catch (Exception ex) {
        }

        return t;
    }
}
