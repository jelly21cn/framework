package com.ct.framework.service.common;

import java.rmi.Remote;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 简约接口基类
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 * @param <Data> 数据
 * @param <Contract> 契约
 *****************************************************************/
public interface SimpleInterfaceBase<Data, Contract> extends Remote {
}
