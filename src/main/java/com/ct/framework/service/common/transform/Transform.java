package com.ct.framework.service.common.transform;

import java.lang.reflect.Field;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 转换基类
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class Transform {

    private Transform() {
    }

    /**
     * 通过反射OTO
     *
     * @param src    来源
     * @param target 目标的clas
     * @return T
     */
    public static <T> T CopyFields(Object src, Object target) {

        try {

            Field[] srcFields = src.getClass().getDeclaredFields();
            Field[] targetFields = target.getClass().getDeclaredFields();

            for (Field srcField : srcFields) {
                srcField.setAccessible(true);
                for (Field targetField : targetFields) {
                    targetField.setAccessible(true);

                    String fieldSrcName = srcField.getName().toLowerCase();

                    String fieldTargetName = targetField.getName().toLowerCase();

                    if (fieldSrcName.equalsIgnoreCase(fieldTargetName)) {
                        Object value = srcField.get(src);
                        if (value == null) {
                            continue;
                        }
                        targetField.set(target, value);
                    }
                }
            }
        } catch (Exception ex) {
        }
        return (T) target;
    }
}
