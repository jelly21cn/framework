package com.ct.framework.service.common.entity;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 分页实体
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class PagingInfoEntity {

    private String sortType;

    private String sortField;

    private int pageCurrent;

    private int pageSize;

    public String GetSortType() {
        return sortType;
    }

    public void SetSortType(String sortType) {
        this.sortType = sortType;
    }

    public String GetSortField() {
        return sortField;
    }

    public void SetSortField(String sortField) {
        this.sortField = sortField;
    }

    public int GetPageCurrent() {
        int index = (this.pageCurrent - 1) * this.pageSize;
        if (index < 0) {
            return 0;
        }
        return index;
    }

    public void SetPageCurrent(int pageCurrent) {
        this.pageCurrent = pageCurrent;
    }

    public int GetPageSize() {
        return pageSize;
    }

    public void SetPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
