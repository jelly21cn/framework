package com.ct.framework.service.common.entity;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 查询【结果】基类.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 * @param <T> 范型
 *****************************************************************/
public class QueryResultEntity<T> {

    /**
     * 泛型 返回列表.
     */
    private T resultList;

    /**
     * 总量.
     */
    private long totalCount;

    /**
     * 总量.
     *
     * @return T
     */
    public T getResultList() {
        return resultList;
    }

    /**
     * 设置列表.
     *
     * @param resultList 列表
     */
    public void setResultList(T resultList) {
        this.resultList = resultList;
    }

    /**
     * 读取总量.
     *
     * @return long
     */
    public long getTotalCount() {
        return totalCount;
    }

    /**
     * 设置总量.
     *
     * @param totalCount 总量
     */
    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }
}
