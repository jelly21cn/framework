package com.ct.framework.service.common.entity;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 上下文信息
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class MessageHeaderEntity {

    /**
     * 来源系统
     */
    private String fromSystem;

    /**
     * 目标系统
     */
    private String toSystem;

    private String displayName;
    /**
     * 登录名 用户名
     */
    private String loginName;

    /**
     * 组织
     */
    private Integer organizationId;
    /**
     * 多租户时使用
     */
    private String companyCode;

    private Integer userSysNo;

    private Integer groupId;

    public String GetFromSystem() {
        return fromSystem;
    }

    public void SetFromSystem(String fromSystem) {
        this.fromSystem = fromSystem;
    }

    public String GetToSystem() {
        return toSystem;
    }

    public void SetToSystem(String toSystem) {
        this.toSystem = toSystem;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void SetDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String GetLoginName() {
        return loginName;
    }

    public void SetLoginName(String loginName) {
        this.loginName = loginName;
    }

    public Integer GetOrganizationId() {
        return organizationId;
    }

    public void SetOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    public String GetCompanyCode() {
        return companyCode;
    }

    public void SetCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public Integer GetUserSysNo() {
        return userSysNo;
    }

    public void SetUserSysNo(Integer userSysNo) {
        this.userSysNo = userSysNo;
    }

    public Integer GetGroupId() {
        return groupId;
    }

    public void SetGroupId(Integer groupId) {
        this.groupId = groupId;
    }
}
