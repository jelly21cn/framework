package com.ct.framework.service.common.transform;

import com.ct.framework.contract.DefaultQueryContract;
import com.ct.framework.contract.IQueryContract;
import com.ct.framework.service.common.entity.QueryEntityBase;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: OTO 查询基类，用于服务端
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 * @param <Criterias> 查询BO
 * @param <TQueryEntity> 查询VO
 * @param <TQueryData> 查询PO
 *****************************************************************/
public abstract class TransformQueryBase<Criterias extends DefaultQueryContract & IQueryContract<TQueryData>, TQueryData, TQueryEntity extends QueryEntityBase> {

    /**
     * 查询条件转换.
     *
     * @param queryContract 查询BO
     * @return TQueryEntity
     */
    public TQueryEntity ToQueryEntity(Criterias queryContract) {

        Type superclass = getClass().getGenericSuperclass();

        ParameterizedType parameterizedType = (ParameterizedType) superclass;

        Type[] typeArray = parameterizedType.getActualTypeArguments();

        Class clazz = (Class) typeArray[2];

        TQueryEntity tQueryEntity = null;

        try {
            tQueryEntity = (TQueryEntity) clazz.newInstance();
            tQueryEntity = Transform.CopyFields(queryContract.GetBody(), tQueryEntity);
            tQueryEntity.SetPageInfo(PagingTransform.ToEntity(queryContract.GetPagingInfo()));
            tQueryEntity.SetHeaderEntity(HeaderTransform.ToEntity(queryContract.GetHeader()));
        } catch (Exception ex) {
        }
        return tQueryEntity;
    }
}
