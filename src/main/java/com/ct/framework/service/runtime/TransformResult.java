package com.ct.framework.service.runtime;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Deprecated
public class TransformResult {
    /**
     * 名称.
     */
    private String name;
    /**
     * 描述.
     */
    private String description;

    /**
     * 获取名称.
     *
     * @return T
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称.
     *
     * @param nm nm
     */
    public void setName(String nm) {
        this.name = nm;
    }

    /**
     * 返回描述.
     *
     * @return String
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置描述.
     *
     * @param desc desc
     */
    public void setDescription(String desc) {
        this.description = desc;
    }
}
