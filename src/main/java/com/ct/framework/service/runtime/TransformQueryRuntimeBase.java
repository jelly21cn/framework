package com.ct.framework.service.runtime;

import com.ct.framework.service.common.transform.Transform;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: OTO 查询基类..
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 * @param <TQueryData> 查询BO
 * @param <TQueryEntity>  查询PO
 *****************************************************************/
@Deprecated
public abstract class TransformQueryRuntimeBase<TQueryData, TQueryEntity> {

    /**
     * 查询条件转换.
     *
     * @param tQueryData 查询BO
     * @return TQueryEntity
     */
    public TQueryEntity ToQueryEntity(TQueryData tQueryData) {

        Type superclass = getClass().getGenericSuperclass();

        ParameterizedType parameterizedType = (ParameterizedType) superclass;

        Type[] typeArray = parameterizedType.getActualTypeArguments();

        Class clazz = (Class) typeArray[0];

        TQueryEntity tQueryEntity = null;

        try {
            tQueryEntity = Transform.CopyFields(tQueryData, clazz.newInstance());
        } catch (Exception ex) {
        }

        return tQueryEntity;
    }
}
