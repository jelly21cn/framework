package com.ct.framework.service.runtime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 操作枚举.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@ApiModel("动作枚举")
public enum OperationType {
    /**
     * 查询.
     */
    @ApiModelProperty("分页")
    Paging,
    /**
     * 查询.
     */
    @ApiModelProperty("查询")
    Search,
    /**
     * 创建.
     */
    @ApiModelProperty("创建")
    Create,
    /**
     * 更新.
     */
    @ApiModelProperty("更新")
    Update,
    /**
     * 保存.
     */
    @ApiModelProperty("保存")
    Save,
    /**
     * 删除.
     */
    @ApiModelProperty("删除")
    Delete
}
