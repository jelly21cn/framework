package com.ct.framework.service.runtime;

import com.ct.framework.contract.*;

import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: VO与BO的转换基类.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 * @param <ViewEntity> VO
 * @param <DataEntity> PO
 *****************************************************************/
public interface ITransform<DataEntity, ViewEntity> {

    /**
     * 展示对象 从BO转换为VO.
     *
     * @param contract viewObject
     * @param <T>      范型
     * @return DataViewObject
     */
    <T extends IDataContract<DataEntity>> DataViewObject<ViewEntity> ToDataViewObject(T contract);

    /**
     * 展示对象 从VO转换为BO.
     *
     * @param viewObject viewObject
     * @param <T>        范型
     * @return T
     */
    <T extends DefaultDataContract & IDataContract<DataEntity>> T ToDataContract(DataViewObject<ViewEntity> viewObject);

    /**
     * 展示对象 从VO转换为BO.
     *
     * @param viewObject viewObject
     * @param <T>        范型
     * @return T
     */
    <T extends DefaultQueryContract & IQueryContract<DataEntity>> T ToQueryContract(QueryViewObject<ViewEntity> viewObject);

    /**
     * 查询列表 从BO转换为VO.
     *
     * @param contract contract
     * @return 返回查询VO对象
     */
    QueryViewObject<List<ViewEntity>> ToQueryViewObject(QueryResultContract<List<DataEntity>> contract);

    /**
     * 展示对象 List<VO>转换为List<BO>.
     *
     * @param <T>      范型
     * @param viewList VO列表
     * @return T
     */
    <T extends DefaultDataContract & IDataContract<List<DataEntity>>> T ToDataContracts(DataViewObject<List<ViewEntity>> viewList);

    /**
     * 展示对象 List<BO>转换为List<VO>.
     *
     * @param contract BO
     * @param <T>      范型
     * @return T
     */
    <T extends IDataContract<List<DataEntity>>> DataViewObject<List<ViewEntity>> ToDataViewObjects(T contract);
}
