package com.ct.framework.service.runtime;

import com.ct.framework.contract.SortType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 分页信息.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Data
@ApiModel("分页信息")
public class PageInfo {
    /**
     * 总数量.
     */
    @ApiModelProperty("总数量")
    private long totalCount;
    /**
     * 当前页.
     */
    @ApiModelProperty("当前页")
    private int pageIndex;
    /**
     * 每页数量.
     */
    @ApiModelProperty("每页数量")
    private int pageSize;
    /**
     * 排序字段.
     */
    @ApiModelProperty("排序字段")
    private String sortField;
    /**
     * 排序类型.
     */
    @ApiModelProperty("排序类型")
    private SortType sortType;
}
