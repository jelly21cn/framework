package com.ct.framework.service.runtime;

import com.ct.framework.contract.*;
import com.ct.framework.utility.string.PatternHelper;
import com.google.gson.internal.LinkedTreeMap;
import org.apache.tomcat.util.collections.CaseInsensitiveKeyMap;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 展示层OTO基类,用于客户端
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 * @param <Contract> 展示契约BO
 * @param <Criteria> 查询契约BO
 * @param <DataEntity> 数据实体PO
 * @param <ViewEntity> 展示实体VO
 *****************************************************************/
public abstract class TransformRuntimeBase<Criteria, Contract, DataEntity, ViewEntity> implements ITransform<DataEntity, ViewEntity> {

    /**
     * 3.
     */
    private static final int THREE = 3;

    /**
     * 时间格式.
     */
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * BO转VO.
     *
     * @param <T>      范型
     * @param contract BO
     */
    @Override
    public <T extends IDataContract<DataEntity>> DataViewObject<ViewEntity> ToDataViewObject(T contract) {
        DataViewObject<ViewEntity> result = new DataViewObject<>();
        result.setBody(this.ToViewEntity(contract.GetBody()));

        DefaultDataContract dataContract = (DefaultDataContract) contract;

        result.setHasError(((DefaultDataContract) contract).GetFaults() != null && ((DefaultDataContract) contract).GetFaults().size() > 0);

        if (result.isHasError()) {
            result.setErrorCode(dataContract.GetFaults().get(0).getErrorCode());
            result.setNotifyMessage(dataContract.GetFaults().get(0).getErrorDescription());
        }

        return result;
    }

    /**
     * VO转BO.
     *
     * @param <T>        范型
     * @param viewObject VO
     * @return T
     */
    @Override
    public <T extends DefaultDataContract & IDataContract<DataEntity>> T ToDataContract(DataViewObject<ViewEntity> viewObject) {

        Type superclass = getClass().getGenericSuperclass();

        ParameterizedType parameterizedType = (ParameterizedType) superclass;

        Type[] typeArray = parameterizedType.getActualTypeArguments();

        Class clazz = (Class) typeArray[1];

        T t = null;

        try {
            t = (T) clazz.newInstance();

            t.SetBody(this.ToDataEntity(viewObject.getBody()));

            t.SetHeader(this.BuildContractHeader(viewObject.getHeader()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return t;
    }

    /**
     * 查询VO转查询BO.
     *
     * @param <T>        范型
     * @param viewObject 查询VO
     * @return T
     */
    @Override
    public <T extends DefaultQueryContract & IQueryContract<DataEntity>> T ToQueryContract(QueryViewObject<ViewEntity> viewObject) {
        Type superclass = getClass().getGenericSuperclass();

        ParameterizedType parameterizedType = (ParameterizedType) superclass;

        Type[] typeArray = parameterizedType.getActualTypeArguments();

        Class clazz = (Class) typeArray[0];

        T t = null;

        try {
            t = (T) clazz.newInstance();
            t.SetBody(this.ToDataEntity(viewObject.getBody()));
            t.SetHeader(this.BuildContractHeader(viewObject.getHeader()));
            t.SetPagingInfo(this.ToContractPaging(viewObject.getPagingInfo()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return t;
    }

    /**
     * 查询BO转查询VO.
     *
     * @param contract 查询VO
     * @return QueryViewObject<List < ViewEntity>>
     */
    @Override
    public QueryViewObject<List<ViewEntity>> ToQueryViewObject(QueryResultContract<List<DataEntity>> contract) {

        QueryViewObject<List<ViewEntity>> result = new QueryViewObject<>();
        result.setBody(this.ToViewEntityList(contract.GetResultList()));
        result.setPagingInfo(this.ToViewPaging(contract.GetPageInfo()));

        result.setHasError(contract.GetFaults() != null && contract.GetFaults().size() > 0);

        if (result.isHasError()) {
            result.setErrorCode(contract.GetFaults().get(0).getErrorCode());
            result.setNotifyMessage(contract.GetFaults().get(0).getErrorDescription());
        }

        return result;
    }

    /**
     * VO转BO（列表）.
     *
     * @param <T>      范型
     * @param viewList 查询VO
     * @return T
     */
    @Override
    public <T extends DefaultDataContract & IDataContract<List<DataEntity>>> T ToDataContracts(DataViewObject<List<ViewEntity>> viewList) {

        Type superclass = getClass().getGenericSuperclass();

        ParameterizedType parameterizedType = (ParameterizedType) superclass;

        Type[] typeArray = parameterizedType.getActualTypeArguments();

        Class clazz = (Class) typeArray[1];

        T t = null;

        try {
            t = (T) clazz.newInstance();

            List<DataEntity> list = new ArrayList<>();

            for (ViewEntity entity : viewList.getBody()) {
                list.add(this.ToDataEntity(entity));
            }

            t.SetBody(list);
            t.SetHeader(this.BuildContractHeader(viewList.getHeader()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return t;
    }

    /**
     * BO转VO（列表）.
     *
     * @param <T>      范型
     * @param contract 契约
     * @return DataViewObject
     */
    @Override
    public <T extends IDataContract<List<DataEntity>>> DataViewObject<List<ViewEntity>> ToDataViewObjects(T contract) {
        DataViewObject<List<ViewEntity>> result = new DataViewObject<>();
        List<ViewEntity> list = new ArrayList<>();
        if (contract.GetBody() != null) {
            for (DataEntity entity : contract.GetBody()) {
                list.add(this.ToViewEntity(entity));
            }
            result.setBody(list);
        }
        DefaultDataContract dataContract = (DefaultDataContract) contract;
        result.setHasError(((DefaultDataContract) contract).GetFaults() != null && ((DefaultDataContract) contract).GetFaults().size() > 0);
        if (result.isHasError()) {
            result.setErrorCode(dataContract.GetFaults().get(0).getErrorCode());
            result.setNotifyMessage(dataContract.GetFaults().get(0).getErrorDescription());
        }
        return result;
    }

    //region PRIVATE FUNCTION

    /**
     * PO转VO.
     *
     * @param dataEntity PO
     * @return ViewEntity
     */
    private ViewEntity ToViewEntity(DataEntity dataEntity) {
        Type superclass = getClass().getGenericSuperclass();
        ParameterizedType parameterizedType = (ParameterizedType) superclass;
        Type[] typeArray = parameterizedType.getActualTypeArguments();
        Class dataEntityType = (Class) typeArray[2];
        Class viewEntityType = (Class) typeArray[THREE];

        return (ViewEntity) this.Transform(dataEntityType, viewEntityType, dataEntity);
    }

    /**
     * VO转PO.
     *
     * @param viewObject VO
     * @return DataEntity
     */
    private DataEntity ToDataEntity(ViewEntity viewObject) {
        Type superclass = getClass().getGenericSuperclass();
        ParameterizedType parameterizedType = (ParameterizedType) superclass;
        Type[] typeArray = parameterizedType.getActualTypeArguments();
        Class dataEntityType = (Class) typeArray[2];
        Class viewEntityType = (Class) typeArray[THREE];

        return (DataEntity) this.Transform(viewEntityType, dataEntityType, viewObject);
    }

    /**
     * VO的分页转Bo分页.
     *
     * @param viewPage viewPage
     * @return PagingInfo
     */
    private PagingInfo ToContractPaging(PageInfo viewPage) {
        if (viewPage == null) {
            return null;
        }
        PagingInfo result = new PagingInfo();

        result.SetPageSize(viewPage.getPageSize());
        result.SetSortField(viewPage.getSortField());
        result.SetSortType(viewPage.getSortType());
        result.SetStartRowIndex(viewPage.getPageIndex());

        return result;
    }

    /**
     * 类型转换.
     *
     * @param source     对象
     * @param sourceType 来源类型
     * @param targetType 目标类型
     * @return object
     */
    private Object Transform(Class sourceType, Class targetType, Object source) {

        if (source == null) {
            return null;
        }

        boolean isMap = false;

        CaseInsensitiveKeyMap caseInsensitiveKeyMap = new CaseInsensitiveKeyMap();

        if (source instanceof LinkedTreeMap) {
            isMap = true;

            Map<String, Object> map = (Map<String, Object>) source;

            for (Map.Entry<String, Object> entry : map.entrySet()) {
                caseInsensitiveKeyMap.put(entry.getKey(), entry.getValue());
            }
        }

        Field[] sourceFields = sourceType.getDeclaredFields();
        Field[] targetFields = targetType.getDeclaredFields();
        Object result = null;
        try {
            result = targetType.newInstance();

            for (Field sf : sourceFields) {
                for (Field tf : targetFields) {
                    sf.setAccessible(true);
                    tf.setAccessible(true);
                    String sourceName = sf.getName();
                    String targetName = tf.getName();

                    if (sourceName.equalsIgnoreCase(targetName)) {

                        if (isMap) {

                            Object value = caseInsensitiveKeyMap.get(targetName);

                            if (value == null) {
                                tf.set(result, null);
                                continue;
                            }

                            String typeName = tf.getType().getName();

                            if (value instanceof Double) {

                                double dbNum = (double) value;
                                long lngNum = new Double(dbNum).longValue();

                                if (typeName.equalsIgnoreCase(Boolean.class.getTypeName())) {
                                    if (lngNum > 0) {
                                        tf.set(result, true);
                                    } else {
                                        tf.set(result, false);
                                    }
                                } else {
                                    //region int/double/long
                                    if (dbNum == lngNum) {

                                        //整数值
                                        if (lngNum > Integer.MAX_VALUE) {
                                            if (typeName.equalsIgnoreCase(Double.class.getTypeName())) {
                                                Double db = new Double(dbNum);
                                                tf.set(result, db);
                                            } else if (typeName.equalsIgnoreCase(Long.class.getTypeName())) {
                                                Long lg = new Long(new Double(dbNum).longValue());
                                                tf.set(result, lg);
                                            } else {
                                                tf.set(result, null);
                                            }
                                        } else {
                                            if (typeName.equalsIgnoreCase(Long.class.getTypeName())) {
                                                Long lg = new Long(new Double(dbNum).longValue());
                                                tf.set(result, lg);
                                            } else if (typeName.equalsIgnoreCase(Integer.class.getTypeName())) {
                                                Integer val = new Integer(new Double(dbNum).intValue());
                                                tf.set(result, val);
                                            } else {
                                                tf.set(result, null);
                                            }
                                        }


                                    } else {
                                        tf.set(result, dbNum);
                                    }
                                    //endregion
                                }
                            } else if (value instanceof Long) {
                                Long lg = (Long) value;
                                if (typeName.equalsIgnoreCase(Long.class.getTypeName())) {
                                    tf.set(result, lg);
                                } else if (typeName.equalsIgnoreCase(Integer.class.getTypeName())) {
                                    Integer val = lg.intValue();
                                    tf.set(result, val);
                                } else if (typeName.equalsIgnoreCase(String.class.getTypeName())) {
                                    tf.set(result, lg.toString());
                                }
                            } else if (value instanceof Boolean) {
                                if (typeName.equalsIgnoreCase(Boolean.class.getTypeName())) {
                                    Boolean val = (Boolean) value;
                                    tf.set(result, val);
                                }
                            } else if (value instanceof String) {
                                String str = value.toString();
                                if (PatternHelper.IsDateTime(str)) {
                                    tf.set(result, simpleDateFormat.parse(str));
                                } else if (PatternHelper.IsInt(str)) {
                                    if (typeName.equalsIgnoreCase(Long.class.getTypeName())) {
                                        Long lg = new Long(str);
                                        tf.set(result, lg);
                                    } else if (typeName.equalsIgnoreCase(Integer.class.getTypeName())) {
                                        Integer val = new Integer(str);
                                        tf.set(result, val);
                                    } else {
                                        if (str.length() > 0) {
                                            tf.set(result, str);
                                        } else {
                                            tf.set(result, null);
                                        }
                                    }
                                } else {
                                    if (str.length() > 0) {
                                        tf.set(result, str);
                                    } else {
                                        tf.set(result, null);
                                    }
                                }
                            } else {
                                tf.set(result, value);
                            }
                        } else {
                            Object value = sf.get(source);
                            tf.set(result, value);
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * 创建BO的头部.
     *
     * @param headerView headerView
     * @return MessageHeader
     */
    private MessageHeader BuildContractHeader(MessageHeaderView headerView) {
        if (headerView == null) {
            return null;
        }

        MessageHeader msgHeader = new MessageHeader();

        msgHeader.SetLanguage(headerView.getLanguage());
        msgHeader.SetFromSystem(headerView.getFromSystem());
        msgHeader.SetAction(headerView.getOperation().name());
        msgHeader.SetSender(headerView.getSender());

        OperationUser operationUser = new OperationUser();

        operationUser.SetGroupId(headerView.getOperationUser().getGroupId());
        operationUser.SetOrganizationId(headerView.getOperationUser().getOrganizationId());
        operationUser.SetLoginName(headerView.getOperationUser().getLoginName());
        operationUser.SetDisplayName(headerView.getOperationUser().getDisplayName());
        operationUser.SetCompanyCode(headerView.getOperationUser().getCompanyCode());
        operationUser.SetUserSysNo(headerView.getOperationUser().getUserSysNo());

        String format = "{0}-{1}-{2}[{3}]";

        //this.uniqueUserName = String.format("{0}\\{1}\\{2}[{3}]", this.loginName, this.groupId, this.organizationId, this.companyCode);
        operationUser.SetUniqueUserName(MessageFormat.format(format,
                headerView.getOperationUser().getLoginName(),
                headerView.getOperationUser().getGroupId(),
                headerView.getOperationUser().getOrganizationId(),
                headerView.getOperationUser().getCompanyCode()));

        operationUser.SetUserSysNo(headerView.getOperationUser().getUserSysNo());

        msgHeader.SetOperationUser(operationUser);

        return msgHeader;
    }

    /**
     * List<PO>转List<VO>.
     *
     * @param dataEntityList dataEntityList
     * @return List
     */
    private List<ViewEntity> ToViewEntityList(List<DataEntity> dataEntityList) {
        if (dataEntityList == null) {
            return null;
        }

        List<ViewEntity> result = new ArrayList<>();

        for (DataEntity dataEntity : dataEntityList) {
            ViewEntity viewEntity = this.ToViewEntity(dataEntity);
            result.add(viewEntity);
        }

        return result;
    }

    /**
     * 转换为PageInfo.
     *
     * @param paging paging
     * @return PageInfo
     */
    private PageInfo ToViewPaging(PagingInfo paging) {
        PageInfo pageInfo = new PageInfo();
        pageInfo.setPageSize(paging.GetPageSize());
        pageInfo.setPageSize(paging.GetStartRowIndex());
        pageInfo.setSortField(paging.GetSortField());
        pageInfo.setSortType(paging.GetSortType());
        pageInfo.setTotalCount(paging.GetTotalCount());
        return pageInfo;
    }
    //endregion
}
