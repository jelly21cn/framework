package com.ct.framework.service.runtime;

import com.ct.framework.utility.SerializeHelper;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 展示层 - VO基类.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Data
@ApiModel("展示层 - VO基类")
public abstract class ViewObjectBase implements Serializable {

    /**
     * 是否报错.
     */
    @ApiModelProperty("是否报错")
    private boolean hasError;
    /**
     * 错误信息.
     */
    @ApiModelProperty("错误信息")
    private String notifyMessage;
    /**
     * 错误码
     */
    @ApiModelProperty("错误码")
    private String errorCode;
    /**
     * 头部.
     */
    @ApiModelProperty("头部信息体")
    private MessageHeaderView header;

    /**
     * 初始化.
     *
     * @param viewObject viewObject
     */
    private static void Init(ViewObjectBase viewObject) {
        /*
         * OperationUserInfo operationUserInfo = UserContext.GetUserInfo().get();
         * MessageHeaderView messageHeaderView = new MessageHeaderView();
         * messageHeaderView.SetFromSystem(CPContext.Current.GetRequestClientIP());
         * messageHeaderView.SetSender(SystemConfigurationManager.getSingleton().GetRegionName());
         * messageHeaderView.SetOperationUser(viewObject.GetHeader().GetOperationUser());
         * messageHeaderView.SetLanguage(viewObject.GetHeader().GetLanguage());
         * viewObject.SetHeader(messageHeaderView);
         */
    }

    /**
     * 创建对象.
     *
     * @param source source
     * @param <T>    范型
     * @param clazz  类型
     * @return T
     */
    public static <T extends ViewObjectBase> T Build(String source, Class<T> clazz) {
        T t = SerializeHelper.JsonDeserialize(source, clazz);
        Init(t);
        return t;
    }
}
