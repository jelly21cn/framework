package com.ct.framework.service.runtime;

import com.google.gson.internal.LinkedTreeMap;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 数据VO的通用类.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 * @param <Entity> PO
 *****************************************************************/
@Slf4j
@Data
@ApiModel("数据VO的通用类")
public class DataViewObject<Entity extends Object> extends ViewObjectBase {

    /**
     * body.
     */
    @ApiModelProperty("body")
    private Entity body;

    /**
     * 读取值.
     *
     * @param paramName paramName
     * @return object
     */
    public Object GetValue(String paramName) {
        if (body instanceof LinkedTreeMap) {
            return ((Map<?, ?>) body).get(paramName);
        }

        return null;
    }

    /**
     * 返回当前Entity.
     *
     * @param <T>   范型
     * @param clazz 类型
     * @return T
     **/
    public <T> T GetViewEntity(Class clazz) {
        LinkedTreeMap<String, Object> treeMap = (LinkedTreeMap) this.body;
        return this.ConvertToEntity(clazz, treeMap);
    }

    /**
     * 获取VO的列表.
     *
     * @param <T>   范型
     * @param clazz 类型
     * @return List
     */
    public <T> List<T> GetViewListEntity(Class clazz) {
        List<Object> arrayList = (ArrayList) this.body;
        List<T> list = new ArrayList<>();

        for (Object item : arrayList) {
            LinkedTreeMap<String, Object> treeMap = (LinkedTreeMap) item;
            T t = this.ConvertToEntity(clazz, treeMap);
            if (t != null) {
                list.add(t);
            }
        }

        return list;
    }

    /**
     * LinkedTreeMap转换为实体.
     *
     * @param <T>     范型
     * @param clazz   类型
     * @param treeMap treeMap
     * @return List
     */
    private <T> T ConvertToEntity(Class clazz, LinkedTreeMap<String, Object> treeMap) {
        T t = null;
        try {
            t = (T) clazz.newInstance();
            Field[] entityFields = clazz.getDeclaredFields();
            for (Map.Entry<String, Object> m : treeMap.entrySet()) {
                Object val = m.getValue();
                for (Field sf : entityFields) {
                    sf.setAccessible(true);
                    String k = m.getKey();
                    if (val == null) {
                        continue;
                    }
                    String fieldName = sf.getName();
                    if (k.equalsIgnoreCase(fieldName)) {
                        if (sf.getType().equals(Double.class)) {
                            sf.set(t, Double.parseDouble(val.toString()));
                        } else if (sf.getType().equals(String.class)) {
                            sf.set(t, val.toString());
                        } else if (sf.getType().equals(Long.class)) {
                            Long lg = null;
                            if (val instanceof String && StringUtils.isEmpty((CharSequence) val)) {
                                continue;
                            }
                            lg = new Long(new Double(val.toString()).longValue());
                            sf.set(t, lg);
                        } else if (sf.getType().equals(Boolean.class)) {

                            sf.set(t, Boolean.parseBoolean(val.toString()));
                        } else if (sf.getType().equals(Integer.class)) {
                            Integer in = null;
                            if (val instanceof String && StringUtils.isEmpty((CharSequence) val)) {
                                continue;
                            }
                            in = new Integer(new Double(val.toString()).intValue());
                            sf.set(t, in);
                        } else if (sf.getType().equals(Date.class)) {
                            String dateString = val.toString();
                            Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateString);
                            sf.set(t, parse);
                        } else if (sf.getType().equals(Map.class)) {
                            //增加LinkedTreeMap的解析
                            LinkedTreeMap linkedTreeMap = (LinkedTreeMap) val;
                            sf.set(t, linkedTreeMap);
                        }
                    }

                }
            }
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }

        return t;
    }

}
