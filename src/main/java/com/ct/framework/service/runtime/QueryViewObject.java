package com.ct.framework.service.runtime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 查询实体的基类.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 * @param <Entity> PO
 *****************************************************************/
@ApiModel("查询实体的基类")
@Data
public class QueryViewObject<Entity extends Object> extends DataViewObject<Entity> {

    /**
     * 分页.
     */
    @ApiModelProperty("分页实体")
    private PageInfo pagingInfo;
}
