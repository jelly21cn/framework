package com.ct.framework.service.runtime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 客户端的通信头部实体.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Data
@ApiModel("客户端的通信头部实体")
public class MessageHeaderView {
    /**
     * 语言.
     */
    @ApiModelProperty("语言")
    private String language;
    /**
     * 操作用户信息.
     */
    @ApiModelProperty("操作用户信息")
    private OperationUserInfo operationUser;
    /**
     * 发送者.
     */
    @ApiModelProperty("发送者")
    private String sender;
    /**
     * 来源系统.
     */
    @ApiModelProperty("来源系统")
    private String fromSystem;

    /**
     * 操作类型.
     */
    @ApiModelProperty("操作类型")
    private OperationType operation;
}
