package com.ct.framework.service.runtime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 通讯时的用户信息.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Data
@ApiModel("通讯时的用户信息")
public class OperationUserInfo {
    /**
     * 中文名.
     */
    @ApiModelProperty("中文名")
    private String displayName;
    /**
     * 登录名 用户名.
     */
    @ApiModelProperty("登录名")
    private String loginName;
    /**
     * 组织.
     */
    @ApiModelProperty("组织")
    private Integer organizationId;
    /**
     * 多租户时使用.
     */
    @ApiModelProperty("多租户时使用")
    private String companyCode;
    /**
     * 登录用户的主键.
     */
    @ApiModelProperty("登录用户的主键")
    private Integer userSysNo;
    /**
     * token存入时间戳.
     */
    @ApiModelProperty("token存入时间戳")
    private Long tokenTime;
    /**
     * 状态.
     */
    @ApiModelProperty("状态")
    private String status;
    /**
     * 分组ID.
     */
    @ApiModelProperty("分组ID")
    private Integer groupId;
    /**
     * 是否超级管理员
     */
    @ApiModelProperty("是否超级管理员")
    private Boolean isAdmin;
}
