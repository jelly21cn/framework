package com.ct.framework.service.console;

import com.ct.framework.service.broker.entity.ServiceLocationEntity;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: ServiceBrokerUtil.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public final class ServiceBrokerUtil {

    private ServiceBrokerUtil() {
    }

    /**
     * 通过类型绑定RPC.
     *
     * @param serviceLocationEntity 服务地址
     * @return Registry
     */
    public static Registry BuildBindingByType(ServiceLocationEntity serviceLocationEntity) {
        Registry registry = null;
        try {
            registry = LocateRegistry.getRegistry(serviceLocationEntity.getServiceHost(), Integer.parseInt(serviceLocationEntity.getServicePort()));
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return registry;
    }
}
