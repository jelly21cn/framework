package com.ct.framework.service.console;

import com.ct.framework.rpc.remoting.invoker.reference.RpcReferenceBean;
import com.ct.framework.core.exception.ErrorCode;
import com.ct.framework.core.exception.GlobalException;
import com.ct.framework.service.broker.ServiceBrokerHelper;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 发现服务.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public final class CommonServiceBroker {

    private CommonServiceBroker() {
    }

    /**
     * 查找服务.
     *
     * @param clazz 类型
     * @param <T>   范型
     * @return T
     */
    public static <T> T FindService(Class clazz) {
        Registry registry = ServiceBrokerHelper.CheckConfig(clazz);
        T t = null;
        try {
            t = (T) registry.lookup(clazz.getName());
        } catch (RemoteException e) {
            throw new GlobalException(e.getMessage(), ErrorCode.NET_ECONNREFUSED);
        } catch (NotBoundException e) {
            e.printStackTrace();
        }

        return t;
    }

    /**
     * 查找RPC服务.
     *
     * @param clazz 类型
     * @param <T>   范型
     * @return T
     */
    public static <T> T FindRpcService(Class clazz) {
        RpcReferenceBean rpcReferenceBean = ServiceBrokerHelper.CheckRpcConfig(clazz);
        T t = null;
        try {
            t = (T) rpcReferenceBean.getObject();
        } catch (Exception e) {
            throw new GlobalException(e.getMessage(), ErrorCode.NET_ECONNREFUSED);
        }

        return t;
    }
}
