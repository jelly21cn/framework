package com.ct.framework.service.console;

import com.ct.framework.core.configuration.SystemConfigurationManager;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 发现服务.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public final class ServiceBroker {

    private ServiceBroker() {
    }

    /**
     * 查找服务.
     *
     * @param <T>   范型
     * @param clazz 类型
     * @return T
     */
    public static <T> T FindService(Class clazz) {
        if (!SystemConfigurationManager.getSingleton().GetRpcState()) {
            // TODO 启用非负载
            return CommonServiceBroker.FindService(clazz);
        } else {
            //启用负载
            return CommonServiceBroker.FindRpcService(clazz);
        }
    }
}
