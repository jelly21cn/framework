package com.ct.framework.core.query;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 队列实体
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class QueryEntity {

    private String exchange;
    private String exchangeType;
    private String routingKey;
    private String queue;
    private String message;
    private String tag;

    public String GetExchange() {
        return exchange;
    }

    public void SetExchange(String exchange) {
        this.exchange = exchange;
    }

    public String GetExchangeType() {
        return exchangeType;
    }

    public void SetExchangeType(String exchangeType) {
        this.exchangeType = exchangeType;
    }

    public String GetRoutingKey() {
        return routingKey;
    }

    public void SetRoutingKey(String routingKey) {
        this.routingKey = routingKey;
    }

    public String GetQueue() {
        return queue;
    }

    public void SetQueue(String queue) {
        this.queue = queue;
    }

    public String GetMessage() {
        return message;
    }

    public void SetMessage(String message) {
        this.message = message;
    }

    public String GetTag() {
        return tag;
    }

    public void SetTag(String tag) {
        this.tag = tag;
    }
}
