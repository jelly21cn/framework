package com.ct.framework.core.query;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 消息队列
 * 例如 RabbitMQ,ActiveMQ
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public abstract class MessageQuery implements IQuery {
    /**
     * 消息确认
     */
    public abstract void Commit();
}
