package com.ct.framework.core.query;

import java.util.concurrent.Callable;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 队列接口
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public interface IQuery {
    /**
     * 单个.
     */
    void Send(QueryEntity entity);

    /**
     * 处理一条.
     */
    void Receive(Callable<Boolean> func, String queue);

    /**
     * 订阅.
     */
    void Subscribe(Callable<Boolean> func, String queue);
}
