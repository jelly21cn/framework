package com.ct.framework.core.cache;

import com.ct.framework.cache.RedisProvider;
import com.ct.framework.core.configuration.SystemConfigurationManager;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 缓存工厂
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class CacherFactory {

    private static final Map<String, ICache> cacheProviders = new ConcurrentHashMap<>();

    private static Lock lock = new ReentrantLock();

    private static String REDIS = "Redis";
    private static String MEMCACHE = "Memcache";

    private static synchronized ICache GetCacheProvider(String providerName) {

//        lock.tryLock();
        try {
            if (!cacheProviders.containsKey(providerName)) {
                if (REDIS.equalsIgnoreCase(providerName)) {
                    ICache iCacher = new RedisProvider();
                    cacheProviders.put(providerName, iCacher);
                }
            }
        } catch (Exception e) {
            System.out.println("com.ct.framework.core.cache.CacherFactory.GetCacheProvider");
        }
//        lock.unlock();
        return cacheProviders.get(providerName);
    }

    public static ICache DistributedCache() {
        return GetCacheProvider(SystemConfigurationManager.getSingleton().GetCacheManager());
    }
}
