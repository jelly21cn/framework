package com.ct.framework.core.cache;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 多种缓存机制接口
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public interface ICache {

    /**
     * 添加.
     *
     * @param key   key
     * @param value value
     * @return boolean
     */
    boolean Add(String key, String value);

    /**
     * 按时间戳写缓存.
     *
     * @param key               key
     * @param value             value
     * @param slidingExpiration 时间戳
     * @return boolean
     */
    boolean Add(String key, String value, long slidingExpiration);

    /**
     * 按时间戳和分类写缓存.
     *
     * @param key               key
     * @param value             value
     * @param slidingExpiration 时间戳
     * @param groupName         分类
     * @return boolean
     */
    boolean Add(String key, String value, long slidingExpiration, String groupName);

    /**
     * 范型读取缓存.
     *
     * @param key key
     * @return Object
     */
    String GetData(String key);

    /**
     * 范型读取缓存.
     *
     * @param key       key
     * @param groupName 分类
     * @return Object
     */
    String GetData(String key, String groupName);

    /**
     * 通过key移除缓存.
     *
     * @param key key
     * @return boolean
     */
    Boolean Remove(String key);
}
