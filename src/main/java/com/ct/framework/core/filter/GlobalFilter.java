package com.ct.framework.core.filter;

import com.ct.framework.contract.MessageFaultCollection;
import com.ct.framework.core.configuration.HttpContext;
import com.ct.framework.service.runtime.DataViewObject;
import com.ct.framework.utility.SerializeHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  2021-09-08
 * Usage: 全局过滤器
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-09-08              xxx                      业务领域
 * @author xxx
 *****************************************************************/
@Slf4j
public abstract class GlobalFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws ServletException, IOException {
        if (!(servletRequest instanceof HttpServletRequest)) {
            throw new ServletException(servletRequest + " not HttpServletRequest");
        }
        if (!(servletResponse instanceof HttpServletResponse)) {
            throw new ServletException(servletResponse + " not HttpServletResponse");
        }

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        //全局请求与响应
        HttpContext.Current.SetHttpRequest(request);
        HttpContext.Current.SetHttpResponse(response);

        //忽略的路径，例如swagger等
        boolean ignore = IgnoreHandle(request.getRequestURI());
        if (ignore) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        //判断鉴权
        MessageFaultCollection messageFaults = PreHandle(request, response);
        if (CollectionUtils.isNotEmpty(messageFaults)) {
            ResponseToClient(response, messageFaults.get(0).getErrorDescription(), messageFaults.get(0).getErrorCode());
            return;
        }

        //处理业务逻辑
        boolean interceptor = ControllerHandle(request, response);
        if (!interceptor) {
            return;
        }

        //结束
        AfterCompletion(request, response);
    }

    /**
     * 忽略的路径，例如swagger等.
     */
    public abstract boolean IgnoreHandle(String url);

    /*
     * 预处理
     * controller执行之前
     * 一般用于鉴权
     */
    public abstract MessageFaultCollection PreHandle(HttpServletRequest request, HttpServletResponse response);

    /*
     * 处理controller
     */
    public abstract boolean ControllerHandle(HttpServletRequest request, HttpServletResponse response);

    /*
     * 页面渲染后
     */
    public abstract boolean AfterCompletion(HttpServletRequest request, HttpServletResponse response);

    /**
     * 通过 url 定位 method
     *
     * @param c          对应 Controller
     * @param methodName 方法名
     * @param request    用于获取 url 及相应参数 list
     * @param values     参数值的引用，必须是有序的
     * @return Method
     */
    protected Method GetMethodByUrlParameters(Class<?> c, String methodName, HttpServletRequest request, LinkedList<String> values) {
        Method realAction = null;
        // 获取所有参数
        Enumeration<String> names = request.getParameterNames();
        // url key value
        Map<String, String> urlParamsMap = new LinkedHashMap<>();
        // 遍历参数列表
        while (names.hasMoreElements()) {
            String name = names.nextElement();
            String value = request.getParameter(name);
            urlParamsMap.put(name, value);
        }
        // 当前实体的所有方法
        Method[] methods = c.getDeclaredMethods();
        for (Method method : methods) {
            // 方法名不匹配直接 pass
            if (!method.getName().equalsIgnoreCase(methodName)) {
                continue;
            }
            // method key
            List<String> methodKeyList = new LinkedList<>();
            // 方法参数的顺序
            Parameter[] parameters = method.getParameters();
            for (Parameter p : parameters) {
                // 如果请求里参数名有匹配
                if (urlParamsMap.get(p.getName()) != null) {
                    // methodKeyList 里的参数是有序的
                    methodKeyList.add(p.getName());
                    values.add(urlParamsMap.get(p.getName()));
                }
            }
            // url 中匹配的参数  = 方法的参数
            if (methodKeyList.size() == parameters.length) {
                realAction = method;
                break;
            } else {
                values.clear();
            }
        }
        return realAction;
    }

    /**
     * 通过 url 定位 pub方法
     *
     * @param c          对应 Controller
     * @param methodName 方法名
     * @return Method
     */
    protected Method GetMethodByUrlParameters(Class<?> c, String methodName) {
        Method realAction = null;
        // 当前实体的所有方法
        Method[] methods = c.getDeclaredMethods();
        for (Method method : methods) {
            // 方法名不匹配直接 pass
            if (!method.getName().equalsIgnoreCase(methodName)) {
                continue;
            }

            realAction = method;
            break;
        }
        return realAction;
    }

    /**
     * 输出到浏览器.
     */
    protected static void ResponseToClient(HttpServletResponse response, String msg, String code) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.setContentType("application/json; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);

        DataViewObject<String> responseEntity = new DataViewObject<>();

        responseEntity.setHasError(true);
        responseEntity.setErrorCode(code);
        responseEntity.setNotifyMessage(msg);

        try {
            response.getWriter().write(SerializeHelper.JsonSerializer(responseEntity, false));
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }
    }
}
