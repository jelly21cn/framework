package com.ct.framework.core.entity;

import lombok.Data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 数据库元数据
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Data
public class MetaData {
    /**
     * table catalog (may be null).
     */
    private String tableCat;
    /**
     * table schema (may be null).
     */
    private String tableSchemaName;
    /**
     * table Name.
     */
    private String tableName;
    /**
     * column Name.
     */
    private String columnName;
    /**
     * SQL type from java.sql.Types.
     */
    private int dataType;
    /**
     * Data source dependent type name, for a UDT the type name is fully qualified.
     */
    private String dataTypeName;
    /**
     * table schema (may be null).
     */
    private int columnSize;
    /**
     * the number of fractional digits. Null is returned for data.
     * types where DECIMAL_DIGITS is not applicable.
     */
    private int decimalDigits;
    /**
     * Radix (typically either 10 or 2).
     */
    private int numPrecRadix;
    /**
     * is NULL allowed.
     */
    int nullAble;
    /**
     * comment describing column (may be null).
     */
    private String remarks;
    /**
     * default value for the column, which should be interpreted as.
     * a string when the value is enclosed in single quotes (may be null).
     */
    private String columnDef;
    /**
     * sqlDataType.
     */
    private int sqlDataType;
    /**
     * sqlDatetimeSub.
     */
    private int sqlDatetimeSub;
    /**
     * for char types the maximum number of bytes in the column.
     */
    private int charOctetLength;
    /**
     * index of column in table (starting at 1).
     */
    private int ordinalPosition;
    /**
     * ISO rules are used to determine the nullability for a column.
     * YES --- if the parameter can include NULLs;
     * NO --- if the parameter cannot include NULLs;
     * empty string --- if the nullability for the parameter is unknown.
     */
    private String isNullAble;
    /**
     * <p>Indicates whether this column is auto incremented.</p>
     * YES --- if the column is auto incremented.
     * NO --- if the column is not auto incremented.
     * empty string --- if it cannot be determined whether
     * the column is auto incremented parameter is unknown.
     */
    private String isAutoincrement;
}
