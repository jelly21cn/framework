package com.ct.framework.core.entity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 上下文
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class ContextEntity {
    //IP地址
    private String requestClientIP;
    private HttpServletRequest HttpRequest;
    private HttpServletResponse HttpResponse;

    public String GetRequestClientIP() {
        return requestClientIP;
    }

    public void SetRequestClientIP(String requestClientIP) {
        this.requestClientIP = requestClientIP;
    }

    public HttpServletRequest GetHttpRequest() {
        return HttpRequest;
    }

    public void SetHttpRequest(HttpServletRequest httpRequest) {
        HttpRequest = httpRequest;
    }

    public HttpServletResponse GetHttpResponse() {
        return HttpResponse;
    }

    public void SetHttpResponse(HttpServletResponse httpResponse) {
        HttpResponse = httpResponse;
    }
}
