package com.ct.framework.core.entity;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 自研Nacos的配置
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class RpcEntity {
    private int id;
    private String dataId;
    private String groupId;
    private String content;
    private String tenantId;
    private String type;

    public int GetId() {
        return id;
    }

    public void SetId(int id) {
        this.id = id;
    }

    public String GetDataId() {
        return dataId;
    }

    public void SetDataId(String dataId) {
        this.dataId = dataId;
    }

    public String GetGroupId() {
        return groupId;
    }

    public void SetGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String GetContent() {
        return content;
    }

    public void SetContent(String content) {
        this.content = content;
    }

    public String GetTenantId() {
        return tenantId;
    }

    public void SetTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String GetType() {
        return type;
    }

    public void SetType(String type) {
        this.type = type;
    }
}
