package com.ct.framework.core.entity;

public class RpcResponseEntity {
    private String code;
    private String msg;
    private RpcEntity content;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RpcEntity getContent() {
        return content;
    }

    public void setContent(RpcEntity content) {
        this.content = content;
    }
}
