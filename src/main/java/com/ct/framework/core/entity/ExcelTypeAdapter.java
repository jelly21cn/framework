package com.ct.framework.core.entity;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: Excel导入时的类型转换
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public abstract class ExcelTypeAdapter<T, S> {
    /**
     * 输入.
     *
     * @param source 需要被转换的原始值
     * @return 泛型 转换后的值
     */
    public abstract T read(S source);
}
