package com.ct.framework.core.entity;

import com.ct.framework.rpc.remoting.invoker.RpcInvokerFactory;

import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class ConfigurationEntity {

    /**
     * 负载均衡 Mysql跟踪.
     */
    private RpcInvokerFactory rpcInvokerFactory;

    /**
     * 数据源配置文件.
     * DbCommandFiles.xml
     */
    private String databaseListFile;

    /**
     * 应用名称.
     */
    private String regionName;

    /**
     * sql语句的汇总存放路径.
     * Database.xml
     */
    private String dataCommandFile;

    /**
     * 待扩展
     */
    private String commonConfigurationListFile;

    private String serviceBrokerConfigFile;

    private String serviceHostingEnvironment;

    private String serviceHost;

    private String servicePort;

    /**
     * 缓存引擎.
     */
    private String cacheManager;

    /**
     * 日志引擎.
     */
    private String logManager;

    /**
     * 队列引擎.
     */
    private String queryManager;

    /**
     * 文件上传方式Minio/Mongo.
     */
    private String filesManager;

    //文件上传的临时存放路径
    private String uploadPath;

    /**
     * redis 的IP.
     */
    private String redisIp;

    /**
     * redis 的端口.
     */
    private String redisPort;

    /**
     * redis 的密码.
     */
    private String redisPassword;

    /**
     * redis 的数据库.
     */
    private String redisDatabase;

    /**
     * 是否开启Token验证（用于鉴权）.
     */
    private Boolean tokenCheck;

    /**
     * 开否开启目录验证（用于鉴权）.
     */
    private Boolean menuCheck;

    /**
     * 验证码地址.
     */
    private String captchaUrl;

    /**
     * 忽略的地址.
     */
    private String ignore;

    /**
     * 拦截器.
     */
    private List<String> interceptUrls;

    /**
     * 负载均衡地址
     */
    private String rpcUrl;

    /**
     * 是否启用远程配置
     */
    private boolean rpcState;

    /**
     * 远程配置的查询条件
     */
    private String rpcCondition;
    /**
     * 定时任务中心的服务地址.
     */
    private String addresses;
    /**
     * 执行器的名称.
     */
    private String appName;
    /**
     * 执行器的端口.
     */
    private String taskPort;
    /**
     * 定时任务存放日志的路径.
     */
    private String logPath;

    public String GetDatabaseListFile() {
        return databaseListFile;
    }

    public void SetDatabaseListFile(String databaseListFile) {
        this.databaseListFile = databaseListFile;
    }

    public String GetRegionName() {
        return regionName;
    }

    public void SetRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String GetDataCommandFile() {
        return dataCommandFile;
    }

    public void SetDataCommandFile(String dataCommandFile) {
        this.dataCommandFile = dataCommandFile;
    }

    public String GetServiceBrokerConfigFile() {
        return serviceBrokerConfigFile;
    }

    public void SetServiceBrokerConfigFile(String serviceBrokerConfigFile) {
        this.serviceBrokerConfigFile = serviceBrokerConfigFile;
    }

    public String GetServiceHostingEnvironment() {
        return serviceHostingEnvironment;
    }

    public void SetServiceHostingEnvironment(String serviceHostingEnvironment) {
        this.serviceHostingEnvironment = serviceHostingEnvironment;
    }

    public String GetServiceHost() {
        return serviceHost;
    }

    public void SetServiceHost(String serviceHost) {
        this.serviceHost = serviceHost;
    }

    public String GetServicePort() {
        return servicePort;
    }

    public void SetServicePort(String servicePort) {
        this.servicePort = servicePort;
    }

    public String GetCacheManager() {
        return cacheManager;
    }

    public void SetCacheManager(String cacheManager) {
        this.cacheManager = cacheManager;
    }

    public String GetLogManager() {
        return logManager;
    }

    public void SetLogManager(String logManager) {
        this.logManager = logManager;
    }

    public String GetQueryManager() {
        return queryManager;
    }

    public void SetQueryManager(String queryManager) {
        this.queryManager = queryManager;
    }

    public String GetFilesManager() {
        return filesManager;
    }

    public void SetFilesManager(String filesManager) {
        this.filesManager = filesManager;
    }

    public String GetUploadPath() {
        return uploadPath;
    }

    public void SetUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public String GetRedisIp() {
        return redisIp;
    }

    public void SetRedisIp(String redisIp) {
        this.redisIp = redisIp;
    }

    public String GetRedisPort() {
        return redisPort;
    }

    public void SetRedisPort(String redisPort) {
        this.redisPort = redisPort;
    }

    public String GetRedisDatabase() {
        return redisDatabase;
    }

    public void SetRedisDatabase(String redisDatabase) {
        this.redisDatabase = redisDatabase;
    }

    public String GetRedisPassword() {
        return redisPassword;
    }

    public void SetRedisPassword(String redisPassword) {
        this.redisPassword = redisPassword;
    }

    public String GetCommonConfigurationListFile() {
        return commonConfigurationListFile;
    }

    public void SetCommonConfigurationListFile(String commonConfigurationListFile) {
        this.commonConfigurationListFile = commonConfigurationListFile;
    }

    public Boolean GetTokenCheck() {
        return tokenCheck;
    }

    public void SetTokenCheck(Boolean tokenCheck) {
        this.tokenCheck = tokenCheck;
    }

    public Boolean GetMenuCheck() {
        return menuCheck;
    }

    public void SetMenuCheck(Boolean menuCheck) {
        this.menuCheck = menuCheck;
    }

    public String GetCaptchaUrl() {
        return captchaUrl;
    }

    public void SetCaptchaUrl(String captchaUrl) {
        this.captchaUrl = captchaUrl;
    }

    public String GetIgnore() {
        return ignore;
    }

    public void SetIgnore(String ignore) {
        this.ignore = ignore;
    }

    public List<String> GetInterceptUrls() {
        return interceptUrls;
    }

    public void SetInterceptUrls(List<String> interceptUrls) {
        this.interceptUrls = interceptUrls;
    }

    public RpcInvokerFactory GetRpcInvokerFactory() {
        return rpcInvokerFactory;
    }

    public void SetRpcInvokerFactory(RpcInvokerFactory rpcInvokerFactory) {
        this.rpcInvokerFactory = rpcInvokerFactory;
    }

    public String GetRpcUrl() {
        return rpcUrl;
    }

    public void SetRpcUrl(String rpcUrl) {
        this.rpcUrl = rpcUrl;
    }

    public boolean GetRpcState() {
        return rpcState;
    }

    public void SetRpcState(boolean rpcState) {
        this.rpcState = rpcState;
    }

    public String GetRpcCondition() {
        return rpcCondition;
    }

    public void SetRpcCondition(String rpcCondition) {
        this.rpcCondition = rpcCondition;
    }

    public String GetAddresses() {
        return addresses;
    }

    public void SetAddresses(String addresses) {
        this.addresses = addresses;
    }

    public String GetAppName() {
        return appName;
    }

    public void SetAppName(String appname) {
        this.appName = appname;
    }

    public String GetTaskPort() {
        return taskPort;
    }

    public void SetTaskPort(String taskPort) {
        this.taskPort = taskPort;
    }

    public String GetLogPath() {
        return logPath;
    }

    public void SetLogPath(String logpath) {
        this.logPath = logpath;
    }
}
