package com.ct.framework.core.configuration;


import com.ct.framework.core.entity.ContextEntity;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: Http 上下文
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class HttpContext {

    public static ContextEntity Current;

    static {
        if (Current == null) {
            Current = new ContextEntity();
            Current.SetRequestClientIP("127.0.0.1");
        }
    }
}
