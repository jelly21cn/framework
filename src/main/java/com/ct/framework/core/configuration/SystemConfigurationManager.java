package com.ct.framework.core.configuration;

import com.ct.framework.rpc.registry.impl.RpcAdminRegister;
import com.ct.framework.rpc.remoting.invoker.RpcInvokerFactory;
import com.ct.framework.utility.ClassHelper;
import com.ct.framework.utility.SerializeHelper;
import com.ct.framework.utility.http.BasicHttpUtil;
import com.ct.framework.utility.string.StringHelper;
import com.ct.framework.utility.string.XmlHelper;
import com.ct.framework.core.annotation.Config;
import com.ct.framework.core.entity.ConfigurationEntity;
import com.ct.framework.core.entity.RpcResponseEntity;
import lombok.extern.slf4j.Slf4j;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage:读取系统配置
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *
 * 设计模式：单例模式
 *****************************************************************/
@Slf4j
public class SystemConfigurationManager {

    /**
     * 10.
     */
    private static final int TEN = 10 * 6;

    /**
     * 单例配置.
     */
    private static final ConfigurationEntity singleton = new ConfigurationEntity();

    private SystemConfigurationManager() {
    }

    /**
     * 饿汉式.
     */
    public static ConfigurationEntity getSingleton() {
        return singleton;
    }

    /**
     * 初始化.
     */
    public static void init() {
        log.info("framework Config 初始化 开始");

        CreateInstance();

        log.info("framework Config 应用名称 = {}", singleton.GetRegionName());

        log.info("framework Config 初始化 结束");
    }

    /**
     * 读取config.properties配置.
     */
    private static void CreateInstance() {

        NodeList rpcNodes = XmlHelper.GetNodeList("Rpc.xml");

        boolean readRemoteConfig = false;

        String rpcUrl = "", rpcCondition = "";

        if (rpcNodes != null && rpcNodes.getLength() > 0) {

            for (int i = 0; i < rpcNodes.getLength(); i++) {

                Node node = rpcNodes.item(i);
                String nodeName = node.getNodeName();
                if ("#text".equals(nodeName)) {
                    continue;
                }

                if ("#comment".equals(nodeName)) {
                    continue;
                }

                if ("url".equalsIgnoreCase(nodeName)) {
                    rpcUrl = node.getFirstChild().getNodeValue();
                } else if ("state".equalsIgnoreCase(nodeName)) {
                    readRemoteConfig = Boolean.parseBoolean(node.getFirstChild().getNodeValue());
                } else if ("condition".equalsIgnoreCase(nodeName)) {
                    rpcCondition = node.getFirstChild().getNodeValue();
                }

            }
        }
        NodeList configNodes;

        //读取远程系统配置/访问本地
        if (!readRemoteConfig) {
            //访问本地
            configNodes = XmlHelper.GetNodeList("SystemConfig.xml");

        } else {
            //访问远程
            configNodes = XmlHelper.GetNodeListByHttp(rpcUrl, rpcCondition);
        }

        //访问远程自定义用户配置
        try {
            if (StringHelper.isNotBlank(rpcUrl)) {
                //TODO
                LoadRpcConfigClasses("com.ct.xxx.broken.view.object.", rpcUrl);
            }
        } catch (Exception ex) {
            log.error("framework 读取用户自定义配置错误 ");
        }

        if (configNodes == null || configNodes.getLength() == 0) {
            log.error("framework 读取核心配置文件错误 ");
            return;
        }

        //解析文件
        LoadXmlFile(configNodes);

        //启用RPC
        if (readRemoteConfig) {

            singleton.SetRpcUrl(rpcUrl);
            singleton.SetRpcState(true);
            singleton.SetRpcCondition(rpcCondition);

            //启动负载均衡
            RpcInvokerFactory rpcInvokerFactory = new RpcInvokerFactory(RpcAdminRegister.class, new HashMap<String, String>() {{
                put(RpcAdminRegister.ADMIN_ADDRESS, singleton.GetRpcUrl());
                put(RpcAdminRegister.ENV, "default");
                put(RpcAdminRegister.BIZ, SystemConfigurationManager.getSingleton().GetRegionName());
            }});

            try {
                rpcInvokerFactory.start();
                singleton.SetRpcInvokerFactory(rpcInvokerFactory);
            } catch (Exception ex) {
                log.error("framework RPC 错误 " + ex.getMessage());
            }
        }
    }

    private static void LoadXmlFile(NodeList configNodes) {
        //NodeList configNodes = XmlHelper.GetNodeList("SystemConfig.xml");

        for (int i = 0; i < configNodes.getLength(); i++) {

            Node node = configNodes.item(i);

            String nodeName = node.getNodeName();

            if ("#text".equals(nodeName)) {
                continue;
            }

            if ("#comment".equals(nodeName)) {
                continue;
            }

            if ("redis".equals(nodeName)) {
                //region redis
                NodeList redisNodeList = node.getChildNodes();

                Map<String, String> map = XmlHelper.GetMap(redisNodeList);

                for (Map.Entry<String, String> entry : map.entrySet()) {

                    if ("port".equals(entry.getKey())) {
                        singleton.SetRedisPort(entry.getValue());
                    } else if ("host".equals(entry.getKey())) {
                        singleton.SetRedisIp(entry.getValue());
                    } else if ("database".equals(entry.getKey())) {
                        singleton.SetRedisDatabase(entry.getValue());
                    } else if ("password".equals(entry.getKey())) {
                        singleton.SetRedisPassword(entry.getValue());
                    }
                }
                //endregion
            } else if ("files".equalsIgnoreCase(nodeName)) {
                //region Files
                NodeList filesNodeList = node.getChildNodes();
                Map<String, String> map = XmlHelper.GetMap(filesNodeList);

                for (Map.Entry<String, String> entry : map.entrySet()) {

                    if ("databaseListFile".equals(entry.getKey())) {
                        singleton.SetDatabaseListFile(entry.getValue());
                    } else if ("dataCommandFile".equals(entry.getKey())) {
                        singleton.SetDataCommandFile(entry.getValue());
                    } else if ("commonConfigFilePath".equals(entry.getKey())) {
                        singleton.SetCommonConfigurationListFile(entry.getValue());
                    } else if ("serviceBrokerConfigFile".equals(entry.getKey())) {
                        singleton.SetServiceBrokerConfigFile(entry.getValue());
                    } else if ("serviceHostingEnvironment".equals(entry.getKey())) {
                        singleton.SetServiceHostingEnvironment(entry.getValue());
                    }
                }
                //endregion
            } else if ("server".equalsIgnoreCase(nodeName)) {
                //region Server
                NodeList serverNodeList = node.getChildNodes();
                Map<String, String> map = XmlHelper.GetMap(serverNodeList);

                for (Map.Entry<String, String> entry : map.entrySet()) {

                    if ("host".equals(entry.getKey())) {
                        singleton.SetServiceHost(entry.getValue());
                    } else if ("port".equals(entry.getKey())) {
                        singleton.SetServicePort(entry.getValue());
                    }
                }
                //endregion
            } else if ("regionName".equalsIgnoreCase(nodeName)) {
                singleton.SetRegionName(node.getFirstChild().getNodeValue());
            } else if ("cacheManager".equalsIgnoreCase(nodeName)) {
                singleton.SetCacheManager(node.getFirstChild().getNodeValue());
            } else if ("logManager".equalsIgnoreCase(nodeName)) {
                singleton.SetLogManager(node.getFirstChild().getNodeValue());
            } else if ("queryManager".equalsIgnoreCase(nodeName)) {
                singleton.SetQueryManager(node.getFirstChild().getNodeValue());
            } else if ("filesManager".equalsIgnoreCase(nodeName)) {
                singleton.SetFilesManager(node.getFirstChild().getNodeValue());
            } else if ("uploadPath".equalsIgnoreCase(nodeName)) {
                singleton.SetUploadPath(node.getFirstChild().getNodeValue());
            } else if ("auth".equalsIgnoreCase(nodeName)) {
                //region auth
                NodeList authNodeList = node.getChildNodes();
                Map<String, String> map = XmlHelper.GetMap(authNodeList);

                for (Map.Entry<String, String> entry : map.entrySet()) {

                    if ("token".equals(entry.getKey())) {
                        singleton.SetTokenCheck(Boolean.parseBoolean(entry.getValue()));
                    } else if ("menu".equals(entry.getKey())) {
                        singleton.SetMenuCheck(Boolean.parseBoolean(entry.getValue()));
                    } else if ("captcha".equals(entry.getKey())) {
                        singleton.SetCaptchaUrl(entry.getValue());
                    } else if ("ignore".equals(entry.getKey())) {
                        singleton.SetIgnore(entry.getValue());
                    }
                }
                //endregion
            } else if ("intercept".equalsIgnoreCase(nodeName)) {
                //region intercept
                NodeList ignoreNodeList = node.getChildNodes();
                List<String> list = XmlHelper.GetList(ignoreNodeList);
                singleton.SetInterceptUrls(list);
                //endregion
            } else if ("task".equalsIgnoreCase(nodeName)) {
                //region task
                NodeList taskNodeList = node.getChildNodes();
                Map<String, String> map = XmlHelper.GetMap(taskNodeList);

                for (Map.Entry<String, String> entry : map.entrySet()) {

                    if ("addresses".equals(entry.getKey())) {
                        singleton.SetAddresses(entry.getValue());
                    } else if ("appName".equals(entry.getKey())) {
                        singleton.SetAppName(entry.getValue());
                    } else if ("port".equals(entry.getKey())) {
                        singleton.SetTaskPort(entry.getValue());
                    } else if ("logPath".equals(entry.getKey())) {
                        singleton.SetLogPath(entry.getValue());
                    }
                }
                //endregion
            }
        }
    }

    /**
     * 初始化远程配置.
     *
     * @param packageName 包名
     */
    private static void LoadRpcConfigClasses(String packageName, String url) throws Exception {
        HashMap<String, Object> configList = new HashMap<>();
        Set<Class<?>> clsList = ClassHelper.GetClasses(packageName);

        if (clsList != null && clsList.size() > 0) {
            for (Class<?> cls : clsList) {
                Config classAnnotation = cls.getAnnotation(Config.class);
                if (classAnnotation == null) {
                    continue;
                }

                String dataId = classAnnotation.dataId();
                String groupId = classAnnotation.groupId();
                String tenantId = classAnnotation.tenantId();

                String condition = "dataId=" + dataId + "&groupId=" + groupId + "&tenantId=" + tenantId;

                String responseString = BasicHttpUtil.get(url + "/config/get?" + condition, TEN);

                if (StringHelper.isBlank(responseString)) {
                    log.info("framework 读取用户自定义配置 【" + condition + "】内容为空！ ");
                    continue;
                }

                RpcResponseEntity rpcResponseEntity = SerializeHelper.JsonDeserialize(responseString, RpcResponseEntity.class);

                String content = rpcResponseEntity.getContent().GetContent();

                Object t = cls.newInstance();
                Field[] fields = cls.getDeclaredFields();


                String line = null;
                BufferedReader reader = new BufferedReader(new StringReader(content));
                while ((line = reader.readLine()) != null) {

                    String[] array = line.split(": ");
                    //校验格式
                    if (array.length != 2) {
                        continue;
                    }

                    String name = array[0];
                    String value = array[1];

                    if (StringHelper.isBlank(name) || StringHelper.isBlank(value)) {
                        continue;
                    }

                    for (Field field : fields) {
                        field.setAccessible(true);
                        String fieldName = field.getName();

                        if (!name.equalsIgnoreCase(fieldName)) {
                            continue;
                        }

                        //int型转boolean型
                        if (field.getType().equals(Boolean.class) && !"false".equals(value) && !"true".equals(value)) {
                            Integer result = Integer.valueOf(value);
                            if (result > 0) {
                                field.set(t, true);
                            } else {
                                field.set(t, false);
                            }
                        } else if (field.getType().equals(Long.class)) {
                            field.set(t, Long.parseLong(value));
                        } else if (field.getType().equals(Integer.class)) {
                            field.set(t, Integer.parseInt(value));
                        } else {
                            field.set(t, value);
                        }

                    }

                }

                configList.put(classAnnotation.uuid(), t);
            }
        }

        CPContext.CustomConfigList.putAll(configList);
    }
}
