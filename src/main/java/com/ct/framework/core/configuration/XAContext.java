package com.ct.framework.core.configuration;

import com.ct.framework.data.connect.TransactionScope;

import java.util.concurrent.ConcurrentHashMap;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 本地事务上下文
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public final class XAContext {

    private static ConcurrentHashMap<String, TransactionScope> TRANSACTION_THREAD_LOCAL = new ConcurrentHashMap<>();

    private XAContext() {
    }

    public static void AddTransaction(String name, TransactionScope scope) {
        if (!TRANSACTION_THREAD_LOCAL.containsKey(name)) {
            TRANSACTION_THREAD_LOCAL.put(name, scope);
        }
    }

    public static void Remove(String name) {
        TRANSACTION_THREAD_LOCAL.remove(name);
    }

    public static TransactionScope GetTransactionScope(String name) {
        if (!TRANSACTION_THREAD_LOCAL.containsKey(name)) {
            return TRANSACTION_THREAD_LOCAL.get(name);
        }

        return null;
    }
}
