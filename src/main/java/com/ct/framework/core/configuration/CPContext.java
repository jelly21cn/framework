package com.ct.framework.core.configuration;


import com.ct.framework.core.annotation.HttpMethod;

import java.util.HashMap;
import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 全局系统上下文
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class CPContext {

    public static HashMap<String, Map<String, HttpMethod>> ApiList;

    public static HashMap<String, Class<?>> AnnotationClassSet;

    public static HashMap<String, Boolean> NeedTokenList;

    /**
     * 用户自定义配置
     */
    public static HashMap<String, Object> CustomConfigList;

    static {
        ApiList = new HashMap<>();
        AnnotationClassSet = new HashMap<>();
        NeedTokenList = new HashMap<>();

        CustomConfigList = new HashMap<>();
    }

    public static String MYSQL = "MYSQL";
    public static String ORACLE = "ORACLE";
    public static String RABBITMQ = "RABBITMQ";

    public static String HIVE2 = "HIVE2";
    public static String HIVE = "HIVE";
    public static String MSSQL = "MSSQL";
}
