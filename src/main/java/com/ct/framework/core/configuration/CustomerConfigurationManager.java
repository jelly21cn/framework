package com.ct.framework.core.configuration;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 读取自定义配置.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *
 * 设计模式：单例模式
 *****************************************************************/
public class CustomerConfigurationManager<T> {
    /**
     * 加载实例.
     */
    public T LoadCustomerConfiguration(String uuid) {

        Object obj = CPContext.CustomConfigList.get(uuid);

        if (obj == null) {
            return null;
        }

        return (T) obj;
    }
}
