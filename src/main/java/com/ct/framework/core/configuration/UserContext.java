package com.ct.framework.core.configuration;

import com.ct.framework.service.runtime.OperationUserInfo;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 登录信息上下文
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public final class UserContext {
    private static final ThreadLocal<OperationUserInfo> USER_INFO_THREAD_LOCAL = new ThreadLocal<>();

    private UserContext() {
    }

    public static ThreadLocal<OperationUserInfo> GetUserInfo() {
        return USER_INFO_THREAD_LOCAL;
    }

    public static void Remove() {
        USER_INFO_THREAD_LOCAL.remove();
    }
}
