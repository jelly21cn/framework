package com.ct.framework.core.data;

import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: DbDataReader.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public abstract class DbDataReader implements IDataReader {
    public abstract List<String> GetMetaData();

    public abstract long GetLong(int index);

    public abstract Object GetObject(int index);
}
