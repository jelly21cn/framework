package com.ct.framework.core.data;

import lombok.extern.slf4j.Slf4j;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 数据库操作核心类.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public abstract class Database {

    private String connectionString;
    private String userName;
    private String password;
    private DbProviderFactory dbProviderFactory;

    protected Database() {

    }

    protected Database(String connectionString, String userName, String password, DbProviderFactory dbProviderFactory) {
        this.connectionString = connectionString;
        this.userName = userName;
        this.password = password;
        this.dbProviderFactory = dbProviderFactory;
    }

    //region PUBLIC Execute FUNCTION

    public Object ExecuteScalar(DbCommand command) {
        DatabaseConnectionWrapper openConnection = this.GetOpenConnection();
        PrepareCommand(command, openConnection.getConnection());
        return this.DoExecuteScalar(command, CommandBehavior.Default);
    }

    public int ExecuteNonQuery(DbCommand command) {
        DatabaseConnectionWrapper openConnection = this.GetOpenConnection();
        PrepareCommand(command, openConnection.getConnection());
        return this.DoExecuteNonQuery(command, CommandBehavior.Default);
    }

    public int ExecuteDDL(DbCommand command) {
        DatabaseConnectionWrapper openConnection = this.GetOpenConnection();
        PrepareCommand(command, openConnection.getConnection());
        return this.DoExecuteDDL(command, CommandBehavior.Default);
    }

    public IDataReader ExecuteReader(DbCommand command) {
        DatabaseConnectionWrapper openConnection = this.GetOpenConnection();
        PrepareCommand(command, openConnection.getConnection());
        IDataReader innerReader = this.DoExecuteReader(command, CommandBehavior.Default);
        return this.CreateWrappedReader(openConnection, innerReader);
    }

    /**
     * 测试数据库连接.
     */
    public void TestConnection(String dbName) {
        DatabaseConnectionWrapper openConnection = this.GetOpenConnection();
        DbConnection connection = openConnection.getConnection();
        if (connection == null) {
            log.info("数据库：{} 连接失败", dbName);
        } else {
            connection.Close();
            log.info("数据库：{} 连接成功", dbName);
        }
    }

    //endregion

    //region PUBLIC Parameter FUNCTION

    public Object GetParameterValue(DbCommand command, String parameterName) {
        if (command == null) {
            log.info("command 参数错误");
            return null;
        }
        return command.GetDbParameterCollection().GetParameter(this.BuildParameterName(parameterName)).getValue();
    }

    public void SetParameterValue(DbCommand command, String parameterName, Object value) {
        if (command == null) {
            log.info("command 参数错误");
            return;
        }
        command.GetDbParameterCollection().GetParameter(this.BuildParameterName(parameterName)).setValue(value);
    }

//    public void AddParameter(DbCommand command, String name, DbType dbType, int size, ParameterDirection direction, Object value) {
//        if (command == null) {
//            log.error("command 参数错误");
//            return;
//        }
//        DbParameter dbParameter = this.CreateParameter(name, dbType, size, direction, value);
//        command.GetDbParameterCollection().Add(name, dbParameter);
//    }

    //endregion

    //region PRIVATE FUNCTION

    private DatabaseConnectionWrapper GetOpenConnection() {
        return this.GetWrappedConnection();
    }

    private DatabaseConnectionWrapper GetWrappedConnection() {
        return new DatabaseConnectionWrapper(this.GetNewOpenConnection());
    }

    private DbConnection GetNewOpenConnection() {
        DbConnection dbConnection = this.CreateConnection();
        dbConnection.Open();
        return dbConnection;
    }

    private DbConnection CreateConnection() {
        DbConnection dbConnection = this.dbProviderFactory.CreateConnection();
        dbConnection.SetConnectionString(this.connectionString);
        dbConnection.SetUserName(this.userName);
        dbConnection.SetPassword(this.password);
        return dbConnection;
    }

    private void PrepareCommand(DbCommand command, DbConnection connection) {
        if (command == null) {
            log.info("command 参数错误");
            return;
        }
        if (connection == null) {
            log.info("connection 参数错误");
            return;
        }
        command.SetConnection(connection);
    }

    private IDataReader DoExecuteReader(DbCommand command, CommandBehavior cmdBehavior) {
        return command.ExecuteReader(cmdBehavior);
    }

    private int DoExecuteNonQuery(DbCommand command, CommandBehavior cmdBehavior) {
        if (command == null) {
            log.info("command 参数错误");
            return 0;
        }
        return command.ExecuteNonQuery(cmdBehavior);
    }

    private int DoExecuteDDL(DbCommand command, CommandBehavior cmdBehavior) {
        if (command == null) {
            log.info("command 参数错误");
            return 0;
        }
        return command.ExecuteDDL(cmdBehavior);
    }

    private Object DoExecuteScalar(IDbCommand command, CommandBehavior cmdBehavior) {
        if (command == null) {
            log.info("command 参数错误");
            return null;
        }

        return command.ExecuteScalar(cmdBehavior);
    }

    private IDataReader CreateWrappedReader(DatabaseConnectionWrapper connection, IDataReader innerReader) {
        return new RefCountingDataReader(connection, innerReader);
    }

    /**
     * 默认
     */
    public String BuildParameterName(String name) {
        return name;
    }

    public void CloseConnection(IDbCommand command) {
        command.CloseConnection();
    }

//    private DbParameter CreateParameter(String name, DbType dbType, int size, ParameterDirection direction, Object value) {
//        DbParameter dbParameter = this.CreateParameter(name);
//        this.ConfigureParameter(dbParameter, name, dbType, size, direction, value);
//        return dbParameter;
//    }
//
//    private DbParameter CreateParameter(String name) {
//        DbParameter dbParameter = this.dbProviderFactory.CreateParameter();
//        dbParameter.setParameterName(this.BuildParameterName(name));
//        return dbParameter;
//    }

//    private void ConfigureParameter(DbParameter param, String name, DbType dbType, int size, ParameterDirection direction, Object value) {
//        param.setDbType(dbType);
//        param.setSize(size);
//        param.setValue(value);
//        param.setDirection(direction);
//        param.setParameterName(name);
//    }
    //endregion
}
