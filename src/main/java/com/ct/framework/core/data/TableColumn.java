package com.ct.framework.core.data;

import lombok.Data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 表的列
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         表的列
 * @author xxx沈剑峰
 *****************************************************************/
@Data
public class TableColumn {

    private String name;

    private String type;

    private Integer length;

    private boolean isnull;

    private boolean isPrimaryKey;

    private boolean isAutoIncrement;

    private String annotation;

    private String tableName;
    //旧的字段名称
    private String oldName;
}
