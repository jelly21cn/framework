package com.ct.framework.core.data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 抽象类 表示 DbCondition 的参数.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public abstract class DbCondition implements IDbCondition {

    protected DbCondition() {
    }

    private String test;

    private String condition;

    private DbParameter dbParameter;

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public DbParameter getDbParameter() {
        return dbParameter;
    }

    public void setDbParameter(DbParameter dbParameter) {
        this.dbParameter = dbParameter;
    }
}
