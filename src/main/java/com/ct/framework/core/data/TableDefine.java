package com.ct.framework.core.data;

import lombok.Data;

import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 表的定义
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         表的定义
 * @author xxx沈剑峰
 *****************************************************************/
@Data
public class TableDefine {

    /**
     * 数据库类型.
     * 11 mysql
     * 13 hive
     */
    private String dbType;

    /**
     * 数据表名称.
     */
    private String tableName;

    private String annotation;

    /**
     * 列.
     */
    private List<TableColumn> columns;
}
