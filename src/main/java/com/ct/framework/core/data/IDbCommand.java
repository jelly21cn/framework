package com.ct.framework.core.data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 表示连接到数据源时执行的 SQL 语句，并由访问关系数据库的数据提供程序实现.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public interface IDbCommand {
    /**
     * 返回影响行数
     */
    int ExecuteNonQuery(CommandBehavior behavior);
    /**
     * 返回执行DDL语句的执行结果
     */
    int ExecuteDDL(CommandBehavior behavior);

    /**
     * 返回列表或者单行
     */
    IDataReader ExecuteReader(CommandBehavior behavior);

    Object ExecuteScalar(CommandBehavior behavior);

    void CloseConnection();
}
