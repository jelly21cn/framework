package com.ct.framework.core.data;

import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 一层包装，方便后续扩展
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class DataReaderWrapper implements IDataReader, IDataRecord {

    private IDataReader innerReader;

    public DataReaderWrapper(IDataReader innerReader) {
        this.innerReader = innerReader;
    }

    @Override
    public Map<String, Object> GetDataRecords() {
        return innerReader.GetDataRecords();
    }

    @Override
    public void Close() {
        innerReader.Close();
    }

    @Override
    public boolean Read() {
        return innerReader.Read();
    }

    public IDataReader GetInnerReader() {
        return this.innerReader;
    }

    public int GetFieldCount() {
        return innerReader.GetDataRecords().size();
    }
}
