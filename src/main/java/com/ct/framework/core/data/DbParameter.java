package com.ct.framework.core.data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 抽象类 表示 DbCommand 的参数，还可以是它到 ResultSet 列的映射.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public abstract class DbParameter implements IDbDataParameter {

    protected DbParameter() {
    }

    //region MEMBER VARIABLE

    private DbType dbType;

    private ParameterDirection direction;

    private String parameterName;

    private Object value;

    private int size;


    public DbType getDbType() {
        return dbType;
    }

    public void setDbType(DbType dbType) {
        this.dbType = dbType;
    }


    public ParameterDirection getDirection() {
        return direction;
    }

    public void setDirection(ParameterDirection direction) {
        this.direction = direction;
    }


    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    /**
     * 获取或设置列中的数据的最大大小（以字节为单位）
     */
    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }


    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    //endregion

    public abstract void ResetDbType();
}
