package com.ct.framework.core.data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 数据库字段枚举.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public abstract class DbColumn implements IDbColumn {
    /**
     * 读取oracle/mysql/hive的 字段类型，转换成 标准 类型.
     * <p>
     * int -> string
     *
     * @param dataType oracle/mysql/hive 的数据类型
     * @return 映射成 标准 类型.
     */
    public abstract String IntToStringMapping(int dataType);

    /**
     * 读取oracle/mysql/hive的 字段类型，转换成 标准 类型.
     * <p>
     * int -> int
     *
     * @param dataType oracle/mysql/hive 的数据类型
     * @return 映射成  标准 类型.
     */
    public abstract int IntToIntMapping(int dataType);

    /**
     * 标准字段类型转换成字符串(用于前端展示).
     * int -> String
     *
     * @param dataType 标准类型
     * @return string 标准类型字符串
     */
    protected String IntToStringMappingStandard(int dataType) {
        switch (dataType) {
            case 0:
                return "INTEGER";
            case 1:
                return "SMALLINT";
            case 2:
                return "DECIMAL";
            case 3:
                return "NUMERIC";
            case 4:
                return "DATETIME";
            case 5:
                return "DATE";
            case 6:
                return "TIMESTAMP";
            case 7:
                return "TIME";
            case 8:
                return "CHAR";
            case 9:
                return "VARCHAR";
            case 10:
                return "BINARY";
            case 11:
                return "VARBINARY";
            case 12:
                return "BLOB";
            case 13:
                return "TEXT";
            case 20:
                return "枚举";
            case 99:
                return "维度表";
            default:
                return "Unknow";
        }
    }
}
