package com.ct.framework.core.data;

import com.ct.framework.core.entity.MetaData;

import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 抽象类 表示 DbMetaData 的参数，数据库元数据.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public abstract class DbMetaData implements IDbMetaData {
    protected DbMetaData() {
    }

    /**
     * 获取列的元数据信息.
     *
     * @param connection 连接
     * @param tableName  表名
     * @return list
     */
    public abstract List<MetaData> GetColumnMetaDataList(DbConnection connection, String tableName);

    /**
     * 获取表的元数据信息.
     *
     * @param connection 连接
     * @return list
     */
    public abstract List<MetaData> GetTableMetaDataList(DbConnection connection);
}
