package com.ct.framework.core.data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 数据库字段.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public interface IDbColumn {

    /**
     * 逆向，把标准转换为各自数据库的字段类型.
     *
     * @param dataType 标准字段类型
     * @return string 数据库的字段类型
     */
    String Reverse(int dataType);
}
