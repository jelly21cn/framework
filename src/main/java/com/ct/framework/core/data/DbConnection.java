package com.ct.framework.core.data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 抽象类 表示数据库连接.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public abstract class DbConnection<T> implements IDbConnection {
    public abstract T GetConnect();

    @Override
    abstract public void Open();

    public abstract void SetConnectionString(String connectionString);

    public abstract void SetUserName(String userName);

    public abstract void SetPassword(String password);

    public abstract void Commit();

    public abstract void RollBack();

}
