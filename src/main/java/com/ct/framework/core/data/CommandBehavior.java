package com.ct.framework.core.data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 提供查询结果及其对数据库的影响的说明.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public enum CommandBehavior {
    Default,
    SingleResult,
    SchemaOnly,
    KeyInfo,
    SingleRow,
    SequentialAccess,
    CloseConnection
}
