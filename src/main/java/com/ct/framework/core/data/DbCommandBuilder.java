package com.ct.framework.core.data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 抽象类 初始化从 DbCommandBuilder 类继承的类的新实例.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Deprecated
public abstract class DbCommandBuilder {

    private final String DeleteFrom          = "DELETE FROM ";

    private final String InsertInto          = "INSERT INTO ";
    private final String DefaultValues       = " DEFAULT VALUES";
    private final String Values              = " VALUES ";

    private final String Update              = "UPDATE ";

    private final String Set                 = " SET ";
    private final String Where               = " WHERE ";
    private final String SpaceLeftParenthesis = " (";

    private final String Comma               = ", ";
    private final String Equal               = " = ";
    private final String LeftParenthesis     = "(";
    private final String RightParenthesis    = ")";
    private final String NameSeparator       = ".";

    private final String IsNull              = " IS NULL";
    private final String EqualOne            = " = 1";
    private final String And                 = " AND ";
    private final String Or                  = " OR ";

    private DbCommandBuilder dbCommandBuilder;

    private DbDataAdapter dataAdapter;
    private DbCommand insertCommand;
    private DbCommand updateCommand;
    private DbCommand deleteCommand;

    protected DbCommandBuilder() {
    }

    /**
     * 获取在数据源中执行删除操作所需的自动生成的 System.Data.Common.DbCommand 对象。
     */
    public DbCommand GetDeleteCommand() {
        return null;
    }

    /**
     * 获取对数据源执行删除操作所必需的自动生成的 System.Data.Common.DbCommand 对象，可选择使用列作为参数名。
     */
    public DbCommand GetDeleteCommand(boolean useColumnsForParameterNames) {
        return null;
    }


    /**
     * 获取在数据源中执行插入操作所需的自动生成的 System.Data.Common.DbCommand 对象。
     */
    public DbCommand GetInsertCommand() {
        return null;
    }

    /**
     * 获取对数据源执行插入操作所必需的自动生成的 System.Data.Common.DbCommand 对象，可选择使用列作为参数名。
     */
    public DbCommand GetInsertCommand(boolean useColumnsForParameterNames) {
        return null;
    }


    /**
     * 获取对数据源执行更新操作所必需的自动生成的 System.Data.Common.DbCommand 对象，可选择使用列作为参数名。
     */
    public DbCommand GetUpdateCommand(boolean useColumnsForParameterNames) {
        return null;
    }

    /**
     * 获取在数据源中执行更新操作所需的自动生成的 System.Data.Common.DbCommand 对象。
     */
    public DbCommand GetUpdateCommand() {
        return null;
    }


    /**
     * 给定部分参数名，返回完整参数名.
     */
    protected String GetParameterName(String parameterName) {
        return null;
    }
}
