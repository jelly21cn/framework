package com.ct.framework.core.data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 描述与数据源连接的当前状态
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Deprecated
public enum ConnectionState {

    /**
     * 连接已关闭.
     */
    Closed,

    /**
     * 连接处于打开状态.
     */
    Open,

    /**
     * 连接对象正在与数据源连接.
     */
    Connecting,

    /**
     * 连接对象正在执行命令.
     */
    Executing,

    /**
     * 连接对象正在检索数据.
     */
    Fetching,

    /**
     * 与数据源的连接中断。 只有在连接打开之后才可能发生这种情况。 可以关闭处于这种状态的连接，然后重新打开.
     */
    Broken
}
