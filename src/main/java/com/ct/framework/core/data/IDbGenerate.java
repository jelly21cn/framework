package com.ct.framework.core.data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 数据库创建和数据表创建.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public interface IDbGenerate {
    /**
     * 获取创建库的sql语句.
     */
    String CreateDatabaseDDL(DatabaseDefine database);

    /**
     * 获取创建表的sql语句.
     */
    String CreateTableDDL(TableDefine table);

    /**
     * 新增表字段的sql语句.
     */
    String InsertTableDDL(TableDefine table);

    /**
     * 删除表的sql语句.
     */
    String DeleteTableDDL(TableDefine table);

    /**
     * 修改表字段表的sql语句.
     */
    String UpdateTableDDL(TableDefine table);


}
