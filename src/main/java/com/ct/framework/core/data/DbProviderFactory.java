package com.ct.framework.core.data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 表示一组方法，这些方法用于创建数据源类的提供程序实现的实例
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public abstract class DbProviderFactory {

    /**
     * 返回实现 DbCommand 类的提供程序类的一个新实例.
     */
    protected DbCommand CreateCommand() {
         return null;
    }

    /**
     * 返回实现 DbCommandBuilder 类的提供程序类的一个新实例。
     */
    protected DbCommandBuilder CreateCommandBuilder() {
        return null;
    }

    /**
     * 返回实现 DbConnection 类的提供程序类的一个新实例。
     */
    protected DbConnection CreateConnection() {
        return null;
    }

    /**
     * 返回实现 DbConnectionStringBuilder 类的提供程序类的一个新实例。
     */
    protected DbConnectionStringBuilder CreateConnectionStringBuilder() {
        return null;
    }

    /**
     * 返回实现 DbDataAdapter 类的提供程序类的一个新实例。
     */
    protected DbDataAdapter CreateDataAdapter() {
        return null;
    }

    /**
     * 返回实现 DbDataSourceEnumerator 类的提供程序类的一个新实例。
     */
    protected DbDataSourceEnumerator CreateDataSourceEnumerator() {
        return null;
    }

    /**
     * 返回实现 DbParameter 类的提供程序类的一个新实例。
     */
    protected DbParameter CreateParameter() {
        return null;
    }

    /**
     * 返回实现 CodeAccessPermission 类的提供程序类的一个新实例。
     */
    protected CodeAccessPermission CreatePermission(PermissionState state) {
        return null;
    }
}
