package com.ct.framework.core.data;

import lombok.Data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 数据库的定义
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         数据库的定义
 * @author xxx沈剑峰
 *****************************************************************/
@Data
public class DatabaseDefine {

    /**
     * 数据库类型.
     * 11 mysql
     * 13 hive
     */
    private String dbType;

    /**
     * 数据库名称.
     */
    private String dbName;
}
