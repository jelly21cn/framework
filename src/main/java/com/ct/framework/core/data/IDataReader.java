package com.ct.framework.core.data;

import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: .
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public interface IDataReader {
    Map<String, Object> GetDataRecords();

    /**
     * 关闭 IDataReader 对象
     */
    void Close();

    /**
     * 前进到下一条记录
     */
    boolean Read();
}
