package com.ct.framework.core.data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 指定如何解释命令字符串.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public enum CommandType {

    /**
     * SQL 文本命令.
     */
    Text,

    /**
     * 存储过程的名称.
     */
    StoredProcedure,

    /**
     * 表的名称.
     */
    TableDirect
}
