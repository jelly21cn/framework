package com.ct.framework.core.data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 抽象类 表示要对数据源执行的 SQL 语句或存储过程。提供表示命令的数据库特定类的基类。
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public abstract class DbCommand implements IDbCommand {
    protected DbCommand() {
    }

    public abstract void SetConnection(DbConnection connection);

    public abstract DbConnection GetDbConnection();

    public abstract void SetCommandType(CommandType commandType);

    public abstract void SetCommandText(String commandText);

    public abstract void SetCommandTimeout(int commandTimeout);

    public abstract void SetOrderBy(String orderBy);

    public abstract void SetBeforeText(String before);

    public abstract void SetAfterText(String after);

    public abstract DbConditionCollection GetDbConditionCollection();

    public abstract DbParameterCollection GetDbParameterCollection();

    public abstract void SetStable(String stable);

    public abstract void CloseConnection();

    /**
     * 是否启用事务
     */
    public abstract void UsingTransaction();

    public abstract void Commit();

    public abstract void RollBack();

}
