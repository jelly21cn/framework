package com.ct.framework.core.data;

import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 抽象类 初始化 DbConditionCollection 类的新实例
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public abstract class DbConditionCollection implements IDbConditionCollection {

    protected Map<String, DbCondition> map;

    protected abstract Map<String, DbCondition> GetConditions();

    public abstract DbCondition GetCondition(String conditionName);
}
