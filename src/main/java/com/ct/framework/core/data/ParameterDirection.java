package com.ct.framework.core.data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 指定查询内的有关 ResultSet 的参数的类型
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public enum ParameterDirection {

    /**
     * 该参数为输入参数.
     */
    Input,

    /**
     * 该参数为输出参数.
     */
    Output,

    /**
     * 参数既能输入，也能输出.
     */
    InputOutput,

    /**
     * 该参数表示从某操作（如存储过程、内置函数或用户定义的函数）返回的值.
     */
    ReturnValue
}
