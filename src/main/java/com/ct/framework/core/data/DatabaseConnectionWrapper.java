package com.ct.framework.core.data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 描述与数据源连接的当前状态
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class DatabaseConnectionWrapper {
    private DbConnection connection;

    public DbConnection getConnection() {
        return connection;
    }

    public DatabaseConnectionWrapper(DbConnection connection) {
        this.connection = connection;
    }
}
