package com.ct.framework.core.annotation;

import com.ct.framework.core.configuration.CPContext;
import com.ct.framework.utility.ClassHelper;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  2021-09-08
 * Usage: 注解加载器(类+方法)
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-09-08              xxx                      业务领域
 * @author xxx
 *****************************************************************/
@Slf4j
public final class AnnotationClassWithMethodLoader {
    private AnnotationClassWithMethodLoader() {
    }

    /**
     * 初始化.
     *
     * @param packageName 包名
     */
    public static void InitAnnotationClasses(String packageName) throws Exception {
        HashMap<String, Class<?>> controllers = new HashMap<>();
        HashMap<String, Map<String, HttpMethod>> apiList = new HashMap<>();
        HashMap<String, Boolean> needTokenList = new HashMap<>();
        Set<Class<?>> clsList = ClassHelper.GetClasses(packageName);
        if (clsList != null && clsList.size() > 0) {
            for (Class<?> cls : clsList) {

                GlobalAnnotation classAnnotation = cls.getAnnotation(GlobalAnnotation.class);

                if (classAnnotation == null) {
                    continue;
                }

                Method[] methods = cls.getDeclaredMethods();

                Map<String, HttpMethod> map = new HashMap<>();

                for (Method m : methods) {
                    GlobalAnnotation methodAnnotation = m.getAnnotation(GlobalAnnotation.class);
                    if (methodAnnotation == null) {
                        continue;
                    }
                    map.put(methodAnnotation.value().toLowerCase(), methodAnnotation.method());

                    //是否需要Token验证
                    NeedToken needTokenAnnotation = m.getAnnotation(NeedToken.class);
                    if (needTokenAnnotation != null) {
                        needTokenList.put(classAnnotation.name().toLowerCase() + methodAnnotation.value().toLowerCase(), needTokenAnnotation.required());
                    }
                }

                apiList.put(classAnnotation.name().toLowerCase(), map);
                controllers.put(classAnnotation.name().toLowerCase(), cls);

            }
        }

        CPContext.AnnotationClassSet.putAll(controllers);
        CPContext.ApiList.putAll(apiList);
        CPContext.NeedTokenList.putAll(needTokenList);
    }

}
