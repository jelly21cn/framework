package com.ct.framework.core.annotation;

import java.lang.annotation.*;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  2021-09-08
 * Usage: 全局拦截器
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-09-08              xxx                      业务领域
 * @author xxx
 *****************************************************************/
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface GlobalAnnotation {
    /**
     * 用于Class.
     * <p>
     *
     * @return string
     */
    String name() default "";

    /**
     * 用于Method.
     *
     * @return string
     */
    String value() default "";

    /**
     * 用于Method.
     *
     * @return HttpMethod
     */
    HttpMethod method() default HttpMethod.GET;
}
