package com.ct.framework.core.annotation;

import com.ct.framework.rpc.remoting.invoker.call.CallType;
import com.ct.framework.rpc.remoting.invoker.route.LoadBalance;
import com.ct.framework.rpc.remoting.net.Client;
import com.ct.framework.rpc.remoting.net.impl.netty.client.NettyClient;
import com.ct.framework.rpc.serialize.Serializer;
import com.ct.framework.rpc.serialize.impl.HessianSerializer;

import java.lang.annotation.*;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface RpcReference {

    Class<? extends Client> client() default NettyClient.class;

    Class<? extends Serializer> serializer() default HessianSerializer.class;

    CallType callType() default CallType.SYNC;

    LoadBalance loadBalance() default LoadBalance.ROUND;

    String version() default "";

    long timeout() default 30000;

    String address() default "";

    String accessToken() default "";
}
