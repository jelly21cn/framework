package com.ct.framework.core.annotation;

import java.lang.annotation.*;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  2021-09-08
 * Usage: 需要鉴权的Controller接口，
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-09-08              xxx                      业务领域
 * @author xxx
 *****************************************************************/
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NeedToken {
    String name() default "";

    /**
     * false 不需要token验证
     */
    boolean required() default true;
}
