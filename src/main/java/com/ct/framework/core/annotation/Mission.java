package com.ct.framework.core.annotation;

import java.lang.annotation.*;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  01/06/2021
 * Usage: 任务注解.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2022-10-1             xxx                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Mission {

    /**
     * 名称.
     */
    String value();

    /**
     * 初始.
     */
    String init() default "";

    /**
     * 移除.
     */
    String destroy() default "";

}
