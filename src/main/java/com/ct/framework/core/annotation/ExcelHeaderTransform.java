package com.ct.framework.core.annotation;

import java.lang.annotation.*;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  2022/5/11
 * Usage: excel字段相关属性
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2022/5/11              xxx                      excel字段相关属性
 * @author xxx
 *****************************************************************/
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ExcelHeaderTransform {

    Class<?> value();
}
