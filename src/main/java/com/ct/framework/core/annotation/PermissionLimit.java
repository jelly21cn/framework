package com.ct.framework.core.annotation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  01/06/2021
 * Usage: 权限限制
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             xxx                         新增
 * @author xxx
 *****************************************************************/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PermissionLimit {

    /**
     * 登录拦截.
     *
     * @return boolean
     */
    boolean limit() default true;

    /**
     * 要求管理员权限.
     *
     * @return boolean
     */
    boolean adminuser() default false;
}