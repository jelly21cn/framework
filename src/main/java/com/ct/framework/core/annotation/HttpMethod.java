package com.ct.framework.core.annotation;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  2021-09-08
 * Usage: 自定义请求方式
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-09-08              xxx                      业务领域
 * @author xxx
 *****************************************************************/
public enum HttpMethod {

    /**
     * POST.
     */
    POST,

    /**
     * GET.
     */
    GET,

    /**
     * PUT.
     */
    PUT
}
