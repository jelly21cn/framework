package com.ct.framework.core.annotation;

import java.lang.annotation.*;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  2022/5/11
 * Usage: 远程自定义配置.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2022/5/11              xxx                      excel字段相关属性
 * @author xxx
 *****************************************************************/
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Config {

    String uuid() default "";

    /**
     * dataId.
     *
     * @return String
     */
    String dataId() default "customer-config";

    /**
     * groupId.
     *
     * @return String
     */
    String groupId() default "DEFAULT_GROUP";

    /**
     * tenantId.
     *
     * @return String
     */
    String tenantId() default "dtube-uat";
}
