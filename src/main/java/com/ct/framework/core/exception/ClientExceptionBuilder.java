package com.ct.framework.core.exception;

import java.util.ArrayList;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 客户端日志建造者，用于控制日志的输出先后顺序
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *
 * 设计模式：建造者(Builder)
 *****************************************************************/
public class ClientExceptionBuilder extends ExceptionBuilder {
    private ClientException clientException = new ClientException();

    @Override
    public void SetSequence(ArrayList<String> sequence) {
        this.clientException.SetSequence(sequence);
    }

    @Override
    public BusinessException BuildException() {
        return clientException;
    }
}
