package com.ct.framework.core.exception;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 扩展系统级日志
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *
 * 设计模式：装饰模式(Decorator)
 *****************************************************************/
public abstract class Decorator extends BusinessException {
    private BusinessException businessException;

    public Decorator(BusinessException bx) {
        this.businessException = bx;
    }

    public void ShowMessage(String message) {
        this.businessException.ShowMessage(message);
    }

    public void SendElk(Exception ex, String message) {
        this.businessException.SendElk(ex, message);
    }

    public void PrintMessage(Exception ex, String message) {
        this.businessException.PrintMessage(ex, message);
    }
}
