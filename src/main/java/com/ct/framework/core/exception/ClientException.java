package com.ct.framework.core.exception;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 客户端的错误日志
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *
 * 设计模式：模板方法(Template Method)
 *****************************************************************/
public class ClientException extends BusinessException {
    @Override
    public void ShowMessage(String message) {

        

        System.out.println("往API显示错误信息");
    }

    @Override
    public void SendElk(Exception ex, String message) {
        System.out.println("ELK日志");
    }

    @Override
    public void PrintMessage(Exception ex, String message) {
        //不用处理
    }
}
