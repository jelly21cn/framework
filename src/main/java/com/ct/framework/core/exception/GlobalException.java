package com.ct.framework.core.exception;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 自定义错误信息
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *
 * 设计模式：建造者(Builder)
 *****************************************************************/
public class GlobalException extends RuntimeException {
    /**
     * 保存异常信息
     */
    private String message;

    /**
     * 保存响应状态码
     */
    private ErrorCode code;

    /**
     * 根据异常信息、响应状态码构建 一个异常实例对象
     *
     * @param message 异常信息
     * @param code    响应状态码
     */
    public GlobalException(String message, ErrorCode code) {
        super(message);
        this.message = message;
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void SetMessage(String message) {
        this.message = message;
    }

    public ErrorCode getCode() {
        return code;
    }

    public void SetCode(ErrorCode code) {
        this.code = code;
    }
}