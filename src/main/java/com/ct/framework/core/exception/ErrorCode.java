package com.ct.framework.core.exception;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage:
 * 0000 成功
 * 1 登录类
 * 2 Client方面
 * 3 Server方面
 * 4 数据库方面
 * 5 权限/用户/组织/鉴权方面
 * 6 加解密方面
 * 7 页面交互
 * 8 网络通信类
 * 9 系统级类
 *
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public enum ErrorCode {

    SUCCESS("0000", "成功"),

    LOGIN_USERNAME_PASSWORD_ERROR("1000", "账号或密码错误"),
    LOGIN_USER_DISABLED("1001", "该用户已被停用"),
    LOGIN_NO_TOKEN("1002", "TOKEN为空"),
    LOGIN_TOKEN_VERIFY_FAILURE("1003", "TOKEN解码失败"),
    LOGIN_FAIL_TO_SIGN("1004", "签名校验失败"),
    LOGIN_HEADER_NO_MENU("1005", "header中缺少menu数据"),
    LOGIN_TOKEN_ACCESS_TIMEOUT("1006", "token超时或已经失效"),
//    VERIFICATION_ERROR("1007", "验证码错误"),
//    VERIFICATION_COOL_DOWN("1008", "30秒内不可重复发送验证码"),

//    INSUFFICIENT_PRIVILEGES_ERROR("1013", "权限不足!"),

    CLIENT_API_NULL("2000", "无此API接口"),
    CLIENT_API_NOT_METHOD("2001", "此API无此方法"),
    CLIENT_API_HTTPMETHOD_ERROR("2003", "POST/GET方式错误"),
    CLIENT_API_NO_ANNOTATION("2004", "方法未配置注解"),
    CLIENT_ENTITY_TRANSFORM_ERROR("2005", "获取ViewEntity失败"),
    CLIENT_API_ERROR("2006", "Api请求错误"),
//    CLIENT_FILE_NAME_ERROR("2007", "文件名错误"),
//    CLIENT_FILE_TYPE_ERROR("2008", "该文件类型不支持"),
    CLIENT_REQ_EMPTY("2009", "请求内容未空"),
    CLIENT_REQ_ACTION_ERROR("2010", "Action错误"),

    SERVER_UPDATE_FAILER("3000", "修改失败"),
    SERVER_DELETE_FAILER("3001", "删除失败"),

//    SERVER_FILE_NO_EXISTS_FAILER("3003", "文件不存在，是一个目录"),
//    SERVER_FILE_CANNOT_W_FAILER("3004", "文件写入失败"),
//    SERVER_FILE_CANNOT_CREATE_FAILER("3005", "文件夹不能创建"),

    SQL_ERROR("4000", "数据库操作错误"),
    SQL_TRANSACTION_ERROR("4001", "数据库执行事务错误"),
//    SQL_INTEGRITY_CONSTRAINT_ERROR("4002", "违反重复约束"),
//    SQL_CONNECT_ERROR("4003", "数据库连接失败"),
//    SQL_NOT_SUPPORT_ERROR("4004", "不支持当前数据库"),
//    SQL_EMPTY_ERROR("4005", "SQL语句为空或者格式错误"),
//    SQL_TABLE_EMPTY("4006", "数据表不存在或记录为空"),
//    SQL_SCHEMA_EMPTY("4007", "数据库不存在或记录为空"),
//    DATAUPDATE_ERROR("1009", "数据更新失败"),
//    DATASELECT_ERROR("1010", "数据查询失败"),
//    DATADELETE_ERROR("1011", "数据删除失败"),
//    DATAINSERT_ERROR("1012", "数据新增失败"),


    ENCRYPT_ERROR("6000", "加密失败"),
    DECRYPT_ERROR("6001", "解密失败"),

    PAGE_ID_NOT_FOUND("7000", "查找不到此id对应的记录"),
    PAGE_RECORD_NOT_FOUND("7001", "查询不到记录"),
    PAGE_EXPORT_ERROR("7002", "导出失败"),
    PAGE_IMPORT_ERROR("7003", "导入失败"),
    PAGE_FILE_DOWNLOAD_FAILER("7004", "文件下载失败"),
    PAGE_FILE_UPLOAD_FAILER("7005", "文件下载失败"),

    NET_ECONNREFUSED("8000", "对方网络拒绝"),
    NET_WRITE_ERROR("8001", "网络输出错误"),
    NET_RPC_ERROR("8002", "负载均衡错误"),

    SYS_INDEX_ERROR("9100", "索引超出界限"),
    SYS_ERROR("9999", "系统未知错误"),

    ;

    private String code;

    private String message;

    ErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String GetCode() {
        return code;
    }

    public String GetMessage() {
        return message;
    }
}
