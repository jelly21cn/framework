package com.ct.framework.core.exception;

import java.util.ArrayList;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 错误日志
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *
 * 设计模式：建造者(Builder)
 *****************************************************************/
public abstract class ExceptionBuilder {

    public abstract void SetSequence(ArrayList<String> sequence);

    public abstract BusinessException BuildException();
}
