package com.ct.framework.core.exception;

import java.util.ArrayList;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 用于控制日志的输出内容先后顺序
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *
 * 设计模式：建造者(Builder)
 *****************************************************************/
public class Director {
    private ArrayList<String> sequence = new ArrayList<>();
    private ClientExceptionBuilder clientExceptionBuilder = new ClientExceptionBuilder();
    private ServerExceptionBuilder serverExceptionBuilder = new ServerExceptionBuilder();

    public ClientException GetClientException() {
        this.sequence.clear();
        this.sequence.add("show");
        this.sequence.add("elk");
        this.clientExceptionBuilder.SetSequence(this.sequence);
        return (ClientException) this.clientExceptionBuilder.BuildException();
    }

    public ServerException GetServerException() {
        this.sequence.clear();
        this.sequence.add("show");
        this.sequence.add("elk");
        this.sequence.add("print");
        this.serverExceptionBuilder.SetSequence(this.sequence);
        return (ServerException) this.serverExceptionBuilder.BuildException();
    }
}
