package com.ct.framework.core.exception;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 系统日志，增加邮件发送方式
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *
 * 设计模式：装饰模式(Decorator)
 *****************************************************************/
public class SystemExceptionDecorator extends Decorator {

    public SystemExceptionDecorator(BusinessException bx) {
        super(bx);
    }

    /**
     * 增加邮件发送
     **/
    private void SendMail(Exception ex, String message) {
        System.out.println("邮件发送");
    }

    @Override
    public void PrintMessage(Exception ex, String message) {
        this.SendMail(ex, message);
        super.PrintMessage(ex, message);
    }
}
