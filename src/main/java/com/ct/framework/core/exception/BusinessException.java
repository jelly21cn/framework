package com.ct.framework.core.exception;

import java.util.ArrayList;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 错误日志
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *
 * 设计模式：模板方法(Template Method)
 * 设计模式：建造者(Builder)
 *****************************************************************/
public abstract class BusinessException {
    private ArrayList<String> sequence = new ArrayList<>();

    /**
     * 往前端API推送错误日志，用于web交互
     */
    public abstract void ShowMessage(String message);

    /**
     * 往ELK推送日志，用于记录
     */
    public abstract void SendElk(Exception ex, String message);

    /**
     * 往文件写日志
     */
    public abstract void PrintMessage(Exception ex, String message);

    /**
     * 控制日志输出方式
     */
    final public void run(Exception ex, String message) {
        if (this.sequence.contains("show")) {
            this.ShowMessage(message);
        }

        if (this.sequence.contains("elk")) {
            this.SendElk(ex, message);
        }

        if (this.sequence.contains("print")) {
            this.PrintMessage(ex, message);
        }
    }

    final public void SetSequence(ArrayList<String> sequence) {
        this.sequence = sequence;
    }
}
