package com.ct.framework.core.file;

import com.ct.framework.core.configuration.SystemConfigurationManager;
import com.ct.framework.files.MinioProvider;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 文件操作缓存
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class FileFactory {

    private static final Map<String, IFileHandler> fileProviders = new ConcurrentHashMap<>();

    private static Lock lock = new ReentrantLock();

    private static String MINIO = "MiniO";
    private static String MONGODB = "Mongo";

    private static synchronized IFileHandler GetFilesProvider(String providerName) {

//        lock.tryLock();
        try {
            if (!fileProviders.containsKey(providerName)) {
                if (MINIO.equalsIgnoreCase(providerName)) {
                    IFileHandler iFileHandler = new MinioProvider();
                    fileProviders.put(providerName, iFileHandler);
                }
            }
        } catch (Exception e) {
            System.out.println("com.ct.framework.core.file.FileFactory.GetFileProvider");
        }
//        lock.unlock();
        return fileProviders.get(providerName);
    }

    public static IFileHandler DistributedFiles() {
        return GetFilesProvider(SystemConfigurationManager.getSingleton().GetFilesManager());
    }
}
