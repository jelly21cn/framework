package com.ct.framework.core.file;

import com.ct.framework.files.entity.UploadOptions;
import com.ct.framework.files.entity.UploadRequest;
import com.ct.framework.files.entity.UploadResponse;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 多种 文件和下载 接口
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public interface IFileHandler {
    UploadResponse Save(String fileName, byte[] body, UploadOptions options);

    UploadResponse Save(UploadRequest request);
}
