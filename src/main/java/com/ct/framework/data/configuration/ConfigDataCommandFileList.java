package com.ct.framework.data.configuration;

import com.ct.framework.data.entity.DataCommandFile;

import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 映射 dbcommandfiles.xml
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class ConfigDataCommandFileList {

    /**
     * 数据库文件
     */
    private List<DataCommandFile> fileList;

    public List<DataCommandFile> getFileList() {
        return fileList;
    }

    public void setFileList(List<DataCommandFile> fileList) {
        this.fileList = fileList;
    }
}
