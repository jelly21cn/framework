package com.ct.framework.data.configuration;

import com.ct.framework.core.configuration.SystemConfigurationManager;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 数据库配置
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class DataAccessSetting {

    private static final String databaseConfigFile;

    private static final String dataCommandFileListConfigFile;

    /**
     * 用于判断是否启用 new CommonDataInstance();
     */
    private static final String commonConfigurationListFile;

    private static final String serviceBrokerConfigFile;

    private static final String serviceHostingEnvironment;

    static {
        databaseConfigFile = SystemConfigurationManager.getSingleton().GetDatabaseListFile();

        dataCommandFileListConfigFile = SystemConfigurationManager.getSingleton().GetDataCommandFile();

        commonConfigurationListFile = SystemConfigurationManager.getSingleton().GetCommonConfigurationListFile();

        serviceBrokerConfigFile = SystemConfigurationManager.getSingleton().GetServiceBrokerConfigFile();

        serviceHostingEnvironment = SystemConfigurationManager.getSingleton().GetServiceHostingEnvironment();
    }

    public static String getDatabaseConfigFile() {
        return databaseConfigFile;
    }

    public static String getDataCommandFileListConfigFile() {
        return dataCommandFileListConfigFile;
    }

    public static String getCommonConfigurationListFile() {
        return commonConfigurationListFile;
    }

    public static String getServiceBrokerConfigFile() {
        return serviceBrokerConfigFile;
    }

    public static String getServiceHostingEnvironment() {
        return serviceHostingEnvironment;
    }
}
