package com.ct.framework.data.oracle;

import com.ct.framework.core.data.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 表示要对 Oracle 数据库执行的一个 Transact-SQL 语句或存储过程,此类不能被继承.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class OracleCommand extends DbCommand {

    /**
     * SQL 模板
     */
    private String commandText;

    private DbConnection dbConnection;

    @Override
    public void SetConnection(DbConnection connection) {
        this.dbConnection = connection;
    }

    @Override
    public DbConnection GetDbConnection() {
        return this.dbConnection;
    }

    @Override
    public void SetCommandType(CommandType commandType) {

    }

    @Override
    public void SetCommandText(String commandText) {
        this.commandText = commandText;
    }

    @Override
    public void SetCommandTimeout(int commandTimeout) {

    }

    @Override
    public void SetOrderBy(String orderBy) {

    }

    @Override
    public void SetBeforeText(String before) {

    }

    @Override
    public void SetAfterText(String after) {

    }

    @Override
    public void CloseConnection() {
        this.dbConnection.Close();
    }

    @Override
    public DbConditionCollection GetDbConditionCollection() {
        return null;
    }

    @Override
    public DbParameterCollection GetDbParameterCollection() {
        return null;
    }

    @Override
    public void SetStable(String stable) {

    }


    @Override
    public int ExecuteNonQuery(CommandBehavior behavior) {
        try {
            Connection connection = ((OracleConnection) this.dbConnection).GetConnect();
            Statement ps = connection.createStatement();
            ps.execute(this.commandText);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return 0;
        }
        return 1;
    }

    @Override
    public int ExecuteDDL(CommandBehavior behavior) {
        try {
            Connection connection = ((OracleConnection) this.dbConnection).GetConnect();
            Statement ps = connection.createStatement();
            ps.execute(this.commandText);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return 0;
        }
        return 1;
    }

    @Override
    public IDataReader ExecuteReader(CommandBehavior behavior) {
        return null;
    }

    @Override
    public Object ExecuteScalar(CommandBehavior behavior) {
        return null;
    }

    @Override
    public void UsingTransaction() {

    }

    @Override
    public void Commit() {
    }

    @Override
    public void RollBack() {
    }
}
