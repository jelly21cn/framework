package com.ct.framework.data.oracle;

import com.ct.framework.core.data.DbColumn;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: Oracle数据库字段.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class OracleColumn extends DbColumn {
    @Override
    public String IntToStringMapping(int dataType) {
        return null;
    }

    @Override
    public int IntToIntMapping(int dataType) {
        return 0;
    }

    @Override
    public String Reverse(int dataType) {
        return null;
    }
}
