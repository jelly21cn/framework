package com.ct.framework.data.oracle;

import com.ct.framework.core.data.DbConnection;
import com.ct.framework.core.exception.ErrorCode;
import com.ct.framework.core.exception.GlobalException;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   刘宁宁
 * Create Date:  09/15/2021
 * Usage: 表示到 oracle 数据库的连接,此类不能被继承.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-09-15            Mr.LiuNingNing                   新增
 * @author 刘宁宁
 *****************************************************************/
@Slf4j
public class OracleConnection extends DbConnection<Connection> {

    private String connectionString;
    private String userName;
    private String password;
    private Connection connection;

    public OracleConnection() {

        try {
            Class.forName("oracle.jdbc.OracleDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Connection GetConnect() {
        return this.connection;
    }

    @Override
    public void Open() {
        try {
            this.connection = DriverManager.getConnection(this.connectionString, this.userName, this.password);
        } catch (SQLException ex) {
            log.info(ex.getMessage());
            throw new GlobalException(ex.getMessage(), ErrorCode.SQL_ERROR);
        }
    }

    @Override
    public void Close() {
        try {
            if (this.connection != null) {
                this.connection.close();
            }
        } catch (SQLException ex) {
            log.info(ex.getMessage());
            throw new GlobalException(ex.getMessage(), ErrorCode.SQL_ERROR);
        }
    }

    @Override
    public void SetConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    @Override
    public void SetUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public void SetPassword(String password) {
        this.password = password;
    }

    @Override
    public void Commit() {
        try {
            if (this.connection != null) {
                this.connection.commit();
            }
        } catch (SQLException ex) {
            log.info(ex.getMessage());
            throw new GlobalException(ex.getMessage(), ErrorCode.SQL_TRANSACTION_ERROR);
        }
    }

    @Override
    public void RollBack() {
        try {
            if (this.connection != null) {
                this.connection.rollback();
            }
        } catch (SQLException ex) {
            log.info(ex.getMessage());
            throw new GlobalException(ex.getMessage(), ErrorCode.SQL_TRANSACTION_ERROR);
        }
    }
}
