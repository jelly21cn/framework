package com.ct.framework.data.oracle;

import com.ct.framework.core.data.DbConnectionStringBuilder;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   刘宁宁
 * Create Date:  09/15/2021
 * Usage: DbConnectionStringBuilder
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             刘宁宁                         新增
 * @author 刘宁宁
 *****************************************************************/
public class OracleConnectionStringBuilder extends DbConnectionStringBuilder {
}
