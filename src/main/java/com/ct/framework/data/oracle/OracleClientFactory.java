package com.ct.framework.data.oracle;

import com.ct.framework.core.data.*;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: OracleClientFactory.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class OracleClientFactory extends DbProviderFactory {

    public static OracleClientFactory Instance = new OracleClientFactory();

    public OracleClientFactory() {
    }

    @Override
    public DbCommand CreateCommand() {
        return new OracleCommand();
    }

    @Override
    public DbCommandBuilder CreateCommandBuilder() {
        return new OracleCommandBuilder();
    }

    @Override
    public DbConnection CreateConnection() {
        return new OracleConnection();
    }

    @Override
    public DbConnectionStringBuilder CreateConnectionStringBuilder() {
        return new OracleConnectionStringBuilder();
    }

    @Override
    public DbDataAdapter CreateDataAdapter() {
        return new OracleDataAdapter();
    }

    @Override
    public DbParameter CreateParameter() {
        return new OracleParameter();
    }
}
