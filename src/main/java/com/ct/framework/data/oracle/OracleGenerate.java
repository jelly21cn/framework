package com.ct.framework.data.oracle;

import com.ct.framework.core.data.DatabaseDefine;
import com.ct.framework.core.data.DbGenerate;
import com.ct.framework.core.data.TableColumn;
import com.ct.framework.core.data.TableDefine;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 表示要对 Oracle 生成DDL语句.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class OracleGenerate extends DbGenerate {
    @Override
    public String CreateDatabaseDDL(DatabaseDefine database) {
        OracleDatabaseDefine oracleDatabaseDefine = (OracleDatabaseDefine) database;

        return String.format("CREATE %s TABLESPACE \"%s\" LOGGING %s SIZE 50M AUTOEXTEND ON NEXT 50M MAXSIZE 2048M", oracleDatabaseDefine.isTemporary() ? "TEMPORARY" : "", oracleDatabaseDefine.getDbName(), String.format("%s '%s'", oracleDatabaseDefine.isTemporary() ? "TEMPFILE " : "DATAFILE", oracleDatabaseDefine.getFilePath()));
    }

    @Override
    public String CreateTableDDL(TableDefine table) {
        OracleTableDefine oracleDatabaseDefine = (OracleTableDefine) table;

        StringBuilder sql = new StringBuilder();
        for (TableColumn column : oracleDatabaseDefine.getColumns()) {
            String type = column.getType() + (column.getType().contains("CHAR") || "NUMBER".equals(column.getType()) ? "(" + column.getLength() + ")" : "");
            sql.append(String.format(" \"%s\" %s %s %s,", column.getName(), type, column.isIsnull() ? "" : "NOT NULL", column.isPrimaryKey() ? "PRIMARY KEY" : ""));
        }
        sql.deleteCharAt(sql.length() - 1);
        return String.format("CREATE TABLE \"%s\" ( %s )", oracleDatabaseDefine.getTableName(), sql.toString());
    }

    @Override
    public String InsertTableDDL(TableDefine table) {
        return null;
    }

    @Override
    public String DeleteTableDDL(TableDefine table) {
        return null;
    }

    @Override
    public String UpdateTableDDL(TableDefine table) {
        return null;
    }

}
