package com.ct.framework.data.access;

import com.ct.framework.core.configuration.CPContext;
import com.ct.framework.core.data.DbColumn;
import com.ct.framework.data.hive.HiveColumn;
import com.ct.framework.data.mysql.SqlColumn;
import lombok.extern.slf4j.Slf4j;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 数据库字段管理.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *
 * 设计模式：单例模式扩展-多例模式
 *****************************************************************/
@Slf4j
public final class DataColumnsManager {

    private DataColumnsManager() {
    }

    /**
     * 自定义.
     *
     * @param dbType 数据库类型
     * @return DbColumn
     */
    public static DbColumn CreateColumns(String dbType) {
        DbColumn column;
        if (CPContext.MYSQL.equalsIgnoreCase(dbType)) {
            column = new SqlColumn();
        } else if (CPContext.HIVE.equalsIgnoreCase(dbType)) {
            column = new HiveColumn();
        } else if (CPContext.ORACLE.equalsIgnoreCase(dbType)) {
            column = new HiveColumn();
        } else {
            column = new SqlColumn();
        }
        return column;
    }
}
