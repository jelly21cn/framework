package com.ct.framework.data.access;

import com.ct.framework.data.oracle.OracleGenerate;
import com.ct.framework.core.configuration.CPContext;
import com.ct.framework.core.data.DatabaseDefine;
import com.ct.framework.core.data.IDbGenerate;
import com.ct.framework.core.data.TableDefine;
import com.ct.framework.data.hive.HiveGenerate;
import com.ct.framework.data.mysql.SqlGenerate;
import lombok.extern.slf4j.Slf4j;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 数据库DDL管理
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *
 * 设计模式：单例模式扩展-多例模式
 *****************************************************************/
@Slf4j
public final class DatabaseDDLManager {

    private DatabaseDDLManager() {
    }

    /**
     * 创建数据库DDL.
     */
    public static String GenerateDataBaseDDL(DatabaseDefine dataBase) {
        try {
            IDbGenerate generateDDL;
            if (CPContext.MYSQL.equalsIgnoreCase(dataBase.getDbType())) {
                generateDDL = SqlGenerate.class.newInstance();
            } else if (CPContext.ORACLE.equalsIgnoreCase(dataBase.getDbType())) {
                generateDDL = OracleGenerate.class.newInstance();
            } else {
                generateDDL = HiveGenerate.class.newInstance();
            }
            return generateDDL.CreateDatabaseDDL(dataBase);
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }
        return "";
    }

    /**
     * 创建数据表DDL.
     */
    public static String GenerateTableDDL(TableDefine table) {
        try {
            IDbGenerate generateDDL;
            if (CPContext.MYSQL.equalsIgnoreCase(table.getDbType())) {
                generateDDL = SqlGenerate.class.newInstance();
            } else if (CPContext.ORACLE.equalsIgnoreCase(table.getDbType())) {
                generateDDL = OracleGenerate.class.newInstance();
            } else {
                generateDDL = HiveGenerate.class.newInstance();
            }
            return generateDDL.CreateTableDDL(table);
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }
        return "";
    }

    /**
     * 新增数据列.
     */
    public static String InsertTableDDL(TableDefine table) {
        try {
            IDbGenerate generateDDL;
            if (CPContext.MYSQL.equalsIgnoreCase(table.getDbType())) {
                generateDDL = SqlGenerate.class.newInstance();
            } else if (CPContext.ORACLE.equalsIgnoreCase(table.getDbType())) {
                generateDDL = OracleGenerate.class.newInstance();
            } else {
                generateDDL = HiveGenerate.class.newInstance();
            }
            return generateDDL.InsertTableDDL(table);
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }
        return "";
    }

    /**
     * 删除数据表
     */
    public static String DeleteTableDDL(TableDefine table) {
        try {
            IDbGenerate generateDDL;
            if (CPContext.MYSQL.equalsIgnoreCase(table.getDbType())) {
                generateDDL = SqlGenerate.class.newInstance();
            } else if (CPContext.ORACLE.equalsIgnoreCase(table.getDbType())) {
                generateDDL = OracleGenerate.class.newInstance();
            } else {
                generateDDL = HiveGenerate.class.newInstance();
            }
            return generateDDL.DeleteTableDDL(table);
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }
        return "";
    }

    /**
     * 修改数据列.
     */
    public static String UpdateTableDDL(TableDefine table) {
        try {
            IDbGenerate generateDDL;
            if (CPContext.MYSQL.equalsIgnoreCase(table.getDbType())) {
                generateDDL = SqlGenerate.class.newInstance();
            } else if (CPContext.ORACLE.equalsIgnoreCase(table.getDbType())) {
                generateDDL = OracleGenerate.class.newInstance();
            } else {
                generateDDL = HiveGenerate.class.newInstance();
            }
            return generateDDL.InsertTableDDL(table);
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }
        return "";
    }
}
