package com.ct.framework.data.access;

import com.ct.framework.data.configuration.ConfigDataCommandFileList;
import com.ct.framework.data.configuration.DataAccessSetting;
import com.ct.framework.data.entity.*;
import com.ct.framework.utility.string.XmlHelper;
import lombok.extern.slf4j.Slf4j;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 统一维护数据库配置文件
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public class DataCommandManager {

    private static Map<String, DataCommand> dataCommands;

    /**
     * 记录DbCommandFiles.xml和Sql名称之间的关系
     */
    private static Map<String, List<String>> fileCommands;

    private static Map<String, DataOperationsDataCommand> dataConfigurationData;

    private static final Lock lock = new ReentrantLock();

    /**
     * 初始化.
     */
    public static void init() {
        UpdateAllCommandFiles();
    }

    private static void UpdateAllCommandFiles() {
        lock.tryLock();

        //初始化
        fileCommands = new HashMap<>();
        dataConfigurationData = new HashMap<>();
        dataCommands = new HashMap<>();

        //获取每一行file列表
        //<file name="hr/company/DataCommand_User.xml"/>
        NodeList dbCommandFiles = XmlHelper.GetNodeList(DataAccessSetting.getDataCommandFileListConfigFile());

        ConfigDataCommandFileList configDataCommandFileList = new ConfigDataCommandFileList();

        List<DataCommandFile> dataCommandFiles = new ArrayList<>();

        for (int i1 = 0; i1 < dbCommandFiles.getLength(); i1++) {

            DataCommandFile dataCommandFile = new DataCommandFile();

            Node fileNode = dbCommandFiles.item(i1);

            String fileNodeName = fileNode.getNodeName();

            if ("#text".equals(fileNodeName)) {
                continue;
            }

            if ("#comment".equals(fileNodeName)) {
                continue;
            }

            String filePath = fileNode.getAttributes().getNamedItem("name").getNodeValue();

            //加载dataCommandFiles文件路径至DataCommandFile[]
            dataCommandFile.setFileName(filePath);

            dataCommandFiles.add(dataCommandFile);

            UpdateCommandFile(filePath);
        }

        //待后续使用
        configDataCommandFileList.setFileList(dataCommandFiles);

        System.out.println(dataCommandFiles.size());

        lock.unlock();
    }

    private static void UpdateCommandFile(String fileName) {

        //获取每一行dataCommand列表
        //<dataCommand name="Account_InsertAccount" database="数据库A" commandType="SQL语句">
        NodeList dataCommandList = XmlHelper.GetNodeList(fileName);

        DataOperations dataOperations = new DataOperations();

        List<DataOperationsDataCommand> dataOperationsDataCommands = new ArrayList<>();

        for (int i2 = 0; i2 < dataCommandList.getLength(); i2++) {

            DataOperationsDataCommand dataOperationsDataCommand = new DataOperationsDataCommand();

            //dataCommand节点
            Node dataCommandNode = dataCommandList.item(i2);

            //dataCommand
            String dataCommandNodeName = dataCommandNode.getNodeName();

            if ("#text".equals(dataCommandNodeName)) {
                continue;
            }

            if ("#comment".equals(dataCommandNodeName)) {
                continue;
            }

            //region 获取dataCommand的attribute
            String attr1 = dataCommandNode.getAttributes().getNamedItem("name").getNodeValue();
            String attr2 = dataCommandNode.getAttributes().getNamedItem("database").getNodeValue();
            String attr3 = dataCommandNode.getAttributes().getNamedItem("commandType").getNodeValue();
            String attr4 = dataCommandNode.getAttributes().getNamedItem("dbType").getNodeValue();

            dataOperationsDataCommand.setName(attr1);
            dataOperationsDataCommand.setDatabase(attr2);
            dataOperationsDataCommand.setCommandType(DataOperationsDataCommandCommandType.valueOf(attr3));
            dataOperationsDataCommand.setDbType(attr4);

            //endregion

            //获取commandText 以及 parameters 节点
            //<commandText>
            //<parameters>
            NodeList commandTextAndParameterList = dataCommandNode.getChildNodes();

            for (int i3 = 0; i3 < commandTextAndParameterList.getLength(); i3++) {

                //commandText+parameters
                Node commandTextAndParameterChildNode = commandTextAndParameterList.item(i3);

                //dataCommand
                String commandTextAndParameterChildNodeName = commandTextAndParameterChildNode.getNodeName();

                if ("#text".equals(commandTextAndParameterChildNodeName)) {
                    continue;
                }

                if ("#comment".equals(commandTextAndParameterChildNodeName)) {
                    continue;
                }

                if ("commandText".equals(commandTextAndParameterChildNodeName)) {
                    //nodeValue = commandTextAndParameterChildNode.getFirstChild().getNodeValue();
                    String nodeValue = commandTextAndParameterChildNode.getTextContent();
                    dataOperationsDataCommand.setCommandText(nodeValue);
                } else if ("parameters".equals(commandTextAndParameterChildNodeName)) {
                    //获取每一行param列表
                    //<param name="#name" dbType="String" direction="Output" size="4" />
                    NodeList parametersNodeList = commandTextAndParameterChildNode.getChildNodes();

                    DataOperationsDataCommandParameters dataOperationsDataCommandParameters = new DataOperationsDataCommandParameters();
                    List<DataOperationsDataCommandParametersParam> operationsDataCommandParametersParams = new ArrayList<>();

                    for (int i4 = 0; i4 < parametersNodeList.getLength(); i4++) {

                        DataOperationsDataCommandParametersParam dataOperationsDataCommandParametersParam = new DataOperationsDataCommandParametersParam();

                        //param
                        Node paramNode = parametersNodeList.item(i4);

                        //dataCommand
                        String paramNodeName = paramNode.getNodeName();

                        if ("#text".equals(paramNodeName)) {
                            continue;
                        }

                        if ("#comment".equals(paramNodeName)) {
                            continue;
                        }

                        String paramsName = paramNode.getAttributes().getNamedItem("name").getNodeValue();
                        String paramsDbType = paramNode.getAttributes().getNamedItem("dbType").getNodeValue();
                        String paramsDirection = paramNode.getAttributes().getNamedItem("direction").getNodeValue();
                        String paramsSize = paramNode.getAttributes().getNamedItem("size").getNodeValue();

                        dataOperationsDataCommandParametersParam.setName(paramsName);
                        dataOperationsDataCommandParametersParam.setDirection(DataOperationsDataCommandParametersParamDirection.valueOf(paramsDirection));
                        dataOperationsDataCommandParametersParam.setDbType(DataOperationsDataCommandParametersParamDbType.valueOf(paramsDbType));
                        dataOperationsDataCommandParametersParam.setSize(Integer.parseInt(paramsSize));

                        operationsDataCommandParametersParams.add(dataOperationsDataCommandParametersParam);
                    }

                    dataOperationsDataCommandParameters.setParam(operationsDataCommandParametersParams);

                    dataOperationsDataCommand.setParameters(dataOperationsDataCommandParameters);
                } else if ("where".equals(commandTextAndParameterChildNodeName)) {

                    NodeList whereNodeList = commandTextAndParameterChildNode.getChildNodes();

                    DataOperationsDataCommandWhere dataOperationsDataCommandWheres = new DataOperationsDataCommandWhere();

                    List<DataOperationsDataCommandWhereCondition> dataOperationsDataCommandWhereConditions = new ArrayList<>();

                    for (int i5 = 0; i5 < whereNodeList.getLength(); i5++) {

                        DataOperationsDataCommandWhereCondition dataOperationsDataCommandWhereCondition = new DataOperationsDataCommandWhereCondition();

                        Node whereNode = whereNodeList.item(i5);

                        String whereNodeName = whereNode.getNodeName();

                        if ("#text".equals(whereNodeName)) {
                            continue;
                        }

                        if ("#comment".equals(whereNodeName)) {
                            continue;
                        }

                        String test = whereNode.getAttributes().getNamedItem("test").getNodeValue();
                        String condition = whereNode.getTextContent();

                        dataOperationsDataCommandWhereCondition.setTest(test);
                        dataOperationsDataCommandWhereCondition.setCondition(condition);

                        dataOperationsDataCommandWhereConditions.add(dataOperationsDataCommandWhereCondition);
                    }

                    Node stableNode = commandTextAndParameterChildNode.getAttributes().getNamedItem("stable");

                    if (stableNode != null) {
                        dataOperationsDataCommandWheres.setStable(stableNode.getNodeValue());
                    }

                    dataOperationsDataCommandWheres.setWheres(dataOperationsDataCommandWhereConditions);

                    dataOperationsDataCommand.setWhere(dataOperationsDataCommandWheres);

                } else if ("orderBy".equals(commandTextAndParameterChildNodeName)) {
                    String nodeValue = commandTextAndParameterChildNode.getTextContent();
                    dataOperationsDataCommand.setOrderBy(nodeValue);
                } else if ("beforeText".equals(commandTextAndParameterChildNodeName)) {
                    String nodeValue = commandTextAndParameterChildNode.getTextContent();
                    dataOperationsDataCommand.setBeforeText(nodeValue);
                } else if ("afterText".equals(commandTextAndParameterChildNodeName)) {
                    String nodeValue = commandTextAndParameterChildNode.getTextContent();
                    dataOperationsDataCommand.setAfterText(nodeValue);
                }
            }

            dataCommands.put(attr1, dataOperationsDataCommand.GetDataCommand(dataOperationsDataCommand.getDbType()));
            dataConfigurationData.put(attr1, dataOperationsDataCommand);
        }

        //把sql语句全部装载到dataOperations
        dataOperations.setDataCommandField(dataOperationsDataCommands);
        fileCommands.put(fileName, dataOperations.GetCommandNames());
    }

    /**
     * 获取DataCommand.
     */
    public static DataCommand GetDataCommand(String name) {
        DataOperationsDataCommand dataOperationsDataCommand = dataConfigurationData.get(name);
        DataCommand dataCommand = new DataCommand(dataOperationsDataCommand.getDatabase(), dataOperationsDataCommand.GetDataCommand(dataOperationsDataCommand.getDbType()).dbCommand);
        return dataCommand;
    }

    /**
     * 根据具体参数创建.
     */
    public static DataCommand CreateDataCommand(String url, String database, String username, String password, String dbType, String command) {
        DataOperationsDataCommand dataOperationsDataCommand = new DataOperationsDataCommand();
        dataOperationsDataCommand.setCommandText(command);
        dataOperationsDataCommand.setCommandType(DataOperationsDataCommandCommandType.Text);
        dataOperationsDataCommand.setDatabase(database);
        dataOperationsDataCommand.setDbType(dbType);

        DatabaseInstance databaseInstance = new DatabaseInstance();
        databaseInstance.SetConnectionString(url);
        databaseInstance.SetDataBaseType(dbType);
        databaseInstance.SetUserName(username);
        databaseInstance.SetPassword(password);
        databaseInstance.SetName(database);

        DataCommand dataCommand = new DataCommand(database, databaseInstance, dataOperationsDataCommand.GetDataCommand(dataOperationsDataCommand.getDbType()).dbCommand);
        return dataCommand;
    }
}
