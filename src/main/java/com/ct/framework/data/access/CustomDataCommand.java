package com.ct.framework.data.access;

import com.ct.framework.data.mysql.SqlCommand;
import com.ct.framework.core.configuration.CPContext;
import com.ct.framework.core.data.DbCommand;
import com.ct.framework.data.hive.HiveCommand;
import com.ct.framework.data.mssql.MsSqlCommand;
import com.ct.framework.data.oracle.OracleCommand;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 默认的数据库操作类
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *
 * 设计模式：策略模式
 *****************************************************************/
public class CustomDataCommand implements IDataBase {

    private String dataBaseType;

    public CustomDataCommand(String dataBaseType) {
        this.dataBaseType = dataBaseType;
    }

    @Override
    public DbCommand CreateDbCommand() {
        if (CPContext.ORACLE.equalsIgnoreCase(this.dataBaseType)) {
            return new OracleCommand();
        } else if (CPContext.MYSQL.equalsIgnoreCase(this.dataBaseType)) {
            return new SqlCommand();
        } else if (CPContext.HIVE.equalsIgnoreCase(this.dataBaseType)) {
            return new HiveCommand();
        } else if (CPContext.HIVE2.equalsIgnoreCase(this.dataBaseType)) {
            return new HiveCommand();
        } else if (CPContext.MSSQL.equalsIgnoreCase(this.dataBaseType)) {
            return new MsSqlCommand();
        } else {
            return null;
        }
    }
}
