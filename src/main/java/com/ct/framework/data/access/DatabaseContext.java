package com.ct.framework.data.access;

import com.ct.framework.core.data.DbCommand;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 默认的数据库操作类
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 * 设计模式：策略模式
 *****************************************************************/
public class DatabaseContext {
    private IDataBase strategy;

    public DatabaseContext(IDataBase strategy) {
        this.strategy = strategy;
    }

    //封装后的策略方法
    public DbCommand GetDbCommand() {
        return this.strategy.CreateDbCommand();
    }
}
