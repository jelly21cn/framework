package com.ct.framework.data.access;

import com.ct.framework.data.entity.DatabaseInstance;
import com.ct.framework.data.entity.EntityBuilder;
import com.ct.framework.core.data.Database;
import com.ct.framework.core.data.DbCommand;
import com.ct.framework.core.data.IDataReader;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 封装数据库操作 消除结构化与非结构化数据库操作的差异
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *
 * 设计模式 命令模式
 *****************************************************************/
@Slf4j
public class DataCommand {

    //region PROTECT VARIABLE
    protected DbCommand dbCommand;
    protected String databaseName;
    //是否启用事务
    private boolean usingTransaction = false;
    //endregion
    private DatabaseInstance instance;

    private DataCommand() {
    }

    public DataCommand(String databaseName, DbCommand command) {
        this.dbCommand = command;
        this.databaseName = databaseName;
        this.usingTransaction = false;
    }

    public DataCommand(String databaseName, DatabaseInstance instance, DbCommand command) {
        this.dbCommand = command;
        this.databaseName = databaseName;
        this.instance = instance;
        this.usingTransaction = false;
    }

    //region PROTECT FUNCTION
    protected Database GetActualDatabase() {
        return DatabaseManager.GetDatabase(this.databaseName);
    }

    /**
     * 自定义创建数据库实例.
     *
     * @return Database Database
     */
    protected Database CreateActualDatabase() {
        return DatabaseManager.CreateDatabase(instance);
    }

    public DbCommand GetDbCommand() {
        return this.dbCommand;
    }

    //endregion

    //region PUBLIC FUNCTION

    /**
     * 填充参数.
     *
     * @param paramName 参数名
     * @param val       参数值
     */
    public void SetParameterValue(String paramName, Object val) {
        this.GetActualDatabase().SetParameterValue(this.dbCommand, paramName, val);
    }

    /**
     * 返回数据库执行的返回值.
     */
    public Object GetParameterValue(String paramName) {
        return this.GetActualDatabase().GetParameterValue(this.dbCommand, paramName);
    }

    //endregion

    //region EXECUTE

    public <T> T ExecuteScalar() {
        T t = null;
        try {
            t = (T) this.GetActualDatabase().ExecuteScalar(this.dbCommand);
            this.CloseConnection();
        } catch (Exception ex) {
            this.CloseConnection();
            log.info(ex.getMessage());
            //throw new GlobalException(ex.getMessage(), ErrorCode.SQL_ERROR);
        }
        return t;
    }

    /**
     * 返回执行的影响条数 1或者0.
     * 常用与 insert delete update.
     */
    public int ExecuteNonQuery() {
        int count = 0;
        try {
            count = this.GetActualDatabase().ExecuteNonQuery(this.dbCommand);
            this.CloseConnection();
        } catch (Exception ex) {
            this.CloseConnection();
            log.info(ex.getMessage());
            //throw new GlobalException(ex.getMessage(), ErrorCode.SQL_ERROR);
        }
        return count;
    }

    /**
     * 执行DDL语句.
     */
    public int ExecuteDDL() {
        int count = 0;
        try {
            count = this.CreateActualDatabase().ExecuteDDL(this.dbCommand);
            this.CloseConnection();
            //if (StringUtils.isNotBlank(dbName) && !dbType.equals(CPContext.ORACLE)) {
            //    String[] str = url.split("/");
            //    String path = String.format("%s/%s/%s/%s", str[0], str[1], str[2], dbName);
            //    db = CreateActualDatabase(path, username, password, dbType);
            //    db.TestConnection(dbName);
            //}
        } catch (Exception ex) {
            this.CloseConnection();
            log.info(ex.getMessage());
            //throw new GlobalException(ex.getMessage(), ErrorCode.SQL_ERROR);
        }
        return count;
    }

    public <T> T ExecuteEntity(Class<T> clazz) {
        T entity = null;
        try {
            IDataReader reader = this.GetActualDatabase().ExecuteReader(this.dbCommand);
            if (reader.Read()) {
                entity = EntityBuilder.BuildEntity(reader, clazz);
            }
            this.CloseConnection();
        } catch (Exception ex) {
            this.CloseConnection();
            log.info(ex.getMessage());
            //throw new GlobalException(ex.getMessage(), ErrorCode.SQL_ERROR);
        }
        return entity;
    }

    /**
     * 返回列表.
     */
    public <T> List<T> ExecuteEntityList(Class<T> clazz) {
        List<T> list = null;
        try {
            IDataReader reader = this.GetActualDatabase().ExecuteReader(this.dbCommand);
            list = new ArrayList<>();
            while (reader.Read()) {
                T entity = EntityBuilder.BuildEntity(reader, clazz);
                list.add(entity);
            }
            this.CloseConnection();
        } catch (Exception ex) {
            this.CloseConnection();
            log.info(ex.getMessage());
            //throw new GlobalException(ex.getMessage(), ErrorCode.SQL_ERROR);
        }
        return list;
    }

    /**
     * 返回Map（不实体化）列表.
     * 用于直接执行sql语句.
     */
    public List<Map<String, Object>> ExecuteEntityList() {
        List<Map<String, Object>> list = null;
        try {
            IDataReader reader = this.CreateActualDatabase().ExecuteReader(this.dbCommand);
            list = new ArrayList<>();
            while (reader.Read()) {
                Map<String, Object> entity = EntityBuilder.BuildEntity(reader);
                list.add(entity);
            }
            this.CloseConnection();
        } catch (Exception ex) {
            this.CloseConnection();
            log.info(ex.getMessage());
            //throw new GlobalException(ex.getMessage(), ErrorCode.SQL_ERROR);
        }
        return list;
    }

    //endregion

    //region 事务

    public void UsingTransaction() {
        this.usingTransaction = true;
        this.dbCommand.UsingTransaction();
    }

    //如果启用事务，关闭连接 交给 事务完成后处理ø
    public void CloseConnection() {
        if (!this.usingTransaction) {
            this.GetActualDatabase().CloseConnection(this.dbCommand);
        }
    }

    public void Commit() {
        this.dbCommand.Commit();
    }

    public void RollBack() {
        this.dbCommand.RollBack();
    }

    //endregion
}
