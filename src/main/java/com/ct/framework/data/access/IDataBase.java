package com.ct.framework.data.access;

import com.ct.framework.core.data.DbCommand;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 用于选择实例化哪种数据库
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *
 * 设计模式：策略模式
 *****************************************************************/
public interface IDataBase {
    DbCommand CreateDbCommand();
}
