package com.ct.framework.data.access;

import com.ct.framework.data.connect.ConfigInstanceBase;
import com.ct.framework.data.entity.DatabaseInstance;
import com.ct.framework.data.oracle.OracleDatabase;
import com.ct.framework.core.configuration.CPContext;
import com.ct.framework.core.data.Database;
import com.ct.framework.data.hive.HiveDatabase;
import com.ct.framework.data.mssql.MsSqlDatabase;
import com.ct.framework.data.mysql.SqlDatabase;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 数据库管理，文件监控sql文件
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *
 * 设计模式：单例模式扩展-多例模式
 *****************************************************************/
@Slf4j
public class DatabaseManager {

    //region PRIVATE MEMBER VARIABLE
    private final static Map<String, Database> dataBaseHashMap = new HashMap<>();
    //endregion

    /**
     * 初始化.
     */
    public static void init() {
        LoadDatabaseList();
    }

    /**
     * 文件监控.
     */
    private static void FileWatcher() {
        //TODO
        LoadDatabaseList();
    }

    /**
     * 加载数据库实例.
     */
    private static void LoadDatabaseList() {
        List<DatabaseInstance> list = ConfigInstanceBase.GetInstanceList();
        if (list == null || list.size() == 0) {
            return;
        }
        Database db;
        for (DatabaseInstance instance : list) {
            if (CPContext.MYSQL.equalsIgnoreCase(instance.GetDataBaseType())) {
                db = new SqlDatabase(instance.GetConnectionString(), instance.GetUserName(), instance.GetPassword());
            } else if (CPContext.HIVE.equalsIgnoreCase(instance.GetDataBaseType())) {
                db = new HiveDatabase(instance.GetConnectionString(), instance.GetUserName(), instance.GetPassword());
            } else if (CPContext.HIVE2.equalsIgnoreCase(instance.GetDataBaseType())) {
                db = new HiveDatabase(instance.GetConnectionString(), instance.GetUserName(), instance.GetPassword());
            } else if (CPContext.MSSQL.equalsIgnoreCase(instance.GetDataBaseType())) {
                db = new MsSqlDatabase(instance.GetConnectionString(), instance.GetUserName(), instance.GetPassword());
            } else {
                //例如sql server 不用传账号密码
                db = new OracleDatabase(instance.GetConnectionString(), instance.GetUserName(), instance.GetPassword());
            }

            dataBaseHashMap.put(instance.GetName(), db);
        }
    }

    /**
     * 加载数据库实例.
     */
    public static Database GetDatabase(String name) {
        return dataBaseHashMap.get(name);
    }

    /**
     * 自定义创建数据库连接实例.
     */
    public static Database CreateDatabase(DatabaseInstance instance) {
        Database db;
        if (CPContext.MYSQL.equalsIgnoreCase(instance.GetDataBaseType())) {
            db = new SqlDatabase(instance.GetConnectionString(), instance.GetUserName(), instance.GetPassword());
        } else if (CPContext.HIVE.equalsIgnoreCase(instance.GetDataBaseType())) {
            db = new HiveDatabase(instance.GetConnectionString(), instance.GetUserName(), instance.GetPassword());
        } else if (CPContext.HIVE2.equalsIgnoreCase(instance.GetDataBaseType())) {
            db = new HiveDatabase(instance.GetConnectionString(), instance.GetUserName(), instance.GetPassword());
        } else if (CPContext.MSSQL.equalsIgnoreCase(instance.GetDataBaseType())) {
            db = new MsSqlDatabase(instance.GetConnectionString(), instance.GetUserName(), instance.GetPassword());
        } else {
            //例如sql server 不用传账号密码
            db = new OracleDatabase(instance.GetConnectionString(), instance.GetUserName(), instance.GetPassword());
        }
        dataBaseHashMap.put(instance.GetName(), db);
        return db;
    }
}
