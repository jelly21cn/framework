package com.ct.framework.data.connect;

import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: ConfigurationFileList
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Deprecated
public class ConfigurationFileList {
    private List<ConfigurationFile> configurationList;

    public List<ConfigurationFile> getConfigurationList() {
        return configurationList;
    }

    public void setConfigurationList(List<ConfigurationFile> configurationList) {
        this.configurationList = configurationList;
    }
}
