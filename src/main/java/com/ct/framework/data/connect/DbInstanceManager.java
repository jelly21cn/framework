package com.ct.framework.data.connect;

import com.ct.framework.data.entity.DatabaseInstance;

import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 数据源实例管理
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *
 * 设计模式：单例模式扩展-多例模式
 *****************************************************************/
@Deprecated
public class DbInstanceManager {

    /**
     * 通过别名得到连接字符串.
     *
     * @param alisaName 数据库名称
     */
    public static String GetConnectionString(String alisaName) {
        String result = "";

        List<DatabaseInstance> dbList = ConfigInstanceBase.GetInstanceList();

        for (DatabaseInstance item : dbList) {
            if (item.GetName().trim().equals(alisaName.trim())) {
                result = item.GetConnectionString();
                break;
            }
        }
        return result;
    }
}
