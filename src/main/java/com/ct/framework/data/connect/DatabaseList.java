package com.ct.framework.data.connect;

import com.ct.framework.data.entity.DatabaseInstance;

/**
 * Copyright (C) xxx Corporation. All rights reserved.
 * <p/>
 * Usage：数据库实例列表
 * <p>
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-5-1             沈剑峰                         新增
 *
 * @author xxx沈剑峰
 * @date 2021/05/01
 */
@Deprecated
public class DatabaseList {
    private DatabaseInstance[] databaseInstances;

    public DatabaseInstance[] getDatabaseInstances() {
        return databaseInstances;
    }

    public void setDatabaseInstances(DatabaseInstance[] databaseInstances) {
        this.databaseInstances = databaseInstances;
    }
}
