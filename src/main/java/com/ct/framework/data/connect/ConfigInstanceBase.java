package com.ct.framework.data.connect;

import com.ct.framework.data.configuration.DataAccessSetting;
import com.ct.framework.utility.string.XmlHelper;
import com.ct.framework.data.entity.DatabaseInstance;
import lombok.extern.slf4j.Slf4j;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage:加载多数据源配置文件Database.xml
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *
 * 设计模式：单例模式 - 扩展 - 多例
 *****************************************************************/
@Slf4j
public class ConfigInstanceBase {

    private static List<DatabaseInstance> dbConnectList = new ArrayList<>();

    static {
        log.info("framework 加载多数据源 开始");
        LoadDataBaseList();
        log.info("framework 加载多数据源 结束");
    }

    private ConfigInstanceBase() {
    }

    /**
     * 数据库文件列表
     */
    public static List<DatabaseInstance> GetInstanceList() {
        return dbConnectList;
    }

    /**
     * 加载所有数据库链接
     */
    private static void LoadDataBaseList() {

        NodeList databaseList = XmlHelper.GetNodeList(DataAccessSetting.getDatabaseConfigFile());

        for (int i = 0; i < databaseList.getLength(); i++) {

            DatabaseInstance databaseInstance = new DatabaseInstance();

            Node node = databaseList.item(i);

            String nodeName = node.getNodeName();

            if ("#text".equals(nodeName)) {
                continue;
            }

            if ("#comment".equals(nodeName)) {
                continue;
            }

            String attributeName = node.getAttributes().getNamedItem("name").getNodeValue();

            NodeList childNodes = node.getChildNodes();

            Map<String, String> map = XmlHelper.GetMap(childNodes);

            databaseInstance.SetName(attributeName);

            for (Map.Entry<String, String> entry : map.entrySet()) {

                if ("username".equals(entry.getKey())) {
                    databaseInstance.SetUserName(entry.getValue());
                } else if ("password".equals(entry.getKey())) {
                    databaseInstance.SetPassword(entry.getValue());
                } else if ("jdbcurl".equals(entry.getKey())) {
                    databaseInstance.SetConnectionString(entry.getValue());
                } else if ("database-type".equals(entry.getKey())) {
                    databaseInstance.SetDataBaseType(entry.getValue());
                } else {
                    log.info("key = " + entry.getKey() + ", value = " + entry.getValue());
                }
            }

            dbConnectList.add(databaseInstance);

        }
    }
}
