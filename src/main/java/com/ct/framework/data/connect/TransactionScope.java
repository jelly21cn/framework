package com.ct.framework.data.connect;

import com.ct.framework.core.configuration.XAContext;
import com.ct.framework.data.access.DataCommand;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 本地事务
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public class TransactionScope {
    private String scopeId;
    private int count = 0;
    private int total = 0;

    public TransactionScope(Class clazz) {
        this.scopeId = System.currentTimeMillis() + "-" + clazz.hashCode();
        this.dataCommandList = new ArrayList<>();
    }

    private List<DataCommand> dataCommandList;

    public void AddCommand(DataCommand dataCommand) {
        this.total++;
        this.dataCommandList.add(dataCommand);
    }

    public void Start() {
        //本地的多数据源事务
        //TCC等后续完善
        XAContext.AddTransaction(this.scopeId, this);
    }

    private void Commit() {
        for (DataCommand cmd : dataCommandList) {
            cmd.Commit();
        }
    }

    public boolean IsSuccess() {
        if (total == count) {
            return true;
        }

        return false;
    }

    public void Increase() {
        this.count++;
    }

    /**
     * 强制回滚事务
     */
    public void Reset() {
        //回滚事务
        this.RollBack();
        for (DataCommand cmd : dataCommandList) {
            cmd.CloseConnection();
        }
    }

    //释放资源
    public void End() {
        if (IsSuccess()) {
            //提交事务
            this.Commit();
        } else {
            //回滚事务
            this.RollBack();
        }

        for (DataCommand cmd : dataCommandList) {
            cmd.CloseConnection();
        }
    }

    private void RollBack() {
        for (DataCommand cmd : dataCommandList) {
            cmd.RollBack();
        }
    }
}
