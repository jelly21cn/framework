package com.ct.framework.data.hive;

import com.ct.framework.core.data.DbDataReader;
import com.ct.framework.core.exception.ErrorCode;
import com.ct.framework.core.exception.GlobalException;
import lombok.extern.slf4j.Slf4j;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: HiveDataReader.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public class HiveDataReader extends DbDataReader {

    private ResultSet resultSet;

    private List<String> columnsList;

    public HiveDataReader(ResultSet resultSet) {
        this.resultSet = resultSet;
        this.columnsList = new ArrayList<>();
    }

    @Override
    public Map<String, Object> GetDataRecords() {
        Map<String, Object> results = new HashMap<>();

        try {
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            int columns = resultSetMetaData.getColumnCount();

            for (int x = 1; x <= columns; x++) {
                String columnName = resultSetMetaData.getColumnName(x).toLowerCase();
                Object value = this.resultSet.getObject(columnName);
                results.put(columnName, value);
                this.columnsList.add(columnName);
            }
        } catch (Exception ex) {
            log.info(ex.getMessage());
            throw new GlobalException(ex.getMessage(), ErrorCode.SQL_ERROR);
        }
        return results;
    }

    @Override
    public List<String> GetMetaData() {
        return this.columnsList;
    }

    @Override
    public long GetLong(int index) {
        long id = 0L;
        try {
            id = resultSet.getLong(index);
        } catch (SQLException ex) {
            log.info(ex.getMessage());
            throw new GlobalException(ex.getMessage(), ErrorCode.SQL_ERROR);
        }
        return id;
    }

    @Override
    public Object GetObject(int index) {
        Object t;
        try {
            t = resultSet.getObject(index);
        } catch (SQLException ex) {
            log.info(ex.getMessage());
            throw new GlobalException(ex.getMessage(), ErrorCode.SQL_ERROR);
        }
        return t;
    }

    @Override
    public void Close() {

    }

    @Override
    public boolean Read() {
        try {
            return resultSet.next();
        } catch (SQLException ex) {
            log.info(ex.getMessage());
            throw new GlobalException(ex.getMessage(), ErrorCode.SQL_ERROR);
        }
    }

}
