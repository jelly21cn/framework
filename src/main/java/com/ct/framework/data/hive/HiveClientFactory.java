package com.ct.framework.data.hive;

import com.ct.framework.core.data.*;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   刘宁宁
 * Create Date:  09/15/2021
 * Usage: SqlClientFactory.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             刘宁宁                         新增
 * @author 刘宁宁
 *****************************************************************/
public class HiveClientFactory extends DbProviderFactory {

    public static HiveClientFactory Instance = new HiveClientFactory();

    public HiveClientFactory() {
    }

    @Override
    public DbCommand CreateCommand() {
        return new HiveCommand();
    }

    @Override
    public DbCommandBuilder CreateCommandBuilder() {
        return new HiveCommandBuilder();
    }

    @Override
    public DbConnection CreateConnection() {
        return new HiveConnection();
    }

    @Override
    public DbConnectionStringBuilder CreateConnectionStringBuilder() {
        return new HiveConnectionStringBuilder();
    }

    @Override
    public DbDataAdapter CreateDataAdapter() {
        return new HiveDataAdapter();
    }

    @Override
    public DbParameter CreateParameter() {
        return new HiveParameter();
    }
}
