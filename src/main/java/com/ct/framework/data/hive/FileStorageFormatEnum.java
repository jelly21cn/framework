package com.ct.framework.data.hive;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   刘宁宁
 * Create Date:  09/15/2021
 * Usage: hive的文件格式.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             刘宁宁                         新增
 * @author 刘宁宁
 *****************************************************************/
public enum FileStorageFormatEnum {

    TEXTFILE(1, "TEXTFILE"),
    SEQUENCEFILE(2, "SEQUENCEFILE"),
    ORC(3, "ORC"),
    PARQUET(4, "PARQUET");

    public int code;
    public String name;

    FileStorageFormatEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getNameByCode(Integer code) {
        for (FileStorageFormatEnum value : FileStorageFormatEnum.values()) {
            if (code.equals(value.getCode())) {
                return value.getName();
            }
        }
        return "";
    }
}
