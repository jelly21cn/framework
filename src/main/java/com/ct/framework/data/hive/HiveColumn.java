package com.ct.framework.data.hive;

import com.ct.framework.core.data.DbColumn;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: Hive数据库字段.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class HiveColumn extends DbColumn {
    /**
     * 读取Hive所有类型，对应成 标准的数据类型的字符.
     */
    @Override
    public String IntToStringMapping(int dataType) {
        switch (dataType) {
            case -5:
                return "BIGINT";
            case 3:
                return "DECIMAL";
            case 4:
                return "INT";
            case 5:
                return "SMALLINT";
            case 6:
                return "FLOAT";
            case 8:
                return "DOUBLE";
            case 12:
                return "VARCHAR";
            case 16:
                return "BIT";
            case 91:
                return "DATE";
            case 93:
                return "DATETIME";
            default:
                return "Unknow";
        }
    }

    /**
     * 把hive的字段类型转换为标准字段类型.
     *
     * @param dataType mysql的字段类型
     * @return int  标准字段类型
     */
    @Override
    public int IntToIntMapping(int dataType) {
        switch (dataType) {
            case -5:
            case 4:
                return 0;
            case 3:
            case 6:
                return 2;
            case 5:
            case 16:
                return 1;
            case 8:
                return 3;
            case 12:
                return 9;
            case 91:
                return 5;
            case 93:
                return 6;
            default:
                return -99;
        }
    }

    /**
     * 逆向，把标准转换为各自数据库的字段类型..
     */
    @Override
    public String Reverse(int dataType) {
        switch (dataType) {
            case 0:
                return "INT";
            case 1:
                return "SMALLINT";
            case 2:
                return "DECIMAL";
            case 3:
                return "DOUBLE";
            case 4:
            case 5:
            case 7:
                return "DATE";
            case 6:
                return "TIMESTAMP";
            case 8:
                return "CHAR";
            case 9:
                return "STRING";
            default:
                return "Unknow";
        }
    }
}
