package com.ct.framework.data.hive;

import com.ct.framework.core.data.*;
import lombok.extern.slf4j.Slf4j;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   刘宁宁
 * Create Date:  09/15/2021
 * Usage: 表示要对 Hive 数据库执行的一个 Transact-SQL 语句或存储过程,此类不能被继承.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             刘宁宁                         新增
 * @author 刘宁宁
 *****************************************************************/
@Slf4j
public class HiveCommand extends DbCommand {

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * SQL 模板
     */
    private String commandText;

    private DbConnection dbConnection;

    private HiveParameterCollection hiveParameterCollection;

    private String orderBy;

    private HiveConditionCollection hiveConditionCollection;

    public HiveCommand() {
        super();
    }

    @Override
    public void SetConnection(DbConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    @Override
    public DbConnection GetDbConnection() {
        return this.dbConnection;
    }

    @Override
    public void SetCommandType(CommandType commandType) {

    }

    @Override
    public void SetCommandText(String commandText) {
        this.commandText = commandText;
    }

    @Override
    public void SetCommandTimeout(int commandTimeout) {

    }

    @Override
    public void SetOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    @Override
    public void SetBeforeText(String before) {

    }

    @Override
    public void SetAfterText(String after) {

    }

    @Override
    public void CloseConnection() {
        this.dbConnection.Close();
    }

    @Override
    public DbConditionCollection GetDbConditionCollection() {
        if (this.hiveConditionCollection == null) {
            this.hiveConditionCollection = new HiveConditionCollection();
        }
        return this.hiveConditionCollection;
    }

    @Override
    public DbParameterCollection GetDbParameterCollection() {
        if (this.hiveParameterCollection == null) {
            this.hiveParameterCollection = new HiveParameterCollection();
        }
        return this.hiveParameterCollection;
    }

    @Override
    public void SetStable(String stable) {

    }


    //region ExecuteReader
    @Override
    public IDataReader ExecuteReader(CommandBehavior behavior) {
        HiveDataReader hiveDataReader = null;
        try {
            String execSql = ValidateCommand(this.commandText);
            Connection connection = ((HiveConnection) this.dbConnection).GetConnect();
            PreparedStatement ps = connection.prepareStatement(execSql);
            ResultSet resultSet = ps.executeQuery();
            hiveDataReader = new HiveDataReader(resultSet);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return hiveDataReader;
    }

    @Override
    public Object ExecuteScalar(CommandBehavior behavior) {
        Object result = null;
        try {
            String execSql = ValidateCommand(this.commandText);
            Connection connection = ((HiveConnection) this.dbConnection).GetConnect();
            PreparedStatement ps = connection.prepareStatement(execSql);
            ResultSet resultSet = ps.executeQuery();
            HiveDataReader hiveDataReader = new HiveDataReader(resultSet);
            if (hiveDataReader.Read()) {
                result = hiveDataReader.GetObject(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return result;
    }

    @Override
    public void UsingTransaction() {

    }

    @Override
    public void Commit() {
    }

    @Override
    public void RollBack() {
    }

    //endregion

    @Override
    public int ExecuteNonQuery(CommandBehavior behavior) {
        try {
            String execSql = ValidateCommand(this.commandText);
            Connection connection = ((HiveConnection) this.dbConnection).GetConnect();
            Statement ps = connection.createStatement();
            ps.execute(execSql);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return 0;
        }
        return 1;
    }

    @Override
    public int ExecuteDDL(CommandBehavior behavior) {
        try {
            Connection connection = ((HiveConnection) this.dbConnection).GetConnect();
            Statement ps = connection.createStatement();
            ps.execute(this.commandText);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return 0;
        }
        return 1;
    }

    //具有返回值的列表
    private List<String> ParameterOutputList = new ArrayList<>();

    //验证sql语句格式
    private String ValidateCommand(String sqlTemplate) {

        String execSql = sqlTemplate;

        ParameterOutputList.clear();

        if (this.hiveParameterCollection == null) {
            return execSql;
        }

        Map<String, DbParameter> map = this.hiveParameterCollection.GetParameters();

        //拼接Where条件
        Map<String, DbCondition> conditionMap = new HashMap<>();
        if (this.hiveConditionCollection != null) {
            conditionMap = this.hiveConditionCollection.GetConditions();
        }

        StringBuilder whereString = new StringBuilder();

        for (String conditionName : conditionMap.keySet()) {
            for (String paramName : map.keySet()) {

                //过滤#字符
                String newConditionName = "#" + conditionName + "#";
                if (newConditionName.equalsIgnoreCase(paramName)) {

                    DbCondition dbCondition = conditionMap.get(conditionName);
                    if (dbCondition == null) {
                        continue;
                    }
                    dbCondition.setDbParameter(map.get(paramName));
                    if (dbCondition.CheckCondition()) {
                        whereString.append(dbCondition.getCondition());
                    }
                }
            }
        }

        if (whereString.length() > 0) {
            execSql = execSql + " WHERE " + whereString.substring(whereString.indexOf("AND") + 3);
        }

        if (this.orderBy != null && this.orderBy.length() > 0) {
            String newOrderBy = this.orderBy;

            for (String key : map.keySet()) {
                Object obj = map.get(key).getValue();
                if (obj == null) {
                    continue;
                }
                newOrderBy = newOrderBy.replace(key, obj.toString());
            }

            execSql = execSql + " " + newOrderBy;
        }

        //赋值
        for (String key : map.keySet()) {
            ParameterDirection direction = map.get(key).getDirection();

            //返回值 不用
            if (ParameterDirection.Output.equals(direction)) {
                ParameterOutputList.add(key);
                continue;
            }

            String paramType = map.get(key).getDbType().name();
            Object obj = map.get(key).getValue();
            if (obj == null) {
                execSql = execSql.replace(key, "null");
                continue;
            }
            if (DbType.String.name().equalsIgnoreCase(paramType)) {
                String value = obj.toString();
                execSql = execSql.replace(key, "'" + value + "'");
            } else if (DbType.Int32.name().equalsIgnoreCase(paramType)) {
                execSql = execSql.replace(key, obj.toString());
            } else if (DbType.Date.name().equalsIgnoreCase(paramType)) {
                execSql = execSql.replace(key, "'" + simpleDateFormat.format((Date) obj) + "'");
            } else if (DbType.Boolean.name().equalsIgnoreCase(paramType)) {
                execSql = execSql.replace(key, obj.toString());
            } else if (DbType.Schema.name().equalsIgnoreCase(paramType)) {
                execSql = execSql.replace(key, obj.toString());
            } else {
                System.out.println("--");
            }
        }
        //TODO sql
        log.info(execSql);
        return execSql;
    }

}
