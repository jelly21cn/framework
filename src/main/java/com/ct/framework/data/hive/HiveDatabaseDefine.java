package com.ct.framework.data.hive;

import com.ct.framework.core.data.DatabaseDefine;
import lombok.Data;

@Data
public class HiveDatabaseDefine extends DatabaseDefine {

    private String comment;

    private String location;
}
