package com.ct.framework.data.hive;

import com.ct.framework.core.data.DbCondition;
import com.ct.framework.core.data.DbConditionCollection;

import java.util.HashMap;
import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   刘宁宁
 * Create Date:  09/15/2021
 * Usage:  表示与 HiveCondition 关联的参数的集合以及各个参数到列的映射。
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             刘宁宁                         新增
 * @author 刘宁宁
 *****************************************************************/
public class HiveConditionCollection extends DbConditionCollection {

    public HiveConditionCollection() {
        super.map = new HashMap<>();
    }

    @Override
    protected Map<String, DbCondition> GetConditions() {
        return super.map;
    }

    @Override
    public DbCondition GetCondition(String conditionName) {
        return super.map.get(conditionName);
    }

    @Override
    public int Add(String conditionName, DbCondition dbCondition) {
        super.map.put(conditionName, dbCondition);
        return 1;
    }
}
