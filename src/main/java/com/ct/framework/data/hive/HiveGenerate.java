package com.ct.framework.data.hive;

import com.ct.framework.core.data.DatabaseDefine;
import com.ct.framework.core.data.DbGenerate;
import com.ct.framework.core.data.TableColumn;
import com.ct.framework.core.data.TableDefine;
import org.apache.commons.lang3.StringUtils;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 表示要对 Hive 生成DDL语句.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class HiveGenerate extends DbGenerate {
    @Override
    public String CreateDatabaseDDL(DatabaseDefine database) {
        HiveDatabaseDefine hiveDatabaseDefine = (HiveDatabaseDefine) database;

        String comment = StringUtils.isNotBlank(hiveDatabaseDefine.getComment()) ? String.format(" COMMENT '%s' ", hiveDatabaseDefine.getComment()) : "";
        String location = StringUtils.isNotBlank(hiveDatabaseDefine.getLocation()) ? String.format(" LOCATION '%s' ", hiveDatabaseDefine.getLocation()) : "";
        return String.format("CREATE DATABASE IF NOT EXISTS %s%s%s", database.getDbName(), comment, location);
    }

    @Override
    public String CreateTableDDL(TableDefine table) {
        HiveTableDefine hiveDatabaseDefine = (HiveTableDefine) table;

        StringBuilder sql = new StringBuilder();
        for (TableColumn column : hiveDatabaseDefine.getColumns()) {
            sql.append(String.format(" `%s` %s COMMENT '%s',", column.getName(), column.getType(), column.getAnnotation()));
        }
        sql.deleteCharAt(sql.length() - 1);
        return String.format("CREATE %s TABLE IF NOT EXISTS `%s` ( %s ) COMMENT '%s' ROW FORMAT DELIMITED FIELDS TERMINATED BY '%s' LINES TERMINATED BY '%s' STORED AS %s  LOCATION '%s'",
                hiveDatabaseDefine.isInternalTable() ? "EXTERNAL" : "", hiveDatabaseDefine.getTableName(), sql.toString(), hiveDatabaseDefine.getAnnotation(),
                hiveDatabaseDefine.getSeparator(), hiveDatabaseDefine.getLinebreak(), hiveDatabaseDefine.getFileStorageFormat().getName(), hiveDatabaseDefine.getLocation());
    }

    @Override
    public String InsertTableDDL(TableDefine table) {
        return null;
    }

    @Override
    public String DeleteTableDDL(TableDefine table) {
        return null;
    }

    @Override
    public String UpdateTableDDL(TableDefine table) {
        return null;
    }


}
