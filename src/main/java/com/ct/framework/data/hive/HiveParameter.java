package com.ct.framework.data.hive;

import com.ct.framework.core.data.DbParameter;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   刘宁宁
 * Create Date:  09/15/2021
 * Usage: 表示 DbCommand 的参数，还可以是它到列的映射。
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             刘宁宁                         新增
 * @author 刘宁宁
 *****************************************************************/
public class HiveParameter extends DbParameter {

    public HiveParameter() {
        super();
    }

    @Override
    public void ResetDbType() {
    }
}
