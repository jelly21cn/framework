package com.ct.framework.data.exception;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: An exception that occurred when a DataCommand file does not exist or cannot be deserialized
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Deprecated
public class DataCommandFileLoadException extends Exception {
    public DataCommandFileLoadException(String fileName) {
        //TODO 写日志
    }
}
