package com.ct.framework.data.entity;

import com.ct.framework.core.configuration.CPContext;
import com.ct.framework.core.data.DbCondition;
import com.ct.framework.data.hive.HiveCondition;
import com.ct.framework.data.mssql.MsSqlCondition;
import com.ct.framework.data.mysql.SqlCondition;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: sql语句where子条件
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class DataOperationsDataCommandWhereCondition {

    //region MEMBER VARIABLE

    private String test;

    private String condition;

    //endregion


    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public DbCondition GetDbCondition(String dbType) {
        if (CPContext.MYSQL.equalsIgnoreCase(dbType)) {
            SqlCondition sqlCondition = new SqlCondition();
            sqlCondition.setCondition(this.condition);
            sqlCondition.setTest(this.test);
            return sqlCondition;
        } else if (CPContext.ORACLE.equalsIgnoreCase(dbType)) {
            return null;
        } else if (CPContext.HIVE.equalsIgnoreCase(dbType)) {
            HiveCondition hiveCondition = new HiveCondition();
            hiveCondition.setCondition(this.condition);
            hiveCondition.setTest(this.test);
            return hiveCondition;
        } else if (CPContext.HIVE2.equalsIgnoreCase(dbType)) {
            HiveCondition hiveCondition = new HiveCondition();
            hiveCondition.setCondition(this.condition);
            hiveCondition.setTest(this.test);
            return hiveCondition;
        } else if (CPContext.MSSQL.equalsIgnoreCase(dbType)) {
            MsSqlCondition msSqlCondition = new MsSqlCondition();
            msSqlCondition.setCondition(this.condition);
            msSqlCondition.setTest(this.test);
            return msSqlCondition;
        } else {
            return null;
        }
    }
}
