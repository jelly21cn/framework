package com.ct.framework.data.entity;

import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: sql语句参数列表
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class DataOperationsDataCommandParameters {

    private List<DataOperationsDataCommandParametersParam> params;

    public List<DataOperationsDataCommandParametersParam> getParams() {
        return params;
    }

    public void setParam(List<DataOperationsDataCommandParametersParam> params) {
        this.params = params;
    }
}
