package com.ct.framework.data.entity;

import com.ct.framework.core.configuration.CPContext;
import com.ct.framework.core.data.DbParameter;
import com.ct.framework.core.data.DbType;
import com.ct.framework.core.data.ParameterDirection;
import com.ct.framework.data.hive.HiveParameter;
import com.ct.framework.data.mssql.MsSqlParameter;
import com.ct.framework.data.mysql.SqlParameter;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: sql语句参数
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class DataOperationsDataCommandParametersParam {

    //region MEMBER VARIABLE

    private String nameField;

    private DataOperationsDataCommandParametersParamDbType dbTypeField;

    private DataOperationsDataCommandParametersParamDirection directionField;

    private int sizeField;

    public String getName() {
        return nameField;
    }

    public void setName(String nameField) {
        this.nameField = nameField;
    }

    public DataOperationsDataCommandParametersParamDbType getDbType() {
        return dbTypeField;
    }

    public void setDbType(DataOperationsDataCommandParametersParamDbType dbTypeField) {
        this.dbTypeField = dbTypeField;
    }

    public DataOperationsDataCommandParametersParamDirection getDirection() {
        return directionField;
    }

    public void setDirection(DataOperationsDataCommandParametersParamDirection directionField) {
        this.directionField = directionField;
    }

    public int getSize() {
        return sizeField;
    }

    public void setSize(int sizeField) {
        this.sizeField = sizeField;
    }

    //endregion

    public DbParameter GetDbParameter() {
        SqlParameter param = new SqlParameter();
        param.setParameterName(this.nameField);
        param.setDirection(ParameterDirection.valueOf(this.directionField.name()));

        if (this.getSize() != -1) {
            param.setSize(this.sizeField);
        }

        DbType dbType = DbType.valueOf(this.dbTypeField.name());
        param.setDbType(dbType);

        return param;
    }

    public DbParameter GetDbParameter(String dbType) {
        if (CPContext.MYSQL.equalsIgnoreCase(dbType)) {
            SqlParameter param = new SqlParameter();
            param.setParameterName(this.nameField);
            param.setDirection(ParameterDirection.valueOf(this.directionField.name()));

            if (this.getSize() != -1) {
                param.setSize(this.sizeField);
            }

            DbType type = DbType.valueOf(this.dbTypeField.name());
            param.setDbType(type);
            return param;
        } else if (CPContext.ORACLE.equalsIgnoreCase(dbType)) {
            return null;
        } else if (CPContext.HIVE.equalsIgnoreCase(dbType)) {
            HiveParameter param = new HiveParameter();
            param.setParameterName(this.nameField);
            param.setDirection(ParameterDirection.valueOf(this.directionField.name()));

            if (this.getSize() != -1) {
                param.setSize(this.sizeField);
            }

            DbType type = DbType.valueOf(this.dbTypeField.name());
            param.setDbType(type);
            return param;
        } else if (CPContext.HIVE2.equalsIgnoreCase(dbType)) {
            HiveParameter param = new HiveParameter();
            param.setParameterName(this.nameField);
            param.setDirection(ParameterDirection.valueOf(this.directionField.name()));

            if (this.getSize() != -1) {
                param.setSize(this.sizeField);
            }

            DbType type = DbType.valueOf(this.dbTypeField.name());
            param.setDbType(type);
            return param;
        } else if (CPContext.MSSQL.equalsIgnoreCase(dbType)) {
            MsSqlParameter param = new MsSqlParameter();
            param.setParameterName(this.nameField);
            param.setDirection(ParameterDirection.valueOf(this.directionField.name()));

            if (this.getSize() != -1) {
                param.setSize(this.sizeField);
            }

            DbType type = DbType.valueOf(this.dbTypeField.name());
            param.setDbType(type);
            return param;
        } else {
            return null;
        }
    }
}
