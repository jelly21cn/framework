package com.ct.framework.data.entity;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: dataOperations.Xml的映射
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public class DataOperations {

    //region MEMBER VARIABLE

    private List<DataOperationsDataCommand> dataCommandField;

    public List<DataOperationsDataCommand> getDataCommandField() {
        return this.dataCommandField;
    }

    public void setDataCommandField(List<DataOperationsDataCommand> dataCommandField) {
        this.dataCommandField = dataCommandField;
    }
    //endregion

    /**
     * GetCommandNames.
     */
    public List<String> GetCommandNames() {
        if (this.dataCommandField == null || this.dataCommandField.size() == 0) {
            return new ArrayList<>(0);
        }

        List<String> result = new ArrayList<>(this.dataCommandField.size());

        this.dataCommandField.stream().distinct().forEach(item -> result.add(item.getName()));

        long count = this.dataCommandField.stream().distinct().count();

        if (count == this.dataCommandField.size()) {
            return result;
        } else {
            log.error("DataCommand 具有重名的sql");
            return null;
        }
    }

}
