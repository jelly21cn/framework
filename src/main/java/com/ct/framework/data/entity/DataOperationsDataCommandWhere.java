package com.ct.framework.data.entity;

import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: sql语句where条件
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class DataOperationsDataCommandWhere {

    private List<DataOperationsDataCommandWhereCondition> wheres;

    /**
     * 固定条件
     */
    private String stable;

    public List<DataOperationsDataCommandWhereCondition> getWheres() {
        return wheres;
    }

    public void setWheres(List<DataOperationsDataCommandWhereCondition> wheres) {
        this.wheres = wheres;
    }

    /**
     * 固定条件
     * 例如：delete=0
     */
    public String getStable() {
        return stable;
    }

    public void setStable(String stable) {
        this.stable = stable;
    }
}
