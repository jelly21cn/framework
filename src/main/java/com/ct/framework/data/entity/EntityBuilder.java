package com.ct.framework.data.entity;

import com.ct.framework.utility.string.StringHelper;
import com.ct.framework.core.data.IDataReader;
import org.apache.tomcat.util.collections.CaseInsensitiveKeyMap;

import java.lang.reflect.Field;
import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 创建实体
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class EntityBuilder {

    /**
     * 按实体返回.
     *
     * @param clazz       类型
     * @param <T>         泛型
     * @param iDataReader 输入
     */
    public static <T> T BuildEntity(IDataReader iDataReader, Class<T> clazz) {

        Map<String, Object> map = iDataReader.GetDataRecords();
        CaseInsensitiveKeyMap caseInsensitiveKeyMap = new CaseInsensitiveKeyMap();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String fieldName = StringHelper.toLowerCaseFirstOne(StringHelper.GetDBStringToCamelStyle(entry.getKey().trim()));
            caseInsensitiveKeyMap.put(fieldName, entry.getValue());
        }

        T t;
        try {
//            ParameterizedType pt = this. (ParameterizedType)  EntityBuilder.class.getGenericSuperclass();
//            Class<T> clazz = (Class<T>) pt.getActualTypeArguments()[0];
            t = clazz.newInstance();
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                String fieldName = field.getName();
                boolean exists = caseInsensitiveKeyMap.containsKey(fieldName);
                if (!exists) {
                    continue;
                }
                Object value = caseInsensitiveKeyMap.get(fieldName);

                if (value == null) {
                    continue;
                }

                //int型转boolean型
                if (field.getType().equals(Boolean.class) && !"false".equals(value.toString()) && !"true".equals(value.toString())) {
                    Integer result = Integer.valueOf(value.toString());
                    if (result > 0) {
                        value = true;
                    } else {
                        value = false;
                    }
                }

                if (field.getType().equals(Long.class)) {
                    field.set(t, Long.parseLong(value.toString()));
                } else if (field.getType().equals(Integer.class)) {
                    field.set(t, Integer.parseInt(value.toString()));
                } else {
                    field.set(t, value);
                }

            }
            //TODO
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return t;
    }

    /**
     * 直接返回.
     *
     * @param iDataReader 输入
     * @return map
     */
    public static Map<String, Object> BuildEntity(IDataReader iDataReader) {
        return iDataReader.GetDataRecords();
    }
}
