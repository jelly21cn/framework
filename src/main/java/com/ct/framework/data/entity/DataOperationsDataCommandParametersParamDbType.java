package com.ct.framework.data.entity;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 枚举
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public enum DataOperationsDataCommandParametersParamDbType {

    AnsiString,

    
    Binary,

    
    Boolean,

    
    Byte,

    
    Currency,

    
    Date,

    
    DateTime,

    
    Decimal,

    
    Double,

    
    Int16,

    
    Int32,

    
    Int64,

    
    SByte,

    
    Single,

    
    String,

    
    StringFixedLength,

    
    AnsiStringFixedLength,

    
    Time,

    //
    // 摘要: 用于对库的CRUD操作
    //
    Schema,

    
    UInt16,

    
    UInt32,

    
    UInt64,

    
    VarNumeric,

    
    Xml,
    /// <summary>
    /// 支持sql_variant 将使用SqlDbType对其参数设置
    /// </summary>
    Object
}
