package com.ct.framework.data.entity;

import com.ct.framework.data.access.DbCommandFactory;
import com.ct.framework.core.data.CommandType;
import com.ct.framework.core.data.DbCommand;
import com.ct.framework.core.data.DbConditionCollection;
import com.ct.framework.core.data.DbParameterCollection;
import com.ct.framework.data.access.DataCommand;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class DataOperationsDataCommand {

    //region MEMBER VARIABLE

    private String commandTextField;

    private DataOperationsDataCommandParameters parametersField;

    private String nameField;

    private String databaseField;

    private DataOperationsDataCommandCommandType commandTypeField;

    private int timeOutField;

    private String dbTypeField;

    private String orderBy;

    private DataOperationsDataCommandWhere where;

    /**
     * 前置Sql
     */
    private String beforeText;

    /**
     * 后置Sql
     */
    private String afterText;

    public String getCommandText() {
        return commandTextField;
    }

    public void setCommandText(String commandTextField) {
        this.commandTextField = commandTextField;
    }


    public DataOperationsDataCommandParameters getParameters() {
        return this.parametersField;
    }

    public void setParameters(DataOperationsDataCommandParameters parametersField) {
        this.parametersField = parametersField;
    }


    public DataOperationsDataCommandWhere getWhere() {
        return this.where;
    }

    public void setWhere(DataOperationsDataCommandWhere where) {
        this.where = where;
    }


    public String getName() {
        return nameField;
    }

    public void setName(String nameField) {
        this.nameField = nameField;
    }


    public String getDatabase() {
        return databaseField;
    }

    public void setDatabase(String databaseField) {
        this.databaseField = databaseField;
    }


    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }


    public DataOperationsDataCommandCommandType getCommandType() {
        return commandTypeField;
    }

    public void setCommandType(DataOperationsDataCommandCommandType commandTypeField) {
        this.commandTypeField = commandTypeField;
    }


    public int getTimeOut() {
        return timeOutField;
    }

    public void setTimeOut(int timeOutField) {
        this.timeOutField = timeOutField;
    }


    public String getDbType() {
        return dbTypeField;
    }

    public void setDbType(String dbTypeField) {
        this.dbTypeField = dbTypeField;
    }


    public String getBeforeText() {
        return beforeText;
    }

    public void setBeforeText(String beforeText) {
        this.beforeText = beforeText;
    }

    public String getAfterText() {
        return afterText;
    }

    public void setAfterText(String afterText) {
        this.afterText = afterText;
    }

    //endregion

    /**
     * returns a new instance of DataCommand this object represents.
     */
    public DataCommand GetDataCommand(String dbType) {
        DbCommand dbCommand = GetDbCommand(dbType);
        return new DataCommand(this.databaseField, dbCommand);
    }

    /**
     * returns a new instance of DbCommand this object represents
     */
    private DbCommand GetDbCommand(String dbType) {
        DbCommand cmd = DbCommandFactory.CreateDbCommand(dbType);
        assert cmd != null;
        cmd.SetCommandText(this.commandTextField);
        cmd.SetCommandTimeout(this.timeOutField);
        cmd.SetCommandType(CommandType.valueOf(this.commandTypeField.name()));
        cmd.SetOrderBy(this.orderBy);
        cmd.SetBeforeText(this.beforeText);
        cmd.SetAfterText(this.afterText);

        if (this.parametersField != null && this.parametersField.getParams() != null && this.parametersField.getParams().size() > 0) {
            DbParameterCollection dbParameterCollection = cmd.GetDbParameterCollection();
            for (DataOperationsDataCommandParametersParam param : this.parametersField.getParams()) {
                dbParameterCollection.Add(param.getName(), param.GetDbParameter(dbType));
            }
        }

        if (this.where != null && this.where.getWheres() != null && this.where.getWheres().size() > 0) {
            DbConditionCollection dbConditionCollection = cmd.GetDbConditionCollection();
            for (DataOperationsDataCommandWhereCondition condition : this.where.getWheres()) {
                dbConditionCollection.Add(condition.getTest(), condition.GetDbCondition(dbType));
            }
        }

        if (this.where != null && this.where.getStable() != null && this.where.getStable().trim().length() > 0) {
            cmd.SetStable(this.where.getStable());
        }

        return cmd;

    }
}
