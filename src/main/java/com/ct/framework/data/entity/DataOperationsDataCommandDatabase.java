package com.ct.framework.data.entity;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 枚举
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public enum DataOperationsDataCommandDatabase {
    
    Common,

    
    ControlPanel,

    
    ContentManagement,

    
    CustomerMasterProfile,

    
    InventoryManagement,

    
    InvoiceManagement,

    
    OrderManagement,

    
    POASNManagement,

    
    ServiceManagement,

    
    OZZO,

    
    OverseaLocalArchitecture,

    
    Portal,

    ServiceManagementSendSSB,

    ServiceManagementReceiveSSB,

    NEWSQL,

    D2WHP01,

    D2HIS01,

    EHISSQL,

    SSB,

    EIMSManagement
}
