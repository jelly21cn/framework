package com.ct.framework.data.entity;


/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 实体-单个数据库配置
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class DatabaseInstance {

    /**
     * 名称.
     */
    private String name;

    /**
     * 登录名.
     */
    private String userName;

    /**
     * 登录密码.
     */
    private String password;

    /**
     * 链接字符串.
     */
    private String connectionString;

    /**
     * 数据库类型.
     */
    private String dataBaseType;

    public String GetName() {
        return name;
    }

    public void SetName(String name) {
        this.name = name;
    }

    public String GetUserName() {
        return userName;
    }

    public void SetUserName(String userName) {
        this.userName = userName;
    }

    public String GetPassword() {
        return password;
    }

    public void SetPassword(String password) {
        this.password = password;
    }

    public String GetConnectionString() {
        return connectionString;
    }

    public void SetConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    public String GetDataBaseType() {
        return dataBaseType;
    }

    public void SetDataBaseType(String dataBaseType) {
        this.dataBaseType = dataBaseType;
    }
}
