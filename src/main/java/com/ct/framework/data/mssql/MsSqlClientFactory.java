package com.ct.framework.data.mssql;

import com.ct.framework.core.data.*;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: MsSqlClientFactory.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class MsSqlClientFactory extends DbProviderFactory {

    public static MsSqlClientFactory Instance = new MsSqlClientFactory();

    public MsSqlClientFactory() {
    }

    @Override
    public DbCommand CreateCommand() {
        return new MsSqlCommand();
    }

    @Override
    public DbCommandBuilder CreateCommandBuilder() {
        return new MsSqlCommandBuilder();
    }

    @Override
    public DbConnection CreateConnection() {
        return new MsSqlConnection();
    }

    @Override
    public DbConnectionStringBuilder CreateConnectionStringBuilder() {
        return new MsSqlConnectionStringBuilder();
    }

    @Override
    public DbDataAdapter CreateDataAdapter() {
        return new MsSqlDataAdapter();
    }

    @Override
    public DbParameter CreateParameter() {
        return new MsSqlParameter();
    }
}
