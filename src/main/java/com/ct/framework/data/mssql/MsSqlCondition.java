package com.ct.framework.data.mssql;

import com.ct.framework.core.data.DbCondition;
import com.ct.framework.core.data.DbType;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: SqlCondition where条件判断.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class MsSqlCondition extends DbCondition {
    @Override
    public boolean CheckCondition() {
        Object obj = this.getDbParameter().getValue();
        if (obj == null || obj.equals("%%")) {
            return false;
        } else {
            DbType dbType = this.getDbParameter().getDbType();
            String valString = String.valueOf(obj);
            if (dbType == DbType.Int32) {
                int val = Integer.parseInt(valString);
                if (val == Integer.MIN_VALUE) {
                    return false;
                } else {
                    return true;
                }
            } else if (dbType == DbType.Int64) {
                long val = Long.parseLong(valString);
                if (val == Long.MIN_VALUE) {
                    return false;
                } else {
                    return true;
                }
            } else {
                int len = valString.length();
                if (len > 0) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
}
