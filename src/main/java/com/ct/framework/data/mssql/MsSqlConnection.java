package com.ct.framework.data.mssql;

import com.ct.framework.core.data.DbConnection;
import com.ct.framework.core.exception.ErrorCode;
import com.ct.framework.core.exception.GlobalException;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 表示到 Sql 数据库的连接,此类不能被继承.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public class MsSqlConnection extends DbConnection<Connection> {

    private String connectionString;
    private String userName;
    private String password;
    private Connection connection;

    public MsSqlConnection() {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Connection GetConnect() {
        return this.connection;
    }

    @Override
    public void Open() {
        try {
            this.connection = DriverManager.getConnection(this.connectionString, this.userName, this.password);
        } catch (SQLException ex) {
            log.info(ex.getMessage());
            throw new GlobalException(ex.getMessage(), ErrorCode.SQL_ERROR);
        }
    }

    @Override
    public void Close() {
        try {
            if (this.connection != null) {
                this.connection.close();
            }
        } catch (SQLException ex) {
            log.info(ex.getMessage());
            throw new GlobalException(ex.getMessage(), ErrorCode.SQL_ERROR);
        }
    }

    @Override
    public void SetConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    @Override
    public void SetUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public void SetPassword(String password) {
        this.password = password;
    }

    @Override
    public void Commit() {
        try {
            if (this.connection != null) {
                this.connection.commit();
            }
        } catch (SQLException ex) {
            log.info(ex.getMessage());
            throw new GlobalException(ex.getMessage(), ErrorCode.SQL_TRANSACTION_ERROR);
        }
    }

    @Override
    public void RollBack() {
        try {
            if (this.connection != null) {
                this.connection.rollback();
            }
        } catch (SQLException ex) {
            log.info(ex.getMessage());
            throw new GlobalException(ex.getMessage(), ErrorCode.SQL_TRANSACTION_ERROR);
        }
    }
}
