package com.ct.framework.data.mssql;

import com.ct.framework.core.data.DbConnectionStringBuilder;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: DbConnectionStringBuilder
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class MsSqlConnectionStringBuilder extends DbConnectionStringBuilder {
}
