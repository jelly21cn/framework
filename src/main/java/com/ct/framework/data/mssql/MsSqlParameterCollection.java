package com.ct.framework.data.mssql;

import com.ct.framework.core.data.DbParameter;
import com.ct.framework.core.data.DbParameterCollection;

import java.util.HashMap;
import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage:  表示与 SqlCommand 关联的参数的集合以及各个参数到列的映射。
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class MsSqlParameterCollection extends DbParameterCollection {

    public MsSqlParameterCollection() {
        super.map = new HashMap<>();
    }

    @Override
    protected Map<String, DbParameter> GetParameters() {
        return super.map;
    }

    @Override
    public DbParameter GetParameter(String parameterName) {
        return super.map.get(parameterName);
    }

    @Override
    public int Add(String parameterName, DbParameter dbParameter) {
        super.map.put(parameterName, dbParameter);
        return 1;
    }
}
