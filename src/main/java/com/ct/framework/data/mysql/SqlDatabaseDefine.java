package com.ct.framework.data.mysql;

import com.ct.framework.core.data.DatabaseDefine;
import lombok.Data;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 表示要对 Mysql 生成DDL语句.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Data
public class SqlDatabaseDefine extends DatabaseDefine {

    private String charset;

    private String collation;
}
