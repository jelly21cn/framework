package com.ct.framework.data.mysql;

import com.ct.framework.core.data.DbParameter;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 表示 DbCommand 的参数，还可以是它到列的映射。
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class SqlParameter extends DbParameter {

    public SqlParameter() {
        super();
    }

    @Override
    public void ResetDbType() {
    }
}
