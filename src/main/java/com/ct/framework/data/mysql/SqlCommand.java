package com.ct.framework.data.mysql;

import com.ct.framework.core.data.*;
import com.ct.framework.core.exception.ErrorCode;
import com.ct.framework.core.exception.GlobalException;
import lombok.extern.slf4j.Slf4j;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 表示要对 Mysql 数据库执行的一个 Transact-SQL 语句或存储过程,此类不能被继承.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public class SqlCommand extends DbCommand {

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * SQL 模板
     */
    private String commandText;

    private DbConnection dbConnection;

    private SqlParameterCollection sqlParameterCollection;

    private String orderBy;

    private SqlConditionCollection sqlConditionCollection;

    private CommandType commandType;

    private String stable;

    private boolean autoCommit = true;

    public SqlCommand() {
        super();
    }

    @Override
    public void SetConnection(DbConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    @Override
    public DbConnection GetDbConnection() {
        return this.dbConnection;
    }

    @Override
    public void SetCommandType(CommandType commandType) {
        this.commandType = commandType;
    }

    @Override
    public void SetCommandText(String commandText) {
        this.commandText = commandText;
    }

    @Override
    public void SetCommandTimeout(int commandTimeout) {
    }

    @Override
    public void SetOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    @Override
    public void SetBeforeText(String before) {

    }

    @Override
    public void SetAfterText(String after) {

    }

    @Override
    public DbConditionCollection GetDbConditionCollection() {
        if (this.sqlConditionCollection == null) {
            this.sqlConditionCollection = new SqlConditionCollection();
        }
        return this.sqlConditionCollection;
    }

    @Override
    public DbParameterCollection GetDbParameterCollection() {
        if (this.sqlParameterCollection == null) {
            this.sqlParameterCollection = new SqlParameterCollection();
        }
        return this.sqlParameterCollection;
    }

    @Override
    public void SetStable(String stable) {
        this.stable = stable;
    }


    //region ExecuteReader
    @Override
    public IDataReader ExecuteReader(CommandBehavior behavior) {
        SqlDataReader sqlDataReader = null;
        try {
            String execSql = ValidateCommand(this.commandText);
            Connection connection = ((SqlConnection) this.dbConnection).GetConnect();
            connection.setAutoCommit(this.autoCommit);
            ResultSet resultSet = null;
            if (this.commandType == CommandType.Text) {
                PreparedStatement ps = connection.prepareStatement(execSql);
                resultSet = ps.executeQuery();
            }
            if (this.commandType == CommandType.StoredProcedure) {
                CallableStatement cs = connection.prepareCall(execSql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
                resultSet = cs.executeQuery();
            }
            if (resultSet != null) {
                sqlDataReader = new SqlDataReader(resultSet);
            }

        } catch (SQLException ex) {
            log.info(ex.getMessage());
            throw new GlobalException(ex.getMessage(), ErrorCode.SQL_ERROR);
        }

        return sqlDataReader;
    }

    @Override
    public Object ExecuteScalar(CommandBehavior behavior) {
        Object result = null;
        try {
            String execSql = ValidateCommand(this.commandText);
            Connection connection = ((SqlConnection) this.dbConnection).GetConnect();
            connection.setAutoCommit(this.autoCommit);
            if (this.commandType == CommandType.Text) {
                PreparedStatement ps = connection.prepareStatement(execSql);
                ResultSet resultSet = ps.executeQuery();
                SqlDataReader sqlDataReader = new SqlDataReader(resultSet);
                if (sqlDataReader.Read()) {
                    result = sqlDataReader.GetObject(1);
                }
            }

            if (this.commandType == CommandType.StoredProcedure) {
                CallableStatement cs = connection.prepareCall(execSql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
                cs.execute();
                result = cs.getObject(1);
            }
        } catch (SQLException ex) {
            log.info(ex.getMessage());
            throw new GlobalException(ex.getMessage(), ErrorCode.SQL_ERROR);
        }


        return result;
    }

    @Override
    public int ExecuteNonQuery(CommandBehavior behavior) {
        int result = 0;
        try {
            String execSql = ValidateCommand(this.commandText);
            Connection connection = ((SqlConnection) this.dbConnection).GetConnect();
            connection.setAutoCommit(this.autoCommit);
            if (this.commandType == CommandType.Text) {
                PreparedStatement ps = connection.prepareStatement(execSql, Statement.RETURN_GENERATED_KEYS);
                result = ps.executeUpdate();

                ResultSet resultSet = ps.getGeneratedKeys();
                SqlDataReader sqlDataReader = new SqlDataReader(resultSet);

                if (sqlDataReader.Read()) {
                    long value = sqlDataReader.GetLong(1);

                    if (ParameterOutputList.size() > 0) {
                        for (String paramName : this.ParameterOutputList) {
                            this.sqlParameterCollection.GetParameter(paramName).setValue(value);
                        }
                    }

                }
            }

            if (this.commandType == CommandType.StoredProcedure) {

                CallableStatement cs = connection.prepareCall(execSql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);

                if (ParameterOutputList.size() > 0) {
                    for (int i = 1; i <= ParameterOutputList.size(); i++) {
                        //返回值 目前只支持 int
                        cs.registerOutParameter(i, Types.INTEGER);
                    }
                }

                //true if the first result is a ResultSet object;
                //false if the first result is an update count or there is no result
                result = cs.execute() ? 1 : 0;

                for (String paramName : this.ParameterOutputList) {
                    Object obj = cs.getObject("@" + paramName);
                    this.sqlParameterCollection.GetParameter(paramName).setValue(obj);
                }

            }

        } catch (SQLException ex) {
            log.info(ex.getMessage());
            throw new GlobalException(ex.getMessage(), ErrorCode.SQL_ERROR);
        }

        return result;
    }

    @Override
    public int ExecuteDDL(CommandBehavior behavior) {
        try {
            Connection connection = ((SqlConnection) this.dbConnection).GetConnect();
            Statement ps = connection.createStatement();
            ps.execute(this.commandText);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return 0;
        }
        return 1;
    }

    //endregion

    @Override
    public void CloseConnection() {
        this.dbConnection.Close();
    }

    @Override
    public void Commit() {
        this.dbConnection.Commit();
    }

    @Override
    public void RollBack() {
        this.dbConnection.RollBack();
    }

    //具有返回值的列表
    private List<String> ParameterOutputList = new ArrayList<>();

    //验证sql语句格式
    private String ValidateCommand(String sqlTemplate) {

        String execSql = sqlTemplate;

        ParameterOutputList.clear();

        if (this.sqlParameterCollection == null) {
            return execSql;
        }

        Map<String, DbParameter> map = this.sqlParameterCollection.GetParameters();

        //拼接Where条件
        Map<String, DbCondition> conditionMap = new HashMap<>();
        if (this.sqlConditionCollection != null) {
            conditionMap = this.sqlConditionCollection.GetConditions();
        }

        StringBuilder whereString = new StringBuilder();

        for (String conditionName : conditionMap.keySet()) {
            for (String paramName : map.keySet()) {

                //过滤#字符
                String newConditionName = "#" + conditionName + "#";
                if (newConditionName.equalsIgnoreCase(paramName)) {

                    DbCondition dbCondition = conditionMap.get(conditionName);
                    if (dbCondition == null) {
                        continue;
                    }
                    dbCondition.setDbParameter(map.get(paramName));
                    if (dbCondition.CheckCondition()) {
                        whereString.append(dbCondition.getCondition());
                    }
                }
            }
        }

        if (whereString.length() > 0) {
            execSql = execSql + " WHERE " + whereString.substring(whereString.indexOf("AND") + 3);

            if (this.stable != null && this.stable.trim().length() > 0) {
                execSql += " AND " + this.stable;
            }
        } else {
            //增加where 固定条件
            if (this.stable != null && this.stable.trim().length() > 0) {
                execSql += " WHERE " + this.stable;
            }
        }


        if (this.orderBy != null && this.orderBy.length() > 0) {
            String newOrderBy = this.orderBy;

            for (String key : map.keySet()) {
                Object obj = map.get(key).getValue();
                if (obj == null) {
                    continue;
                }
                newOrderBy = newOrderBy.replace(key, obj.toString());
            }

            execSql = execSql + " " + newOrderBy;
        }

        //赋值
        for (String key : map.keySet()) {
            ParameterDirection direction = map.get(key).getDirection();

            //返回值 不用
            if (ParameterDirection.Output.equals(direction)) {
                ParameterOutputList.add(key);
                continue;
            }

            String paramType = map.get(key).getDbType().name();
            Object obj = map.get(key).getValue();
            if (obj == null) {
                execSql = execSql.replace(key, "null");
                continue;
            }
            if (DbType.String.name().equalsIgnoreCase(paramType)) {
                String value = obj.toString();
                execSql = execSql.replace(key, "'" + value + "'");
            } else if (DbType.Int32.name().equalsIgnoreCase(paramType)) {
                execSql = execSql.replace(key, obj.toString());
            } else if (DbType.Int64.name().equalsIgnoreCase(paramType)) {
                execSql = execSql.replace(key, obj.toString());
            } else if (DbType.Date.name().equalsIgnoreCase(paramType)) {
                execSql = execSql.replace(key, "'" + simpleDateFormat.format((Date) obj) + "'");
            } else if (DbType.Boolean.name().equalsIgnoreCase(paramType)) {
                execSql = execSql.replace(key, obj.toString());
            } else if (DbType.Schema.name().equalsIgnoreCase(paramType)) {
                execSql = execSql.replace(key, obj.toString());
            } else {
                System.out.println("--");
            }
        }
        //TODO sql
        log.info(execSql);
        return execSql;
    }

    @Override
    public void UsingTransaction() {
        this.autoCommit = false;
    }
}
