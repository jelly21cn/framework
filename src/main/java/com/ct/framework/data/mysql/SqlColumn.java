package com.ct.framework.data.mysql;

import com.ct.framework.core.data.DbColumn;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: Mysql数据库字段.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class SqlColumn extends DbColumn {
    /**
     * 读取Mysql所有类型，对应成 标准的数据类型的字符.
     * 整数类型：BIT、BOOL、TINY INT、SMALL INT、MEDIUM INT、 INT、 BIG INT
     * 浮点数类型：FLOAT、DOUBLE、DECIMAL
     * 字符串类型：CHAR、VARCHAR、TINY TEXT、TEXT、MEDIUM TEXT、LONGTEXT、TINY BLOB、BLOB、MEDIUM BLOB、LONG BLOB
     * 日期类型：Date、DateTime、TimeStamp、Time、Year
     * 其他数据类型：BINARY、VARBINARY、ENUM、SET、Geometry、Point、MultiPoint、LineString、MultiLineString、Polygon、GeometryCollection等
     */
    @Override
    public String IntToStringMapping(int dataType) {
        switch (dataType) {
            case -1:
                return "TEXT";
            case -2:
                return "TINYBLOB";
            case -5:
                return "BIGINT";
            case -6:
                return "TINYINT";
            case -7:
                return "BIT";
            case 1:
                return "DECIMAL";
            case 3:
                return "CHAR";
            case 4:
                return "INT";
            case 5:
                return "SMALLINT";
            case 7:
                return "FLOAT";
            case 8:
                return "DOUBLE";
            case 12:
                return "VARCHAR";
            case 91:
                return "DATE";
            case 92:
                return "TIME";
            case 93:
                return "DATETIME";
            default:
                return "Unknow";
        }
    }

    /**
     * 把mysql的字段类型转换为标准字段类型.
     *
     * @param dataType mysql的字段类型
     * @return int  标准字段类型
     */
    @Override
    public int IntToIntMapping(int dataType) {
        switch (dataType) {
            case -1:
                return 13;
            case -2:
                return 12;
            case -6:
            case 5:
                return 1;
            case 4:
            case -5:
            case -7:
                return 0;
            case 1:
                return 2;
            case 3:
                return 8;
            case 7:
            case 8:
                return 3;
            case 12:
                return 9;
            case 91:
                return 5;
            case 92:
                return 7;
            case 93:
                return 4;
            default:
                return -99;
        }
    }

    /**
     * 逆向，把标准转换为各自数据库的字段类型..
     */
    @Override
    public String Reverse(int dataType) {
        switch (dataType) {
            case 0:
                return "INT";
            case 1:
                return "SMALLINT";
            case 2:
                return "DECIMAL";
            case 3:
                return "NUMERIC";
            case 4:
                return "DATETIME";
            case 5:
                return "DATE";
            case 6:
                return "TIMESTAMP";
            case 7:
                return "TIME";
            case 8:
                return "CHAR";
            case 9:
                return "VARCHAR";
            case 10:
                return "BINARY";
            case 11:
                return "VARBINARY";
            case 12:
                return "BLOB";
            case 13:
                return "TEXT";
            case 20:
                return "ENUM";
            case 99:
                return "Dimension";
            default:
                return "Unknow";
        }
    }
}
