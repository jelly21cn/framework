package com.ct.framework.data.mysql;

import com.ct.framework.utility.DateHelper;
import com.ct.framework.core.data.DbDataReader;
import com.ct.framework.core.exception.ErrorCode;
import com.ct.framework.core.exception.GlobalException;
import lombok.extern.slf4j.Slf4j;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: SqlDataReader.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public class SqlDataReader extends DbDataReader {

    private ResultSet resultSet;

    private List<String> columnsList;

    public SqlDataReader(ResultSet resultSet) {
        this.resultSet = resultSet;
        this.columnsList = new ArrayList<>();
    }

    @Override
    public Map<String, Object> GetDataRecords() {
        Map<String, Object> results = new HashMap<>();

        try {
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            int columns = resultSetMetaData.getColumnCount();

            for (int x = 1; x <= columns; x++) {
                String columnName = resultSetMetaData.getColumnName(x).toLowerCase();
                int columnType = resultSetMetaData.getColumnType(x);
                Object value = this.resultSet.getObject(columnName);
                if (value == null) {
                    results.put(columnName, null);
                    continue;
                }
                switch (columnType) {
                    case -1:
                        //TEXT
                        results.put(columnName, value.toString());
                        break;
                    case -2:
                        //TINYBLOB
                        results.put(columnName, value);
                        break;
                    case -5:
                        //BIGINT
                        results.put(columnName, new Long(value.toString()));
                        break;
                    case -6:
                        //TINYINT
                        results.put(columnName, new Integer(value.toString()));
                        break;
                    case -7:
                        //BIT
                        results.put(columnName, new Boolean(value.toString()));
                        break;
                    case 1:
                        //DECIMAL
                        results.put(columnName, new Double(value.toString()));
                        break;
                    case 3:
                        //CHAR
                        results.put(columnName, value.toString());
                        break;
                    case 4:
                    case 5:
                        //SMALLINT
                        results.put(columnName, new Integer(value.toString()));
                        break;
                    case 7:
                        //FLOAT
                        results.put(columnName, new Float(value.toString()));
                        break;
                    case 8:
                        //DOUBLE
                        results.put(columnName, new Double(value.toString()));
                        break;
                    case 12:
                        //VARCHAR
                        results.put(columnName, value.toString());
                        break;
                    case 91:
                        //DATE
                        results.put(columnName, DateHelper.parseYyyyMMddHHmmss(value.toString()));
                        break;
                    case 92:
                        //TIME
                        results.put(columnName, DateHelper.parseYyyyMMddHHmmss(value.toString()));
                        break;
                    case 93:
                        //DATETIME
                        results.put(columnName, DateHelper.parseYyyyMMddHHmmss(value.toString()));
                        break;
                    default:
                        //UNKNOW
                        results.put(columnName, value);
                        break;
                }

                this.columnsList.add(columnName);
            }
        } catch (Exception ex) {
            log.info(ex.getMessage());
            throw new GlobalException(ex.getMessage(), ErrorCode.SQL_ERROR);
        }
        return results;
    }

    @Override
    public List<String> GetMetaData() {
        return this.columnsList;
    }

    @Override
    public long GetLong(int index) {
        long id = 0L;
        try {
            id = resultSet.getLong(index);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return id;
    }

    @Override
    public Object GetObject(int index) {
        Object t = null;
        try {
            t = resultSet.getObject(index);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return t;
    }

    @Override
    public void Close() {

    }

    @Override
    public boolean Read() {
        try {
            return resultSet.next();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

}
