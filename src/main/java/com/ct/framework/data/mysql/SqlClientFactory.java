package com.ct.framework.data.mysql;

import com.ct.framework.core.data.*;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: SqlClientFactory.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class SqlClientFactory extends DbProviderFactory {

    public static SqlClientFactory Instance = new SqlClientFactory();

    public SqlClientFactory() {
    }

    @Override
    public DbCommand CreateCommand() {
        return new SqlCommand();
    }

    @Override
    public DbCommandBuilder CreateCommandBuilder() {
        return new SqlCommandBuilder();
    }

    @Override
    public DbConnection CreateConnection() {
        return new SqlConnection();
    }

    @Override
    public DbConnectionStringBuilder CreateConnectionStringBuilder() {
        return new SqlConnectionStringBuilder();
    }

    @Override
    public DbDataAdapter CreateDataAdapter() {
        return new SqlDataAdapter();
    }

    @Override
    public DbParameter CreateParameter() {
        return new SqlParameter();
    }
}
