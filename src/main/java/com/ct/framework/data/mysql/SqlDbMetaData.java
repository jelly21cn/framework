package com.ct.framework.data.mysql;

import com.ct.framework.core.data.DbConnection;
import com.ct.framework.core.data.DbMetaData;
import com.ct.framework.core.entity.MetaData;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: SqlDbMetaData.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class SqlDbMetaData extends DbMetaData {
    /**
     * 返回列的元数据信息.
     *
     * @param tableName  表名
     * @param connection 连接
     * @return list
     */
    @Override
    public List<MetaData> GetColumnMetaDataList(DbConnection connection, String tableName) {
        Connection conn = (Connection) connection.GetConnect();
        try {
            DatabaseMetaData meta = conn.getMetaData();
            ResultSet rs = meta.getColumns(null, null, tableName, null);
            List<MetaData> columns = new ArrayList<>();
            while (rs.next()) {
                MetaData metaData = new MetaData();

                String tableCat = rs.getString("TABLE_CAT");
                metaData.setTableCat(tableCat);
                String tableSchemaName = rs.getString("TABLE_SCHEM");
                metaData.setTableSchemaName(tableSchemaName);
                String tableName_ = rs.getString("TABLE_NAME");
                metaData.setTableName(tableName_);
                String columnName = rs.getString("COLUMN_NAME");
                metaData.setColumnName(columnName);
                int dataType = rs.getInt("DATA_TYPE");
                metaData.setDataType(dataType);
                String dataTypeName = rs.getString("TYPE_NAME");
                metaData.setDataTypeName(dataTypeName);
                int columnSize = rs.getInt("COLUMN_SIZE");
                metaData.setColumnSize(columnSize);
                int decimalDigits = rs.getInt("DECIMAL_DIGITS");
                metaData.setDecimalDigits(decimalDigits);
                int numPrecRadix = rs.getInt("NUM_PREC_RADIX");
                metaData.setNumPrecRadix(numPrecRadix);
                int nullAble = rs.getInt("NULLABLE");
                metaData.setNullAble(nullAble);
                String remarks = rs.getString("REMARKS");
                metaData.setRemarks(remarks);
                String columnDef = rs.getString("COLUMN_DEF");
                metaData.setColumnDef(columnDef);
                int sqlDataType = rs.getInt("SQL_DATA_TYPE");
                metaData.setSqlDataType(sqlDataType);
                int sqlDatetimeSub = rs.getInt("SQL_DATETIME_SUB");
                metaData.setSqlDatetimeSub(sqlDatetimeSub);
                int charOctetLength = rs.getInt("CHAR_OCTET_LENGTH");
                metaData.setCharOctetLength(charOctetLength);
                int ordinalPosition = rs.getInt("ORDINAL_POSITION");
                metaData.setOrdinalPosition(ordinalPosition);
                String isNullAble = rs.getString("IS_NULLABLE");
                metaData.setIsNullAble(isNullAble);
                String isAutoincrement = rs.getString("IS_AUTOINCREMENT");
                metaData.setIsAutoincrement(isAutoincrement);

                columns.add(metaData);
            }

            conn.close();
            return columns;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 返回表的元数据信息.
     *
     * @param connection 连接
     * @return list
     */
    @Override
    public List<MetaData> GetTableMetaDataList(DbConnection connection) {
        Connection conn = (Connection) connection.GetConnect();
        try {
            DatabaseMetaData meta = conn.getMetaData();
            ResultSet rs = meta.getTables(null, null, null, new String[]{"TABLE"});
            List<MetaData> tables = new ArrayList<>();
            while (rs.next()) {
                MetaData metaData = new MetaData();
                String database = rs.getString(1);
                String tableName = rs.getString(3);
                metaData.setTableName(tableName);
                metaData.setTableSchemaName(database);
                tables.add(metaData);
            }
            conn.close();
            return tables;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


}
