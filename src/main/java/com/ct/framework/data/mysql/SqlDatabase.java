package com.ct.framework.data.mysql;

import com.ct.framework.core.data.Database;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 表示要对 Mysql 的一个数据库实例,此类不能被继承.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class SqlDatabase extends Database {

    public SqlDatabase() {

    }

    /**
     * SqlDatabase.
     *
     * @param userName         用户名
     * @param password         密码
     * @param connectionString 连接字符串
     */
    public SqlDatabase(String connectionString, String userName, String password) {
        super(connectionString, userName, password, SqlClientFactory.Instance);
    }

    /**
     * BuildParameterName.
     *
     * @param name 参数名称
     * @return string
     */
    @Override
    public String BuildParameterName(String name) {
        return "#" + name + "#";
    }
}
