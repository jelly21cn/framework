package com.ct.framework.data.mysql;

import com.ct.framework.core.data.DbCondition;
import com.ct.framework.core.data.DbConditionCollection;

import java.util.HashMap;
import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage:  表示与 SqlCondition 关联的参数的集合以及各个参数到列的映射。
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class SqlConditionCollection extends DbConditionCollection {

    public SqlConditionCollection() {
        super.map = new HashMap<>();
    }

    @Override
    protected Map<String, DbCondition> GetConditions() {
        return super.map;
    }

    @Override
    public DbCondition GetCondition(String conditionName) {
        return super.map.get(conditionName);
    }

    @Override
    public int Add(String conditionName, DbCondition dbCondition) {
        super.map.put(conditionName, dbCondition);
        return 1;
    }
}
