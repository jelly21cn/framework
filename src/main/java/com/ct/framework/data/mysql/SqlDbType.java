package com.ct.framework.data.mysql;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: Mysql枚举值。
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public enum SqlDbType  {
    BigInt,
    Binary,
    Bit,
    Char,
    DateTime,
    Decimal,
    Float,
    Image,
    Int,
    Money,
    NChar,
    NText,
    NVarChar,
    Real,
    UniqueIdentifier,
    SmallDateTime,
    SmallInt,
    SmallMoney,
    Text,
    Timestamp,
    TinyInt,
    VarBinary,
    VarChar,
    Variant,
    Xml,
    Udt,
    Structured,
    Date,
    Time,
    DateTime2,
    DateTimeOffset
}
