package com.ct.framework.data.mysql;

import com.ct.framework.core.data.DatabaseDefine;
import com.ct.framework.core.data.DbGenerate;
import com.ct.framework.core.data.TableColumn;
import com.ct.framework.core.data.TableDefine;
import org.apache.commons.lang3.StringUtils;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 表示要对 Mysql 生成DDL语句.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public class SqlGenerate extends DbGenerate {
    @Override
    public String CreateDatabaseDDL(DatabaseDefine database) {
        SqlDatabaseDefine sqlDatabase = (SqlDatabaseDefine) database;

        String charset = StringUtils.isNotBlank(sqlDatabase.getCharset()) ? String.format("DEFAULT CHARACTER SET %s ", sqlDatabase.getCharset()) : "";
        String collation = StringUtils.isNotBlank(sqlDatabase.getCollation()) ? String.format("COLLATE %s ", sqlDatabase.getCollation()) : "";
        return String.format("CREATE DATABASE IF NOT EXISTS %s %s%s;", sqlDatabase.getDbName(), charset, collation);
    }

    @Override
    public String CreateTableDDL(TableDefine table) {
        SqlTableDefine sqlTableDefine = (SqlTableDefine) table;

        StringBuilder sql = new StringBuilder();
        String primaryKey = "";
        for (TableColumn column : sqlTableDefine.getColumns()) {
            String isnull = column.isIsnull() ? "DEFAULT" : "NOT";
            String isAutoIncrement = column.isAutoIncrement() ? "AUTO_INCREMENT" : "";
            String type = column.getType() + ("CHAR".equals(column.getType()) || "VARCHAR".equals(column.getType()) ? "(" + column.getLength() + ")" : ("DECIMAL".equals(column.getType()) ? "(10, 0)" : ""));
            sql.append(String.format(" `%s` %s %s NULL %s COMMENT '%s',", column.getName(), type, isnull, isAutoIncrement, column.getAnnotation()));
            if (column.isPrimaryKey()) {
                primaryKey = String.format(" PRIMARY KEY (`%s`)", column.getName());
            }
        }
        if (StringUtils.isNotBlank(primaryKey)) {
            sql.append(primaryKey);
        } else {
            sql.deleteCharAt(sql.length() - 1);
        }
        return String.format("CREATE TABLE IF NOT EXISTS `%s` ( %s ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='%s';", sqlTableDefine.getTableName(), sql.toString(), sqlTableDefine.getAnnotation());
    }

    @Override
    public String InsertTableDDL(TableDefine table) {
        SqlTableDefine sqlTableDefine = (SqlTableDefine) table;
        StringBuilder sql = new StringBuilder();
        for (TableColumn column : sqlTableDefine.getColumns()) {
            sql.append("alter table "+sqlTableDefine.getTableName()+" add "+column.getName()+"  "+column.getType()+" ( "+column.getLength()+" ) COMMENT"+"'"+column.getAnnotation()+"'");
        }
        return sql.toString();
    }

    @Override
    public String DeleteTableDDL(TableDefine table) {
        StringBuilder sql = new StringBuilder();
        sql.append("drop table "+table.getTableName());
        return sql.toString();
    }

    @Override
    public String UpdateTableDDL(TableDefine table) {
        SqlTableDefine sqlTableDefine = (SqlTableDefine) table;
        StringBuilder sql = new StringBuilder();
        for (TableColumn column : sqlTableDefine.getColumns()) {
            sql.append("alter table "+sqlTableDefine.getTableName()+" change "+column.getOldName()+"  "+column.getName()+" "+column.getType()+" ( "+column.getLength()+" ) COMMENT"+"'"+column.getAnnotation()+"'");
        }
        return sql.toString();
    }


}
