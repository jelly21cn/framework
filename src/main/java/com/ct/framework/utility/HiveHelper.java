package com.ct.framework.utility;

import com.ct.framework.core.configuration.CPContext;
import com.ct.framework.core.data.DbColumn;
import com.ct.framework.core.entity.MetaData;
import com.ct.framework.data.access.DataColumnsManager;
import com.ct.framework.data.hive.HiveConnection;
import com.ct.framework.data.hive.HiveMetaData;

import java.util.ArrayList;
import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: Mysql操作辅助类.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public final class HiveHelper {
    /**
     * 私有构造函数.
     */
    private HiveHelper() {
    }

    /**
     * 返回列的元数据信息.
     *
     * @param tableName        表名
     * @param connectionString 连接
     * @param password         密码
     * @param userName         用户名
     * @return list
     */
    public static List<MetaData> GetColumnMetaDataList(String connectionString, String userName, String password, String tableName) {
        if (GetConnectInfo(connectionString, userName, password)) {
            HiveConnection hiveConnection = new HiveConnection();
            hiveConnection.SetConnectionString(connectionString);
            hiveConnection.SetUserName(userName);
            hiveConnection.SetPassword(password);
            hiveConnection.Open();

            HiveMetaData hiveMetaData = new HiveMetaData();
            return hiveMetaData.GetColumnMetaDataList(hiveConnection, tableName);

        } else {
            return new ArrayList<>();
        }
    }

    /**
     * 返回表的元数据信息.
     *
     * @param connectionString 连接
     * @param password         密码
     * @param userName         用户名
     * @return list
     */
    public static List<MetaData> GetTableMetaDataList(String connectionString, String userName, String password) {
        if (GetConnectInfo(connectionString, userName, password)) {
            HiveConnection hiveConnection = new HiveConnection();
            hiveConnection.SetConnectionString(connectionString);
            hiveConnection.SetUserName(userName);
            hiveConnection.SetPassword(password);
            hiveConnection.Open();

            HiveMetaData hiveMetaData = new HiveMetaData();
            return hiveMetaData.GetTableMetaDataList(hiveConnection);
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * 返回connect情况.
     *
     * @param connectionString 连接
     * @param password         密码
     * @param userName         用户名
     * @return list
     */
    public static boolean GetConnectInfo(String connectionString, String userName, String password) {
        HiveConnection hiveConnection = new HiveConnection();
        hiveConnection.SetConnectionString(connectionString);
        hiveConnection.SetUserName(userName);
        hiveConnection.SetPassword(password);

        try {
            //打开即关闭
            hiveConnection.Open();
            hiveConnection.Close();
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    /**
     * Hive数据类型转换为标准数据类型.
     *
     * @param dataType hive的数据类型
     * @return int 标准的数据类型
     */
    public static int TransformToSystem(int dataType) {
        DbColumn dbColumn = DataColumnsManager.CreateColumns(CPContext.HIVE);
        return dbColumn.IntToIntMapping(dataType);
    }

    /**
     * 逆向，把标准转换为各自数据库的字段类型.
     *
     * @param dataType 标准字段类型
     * @return string 数据库的字段类型
     */
    public static String Reverse(int dataType) {
        DbColumn dbColumn = DataColumnsManager.CreateColumns(CPContext.HIVE);
        return dbColumn.Reverse(dataType);
    }
}
