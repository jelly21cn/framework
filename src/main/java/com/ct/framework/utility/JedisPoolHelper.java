package com.ct.framework.utility;

import com.ct.framework.core.configuration.SystemConfigurationManager;
import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.exceptions.JedisConnectionException;

import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: Jedis操作辅助类.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public final class JedisPoolHelper {
    /**
     * 私有构造函数.
     */
    private JedisPoolHelper() {
    }


    /**
     * 2.
     */
    private static final int TWO = 2;
    /**
     * 3.
     */
    private static final int THREE = 3;
    /**
     * 5.
     */
    private static final int FIVE = 5;
    /**
     * 7.
     */
    private static final int SEVEN = 7;
    /**
     * 10.
     */
    private static final int TEN = 10;
    /**
     * 20.
     */
    private static final int TWENTY = 20;
    /**
     * 24.
     */
    private static final int TWENTY_FOUR = 24;
    /**
     * 1000.
     */
    private static final int THOUSAND = 1000;

    /**
     * 1000L.
     */
    private static final long L_THOUSAND = 1000L;
    /**
     * 1000.
     */
    private static final int TWO_THOUSAND = 2000;
    /**
     * 3600.
     */
    private static final int THREE_THOUSAND_SIX_HUNDRED = 3600;

    /**
     * JedisPool.
     */
    private static JedisPool pool;

    /**
     * 初始化.
     */
    public static void init() {
        log.info("framework Jedis 初始化 开始");
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        //最大连接数，连接全部用完，进行等待
        poolConfig.setMaxTotal(TWENTY);
        //最小空余数
        poolConfig.setMinIdle(TWO);
        //最大空余数
        poolConfig.setMaxIdle(TEN);
        //获取Jedis连接的最大等待时间（5秒）
        poolConfig.setMaxWaitMillis(FIVE * THOUSAND);
        //在获取Jedis连接时，自动检验连接是否可用
        poolConfig.setTestOnBorrow(false);
        //在将连接放回池中前，自动检验连接是否有效
        poolConfig.setTestOnReturn(false);
        //自动测试池中的空闲连接是否都是可用连接
        poolConfig.setTestWhileIdle(true);
        pool = new JedisPool(poolConfig,
                SystemConfigurationManager.getSingleton().GetRedisIp(),
                Integer.parseInt(SystemConfigurationManager.getSingleton().GetRedisPort()),
                TWO_THOUSAND,
                SystemConfigurationManager.getSingleton().GetRedisPassword());
        log.info("framework Jedis 初始化 完成");
    }

    /**
     * 对外提供静态方法.
     *
     * @return Jedis
     */
    private static Jedis getJedis() {
        if (null == pool) {
            init();
        }

        int timeoutCount = 0;

        while (true) {
            try {
                if (null != pool) {
                    Jedis jedis = pool.getResource();
                    jedis.select(Integer.parseInt(SystemConfigurationManager.getSingleton().GetRedisDatabase()));
                    return jedis;
                }
            } catch (Exception e) {
                if (e instanceof JedisConnectionException) {
                    timeoutCount++;
                    log.info("framework Jedis timeoutCount = {}", timeoutCount);
                    if (timeoutCount > THREE) {
                        break;
                    }
                } else {
                    log.warn("framework Jedis  ... NumActive=" + pool.getNumActive()
                            + ", NumIdle=" + pool.getNumIdle()
                            + ", NumWaiters=" + pool.getNumWaiters()
                            + ", isClosed=" + pool.isClosed());
                    log.error("framework Jedis  error,", e);
                    break;
                }
            }
            break;
        }
        return null;
    }

    /**
     * 释放Jedis资源.
     *
     * @param jedis jedis
     */
    private static void returnResource(Jedis jedis) {
        if (null != jedis) {
            jedis.close();
        }
    }

    /**
     * 绝对获取方法（保证一定能够使用可用的连接获取到 目标数据）.
     * Jedis连接使用后放回.
     *
     * @param key key
     * @return String
     */
    public static String safeGet(String key) {
        Jedis jedis = getJedis();
        while (true) {
            if (null != jedis) {
                break;
            } else {
                jedis = getJedis();
            }
        }
        String value = jedis.get(key);
        returnResource(jedis);
        return value;
    }


    /**
     * 绝对设置方法（保证一定能够使用可用的链接设置 数据）.
     * Jedis连接使用后返回连接池.
     *
     * @param key   key
     * @param value value
     */
    public static void safeSet(String key, String value) {
        Jedis jedis = getJedis();
        while (true) {
            if (null != jedis) {
                break;
            } else {
                jedis = getJedis();
            }
        }
        jedis.setex(key, SEVEN * TWENTY_FOUR * THREE_THOUSAND_SIX_HUNDRED * L_THOUSAND, value);
        returnResource(jedis);
    }

    /**
     * 绝对设置方法（保证一定能够使用可用的链接设置 数据）.
     * Jedis连接使用后返回连接池.
     *
     * @param key   key
     * @param time  time
     * @param value value
     */
    public static void safeSet(String key, long time, String value) {
        Jedis jedis = getJedis();
        while (true) {
            if (null != jedis) {
                break;
            } else {
                jedis = getJedis();
            }
        }
        jedis.setex(key, time, value);
        returnResource(jedis);
    }

    /**
     * 删除.
     * Jedis连接使用后返回连接池.
     *
     * @param key key
     */
    public static void safeDel(String key) {
        Jedis jedis = getJedis();
        while (true) {
            if (null != jedis) {
                break;
            } else {
                jedis = getJedis();
            }
        }
        jedis.del(key);
        returnResource(jedis);
    }

    /**
     * safeBatchSet.
     *
     * @param map map
     */
    public static void safeBatchSet(Map<String, String> map) {
        Jedis jedis = getJedis();
        Pipeline pip = jedis.pipelined();
        pip.multi();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            pip.set(key, value);
        }
        pip.exec();
        pip.sync();
        returnResource(jedis);
    }
}
