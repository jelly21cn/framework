package com.ct.framework.utility;

import com.ct.framework.data.mysql.SqlConnection;
import com.ct.framework.core.configuration.CPContext;
import com.ct.framework.core.data.DbColumn;
import com.ct.framework.core.entity.MetaData;
import com.ct.framework.data.access.DataColumnsManager;
import com.ct.framework.data.mysql.SqlDbMetaData;

import java.util.ArrayList;
import java.util.List;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: Mysql操作辅助类.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public final class MysqlHelper {

    /**
     * 私有构造函数.
     */
    private MysqlHelper() {
    }

    /**
     * 返回列的元数据信息.
     *
     * @param tableName        表名
     * @param connectionString 连接
     * @param password         密码
     * @param userName         用户名
     * @return list
     */
    public static List<MetaData> GetColumnMetaDataList(String connectionString, String userName, String password, String tableName) {
        if (GetConnectInfo(connectionString, userName, password)) {
            SqlConnection sqlConnection = new SqlConnection();
            sqlConnection.SetConnectionString(connectionString);
            sqlConnection.SetUserName(userName);
            sqlConnection.SetPassword(password);
            sqlConnection.Open();
            SqlDbMetaData sqlDbMetaData = new SqlDbMetaData();
            return sqlDbMetaData.GetColumnMetaDataList(sqlConnection, tableName);
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * 返回表的元数据信息.
     *
     * @param connectionString 连接
     * @param password         密码
     * @param userName         用户名
     * @return list
     */
    public static List<MetaData> GetTableMetaDataList(String connectionString, String userName, String password) {
        if (GetConnectInfo(connectionString, userName, password)) {
            SqlConnection sqlConnection = new SqlConnection();
            sqlConnection.SetConnectionString(connectionString);
            sqlConnection.SetUserName(userName);
            sqlConnection.SetPassword(password);
            sqlConnection.Open();

            SqlDbMetaData sqlDbMetaData = new SqlDbMetaData();
            return sqlDbMetaData.GetTableMetaDataList(sqlConnection);
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * 返回connect情况.
     *
     * @param connectionString 连接
     * @param password         密码
     * @param userName         用户名
     * @return list
     */
    public static boolean GetConnectInfo(String connectionString, String userName, String password) {
        SqlConnection sqlConnection = new SqlConnection();
        sqlConnection.SetConnectionString(connectionString);
        sqlConnection.SetUserName(userName);
        sqlConnection.SetPassword(password);

        try {
            //打开即关闭
            sqlConnection.Open();
            sqlConnection.Close();
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    /**
     * 读取Mysql所有类型，对应成 标准的数据类型的字符.
     * 整数类型：BIT、BOOL、TINY INT、SMALL INT、MEDIUM INT、 INT、 BIG INT
     * 浮点数类型：FLOAT、DOUBLE、DECIMAL
     * 字符串类型：CHAR、VARCHAR、TINY TEXT、TEXT、MEDIUM TEXT、LONGTEXT、TINY BLOB、BLOB、MEDIUM BLOB、LONG BLOB
     * 日期类型：Date、DateTime、TimeStamp、Time、Year
     * 其他数据类型：BINARY、VARBINARY、ENUM、SET、Geometry、Point、MultiPoint、LineString、MultiLineString、Polygon、GeometryCollection等
     *
     * @param dataType mysql数据类型
     */
    public static String GetDataTypeFromMysql(int dataType) {
        DbColumn dbColumn = DataColumnsManager.CreateColumns(CPContext.MYSQL);
        return dbColumn.IntToStringMapping(dataType);
    }

    /**
     * mysql数据类型转换为标准数据类型.
     *
     * @param dataType mysql的数据类型
     * @return int 标准的数据类型
     */
    public static int TransformToSystem(int dataType) {
        DbColumn dbColumn = DataColumnsManager.CreateColumns(CPContext.MYSQL);
        return dbColumn.IntToIntMapping(dataType);
    }

    /**
     * 逆向，把标准转换为各自数据库的字段类型.
     *
     * @param dataType 标准字段类型
     * @return string 数据库的字段类型
     */
    public static String Reverse(int dataType) {
        DbColumn dbColumn = DataColumnsManager.CreateColumns(CPContext.MYSQL);
        return dbColumn.Reverse(dataType);
    }
}
