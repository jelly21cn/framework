package com.ct.framework.utility;

import com.google.gson.internal.LinkedTreeMap;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public final class ClassHelper {

    /**
     * 6.
     */
    private static final int SIX = 6;

    /**
     * 私有构造函数.
     */
    private ClassHelper() {
    }

    /**
     * 查找.
     */
    private static final HashMap<String, Class<?>> PRIM_CLASSES = new HashMap<>();

    static {
        PRIM_CLASSES.put("boolean", boolean.class);
        PRIM_CLASSES.put("byte", byte.class);
        PRIM_CLASSES.put("char", char.class);
        PRIM_CLASSES.put("short", short.class);
        PRIM_CLASSES.put("int", int.class);
        PRIM_CLASSES.put("long", long.class);
        PRIM_CLASSES.put("float", float.class);
        PRIM_CLASSES.put("double", double.class);
        PRIM_CLASSES.put("void", void.class);
    }

    /**
     * 查找.
     *
     * @param className 类型
     * @return Class
     */
    public static Class<?> resolveClass(String className) throws ClassNotFoundException {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException ex) {
            Class<?> cl = PRIM_CLASSES.get(className);
            if (cl != null) {
                return cl;
            } else {
                throw ex;
            }
        }
    }

    /**
     * LinkedTreeMap转换为实体.
     *
     * @param clazz 类型
     * @param map   map
     * @param <T>   范型
     * @return T
     */
    public static <T> T ConvertToEntity(Class clazz, Map<String, Object> map) {
        T t = null;
        try {
            t = (T) clazz.newInstance();
            Field[] entityFields = clazz.getDeclaredFields();
            for (Map.Entry<String, Object> m : map.entrySet()) {
                Object val = m.getValue();
                for (Field sf : entityFields) {
                    sf.setAccessible(true);
                    String k = m.getKey();
                    if (val == null) {
                        continue;
                    }
                    String fieldName = sf.getName();
                    if (k.equalsIgnoreCase(fieldName)) {
                        if (sf.getType().equals(Double.class)) {
                            sf.set(t, Double.parseDouble(val.toString()));
                        } else if (sf.getType().equals(String.class)) {
                            sf.set(t, val.toString());
                        } else if (sf.getType().equals(Long.class)) {
                            Long lg = new Long(new Double(val.toString()).longValue());
                            sf.set(t, lg);
                        } else if (sf.getType().equals(Boolean.class)) {

                            sf.set(t, Boolean.parseBoolean(val.toString()));
                        } else if (sf.getType().equals(Integer.class)) {
                            Integer in = new Integer(new Double(val.toString()).intValue());
                            sf.set(t, in);
                        } else if (sf.getType().equals(Date.class)) {
                            String dateString = val.toString();
                            Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateString);
                            sf.set(t, parse);
                        } else if (sf.getType().equals(Map.class)) {
                            //增加LinkedTreeMap的解析
                            //Edit by niuz
                            Map mp = (Map) val;
                            sf.set(t, mp);
                        }
                    }

                }
            }
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }

        return t;
    }

    /**
     * 实体转换为LinkedTreeMap.
     *
     * @param clazz 类型
     * @param obj   实体
     * @return List
     */
    public static LinkedTreeMap EntityToMap(Class clazz, Object obj) {
        LinkedTreeMap map = new LinkedTreeMap();
        try {
            Field[] entityFields = clazz.getDeclaredFields();
            for (Field sf : entityFields) {
                sf.setAccessible(true);
                String name = sf.getName();
                Object value = sf.get(obj);
                if (value == null) {
                    continue;
                }
                if (sf.getType().equals(Double.class)) {
                    map.put(name, Double.parseDouble(value.toString()));
                } else if (sf.getType().equals(String.class)) {
                    map.put(name, value.toString());
                } else if (sf.getType().equals(Long.class)) {
                    Long lg = new Long(new Double(value.toString()).longValue());
                    map.put(name, lg);
                } else if (sf.getType().equals(Boolean.class)) {
                    Boolean bol = Boolean.parseBoolean(value.toString());
                    map.put(name, bol);
                } else if (sf.getType().equals(Integer.class)) {
                    Integer in = new Integer(new Double(value.toString()).intValue());
                    map.put(name, in);
                } else if (sf.getType().equals(Date.class)) {
                    String dateString = value.toString();
                    Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateString);
                    map.put(name, parse);
                } else {
                    map.put(name, value);
                }
            }
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }

        return map;
    }

    /**
     * 从包package中获取所有的Class.
     *
     * @param packageName
     * @return set
     */
    public static Set<Class<?>> GetClasses(String packageName) throws Exception {

        // 第一个class类的集合
        //List<Class<?>> classes = new ArrayList<Class<?>>();
        Set<Class<?>> classes = new HashSet<>();
        // 是否循环迭代
        boolean recursive = true;
        // 获取包的名字 并进行替换
        String packageDirName = packageName.replace('.', '/');
        // 定义一个枚举的集合 并进行循环来处理这个目录下的things
        Enumeration<URL> dirs;
        try {
            dirs = Thread.currentThread().getContextClassLoader().getResources(packageDirName);
            // 循环迭代下去
            while (dirs.hasMoreElements()) {
                // 获取下一个元素
                URL url = dirs.nextElement();
                // 得到协议的名称
                String protocol = url.getProtocol();
                // 如果是以文件的形式保存在服务器上
                if ("file".equals(protocol)) {
                    // 获取包的物理路径
                    String filePath = URLDecoder.decode(url.getFile(), "UTF-8");
                    // 以文件的方式扫描整个包下的文件 并添加到集合中
                    addClass(classes, filePath, packageName);
                } else if ("jar".equals(protocol)) {
                    // 如果是jar包文件
                    // 定义一个JarFile
                    JarFile jar;
                    try {
                        // 获取jar
                        jar = ((JarURLConnection) url.openConnection()).getJarFile();
                        // 从此jar包 得到一个枚举类
                        Enumeration<JarEntry> entries = jar.entries();
                        // 同样的进行循环迭代
                        while (entries.hasMoreElements()) {
                            // 获取jar里的一个实体 可以是目录 和一些jar包里的其他文件 如META-INF等文件
                            JarEntry entry = entries.nextElement();
                            String name = entry.getName();
                            // 如果是以/开头的
                            if (name.charAt(0) == '/') {
                                // 获取后面的字符串
                                name = name.substring(1);
                            }
                            // 如果前半部分和定义的包名相同
                            if (name.startsWith(packageDirName)) {
                                int idx = name.lastIndexOf('/');
                                // 如果以"/"结尾 是一个包
                                if (idx != -1) {
                                    // 获取包名 把"/"替换成"."
                                    packageName = name.substring(0, idx).replace('/', '.');
                                }
                                // 如果可以迭代下去 并且是一个包
                                if ((idx != -1) || recursive) {
                                    // 如果是一个.class文件 而且不是目录
                                    if (name.endsWith(".class") && !entry.isDirectory()) {
                                        // 去掉后面的".class" 获取真正的类名
                                        String className = name.substring(packageName.length() + 1, name.length() - SIX);
                                        try {
                                            // 添加到classes
                                            classes.add(Class.forName(packageName + '.' + className));
                                        } catch (ClassNotFoundException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return classes;
    }

    private static void addClass(Set<Class<?>> classes, String filePath, String packageName) throws Exception {
        File[] files = new File(filePath).listFiles(file -> (file.isFile() && file.getName().endsWith(".class")) || file.isDirectory());
        if (files == null || files.length == 0) {
            return;
        }

        for (File file : files) {
            String fileName = file.getName();

            if (file.isDirectory()) {
                addClass(classes, filePath + fileName, packageName + fileName);
                continue;
            }

            if (file.isFile()) {
                String classsName = fileName.substring(0, fileName.lastIndexOf("."));
                if (!packageName.isEmpty()) {
                    classsName = packageName + "." + classsName;
                }
                doAddClass(classes, classsName);
            }

        }
    }

    private static void doAddClass(Set<Class<?>> classes, final String classsName) throws Exception {
        ClassLoader classLoader = new ClassLoader() {
            @Override
            public Class<?> loadClass(String name) throws ClassNotFoundException {
                return super.loadClass(name);
            }
        };
        classes.add(classLoader.loadClass(classsName));
    }
}
