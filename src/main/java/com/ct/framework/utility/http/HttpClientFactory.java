package com.ct.framework.utility.http;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import javax.net.ssl.SSLContext;
import java.security.NoSuchAlgorithmException;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:
 * Create Date:  01/06/2021
 * Usage: HttpClientFactory.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1                                          新增
 * @author xxx
 *****************************************************************/
public final class HttpClientFactory {

    /**
     * HttpClientFactory instance.
     */
    private static HttpClientFactory instance = null;

    /**
     * 私有构造函数.
     */
    private HttpClientFactory() {
    }

    /**
     * getInstance.
     *
     * @return HttpClientFactory
     */
    public static synchronized HttpClientFactory getInstance() {
        if (instance == null) {
            instance = new HttpClientFactory();
        }
        return instance;
    }

    /**
     * getHttpClient.
     *
     * @return HttpClient
     */
    public synchronized HttpClient getHttpClient() {
        HttpClient httpClient = null;
        if (HttpConstant.IS_KEEP_ALIVE) {
            //获取长连接
            httpClient = new KeepAliveHttpClientBuilder().getKeepAliveHttpClient();
        } else {
            // 获取短连接
            httpClient = new HttpClientBuilder().getHttpClient();
        }
        return httpClient;
    }

    /**
     * httpPost.
     *
     * @param httpUrl url地址
     * @return HttpPost
     */
    public HttpPost httpPost(String httpUrl) {
        HttpPost httpPost = null;
        httpPost = new HttpPost(httpUrl);
        if (HttpConstant.IS_KEEP_ALIVE) {
            // 设置为长连接，服务端判断有此参数就不关闭连接。
            httpPost.setHeader("Connection", "Keep-Alive");
        }
        return httpPost;
    }

    /**
     * httpGet.
     *
     * @param httpUrl url地址
     * @return HttpGet
     */
    public HttpGet httpGet(String httpUrl) {
        HttpGet httpGet = null;
        httpGet = new HttpGet(httpUrl);
        if (HttpConstant.IS_KEEP_ALIVE) {
            // 设置为长连接，服务端判断有此参数就不关闭连接。
            httpGet.setHeader("Connection", "Keep-Alive");
        }
        return httpGet;
    }

    /**
     * HttpDelete.
     *
     * @param httpUrl url地址
     * @return HttpDelete
     */
    public HttpDelete httpDelete(String httpUrl) {
        HttpDelete httpDelete;
        httpDelete = new HttpDelete(httpUrl);
        if (HttpConstant.IS_KEEP_ALIVE) {
            // 设置为长连接，服务端判断有此参数就不关闭连接。
            httpDelete.setHeader("Connection", "Keep-Alive");
        }
        return httpDelete;
    }

    /**
     * HttpPut.
     *
     * @param httpUrl url地址
     * @return HttpPut
     */
    public HttpPut httpPut(String httpUrl) {
        HttpPut httpPut = null;
        httpPut = new HttpPut(httpUrl);
        if (HttpConstant.IS_KEEP_ALIVE) {
            // 设置为长连接，服务端判断有此参数就不关闭连接。
            httpPut.setHeader("Connection", "Keep-Alive");
        }
        return httpPut;
    }

    /**
     * 内部类.
     */
    private static class KeepAliveHttpClientBuilder {
        /**
         * HttpClient.
         */
        private static HttpClient httpClient;

        /**
         * 获取http长连接.
         *
         * @return HttpClient
         */
        private synchronized HttpClient getKeepAliveHttpClient() {
            if (httpClient == null) {
                LayeredConnectionSocketFactory sslsf = null;
                try {
                    sslsf = new SSLConnectionSocketFactory(SSLContext.getDefault());
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }

                Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder
                        .<ConnectionSocketFactory>create().register("https", sslsf)
                        .register("http", new PlainConnectionSocketFactory()).build();
                PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
                cm.setMaxTotal(HttpConstant.MAX_TOTAL);
                cm.setDefaultMaxPerRoute(HttpConstant.MAX_CONN_PER_ROUTE);

                RequestConfig requestConfig = RequestConfig.custom()
                        .setConnectTimeout(HttpConstant.CONNECT_TIMEOUT)
                        .setSocketTimeout(HttpConstant.SOCKET_TIMEOUT).build();
                // 创建连接
                httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).setConnectionManager(cm).build();
            }

            return httpClient;
        }
    }

    /**
     * 内部类.
     */
    private static class HttpClientBuilder {

        /**
         * HttpClient.
         */
        private HttpClient httpClient;

        /**
         * 获取http短连接.
         *
         * @return HttpClient
         */
        private synchronized HttpClient getHttpClient() {
            if (httpClient == null) {
                RequestConfig requestConfig = RequestConfig.custom()
                        // 设置请求超时时间
                        .setConnectTimeout(HttpConstant.CONNECT_TIMEOUT)
                        // 设置响应超时时间
                        .setSocketTimeout(HttpConstant.SOCKET_TIMEOUT).build();
                // 创建连接
                httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
            }
            return httpClient;
        }
    }
}
