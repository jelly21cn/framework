package com.ct.framework.utility.http;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:
 * Create Date:  01/06/2021
 * Usage: 时间处理类.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1                                          新增
 * @author xxx
 *****************************************************************/
public final class HttpConstant {
    /**
     * 私有构造函数.
     */
    private HttpConstant() {
    }

    /**
     * httpClient连接超时时间,单位毫秒.
     */
    public static final int CONNECT_TIMEOUT = 15 * 1000;

    /**
     * httpClient请求获取数据的超时时间(即响应时间) 单位毫秒.
     */
    public static final int SOCKET_TIMEOUT = 15 * 1000;

    /**
     * http连接池大小.
     */
    public static final int MAX_TOTAL = 10;

    /**
     * 分配给同一个route(路由)最大的并发连接数.
     */
    public static final int MAX_CONN_PER_ROUTE = 2;

    /**
     * http连接是否是长连接.
     */
    public static final boolean IS_KEEP_ALIVE = true;

    /**
     * 调用接口失败默认重新调用次数.
     */
    public static final int REQ_TIMES = 3;

    /**
     * utf-8编码.
     */
    public static final String UTF8_ENCODE = "UTF-8";

    /**
     * application/json.
     */
    public static final String APPLICATION_JSON = "application/json";

    /**
     * multipart/form-data.
     */
    public static final String MULTIPART_FORM_DATA = "multipart/form-data";

    /**
     * application/json;charset=UTF-8.
     */
    public static final String CONTENT_TYPE = "application/json;charset=UTF-8";

    /**
     * text/xml.
     */
    public static final String TEXT_XML = "text/xml";

    /**
     * 鉴权Token.
     */
    public static final String DTUBE_JOB_ACCESS_TOKEN = "DTUBE-JOB-ACCESS-TOKEN";

    /**
     * 登录成功后的Key.
     */
    public static final String LOGIN_IDENTITY_KEY = "DTUBE_JOB_LOGIN_IDENTITY";
}
