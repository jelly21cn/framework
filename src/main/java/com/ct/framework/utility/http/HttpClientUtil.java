package com.ct.framework.utility.http;

import com.ct.framework.task.biz.model.ReturnT;
import com.ct.framework.utility.SerializeHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.stream.Collectors;

import static com.ct.framework.utility.http.HttpConstant.CONNECT_TIMEOUT;
import static com.ct.framework.utility.http.HttpConstant.DTUBE_JOB_ACCESS_TOKEN;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:
 * Create Date:  01/06/2021
 * Usage: 复杂的Post和Get请求.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1                                          新增
 * @author xxx
 *****************************************************************/
@Slf4j
public final class HttpClientUtil {

    /**
     * 1000.
     */
    private static final int THOUSAND = 1000;

    /**
     * 私有构造函数.
     */
    private HttpClientUtil() {
    }

    /**
     * 通过get方式调用http接口.
     *
     * @param url       url路径
     * @param jsonParam json格式的参数
     * @param reSend    重发次数
     * @param param     param
     * @return String
     */
    public static String sendPostByJson(String url, String jsonParam, int reSend, Map<String, String> param) {
        //声明返回结果
        String result = "";
        //开始请求API接口时间
        long startTime = System.currentTimeMillis();
        //请求API接口的响应时间
        long endTime = 0L;
        HttpEntity httpEntity = null;
        HttpResponse httpResponse = null;
        HttpClient httpClient = null;
        try {
            // 创建连接
            httpClient = HttpClientFactory.getInstance().getHttpClient();
            // 设置请求头和报文
            HttpPost httpPost = HttpClientFactory.getInstance().httpPost(url);
            for (Map.Entry<String, String> entry : param.entrySet()) {
                httpPost.setHeader(entry.getKey(), entry.getValue());
            }
            // 设置报文和通讯格式
            StringEntity stringEntity = new StringEntity(jsonParam, HttpConstant.UTF8_ENCODE);
            stringEntity.setContentEncoding(HttpConstant.UTF8_ENCODE);
            stringEntity.setContentType(HttpConstant.APPLICATION_JSON);
            httpPost.setEntity(stringEntity);
            log.info("请求{}接口的参数为{}", url, jsonParam);
            httpResponse = httpClient.execute(httpPost);
            httpEntity = httpResponse.getEntity();
            result = EntityUtils.toString(httpEntity);
        } catch (Exception e) {
            log.error("请求{}接口出现异常", url, e);
            if (reSend > 0) {
                log.info("请求{}出现异常:{}，进行重发。进行第{}次重发", url, e.getMessage(), (HttpConstant.REQ_TIMES - reSend + 1));
                result = sendPostByJson(url, jsonParam, reSend - 1, param);
                if (result != null && !"".equals(result)) {
                    return result;
                }
            }
        } finally {
            try {
                EntityUtils.consume(httpEntity);
            } catch (IOException e) {
                log.error("http请求释放资源异常", e);
            }
        }
        //请求接口的响应时间
        endTime = System.currentTimeMillis();
        log.info("请求{}接口的响应报文内容为{},本次请求API接口的响应时间为:{}毫秒", url, result, (endTime - startTime));
        return result;
    }

    /**
     * 通过post方式调用http接口.
     *
     * @param url       url路径
     * @param jsonParam json格式的参数
     * @param reSend    重发次数
     * @param cookie    cookie
     * @return String
     */
//    public static String sendPostByJson(String url, String jsonParam, int reSend, String cookie) {
//        //声明返回结果
//        String result = "";
//        //开始请求API接口时间
//        long startTime = System.currentTimeMillis();
//        HttpEntity httpEntity = null;
//        HttpResponse httpResponse;
//        HttpClient httpClient;
//        try {
//            // 创建连接
//            httpClient = HttpClientFactory.getInstance().getHttpClient();
//            // 设置请求头和报文
//            HttpPost httpPost = HttpClientFactory.getInstance().httpPost(url);
//            httpPost.setHeader("Cookie", cookie);
//            httpPost.setHeader("Accept", HttpConstant.APPLICATION_JSON);
//            httpPost.setHeader("Content-Type", HttpConstant.APPLICATION_JSON);
//            // 设置报文和通讯格式
//            StringEntity stringEntity = new StringEntity(jsonParam, HttpConstant.UTF8_ENCODE);
//            stringEntity.setContentEncoding(HttpConstant.UTF8_ENCODE);
//            stringEntity.setContentType(HttpConstant.APPLICATION_JSON);
//            httpPost.setEntity(stringEntity);
//            log.info("请求:{} 接口的参数为:{}", url, jsonParam);
//            //执行发送，获取相应结果
//            httpResponse = httpClient.execute(httpPost);
//            httpEntity = httpResponse.getEntity();
//            result = EntityUtils.toString(httpEntity);
//        } catch (Exception e) {
//            log.error("请求{}接口出现异常", url, e);
//            if (reSend > 0) {
//                log.info("请求{}出现异常:{}，进行重发", url, e.getMessage());
//                result = sendPostByJson(url, jsonParam, reSend - 1, cookie);
//                if (StringUtils.isNotBlank(result)) {
//                    return result;
//                }
//            }
//        } finally {
//            try {
//                EntityUtils.consume(httpEntity);
//            } catch (IOException e) {
//                log.error("http请求释放资源异常", e);
//            }
//        }
//        //请求API接口的响应时间
//        long endTime = System.currentTimeMillis();
//        log.info("请求{}接口的响应报文内容为{},本次请求API接口的响应时间为:{}毫秒", url, result, (endTime - startTime));
//        return result;
//    }

    /**
     * 通过post方式(form-data)调用http接口.
     *
     * @param url   url路径
     * @param token 鉴权token
     * @return String
     */
    public static String sendPostByFormData(String url, String token) {
        //声明返回结果
        String result = "";
        //开始请求API接口时间
        long startTime = System.currentTimeMillis();
        HttpEntity httpEntity = null;
        HttpResponse httpResponse;
        HttpClient httpClient;
        try {
            // 创建连接
            httpClient = HttpClientFactory.getInstance().getHttpClient();
            // 设置请求头和报文
            HttpPost httpPost = HttpClientFactory.getInstance().httpPost(url);
            httpPost.setHeader("token", token);
            log.info("请求:{} ", url);
            //执行发送，获取相应结果
            httpResponse = httpClient.execute(httpPost);
            httpEntity = httpResponse.getEntity();
            result = EntityUtils.toString(httpEntity);
        } catch (Exception e) {
            log.error("请求{}接口出现异常", url, e);
        } finally {
            try {
                EntityUtils.consume(httpEntity);
            } catch (IOException e) {
                log.error("http请求释放资源异常", e);
            }
        }
        //请求API接口的响应时间
        long endTime = System.currentTimeMillis();
        log.info("请求{}接口的响应报文内容为{},本次请求API接口的响应时间为:{}毫秒", url, result, (endTime - startTime));
        return result;
    }

    /**
     * post请求.
     *
     * @param accessToken        token
     * @param requestObj         请求
     * @param returnTargClassOfT 返回类型
     * @param timeout            超时时间
     * @param url                请求地址
     */
    public static ReturnT postBody(String url, String accessToken, int timeout, Object requestObj, Class returnTargClassOfT) {
        HttpURLConnection connection = null;
        BufferedReader bufferedReader = null;
        try {
            URL realUrl = new URL(url);
            connection = (HttpURLConnection) realUrl.openConnection();

            boolean useHttps = url.startsWith("https");
            if (useHttps) {
                HttpsURLConnection https = (HttpsURLConnection) connection;
                trustAllHosts(https);
            }

            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setReadTimeout(timeout * THOUSAND);
            connection.setConnectTimeout(CONNECT_TIMEOUT);
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            connection.setRequestProperty("Accept-Charset", "application/json;charset=UTF-8");

            if (accessToken != null && accessToken.trim().length() > 0) {
                connection.setRequestProperty(DTUBE_JOB_ACCESS_TOKEN, accessToken);
            }

            connection.connect();

            if (requestObj != null) {
                String requestBody = SerializeHelper.JsonSerializer(requestObj);

                DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
                dataOutputStream.write(requestBody.getBytes("UTF-8"));
                dataOutputStream.flush();
                dataOutputStream.close();
            }

            int statusCode = connection.getResponseCode();
            if (statusCode != 200) {
                return new ReturnT<String>(ReturnT.FAIL_CODE, "网路通信失败, 状态码(" + statusCode + ") 无效. url : " + url);
            }

            bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
            StringBuilder result = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                result.append(line);
            }
            String resultJson = result.toString();

            try {
                ReturnT returnT = SerializeHelper.JsonDeserialize(resultJson, ReturnT.class, returnTargClassOfT);
                return returnT;
            } catch (Exception e) {
                log.error("远程通信 (url=" + url + ") 响应内容无效(" + resultJson + ").", e);
                return new ReturnT<String>(ReturnT.FAIL_CODE, "网路通信 (url=" + url + ") 响应内容无效 (" + resultJson + ").");
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ReturnT<String>(ReturnT.FAIL_CODE, "网路通信 错误 (" + e.getMessage() + "), url : " + url);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (connection != null) {
                    connection.disconnect();
                }
            } catch (Exception e2) {
                log.error(e2.getMessage(), e2);
            }
        }
    }

    /**
     * 通过post方式调用http接口.
     *
     * @param url    url路径
     * @param reSend 重发次数
     * @param cookie cookie
     * @return String
     */
//    public static String sendDelete(String url, int reSend, String cookie) {
//        //声明返回结果
//        String result = "";
//        //开始请求API接口时间
//        long startTime = System.currentTimeMillis();
//        HttpEntity httpEntity = null;
//        HttpResponse httpResponse;
//        HttpClient httpClient;
//        try {
//            // 创建连接
//            httpClient = HttpClientFactory.getInstance().getHttpClient();
//            // 设置请求头和报文
//            HttpDelete httpDelete = HttpClientFactory.getInstance().httpDelete(url);
//            httpDelete.setHeader("Cookie", cookie);
//            httpDelete.setHeader("Accept", HttpConstant.APPLICATION_JSON);
//            httpDelete.setHeader("Content-Type", HttpConstant.APPLICATION_JSON);
//            //执行发送，获取相应结果
//            httpResponse = httpClient.execute(httpDelete);
//            httpEntity = httpResponse.getEntity();
//        } catch (Exception e) {
//            log.error("请求{}接口出现异常", url, e);
//            if (reSend > 0) {
//                log.info("请求{}出现异常:{}，进行重发", url, e.getMessage());
//                result = sendDelete(url, reSend - 1, cookie);
//                if (StringUtils.isNotBlank(result)) {
//                    return result;
//                }
//            }
//        } finally {
//            try {
//                EntityUtils.consume(httpEntity);
//            } catch (IOException e) {
//                log.error("http请求释放资源异常", e);
//            }
//        }
//        //请求API接口的响应时间
//        long endTime = System.currentTimeMillis();
//        log.info("请求{}接口的响应报文内容为{},本次请求API接口的响应时间为:{}毫秒", url, result, (endTime - startTime));
//        return result;
//    }

    /**
     * 通过put方式调用http接口.
     *
     * @param url       url路径
     * @param jsonParam json格式的参数
     * @param reSend    重发次数
     * @param token     token
     * @param cookie    cookie
     * @return String
     */
//    public static String sendPutByJson(String url, String jsonParam, String token, int reSend, String cookie) {
//        //声明返回结果
//        String result = "";
//        //开始请求API接口时间
//        long startTime = System.currentTimeMillis();
//        HttpEntity httpEntity = null;
//        HttpResponse httpResponse;
//        HttpClient httpClient;
//        try {
//            // 创建连接
//            httpClient = HttpClientFactory.getInstance().getHttpClient();
//            // 设置请求头和报文
//            HttpPut httpPut = HttpClientFactory.getInstance().httpPut(url);
//            httpPut.setHeader("Cookie", cookie);
//            httpPut.setHeader("Accept", HttpConstant.APPLICATION_JSON);
//            httpPut.setHeader("Content-Type", HttpConstant.APPLICATION_JSON);
//            httpPut.setHeader("token", token);
//            // 设置报文和通讯格式
//            StringEntity stringEntity = new StringEntity(jsonParam, HttpConstant.UTF8_ENCODE);
//            stringEntity.setContentEncoding(HttpConstant.UTF8_ENCODE);
//            stringEntity.setContentType(HttpConstant.APPLICATION_JSON);
//            httpPut.setEntity(stringEntity);
//            log.info("请求:{} 接口的参数为:{}", url, jsonParam);
//            //执行发送，获取相应结果
//            httpResponse = httpClient.execute(httpPut);
//            httpEntity = httpResponse.getEntity();
//            result = EntityUtils.toString(httpEntity);
//        } catch (Exception e) {
//            log.error("请求{}接口出现异常", url, e);
//            if (reSend > 0) {
//                log.info("请求{}出现异常:{}，进行重发", url, e.getMessage());
//                result = sendPutByJson(url, jsonParam, token, reSend - 1, cookie);
//                if (StringUtils.isNotBlank(result)) {
//                    return result;
//                }
//            }
//        } finally {
//            try {
//                EntityUtils.consume(httpEntity);
//            } catch (IOException e) {
//                log.error("http请求释放资源异常", e);
//            }
//        }
//        //请求API接口的响应时间
//        long endTime = System.currentTimeMillis();
//        log.info("请求{}接口的响应报文内容为{},本次请求API接口的响应时间为:{}毫秒", url, result, (endTime - startTime));
//        return result;
//    }


    /**
     * 通过post方式调用http接口.
     *
     * @param url    url路径
     * @param map    map
     * @param reSend 重发次数
     * @return String
     */
//    public static String sendPostByForm(String url, MultiValueMap<String, String> map, int reSend) {
//        //声明返回结果
//        String result = "";
//        //开始请求API接口时间
//        long startTime = System.currentTimeMillis();
//        //请求API接口的响应时间
//        long endTime = 0L;
//        HttpEntity httpEntity = null;
//        UrlEncodedFormEntity entity = null;
//        HttpResponse httpResponse = null;
//        HttpClient httpClient = null;
//        try {
//            // 创建连接
//            httpClient = HttpClientFactory.getInstance().getHttpClient();
//            // 设置请求头和报文
//            HttpPost httpPost = HttpClientFactory.getInstance().httpPost(url);
//            //设置参数
//            List<NameValuePair> list = new ArrayList<NameValuePair>();
//            Iterator iterator = map.entrySet().iterator();
//            while (iterator.hasNext()) {
//                Map.Entry<String, String> elem = (Map.Entry<String, String>) iterator.next();
//                list.add(new BasicNameValuePair(elem.getKey(), elem.getValue()));
//            }
//            entity = new UrlEncodedFormEntity(list, HttpConstant.UTF8_ENCODE);
//            httpPost.setEntity(entity);
//            log.info("请求{}接口的参数为{}", url, map);
//            //执行发送，获取相应结果
//            httpResponse = httpClient.execute(httpPost);
//            httpEntity = httpResponse.getEntity();
//            result = EntityUtils.toString(httpEntity);
//        } catch (Exception e) {
//            log.error("请求{}接口出现异常", url, e);
//            if (reSend > 0) {
//                log.info("请求{}出现异常:{}，进行重发。进行第{}次重发", url, e.getMessage(), (HttpConstant.REQ_TIMES - reSend + 1));
//                result = sendPostByForm(url, map, reSend - 1);
//                if (result != null && !"".equals(result)) {
//                    return result;
//                }
//            }
//        } finally {
//            try {
//                EntityUtils.consume(httpEntity);
//            } catch (IOException e) {
//                log.error("http请求释放资源异常", e);
//            }
//        }
//        //请求接口的响应时间
//        endTime = System.currentTimeMillis();
//        log.info("请求{}接口的响应报文内容为{},本次请求API接口的响应时间为:{}毫秒", url, result, (endTime - startTime));
//        return result;
//
//    }

    /**
     * 通过post方式调用http接口.
     *
     * @param url    url路径
     * @param map    json格式的参数
     * @param reSend 重发次数
     * @return String
     */
//    public static String sendPostByFormWithResponse(String url, Map<String, String> map, int reSend) {
//        //声明返回结果
//        String result = "";
//        //开始请求API接口时间
//        long startTime = System.currentTimeMillis();
//        UrlEncodedFormEntity entity;
//        HttpResponse httpResponse = null;
//        HttpClient httpClient;
//        HttpEntity httpEntity;
//        try {
//            // 创建连接
//            httpClient = HttpClientFactory.getInstance().getHttpClient();
//            // 设置请求头和报文
//            HttpPost httpPost = HttpClientFactory.getInstance().httpPost(url);
//            //设置参数
//            List<NameValuePair> list = new ArrayList<>();
//            for (Map.Entry<String, String> elem : map.entrySet()) {
//                list.add(new BasicNameValuePair(elem.getKey(), elem.getValue()));
//            }
//            entity = new UrlEncodedFormEntity(list, HttpConstant.UTF8_ENCODE);
//            httpPost.setEntity(entity);
//            httpPost.setHeader("language", "zh_CN");
//            log.info("请求{}接口的参数为{}", url, map);
//            //执行发送，获取相应结果
//            httpResponse = httpClient.execute(httpPost);
//            httpEntity = httpResponse.getEntity();
//            result = EntityUtils.toString(httpEntity);
//        } catch (Exception e) {
//            log.error("请求{}接口出现异常", url, e);
//            if (reSend > 0) {
//                log.info("请求{}出现异常:{}，进行重发。进行第{}次重发", url, e.getMessage(), (HttpConstant.REQ_TIMES - reSend + 1));
//                return sendPostByFormWithResponse(url, map, reSend - 1);
//            }
//        } finally {
//            try {
//                if (httpResponse != null) {
//                    EntityUtils.consume(httpResponse.getEntity());
//                }
//            } catch (IOException e) {
//                log.error("http请求释放资源异常", e);
//            }
//        }
//        //请求接口的响应时间
//        long endTime = System.currentTimeMillis();
//        log.info("请求{}接口的响应报文内容为{},本次请求API接口的响应时间为:{}毫秒", url, result, (endTime - startTime));
//        return result;
//    }

    /**
     * 通过post方式调用http接口.
     *
     * @param url       url路径
     * @param map       json格式的参数
     * @param reSend    重发次数
     * @param sessionId sessionId
     * @param cookie    Cookie
     * @return String
     */
//    public static String sendPostByFormWithResponse(String url, Map<String, String> map, int reSend, String sessionId, String cookie) {
//        //声明返回结果
//        String result = "";
//        //开始请求API接口时间
//        long startTime = System.currentTimeMillis();
//        UrlEncodedFormEntity entity;
//        HttpResponse httpResponse = null;
//        HttpClient httpClient;
//        HttpEntity httpEntity;
//        try {
//            // 创建连接
//            httpClient = HttpClientFactory.getInstance().getHttpClient();
//            // 设置请求头和报文
//            HttpPost httpPost = HttpClientFactory.getInstance().httpPost(url);
//            //设置参数
//            List<NameValuePair> list = new ArrayList<>();
//            for (Map.Entry<String, String> elem : map.entrySet()) {
//                list.add(new BasicNameValuePair(elem.getKey(), elem.getValue()));
//            }
//            httpPost.setHeader("sessionId", sessionId);
//            httpPost.setHeader("Cookie", cookie);
//            httpPost.setHeader("language", "zh_CN");
//
//            entity = new UrlEncodedFormEntity(list, HttpConstant.UTF8_ENCODE);
//            httpPost.setEntity(entity);
//            log.info("请求{}接口的参数为{}", url, map);
//            //执行发送，获取相应结果
//            httpResponse = httpClient.execute(httpPost);
//            httpEntity = httpResponse.getEntity();
//            result = EntityUtils.toString(httpEntity);
//        } catch (Exception e) {
//            log.error("请求{}接口出现异常", url, e);
//            if (reSend > 0) {
//                log.info("请求{}出现异常:{}，进行重发。进行第{}次重发", url, e.getMessage(), (HttpConstant.REQ_TIMES - reSend + 1));
//                return sendPostByFormWithResponse(url, map, reSend - 1);
//            }
//        } finally {
//            try {
//                if (httpResponse != null) {
//                    EntityUtils.consume(httpResponse.getEntity());
//                }
//            } catch (IOException e) {
//                log.error("http请求释放资源异常", e);
//            }
//        }
//        //请求接口的响应时间
//        long endTime = System.currentTimeMillis();
//        log.info("请求{}接口的响应报文内容为{},本次请求API接口的响应时间为:{}毫秒", url, result, (endTime - startTime));
//        return result;
//    }

    /**
     * 通过post方式调用http接口.
     *
     * @param url    url路径
     * @param map    json格式的参数
     * @param reSend 重发次数
     * @param cookie Cookie
     * @return String
     */
//    public static String sendPostByFormWithResponse(String url, Map<String, String> map, int reSend, String cookie) {
//        //声明返回结果
//        String result = "";
//        //开始请求API接口时间
//        long startTime = System.currentTimeMillis();
//        UrlEncodedFormEntity entity;
//        HttpResponse httpResponse = null;
//        HttpClient httpClient = null;
//        HttpEntity httpEntity = null;
//        try {
//            // 创建连接
//            httpClient = HttpClientFactory.getInstance().getHttpClient();
//            // 设置请求头和报文
//            HttpPost httpPost = HttpClientFactory.getInstance().httpPost(url);
//            //设置参数
//            List<NameValuePair> list = new ArrayList<>();
//            for (Map.Entry<String, String> elem : map.entrySet()) {
//                list.add(new BasicNameValuePair(elem.getKey(), elem.getValue()));
//            }
//            httpPost.setHeader("Upgrade-Insecure-Requests", "1");
//            httpPost.setHeader("Cookie", cookie);
//            httpPost.setHeader("language", "zh_CN");
//
//            entity = new UrlEncodedFormEntity(list, HttpConstant.UTF8_ENCODE);
//            httpPost.setEntity(entity);
//            log.info("请求{}接口的参数为{}", url, map);
//            //执行发送，获取相应结果
//            httpResponse = httpClient.execute(httpPost);
//            httpEntity = httpResponse.getEntity();
//            int statusCode = httpResponse.getStatusLine().getStatusCode();
//            result = String.valueOf(statusCode);
//        } catch (Exception e) {
//            log.error("请求{}接口出现异常", url, e);
//            if (reSend > 0) {
//                log.info("请求{}出现异常:{}，进行重发。进行第{}次重发", url, e.getMessage(), (HttpConstant.REQ_TIMES - reSend + 1));
//                return sendPostByFormWithResponse(url, map, reSend - 1);
//            }
//        } finally {
//            try {
//                if (httpEntity != null) {
//                    EntityUtils.consume(httpEntity);
//                }
//            } catch (IOException e) {
//                log.error("http请求释放资源异常", e);
//            }
//        }
//        //请求接口的响应时间
//        long endTime = System.currentTimeMillis();
//        log.info("请求{}接口的响应报文内容为{},本次请求API接口的响应时间为:{}毫秒", url, result, (endTime - startTime));
//        return result;
//    }

    /**
     * 通过post方式调用http接口.
     *
     * @param url      url路径
     * @param xmlParam xml格式的参数
     * @param reSend   重发次数
     * @param token    token
     * @param menu     menu
     * @param cookie   cookie
     * @return String
     */
//    public static String sendPostByXml(String url, String xmlParam, int reSend, String token, String menu, String cookie) {
//        //声明返回结果
//        String result = "";
//        //开始请求API接口时间
//        long startTime = System.currentTimeMillis();
//        //请求API接口的响应时间
//        long endTime = 0L;
//        HttpEntity httpEntity = null;
//        HttpResponse httpResponse = null;
//        HttpClient httpClient = null;
//        try {
//            // 创建连接
//            httpClient = HttpClientFactory.getInstance().getHttpClient();
//            // 设置请求头和报文
//            HttpPost httpPost = HttpClientFactory.getInstance().httpPost(url);
//            StringEntity stringEntity = new StringEntity(xmlParam, HttpConstant.UTF8_ENCODE);
//            stringEntity.setContentEncoding(HttpConstant.UTF8_ENCODE);
//            stringEntity.setContentType(HttpConstant.TEXT_XML);
//            httpPost.setEntity(stringEntity);
//            log.info("请求{}接口的参数为{}", url, xmlParam);
//            //执行发送，获取相应结果
//            httpResponse = httpClient.execute(httpPost);
//            httpEntity = httpResponse.getEntity();
//            result = EntityUtils.toString(httpEntity, HttpConstant.UTF8_ENCODE);
//        } catch (Exception e) {
//            log.error("请求{}接口出现异常", url, e);
//            if (reSend > 0) {
//                log.info("请求{}出现异常:{}，进行重发。进行第{}次重发", url, e.getMessage(), (HttpConstant.REQ_TIMES - reSend + 1));
//                result = sendPostByJson(url, xmlParam, reSend - 1, token, menu, cookie);
//                if (result != null && !"".equals(result)) {
//                    return result;
//                }
//            }
//        } finally {
//            try {
//                EntityUtils.consume(httpEntity);
//            } catch (IOException e) {
//                log.error("http请求释放资源异常", e);
//            }
//            //请求接口的响应时间
//            endTime = System.currentTimeMillis();
//            log.info("请求{}接口的响应报文内容为{},本次请求API接口的响应时间为:{}毫秒", url, result, (endTime - startTime));
//            return result;
//        }
//
//    }


    /**
     * 通过get方式调用http接口.
     *
     * @param url    url路径
     * @param reSend 重发次数
     * @param param  Head参数(包含cookie等)
     * @return String
     */
    public static String sendGet(String url, int reSend, Map<String, String> param) {
        //声明返回结果
        String result = "";
        //开始请求API接口时间
        long startTime = System.currentTimeMillis();
        //请求API接口的响应时间
        long endTime = 0L;
        HttpEntity httpEntity = null;
        HttpResponse httpResponse = null;
        HttpClient httpClient = null;
        try {
            // 创建连接
            httpClient = HttpClientFactory.getInstance().getHttpClient();
            // 设置请求头和报文
            HttpGet httpGet = HttpClientFactory.getInstance().httpGet(url);
            for (Map.Entry<String, String> entry : param.entrySet()) {
                httpGet.setHeader(entry.getKey(), entry.getValue());
            }
            //执行发送，获取相应结果
            httpResponse = httpClient.execute(httpGet);
            httpEntity = httpResponse.getEntity();
            result = EntityUtils.toString(httpEntity);
        } catch (Exception e) {
            log.error("请求{}接口出现异常", url, e);
            if (reSend > 0) {
                log.info("请求{}出现异常:{}，进行重发。进行第{}次重发", url, e.getMessage(), (HttpConstant.REQ_TIMES - reSend + 1));
                result = sendGet(url, reSend - 1, param);
                if (result != null && !"".equals(result)) {
                    return result;
                }
            }
        } finally {
            try {
                EntityUtils.consume(httpEntity);
            } catch (IOException e) {
                log.error("http请求释放资源异常", e);
            }
        }
        //请求接口的响应时间
        endTime = System.currentTimeMillis();
        log.info("请求{}接口的响应报文内容为{},本次请求API接口的响应时间为:{}毫秒", url, result, (endTime - startTime));
        return result;

    }

    /**
     * 通过get方式调用http接口.
     *
     * @param uri    url路径
     * @param reSend 重发次数
     * @param token  token
     * @param menu   menu
     * @param cookie cookie
     * @return String
     */
//    public static String sendGet(URI uri, int reSend, String token, String menu, String cookie) {
//        //声明返回结果
//        String result = "";
//        //开始请求API接口时间
//        long startTime = System.currentTimeMillis();
//        //请求API接口的响应时间
//        long endTime = 0L;
//        HttpEntity httpEntity = null;
//        HttpResponse httpResponse = null;
//        HttpClient httpClient = null;
//        try {
//            // 创建连接
//            httpClient = HttpClientFactory.getInstance().getHttpClient();
//            // 设置请求头和报文
//            HttpGet httpGet = new HttpGet(uri);
//            Header header = new BasicHeader("Accept-Encoding", null);
//            //httpPost.setHeader(header);
//            httpGet.setHeader("token", token);
//            httpGet.setHeader("menu", menu);
//            httpGet.setHeader("Cookie", cookie);
//            //执行发送，获取相应结果
//            httpResponse = httpClient.execute(httpGet);
//            httpEntity = httpResponse.getEntity();
//            result = EntityUtils.toString(httpEntity);
//        } catch (Exception e) {
//            log.error("请求{}接口出现异常", uri, e);
//            if (reSend > 0) {
//                log.info("请求{}出现异常:{}，进行重发。进行第{}次重发", uri, e.getMessage(), (HttpConstant.REQ_TIMES - reSend + 1));
//                result = sendGet(uri, reSend - 1, token, menu, cookie);
//                if (result != null && !"".equals(result)) {
//                    return result;
//                }
//            }
//        } finally {
//            try {
//                EntityUtils.consume(httpEntity);
//            } catch (IOException e) {
//                log.error("http请求释放资源异常", e);
//            }
//        }
//        //请求接口的响应时间
//        endTime = System.currentTimeMillis();
//        log.info("请求{}接口的响应报文内容为{},本次请求API接口的响应时间为:{}毫秒", uri, result, (endTime - startTime));
//        return result;
//
//    }

    /**
     * 通过get方式调用http接口.
     *
     * @param uri    url路径
     * @param reSend 重发次数
     * @param cookie cookie
     * @return String
     */
    public static String sendGetHttp(URI uri, int reSend, String cookie) {
        //声明返回结果
        String result = "";
        //开始请求API接口时间
        long startTime = System.currentTimeMillis();
        //请求API接口的响应时间
        long endTime = 0L;
        HttpEntity httpEntity = null;
        HttpResponse httpResponse = null;
        HttpClient httpClient = null;
        try {
            // 创建连接
            httpClient = HttpClientFactory.getInstance().getHttpClient();
            // 设置请求头和报文
            HttpGet httpGet = new HttpGet(uri);
            //Header header = new BasicHeader("Accept-Encoding", null);
            //httpPost.setHeader(header);
            httpGet.setHeader("Cookie", cookie);
            //执行发送，获取相应结果
            httpResponse = httpClient.execute(httpGet);
            httpEntity = httpResponse.getEntity();
            result = new BufferedReader(new InputStreamReader(httpEntity.getContent())).lines().collect(Collectors.joining(System.lineSeparator()));
        } catch (Exception e) {
            log.error("请求{}接口出现异常", uri, e);
            if (reSend > 0) {
                log.info("请求{}出现异常:{}，进行重发。进行第{}次重发", uri, e.getMessage(), (HttpConstant.REQ_TIMES - reSend + 1));
                result = sendGetHttp(uri, reSend - 1, cookie);
                if (result != null && !"".equals(result)) {
                    return result;
                }
            }
        } finally {
            try {
                EntityUtils.consume(httpEntity);
            } catch (IOException e) {
                log.error("http请求释放资源异常", e);
            }
        }
        //请求接口的响应时间
        endTime = System.currentTimeMillis();
        log.info("请求{}接口的响应报文内容为{},本次请求API接口的响应时间为:{}毫秒", uri, result, (endTime - startTime));
        return result;

    }

    /**
     * superset登录获取csrf_token.
     *
     * @param uri           url路径
     * @param reSend        重发次数
     * @param token         token
     * @param menu          menu
     * @param cookie        cookie
     * @param authorization authorization
     * @return String
     */
//    public static String sendGet(URI uri, int reSend, String token, String menu, String cookie, String authorization) {
//        //声明返回结果
//        String result = "";
//        //开始请求API接口时间
//        long startTime = System.currentTimeMillis();
//        //请求API接口的响应时间
//        long endTime = 0L;
//        HttpEntity httpEntity = null;
//        HttpResponse httpResponse = null;
//        HttpClient httpClient = null;
//        try {
//            // 创建连接
//            httpClient = HttpClientFactory.getInstance().getHttpClient();
//            // 设置请求头和报文
//            HttpGet httpGet = new HttpGet(uri);
//            Header header = new BasicHeader("Accept-Encoding", null);
//            //httpPost.setHeader(header);
//            httpGet.setHeader("token", token);
//            httpGet.setHeader("Cookie", cookie);
//            httpGet.setHeader("Authorization", authorization);
//            //执行发送，获取相应结果
//            httpResponse = httpClient.execute(httpGet);
//            httpEntity = httpResponse.getEntity();
//            result = EntityUtils.toString(httpEntity);
//        } catch (Exception e) {
//            log.error("请求{}接口出现异常", uri, e);
//            if (reSend > 0) {
//                log.info("请求{}出现异常:{}，进行重发。进行第{}次重发", uri, e.getMessage(), (HttpConstant.REQ_TIMES - reSend + 1));
//                result = sendGet(uri, reSend - 1, token, menu, cookie, authorization);
//                if (result != null && !"".equals(result)) {
//                    return result;
//                }
//            }
//        } finally {
//            try {
//                EntityUtils.consume(httpEntity);
//            } catch (IOException e) {
//                log.error("http请求释放资源异常", e);
//            }
//        }
//        //请求接口的响应时间
//        endTime = System.currentTimeMillis();
//        log.info("请求{}接口的响应报文内容为{},本次请求API接口的响应时间为:{}毫秒", uri, result, (endTime - startTime));
//        return result;
//
//    }

    /**
     * 通过post方式调用http接口.
     *
     * @param url         url路径
     * @param jsonParam   jsonParam
     * @param reSend      重发次数
     * @param cookie      cookie
     * @param contentType contentType
     * @return String
     */
//    public static String sendPostByJson(String url, String jsonParam, int reSend, String cookie, String contentType) {
//        //声明返回结果
//        String result = "";
//        //开始请求API接口时间
//        long startTime = System.currentTimeMillis();
//        HttpEntity httpEntity = null;
//        HttpResponse httpResponse;
//        HttpClient httpClient;
//        try {
//            // 创建连接
//            httpClient = HttpClientFactory.getInstance().getHttpClient();
//            // 设置请求头和报文
//            HttpPost httpPost = HttpClientFactory.getInstance().httpPost(url);
//            httpPost.setHeader("Cookie", cookie);
////            httpPost.setHeader("Accept", HttpConstant.APPLICATION_JSON);
//            httpPost.setHeader("Content-Type", contentType);
//            // 设置报文和通讯格式
//            StringEntity stringEntity = new StringEntity(jsonParam, HttpConstant.UTF8_ENCODE);
////            stringEntity.setContentEncoding(HttpConstant.UTF8_ENCODE);
//            stringEntity.setContentType(contentType);
//            httpPost.setEntity(stringEntity);
//            log.info("请求:{} 接口的参数为:{}", url, jsonParam);
//            //执行发送，获取相应结果
//            httpResponse = httpClient.execute(httpPost);
//            httpEntity = httpResponse.getEntity();
//            result = EntityUtils.toString(httpEntity);
//        } catch (Exception e) {
//            log.error("请求{}接口出现异常", url, e);
//            if (reSend > 0) {
//                log.info("请求{}出现异常:{}，进行重发", url, e.getMessage());
//                result = sendPostByJson(url, jsonParam, reSend - 1, cookie);
//                if (StringUtils.isNotBlank(result)) {
//                    return result;
//                }
//            }
//        } finally {
//            try {
//                EntityUtils.consume(httpEntity);
//            } catch (IOException e) {
//                log.error("http请求释放资源异常", e);
//            }
//        }
//        //请求API接口的响应时间
//        long endTime = System.currentTimeMillis();
//        log.info("请求{}接口的响应报文内容为{},本次请求API接口的响应时间为:{}毫秒", url, result, (endTime - startTime));
//        return result;
//    }

    /**
     * 通过get方式调用http接口.
     *
     * @param url       url路径
     * @param jsonParam jsonParam
     * @param reSend    重发次数
     * @param cookie    cookie
     * @param sessionId sessionId
     * @return String
     */
//    public static String sendPost(String url, String jsonParam, int reSend, String sessionId, String cookie) {
//        //声明返回结果
//        String result = "";
//        //开始请求API接口时间
//        long startTime = System.currentTimeMillis();
//        //请求API接口的响应时间
//        long endTime = 0L;
//        HttpEntity httpEntity = null;
//        HttpResponse httpResponse = null;
//        HttpClient httpClient = null;
//        try {
//            // 创建连接
//            httpClient = HttpClientFactory.getInstance().getHttpClient();
//            // 设置请求头和报文
//            HttpPost httpPost = HttpClientFactory.getInstance().httpPost(url);
//            Header header = new BasicHeader("Accept-Encoding", null);
//            //httpPost.setHeader(header);
//            httpPost.setHeader("sessionId", sessionId);
//            httpPost.setHeader("Cookie", cookie);
//            // 设置报文和通讯格式
//            StringEntity stringEntity = new StringEntity(jsonParam, HttpConstant.UTF8_ENCODE);
//            stringEntity.setContentEncoding(HttpConstant.UTF8_ENCODE);
//            stringEntity.setContentType(HttpConstant.APPLICATION_JSON);
//            httpPost.setEntity(stringEntity);
//            log.info("请求{}接口的参数为{}", url, jsonParam);
//            httpResponse = httpClient.execute(httpPost);
//            httpEntity = httpResponse.getEntity();
//            result = EntityUtils.toString(httpEntity);
//        } catch (Exception e) {
//            log.error("请求{}接口出现异常", url, e);
//            if (reSend > 0) {
//                log.info("请求{}出现异常:{}，进行重发。进行第{}次重发", url, e.getMessage(), (HttpConstant.REQ_TIMES - reSend + 1));
//                result = sendPostByJson(url, jsonParam, reSend - 1, sessionId, cookie);
//                if (result != null && !"".equals(result)) {
//                    return result;
//                }
//            }
//        } finally {
//            try {
//                EntityUtils.consume(httpEntity);
//            } catch (IOException e) {
//                log.error("http请求释放资源异常", e);
//            }
//        }
//        //请求接口的响应时间
//        endTime = System.currentTimeMillis();
//        log.info("请求{}接口的响应报文内容为{},本次请求API接口的响应时间为:{}毫秒", url, result, (endTime - startTime));
//        return result;
//    }

    /**
     * 证书管理.
     */
    private static final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[]{};
        }

        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }
    }};

    private static void trustAllHosts(HttpsURLConnection connection) {
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            SSLSocketFactory newFactory = sc.getSocketFactory();

            connection.setSSLSocketFactory(newFactory);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        connection.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });
    }
}
