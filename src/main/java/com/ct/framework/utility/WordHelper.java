package com.ct.framework.utility;

import com.ct.framework.utility.http.HttpConstant;
import com.ct.framework.core.configuration.HttpContext;
import com.ct.framework.core.exception.ErrorCode;
import com.ct.framework.core.exception.GlobalException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTc;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   yan hongbo
 * Create Date:  01/06/2021
 * Usage: Word工具类 倒入与导出.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1                                          新增
 * @author xxx
 *****************************************************************/
public final class WordHelper {

    /**
     * 私有构造函数
     */
    private WordHelper() {
    }

    /**
     * 根据模板导出文件
     *
     * @param templatePath
     * @param fileName
     * @param paraParams
     * @param tableParamsList
     * @throws Exception
     */
    public static void exportWord(String templatePath, String fileName, Map<String, Object> paraParams, List<Map<String, Object>> tableParamsList) throws Exception {
        download(initWord(templatePath, paraParams, tableParamsList), fileName);
    }

    /**
     * 初始化文件
     *
     * @param templatePath
     * @param paraParams
     * @param tableParamsList
     * @return
     * @throws IOException
     */
    public static XWPFDocument initWord(String templatePath, Map<String, Object> paraParams, List<Map<String, Object>> tableParamsList) throws IOException {
        InputStream is = WordHelper.class.getClassLoader().getResourceAsStream(templatePath);
        XWPFDocument document = new XWPFDocument(is);
        if (MapUtils.isNotEmpty(paraParams)) {
            replaceInPara(document, paraParams);
        }
        if (CollectionUtils.isNotEmpty(tableParamsList)) {
            replaceInTable(document, tableParamsList);
        }
        return document;
    }

    /**
     * 添加自定义表格
     *
     * @param document
     * @param tableName
     * @param values
     */
    public static void enhanceTable(XWPFDocument document, String tableName, List<List<String>> values) {
        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun run = paragraph.insertNewRun(0);
        run.setText(tableName);
        run.setBold(true);

        XWPFTable table = document.createTable(values.size(), values.get(0).size());
        for (int i = 0; i < values.size(); i++) {
            XWPFTableRow row = table.getRow(i);
            List<String> value = values.get(i);
            for (int j = 0; j < value.size(); j++) {
                if (i == 0) {
                    row.getCell(j).setColor("CCCCCC");
                }
                row.getCell(j).setText(value.get(j));
            }
        }

        XWPFParagraph p = document.createParagraph();
        run = p.createRun();
        run.addCarriageReturn();
    }

    /**
     * 文件下载
     *
     * @param document
     * @param fileName
     */
    public static void download(XWPFDocument document, String fileName) {
        try {
            HttpContext.Current.GetHttpResponse().reset();
            HttpContext.Current.GetHttpResponse().setCharacterEncoding(HttpConstant.UTF8_ENCODE);
            HttpContext.Current.GetHttpResponse().setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes("gbk"), "iso8859-1"));
            HttpContext.Current.GetHttpResponse().setHeader("Access-Control-Allow-Origin", "*");
            HttpContext.Current.GetHttpResponse().setContentType("application/octet-stream");
            OutputStream toClient = new BufferedOutputStream(HttpContext.Current.GetHttpResponse().getOutputStream());
            document.write(toClient);
            toClient.flush();
            toClient.close();
        } catch (IOException e) {
            throw new GlobalException(e.getMessage(), ErrorCode.PAGE_FILE_DOWNLOAD_FAILER);
        }
    }

    /**
     * 模板文档占位符替换
     *
     * @param document
     * @param params
     */
    private static void replaceInPara(XWPFDocument document, Map<String, Object> params) {
        Iterator<XWPFParagraph> iterator = document.getParagraphsIterator();
        XWPFParagraph paragraph;
        while (iterator.hasNext()) {
            paragraph = iterator.next();
            replaceInPara(paragraph, params);
        }
    }

    /**
     * 模板文档占位符替换
     *
     * @param paragraph
     * @param params
     */
    private static void replaceInPara(XWPFParagraph paragraph, Map<String, Object> params) {
        List<XWPFRun> runs;
        Matcher matcher;
        if (matcher(paragraph.getParagraphText()).find()) {
            runs = paragraph.getRuns();

            int start = -1;
            int end = -1;
            String str = "";
            for (int i = 0; i < runs.size(); i++) {
                XWPFRun run = runs.get(i);
                String runText = run.toString();
                if ('$' == runText.charAt(0) && '{' == runText.charAt(1)) {
                    start = i;
                }
                if (start != -1) {
                    str += runText;
                }
                if ('}' == runText.charAt(runText.length() - 1)) {
                    if (start != -1) {
                        end = i;
                        break;
                    }
                }
            }

            for (int i = start; i <= end; i++) {
                paragraph.removeRun(i);
                i--;
                end--;
            }

            for (String key : params.keySet()) {
                if (str.equals(key)) {
                    String param = String.valueOf(params.get(key));
                    if (param.indexOf("\n") > -1) {
                        String[] texts = param.split("\n");
                        for (int i = 0; i < texts.length; i++) {
                            paragraph.createRun().setText(texts[i]);
                            paragraph.createRun().addCarriageReturn();
                        }
                    } else {
                        paragraph.createRun().setText(param);
                    }
                }
            }
        }
    }

    /**
     * 模板表格占位符替换
     *
     * @param document
     * @param tableFillList
     */
    private static void replaceInTable(XWPFDocument document, List<Map<String, Object>> tableFillList) {
        Iterator<XWPFTable> iterator = document.getTablesIterator();
        XWPFTable table;
        List<XWPFTableRow> rows;
        List<XWPFTableCell> cells;
        List<XWPFParagraph> paragraphs;

        while (iterator.hasNext()) {
            table = iterator.next();
            rows = table.getRows();
            addRows(table, 1, tableFillList.size(), 1);

            for (int i = 1; i < rows.size(); i++) {
                XWPFTableRow row = rows.get(i);
                cells = row.getTableCells();
                for (XWPFTableCell cell : cells) {
                    paragraphs = cell.getParagraphs();
                    for (XWPFParagraph paragraph : paragraphs) {
                        replaceInPara(paragraph, tableFillList.get(i - 1));
                    }
                }
            }
        }
    }

    /**
     * 表格添加行
     *
     * @param table
     * @param source
     * @param rows
     * @param insertRowIndex
     */
    private static void addRows(XWPFTable table, int source, int rows, int insertRowIndex) {

        for (int i = 1; i < rows; i++) {
            XWPFTableRow sourceRow = table.getRow(source);
            XWPFTableRow targetRow = table.insertNewTableRow(insertRowIndex++);
            targetRow.getCtRow().setTrPr(sourceRow.getCtRow().getTrPr());
            List<XWPFTableCell> sourceCells = sourceRow.getTableCells();
            for (XWPFTableCell sourceCell : sourceCells) {
                XWPFTableCell newCell = targetRow.addNewTableCell();
                newCell.setText(sourceCell.getText());
                newCell.getCTTc().setTcPr(sourceCell.getCTTc().getTcPr());
                newCell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
                CTPPr pPr = sourceCell.getCTTc().getPList().get(0).getPPr();
                if (pPr != null && pPr.getJc() != null && pPr.getJc().getVal() != null) {
                    CTTc ctTc = newCell.getCTTc();
                    CTP ctp = ctTc.getPList().get(0);
                    CTPPr ctpPr = ctp.getPPr();
                    if (ctpPr == null) {
                        ctpPr = ctp.addNewPPr();
                    }
                    CTJc ctJc = ctpPr.getJc();
                    if (ctJc == null) {
                        ctJc = ctpPr.addNewJc();
                    }
                    ctJc.setVal(pPr.getJc().getVal());
                }

                List<XWPFParagraph> sourceParagraphs = sourceCell.getParagraphs();
                if (StringUtils.isBlank(sourceCell.getText())) {
                    continue;
                }
                XWPFParagraph sourceParagraph = sourceParagraphs.get(0);
                List<XWPFParagraph> targetParagraphs = newCell.getParagraphs();
                sourceParagraph.getRuns();
                if (StringUtils.isBlank(newCell.getText())) {
                    XWPFParagraph ph = newCell.addParagraph();
                    ph.getCTP().setPPr(sourceParagraph.getCTP().getPPr());
                    XWPFRun run = ph.getRuns().isEmpty() ? ph.createRun() : ph.getRuns().get(0);
                    run.setFontFamily(sourceParagraph.getRuns().get(0).getFontFamily());
                } else {
                    XWPFParagraph ph = targetParagraphs.get(0);
                    ph.getCTP().setPPr(sourceParagraph.getCTP().getPPr());
                    XWPFRun run = ph.getRuns().isEmpty() ? ph.createRun() : ph.getRuns().get(0);
                    run.setFontFamily(sourceParagraph.getRuns().get(0).getFontFamily());
                }
            }
        }
    }

    /**
     * 字符串匹配
     *
     * @param str
     * @return
     */
    private static Matcher matcher(String str) {
        Pattern pattern = Pattern.compile("\\$\\{(.+?)\\}", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(str);
        return matcher;
    }
}
