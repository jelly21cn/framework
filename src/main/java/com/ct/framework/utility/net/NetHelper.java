package com.ct.framework.utility.net;

import com.ct.framework.core.exception.ErrorCode;
import com.ct.framework.core.exception.GlobalException;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.ServerSocket;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage:
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public final class NetHelper {
    /**
     * 私有构造函数.
     */
    private NetHelper() {
    }

    /**
     * 65535.
     */
    private static final int SIX = 65535;

    /**
     * 查找有效的端口号.
     *
     * @param defaultPort 端口号
     * @return int
     */
    public static int findAvailablePort(int defaultPort) {
        int portTmp = defaultPort;
        while (portTmp < SIX) {
            if (!isPortUsed(portTmp)) {
                return portTmp;
            } else {
                portTmp++;
            }
        }
        portTmp = defaultPort--;
        while (portTmp > 0) {
            if (!isPortUsed(portTmp)) {
                return portTmp;
            } else {
                portTmp--;
            }
        }
        throw new GlobalException("framework-rpc 端口无效!", ErrorCode.NET_RPC_ERROR);
    }

    /**
     * 判断端口号是否使用.
     *
     * @param port 端口号
     * @return boolean
     */
    public static boolean isPortUsed(int port) {
        boolean used = false;
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(port);
            used = false;
        } catch (IOException e) {
            log.info(">>>>>>>>>>> framework-rpc, port[{}] is in use.", port);
            used = true;
        } finally {
            if (serverSocket != null) {
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    log.info("");
                }
            }
        }
        return used;
    }

}
