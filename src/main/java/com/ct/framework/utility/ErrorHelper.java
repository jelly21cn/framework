package com.ct.framework.utility;

import com.ct.framework.contract.DefaultDataContract;
import com.ct.framework.contract.MessageFault;
import com.ct.framework.contract.MessageFaultCollection;
import com.ct.framework.core.exception.ErrorCode;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   april_niu
 * Create Date:  2022-06-27
 * Usage: 错误信息
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2022-06-27              april_niu                   错误信息
 * @author april_niu
 *****************************************************************/
public final class ErrorHelper {
    /**
     * 私有构造函数.
     */
    private ErrorHelper() {
    }

    /**
     * .
     */
    private static final MessageFaultCollection MESSAGE_FAULT_COLLECTION = new MessageFaultCollection();

    /**
     * errorInfo.
     *
     * @param <T>       T
     * @param className className
     * @param t         范型
     * @return T
     */
    public static <T extends DefaultDataContract> T Error(T t, String className) {
        return errorInfo(t, className, null, null);
    }

    /**
     * errorInfo.
     *
     * @param <T>              T
     * @param className        className
     * @param errorDescription 错误描述
     * @param t                范型
     * @return T
     */
    public static <T extends DefaultDataContract> T Error(T t, String className, String errorDescription) {
        return errorInfo(t, className, null, errorDescription);
    }

    /**
     * errorInfo.
     *
     * @param <T>              T
     * @param className        className
     * @param errorCode        错误码
     * @param errorDescription 错误描述
     * @param t                范型
     * @return T
     */
    public static <T extends DefaultDataContract> T Error(T t, String className, String errorCode, String errorDescription) {
        return errorInfo(t, className, errorCode, errorDescription);
    }

    /**
     * 清空日志
     */
    public static void Clear() {
        MESSAGE_FAULT_COLLECTION.clear();
    }

    /**
     * 发送日志到 日志中心
     */
    public static void SendMessage() {
        //TODO

        //For 循坏 MESSAGE_FAULT_COLLECTION
    }

    /**
     * errorInfo.
     *
     * @param <T>              T
     * @param className        className
     * @param errorCode        错误码
     * @param errorDescription 错误描述
     * @param t                范型
     * @return T
     */
    private static <T extends DefaultDataContract> T errorInfo(T t, String className, String errorCode, String errorDescription) {
        MessageFault messageFault1 = new MessageFault();
        messageFault1.setErrorDetail(className);
        messageFault1.setErrorCode(errorCode == null ? ErrorCode.SYS_ERROR.GetCode() : errorCode);
        messageFault1.setErrorDescription(errorDescription == null ? ErrorCode.SYS_ERROR.GetMessage() : errorDescription);
        MESSAGE_FAULT_COLLECTION.add(messageFault1);
        t.SetFaults(MESSAGE_FAULT_COLLECTION);
        return t;
    }

}
