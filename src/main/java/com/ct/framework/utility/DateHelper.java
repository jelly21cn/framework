package com.ct.framework.utility;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: 时间处理类.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1                                          新增
 * @author xxx
 *****************************************************************/
public final class DateHelper {

    /**
     * 实现格式化的时间字符串转时间对象.
     */
    private static final String DATE_FORMAT_DEFAULT = "yyyy-MM-dd HH:mm:ss";
    /**
     * 实现格式化的时间字符串转时间对象.
     */
    private static final String DATE_FORMAT_SIMPLE = "yyyyMMddHHmmss";
    /**
     * yyyy-MM-dd.
     */
    private static final String YYYY_MM_DD = "yyyy-MM-dd";

    /**
     * yyyy-MM-dd HH:mm
     */
    private static final String YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";

    /**
     * 时间转换线程.
     */
    private static final ThreadLocal<Map<String, DateFormat>> dateFormatThreadLocal = new ThreadLocal<Map<String, DateFormat>>();

    /**
     * 私有构造函数.
     */
    private DateHelper() {
    }

    /**
     * 日期格式化.
     *
     * @param date yyyy-MM-dd HH:mm:ss
     * @return Date
     */
    public static Date parseYyyyMMddHHmmss(String date) {
        if (date == null) {
            return null;
        }
        try {
            if (date.contains("T")) {
                date = date.replace("T", " ");
            }
            // 正则表达式匹配
            if (date.matches("\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}")) {
                return new SimpleDateFormat(DATE_FORMAT_DEFAULT).parse(date);
            } else if (date.matches("\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}")) {
                return new SimpleDateFormat(YYYY_MM_DD_HH_MM).parse(date);
            } else if (date.matches("\\d{4}-\\d{2}-\\d{2}")) {
                return new SimpleDateFormat(YYYY_MM_DD).parse(date);
            } else if (date.matches("\\d{4}\\d{2}\\d{2}\\d{2}\\d{2}\\d{2}")) {
                return new SimpleDateFormat(DATE_FORMAT_SIMPLE).parse(date);
            } else {
                throw new RuntimeException("日期格式不正确");
            }
        } catch (ParseException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /**
     * 日期格式化.
     *
     * @param date yyyy-MM-dd
     * @return Date
     */
//    public static Date parseYyyyMMdd(String date) {
//        if (date == null) {
//            return null;
//        }
//        try {
//            return new SimpleDateFormat("yyyy-MM-dd").parse(date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//            throw new RuntimeException(e);
//        }
//    }

    /**
     * 日期格式化yyyy-MM.
     *
     * @param date yyyy-MM
     * @return String
     */
//    public static String parseYyyyMM(Date date) {
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM");
//        String dateString = formatter.format(date);
//        return dateString;
//    }

    /**
     * 日期格式化yyyy-MM.
     *
     * @param date yyyy-MM
     * @return String
     */
    public static String parseYyyyMMdd(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat(YYYY_MM_DD);
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * 日期转化为字符串,格式为yyyy-MM-dd HH:mm:ss.
     *
     * @param date 时间格式
     * @return string
     */
//    public static String getYyyyMMddHHmmss(Date date) {
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT_DEFAULT);
//        return simpleDateFormat.format(date);
//    }

    /**
     * 获取startDate日期后month月的日期.
     *
     * @param startDate 开始日期
     * @param month     几个月后
     * @return Date
     */
//    public static Date getMonthAfterDate(Date startDate, int month) {
//        LocalDateTime localDateTime = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().plusMonths(month);
//        Date date = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
//        return date;
//    }

    /**
     * 获取startDate日期后month月的日期.
     *
     * @param startDate 开始日期
     * @param month     几个月前
     * @return Date
     */
//    public static Date getMonthBeforeDate(Date startDate, int month) {
//        LocalDateTime localDateTime = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().minusMonths(month);
//        Date date = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
//        return date;
//    }

    /**
     * 获得指定日期的前一天.
     *
     * @param specifiedDay yy-MM-dd
     * @param days         Date
     * @return string
     */
//    public static String getBeforeDay(String specifiedDay, Integer days) {
//        Calendar c = Calendar.getInstance();
//        Date date = null;
//        try {
//            date = new SimpleDateFormat(YYYY_MM_DD).parse(specifiedDay);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        c.setTime(date);
//        int day = c.get(Calendar.DATE);
//        c.set(Calendar.DATE, day - days);
//        String dayBefore = new SimpleDateFormat(YYYY_MM_DD).format(c.getTime());
//        return dayBefore;
//    }

    /**
     * 获得指定日期的后一天.
     *
     * @param specifiedDay yy-MM-dd
     * @param days         days
     * @return string
     */
//    public static String getAfterDay(String specifiedDay, Integer days) {
//        Calendar c = Calendar.getInstance();
//        Date date = null;
//        try {
//            date = new SimpleDateFormat(YYYY_MM_DD).parse(specifiedDay);
//        } catch (ParseException e) {
//            e.printStackTrace();
//            System.out.println(e);
//        }
//        c.setTime(date);
//        int day = c.get(Calendar.DATE);
//        c.set(Calendar.DATE, day + days);
//        String dayAfter = new SimpleDateFormat(YYYY_MM_DD).format(c.getTime());
//        return dayAfter;
//    }

    /**
     * .
     *
     * @param yyyyMM  yy-MM-dd
     * @param endTime endTime
     * @return string
     */
//    public static String getBillStartTime(String yyyyMM, int endTime) {
//        String endTimeStr = yyyyMM + "-" + String.format("%02d", endTime) + " 23:59:59";
//        return endTimeStr;
//    }

    /**
     * .
     *
     * @param endTimeStr endTimeStr
     * @return string
     */
//    public static String getBillEndTime(String endTimeStr) {
//        String temp = getYyyyMMddHHmmss(getMonthBeforeDate(parseYyyyMMdd(endTimeStr.split(" ")[0]), 1));
//        String billEndTime = getAfterDay(temp.split(" ")[0], 1) + " 00:00:00";
//        return billEndTime;
//    }

    /**
     * .
     *
     * @param endTimeStr   yy-MM-dd
     * @param billLastTime billLastTime
     * @return string
     */
//    public static String getBillPayDay(String endTimeStr, int billLastTime) {
//        String payDay = getAfterDay(endTimeStr.split(" ")[0], billLastTime) + " 23:59:59";
//        return payDay;
//    }

    /**
     * 日期格式转换yyyy-MM-dd'T'HH:mm:ss.SSSXXX  (yyyy-MM-dd'T'HH:mm:ss.SSSZ) TO  yyyy-MM-dd HH:mm:ss.
     * 2020-04-09T23:00:00.000+08:00 TO 2020-04-09 23:00:00.
     *
     * @param oldDateStr oldDateStr
     * @return String
     */
//    public static String dealDateFormat(String oldDateStr) throws ParseException {
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");  //yyyy-MM-dd'T'HH:mm:ss.SSSZ
//        Date date = df.parse(oldDateStr);
//        SimpleDateFormat df1 = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.UK);
//        Date date1 = df1.parse(date.toString());
//        DateFormat df2 = new SimpleDateFormat(DATE_FORMAT_DEFAULT);
//        return df2.format(date1);
//    }

    /**
     * 日期格式转换 yyyy-MM-dd HH:mm:ss  TO yyyy-MM-dd'T'HH:mm:ss.SSSXXX  (yyyy-MM-dd'T'HH:mm:ss.SSSZ).
     * 2020-04-09 23:00:00 TO 2020-04-09T23:00:00.000+08:00.
     *
     * @param oldDateStr oldDateStr
     * @return String
     */
//    public static String dealDateFormatReverse(String oldDateStr) throws ParseException {
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
//        DateFormat df2 = new SimpleDateFormat(DATE_FORMAT_DEFAULT);
//        Date date1 = df2.parse(oldDateStr);
//        return df.format(date1);
//    }

    /**
     * .
     *
     * @param date date
     * @return Date
     */
//    public static Date parseISO8601TimeStr(String date) {
//        if (date == null) {
//            return null;
//        } else {
//            try {
//                return (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")).parse(date);
//            } catch (ParseException var2) {
//                var2.printStackTrace();
//                throw new RuntimeException(var2);
//            }
//        }
//    }

    /**
     * 格式时间. 示例 "yyyy-MM-dd HH:mm:ss"
     *
     * @param date 时间
     * @return String
     */
    public static String FormatDateTime(Date date) {
        return format(date, DATE_FORMAT_DEFAULT);
    }

    /**
     * 格式时间. 示例 "yyyyMMddHHmmss"
     *
     * @param date 时间
     * @return String
     */
    public static String FormatDateTimeSimple(Date date) {
        return format(date, DATE_FORMAT_SIMPLE);
    }

    // ---------------------- 增加日期 ----------------------
    public static Date addYears(final Date date, final int amount) {
        return add(date, Calendar.YEAR, amount);
    }

    public static Date addMonths(final Date date, final int amount) {
        return add(date, Calendar.MONTH, amount);
    }

    public static Date addDays(final Date date, final int amount) {
        return add(date, Calendar.DAY_OF_MONTH, amount);
    }

    public static Date addHours(final Date date, final int amount) {
        return add(date, Calendar.HOUR_OF_DAY, amount);
    }

    public static Date addMinutes(final Date date, final int amount) {
        return add(date, Calendar.MINUTE, amount);
    }

    private static Date add(final Date date, final int calendarField, final int amount) {
        if (date == null) {
            return null;
        }
        final Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(calendarField, amount);
        return c.getTime();
    }

    private static DateFormat getDateFormat(String pattern) {
        if (pattern == null || pattern.trim().length() == 0) {
            throw new IllegalArgumentException("pattern cannot be empty.");
        }

        Map<String, DateFormat> dateFormatMap = dateFormatThreadLocal.get();
        if (dateFormatMap != null && dateFormatMap.containsKey(pattern)) {
            return dateFormatMap.get(pattern);
        }

        synchronized (dateFormatThreadLocal) {
            if (dateFormatMap == null) {
                dateFormatMap = new HashMap<String, DateFormat>();
            }
            dateFormatMap.put(pattern, new SimpleDateFormat(pattern));
            dateFormatThreadLocal.set(dateFormatMap);
        }

        return dateFormatMap.get(pattern);
    }

    /**
     * 格式时间.
     *
     * @param date   时间
     * @param patten 格式
     * @return String
     */
    private static String format(Date date, String patten) {
        return getDateFormat(patten).format(date);
    }
}
