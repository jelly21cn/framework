package com.ct.framework.utility;

import com.ct.framework.utility.string.StringHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;
import com.google.gson.ToNumberPolicy;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: GSON封装.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public final class SerializeHelper {
    /**
     * 私有构造函数.
     */
    private SerializeHelper() {
    }

    /**
     * 实现格式化的时间字符串转时间对象.
     */
    private static final String DATE_FORMAT_DEFAULT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 对象 转成 json.
     *
     * @param src 对象
     * @return String
     */
    public static String JsonSerializer(Object src) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat(DATE_FORMAT_DEFAULT);
        Gson gson = gsonBuilder.create();
        return gson.toJson(src);
    }

    /**
     * 将对象序列化为Json格式.
     *
     * @param data   date
     * @param format 格式化
     * @return string
     */
    public static String JsonSerializer(Object data, boolean format) {
        return JsonSerializerLongToString(data, format, false);
    }

    /**
     * 将对象序列化为Json格式（支持长整型转换为字符串）.
     *
     * @param data         date
     * @param format       格式化
     * @param longToString longToString
     * @return string
     */
    public static String JsonSerializer(Object data, boolean format, boolean longToString) {
        return JsonSerializerLongToString(data, format, longToString);
    }

    /**
     * 将Long转为String.
     *
     * @param data         data
     * @param format       format
     * @param longToString longToString
     * @return String
     */
    private static String JsonSerializerLongToString(Object data, boolean format, boolean longToString) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat(DATE_FORMAT_DEFAULT);
        if (longToString) {
            gsonBuilder.setLongSerializationPolicy(LongSerializationPolicy.STRING);
        }

        if (format) {
            gsonBuilder.setPrettyPrinting();
        }

        Gson gson = gsonBuilder.create();

        return gson.toJson(data);
    }

    /**
     * 将Json反序列化为对象.
     *
     * @param <T>  范型
     * @param cls  类型
     * @param json json
     * @return T t
     */
    public static <T> T JsonDeserialize(String json, Class<T> cls) {
        //非Json格式的字符串
        if (!Validate(json)) {
            return null;
        }

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat(DATE_FORMAT_DEFAULT);
        //gson解析json，将数值默认转为double导致精度缺失，修改解析策略以解决此问题
        gsonBuilder.setObjectToNumberStrategy(ToNumberPolicy.LONG_OR_DOUBLE);
        Gson gson = gsonBuilder.create();
        return gson.fromJson(json, cls);
    }

    /**
     * json 转成 特定的 rawClass<classOfT> 的Object.
     *
     * @param json    json
     * @param fromCls 当前类型
     * @param toCls   特定类型
     * @param <T>     范型
     * @return T t
     */
    public static <T> T JsonDeserialize(String json, Class<T> fromCls, Class toCls) {
        //非Json格式的字符串
        if (!Validate(json)) {
            return null;
        }

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat(DATE_FORMAT_DEFAULT);
        gsonBuilder.setObjectToNumberStrategy(ToNumberPolicy.LONG_OR_DOUBLE);
        Gson gson = gsonBuilder.create();

        Type type = new ParameterizedType4ReturnT(fromCls, new Class[]{toCls});

        return gson.fromJson(json, type);
    }


    /**
     * 将对象-->byte[].
     *
     * @param object object
     * @return byte
     */
    public static byte[] serialize(Object object) {
        ObjectOutputStream oos = null;
        ByteArrayOutputStream baos = null;
        try {
            // 序列化
            baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            byte[] bytes = baos.toByteArray();
            return bytes;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                oos.close();
                baos.close();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
        return null;
    }


    /**
     * 将byte[] -->Object.
     *
     * @param bytes bytes
     * @param <T>   范型
     * @param clazz 类型
     * @return Object
     */
    public static <T> Object deserialize(byte[] bytes, Class<T> clazz) {
        ByteArrayInputStream bais = null;
        try {
            // 反序列化
            bais = new ByteArrayInputStream(bytes);
            ObjectInputStream ois = new ObjectInputStream(bais);
            return ois.readObject();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                bais.close();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
        return null;
    }

    /**
     * 验证json格式.
     *
     * @param jsonStr json字符串
     * @return boolean
     */
    private static boolean Validate(String jsonStr) {
        boolean result = false;
        if (StringHelper.isNotBlank(jsonStr)) {
            jsonStr = jsonStr.trim();
            if (jsonStr.startsWith("{") && jsonStr.endsWith("}")) {
                result = true;
            } else if (jsonStr.startsWith("[") && jsonStr.endsWith("]")) {
                result = true;
            }
        }
        return result;
    }

    /**
     * 类型转换器.
     */
    public static class ParameterizedType4ReturnT implements ParameterizedType {
        private final Class raw;
        private final Type[] args;

        public ParameterizedType4ReturnT(Class raw, Type[] args) {
            this.raw = raw;
            this.args = args != null ? args : new Type[0];
        }

        @Override
        public Type[] getActualTypeArguments() {
            return args;
        }

        @Override
        public Type getRawType() {
            return raw;
        }

        @Override
        public Type getOwnerType() {
            return null;
        }
    }
}
