package com.ct.framework.utility;

import com.ct.framework.core.exception.ErrorCode;
import com.ct.framework.core.exception.GlobalException;

import java.util.Set;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.function.Predicate;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/04/2022
 * Usage: 线程池.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public final class ThreadPoolHelper {
    /**
     * 私有构造函数.
     */
    private ThreadPoolHelper() {
    }

    /**
     * 60L.
     */
    private static final long SIXTY = 60L;


    /**
     * 1000.
     */
    private static final int THOUSAND = 1000;

    /**
     * make server thread pool.
     *
     * @param serverType   serverType
     * @param corePoolSize corePoolSize
     * @param maxPoolSize  maxPoolSize
     * @return ThreadPoolExecutor
     */
    public static ThreadPoolExecutor makeServerThreadPool(final String serverType, int corePoolSize, int maxPoolSize) {
        ThreadPoolExecutor serverHandlerPool = new ThreadPoolExecutor(
                corePoolSize,
                maxPoolSize,
                SIXTY,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>(THOUSAND),
                new ThreadFactory() {
                    @Override
                    public Thread newThread(Runnable r) {
                        return new Thread(r, "framework-rpc, " + serverType + "-serverHandlerPool-" + r.hashCode());
                    }
                },
                new RejectedExecutionHandler() {
                    @Override
                    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                        throw new GlobalException("framework-rpc " + serverType + " Thread pool is EXHAUSTED!", ErrorCode.NET_RPC_ERROR);
                    }
                });

        return serverHandlerPool;
    }

    /**
     * 去重操作.
     *
     * @param <T>          范型
     * @param keyExtractor keyExtractor
     * @return Predicate
     */
    public static <T> Predicate<T> DistinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

}
