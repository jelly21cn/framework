package com.ct.framework.utility;

import com.ct.framework.utility.http.HttpConstant;
import com.ct.framework.utility.string.StringHelper;
import com.ct.framework.core.annotation.ExcelHeader;
import com.ct.framework.core.annotation.ExcelHeaderTransform;
import com.ct.framework.core.configuration.HttpContext;
import com.ct.framework.core.configuration.SystemConfigurationManager;
import com.ct.framework.core.entity.ExcelTypeAdapter;
import com.ct.framework.core.exception.ErrorCode;
import com.ct.framework.core.exception.GlobalException;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.ProgressListener;
import org.apache.commons.fileupload.RequestContext;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.servlet.ServletRequestContext;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tomcat.util.collections.CaseInsensitiveKeyMap;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage: Excel工具类 倒入与导出.
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1                                          新增
 * @author xxx
 *****************************************************************/
public final class ExcelHelper {
    /**
     * 私有构造函数.
     */
    private ExcelHelper() {
    }

    /**
     * 12.
     */
    private static final short TWELVE = 12;
    /**
     * 13.
     */
    private static final int THIRTEEN = 13;

    /**
     * 100.
     */
    private static final int HUNDRED = 100;

    /**
     * 1024.
     */
    private static final int ONE_THOUSAND_TWENTY_FOUR = 1024;

    /**
     * 默认列数量 30.
     */
    private static final int DEFAULT_COLUMN_SIZE = 30;

    /**
     * Excel 导出，POI实现.
     *
     * @param fileName   文件名
     * @param sheetName  sheet页名称
     * @param sheetTitle 表头列表名
     * @param clazz      clazz
     * @param objects    目标数据集
     */
    public static void writeExcel(String fileName, String sheetName, String sheetTitle, List<?> objects, Class<?> clazz) throws Exception {
        exportExcel(fileName, sheetName, sheetTitle, objects, clazz);
    }

    /**
     * Excel 导出，POI实现
     *
     * @param fileName   文件名
     * @param sheetName  sheet页名称
     * @param sheetTitle 表头列表名
     * @param sheetTitle sheet页Title
     * @param objects    目标数据集
     */
    public static void writeExcel(String fileName, String sheetName, String sheetTitle, List<List<?>> objects, List<String> heads) throws IOException, IllegalAccessException {
        exportExcel(fileName, sheetName, sheetTitle, objects, heads);
    }

    /**
     * 导出字符串数据.
     *
     * @param fileName   文件名
     * @param sheetName  sheet页名称
     * @param sheetTitle 表头列表名
     * @param objects    目标数据
     * @param clazz      类型
     */
    private static void exportExcel(String fileName, String sheetName, String sheetTitle, List<?> objects, Class<?> clazz) throws Exception {
        // 声明一个工作薄
        Workbook workBook = new HSSFWorkbook();

        Map<String, CellStyle> cellStyleMap = styleMap(workBook);
        // 表头样式
        CellStyle headStyle = cellStyleMap.get("head");
        // 正文样式
        CellStyle contentStyle = cellStyleMap.get("content");
        //正文整数样式
        CellStyle contentIntegerStyle = cellStyleMap.get("integer");
        //正文带小数整数样式
        CellStyle contentDoubleStyle = cellStyleMap.get("double");
        // 生成一个表格
        Sheet sheet = workBook.createSheet(sheetName);
        Field[] fields = clazz.getDeclaredFields();
        List<Field> fieldsNeedToShow = getFieldsNeedToShow(fields);
        //最新Excel列索引,从0开始
        int lastRowIndex = 0;
        // 设置表格默认列宽度
        sheet.setDefaultColumnWidth(DEFAULT_COLUMN_SIZE);
        // 合并单元格
        sheet.addMergedRegion(new CellRangeAddress(lastRowIndex, lastRowIndex, 0, fieldsNeedToShow.size() - 1));
        // 产生表格标题行
        Row rowMerged = sheet.createRow(lastRowIndex);
        lastRowIndex++;
        Cell mergedCell = rowMerged.createCell(0);
        mergedCell.setCellStyle(headStyle);
        mergedCell.setCellValue(new HSSFRichTextString(sheetTitle));
        // 产生表格表头列标题行
        Row row = sheet.createRow(lastRowIndex);
        lastRowIndex++;
        for (int i = 0; i < fieldsNeedToShow.size(); i++) {
            RichTextString text;
            if (null != fieldsNeedToShow.get(i).getDeclaredAnnotation(ExcelHeader.class)) {
                ExcelHeader excelHeader = fieldsNeedToShow.get(i).getDeclaredAnnotation(ExcelHeader.class);
                text = new HSSFRichTextString(excelHeader.value());
            } else {
                text = new HSSFRichTextString(fieldsNeedToShow.get(i).getName());
            }
            Cell cell = row.createCell(i);
            cell.setCellStyle(headStyle);
            cell.setCellValue(text);
        }

        // 遍历集合数据,产生数据行,前两行为标题行与表头行
        for (Object dataRow : objects) {
            row = sheet.createRow(lastRowIndex);
            lastRowIndex++;

            for (int j = 0; j < fieldsNeedToShow.size(); j++) {
                fieldsNeedToShow.get(j).setAccessible(true);

                Object dataObject = fieldsNeedToShow.get(j).get(dataRow);
                Object dst = null;
                ExcelTypeAdapter adapter = null;

                //加载转换器
                if (null != fieldsNeedToShow.get(j).getDeclaredAnnotation(ExcelHeaderTransform.class)) {
                    ExcelHeaderTransform transform = fieldsNeedToShow.get(j).getDeclaredAnnotation(ExcelHeaderTransform.class);

                    if (transform != null && transform.value() != null) {
                        Class<ExcelTypeAdapter> adapterClass = (Class<ExcelTypeAdapter>) transform.value();
                        adapter = adapterClass.newInstance();
                        dst = adapter.read(dataObject);
                    }
                }

                Cell contentCell = row.createCell(j);

                if (adapter != null && dst != null) {
                    contentCell.setCellStyle(contentStyle);
                    contentCell.setCellValue(dst.toString());
                    continue;
                }

                if (dataObject != null) {
                    if (dataObject instanceof Integer) {
                        contentCell.setCellStyle(contentIntegerStyle);
                        contentCell.setCellValue(Integer.parseInt(dataObject.toString()));
                    } else if (dataObject instanceof Double) {
                        contentCell.setCellStyle(contentDoubleStyle);
                        contentCell.setCellValue(Double.parseDouble(dataObject.toString()));
                    } else if (dataObject instanceof Long && dataObject.toString().length() == THIRTEEN) {
                        contentCell.setCellStyle(contentStyle);
                        contentCell.setCellValue(DateHelper.FormatDateTime(new Date(Long.parseLong(dataObject.toString()))));
                    } else if (dataObject instanceof Date) {
                        contentCell.setCellStyle(contentStyle);
                        contentCell.setCellValue(DateHelper.FormatDateTime((Date) dataObject));
                    } else if (dataObject instanceof Boolean) {
                        contentCell.setCellStyle(contentStyle);
                        contentCell.setCellValue((Boolean) dataObject);
                    } else {
                        contentCell.setCellStyle(contentStyle);
                        contentCell.setCellValue(dataObject.toString());
                    }
                } else {
                    contentCell.setCellStyle(contentStyle);
                    // 设置单元格内容为字符型
                    contentCell.setCellValue("");
                }

            }
        }
        try {
            HttpContext.Current.GetHttpResponse().reset();
            HttpContext.Current.GetHttpResponse().setCharacterEncoding(HttpConstant.UTF8_ENCODE);
            HttpContext.Current.GetHttpResponse().setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes("gbk"), "iso8859-1"));
            HttpContext.Current.GetHttpResponse().setHeader("Access-Control-Allow-Origin", "*");
            HttpContext.Current.GetHttpResponse().setContentType("application/octet-stream");
            OutputStream toClient = new BufferedOutputStream(HttpContext.Current.GetHttpResponse().getOutputStream());
            workBook.write(toClient);
            toClient.flush();
            toClient.close();


        } catch (IOException e) {
            throw new GlobalException(e.getMessage(), ErrorCode.PAGE_FILE_DOWNLOAD_FAILER);
        }

    }

    private static void exportExcel(String fileName, String sheetName, String sheetTitle, List<List<?>> objects, List<String> head) throws IOException, IllegalAccessException {

        // 声明一个工作薄
        Workbook workBook = new HSSFWorkbook();

        Map<String, CellStyle> cellStyleMap = styleMap(workBook);
        // 表头样式
        CellStyle headStyle = cellStyleMap.get("head");
        //正文整数样式
        CellStyle contentIntegerStyle = cellStyleMap.get("integer");
        //正文带小数整数样式
        CellStyle contentDoubleStyle = cellStyleMap.get("double");
        // 正文样式
        CellStyle contentStyle = cellStyleMap.get("content");
        // 生成一个表格
        Sheet sheet = workBook.createSheet(sheetName);

        //最新Excel列索引,从0开始
        int lastRowIndex = 0;
        // 设置表格默认列宽度
        sheet.setDefaultColumnWidth(DEFAULT_COLUMN_SIZE);
        // 合并单元格
        sheet.addMergedRegion(new CellRangeAddress(lastRowIndex, lastRowIndex, 0, head.size() - 1));
        // 产生表格标题行
        Row rowMerged = sheet.createRow(lastRowIndex);
        lastRowIndex++;
        Cell mergedCell = rowMerged.createCell(0);
        mergedCell.setCellStyle(headStyle);
        mergedCell.setCellValue(new HSSFRichTextString(sheetTitle));
        // 产生表格表头列标题行
        Row row = sheet.createRow(lastRowIndex);
        lastRowIndex++;
        for (int i = 0; i < head.size(); i++) {
            RichTextString text = new HSSFRichTextString(head.get(i));
            Cell cell = row.createCell(i);
            cell.setCellStyle(headStyle);
            cell.setCellValue(text);
        }

        // 遍历集合数据,产生数据行,前两行为标题行与表头行
        for (List<?> dataRow : objects) {
            row = sheet.createRow(lastRowIndex);
            lastRowIndex++;

            for (int j = 0; j < head.size(); j++) {
                Object dataObject = dataRow.get(j);
                Cell contentCell = row.createCell(j);

                if (dataObject != null) {
                    if (dataObject instanceof Integer) {
                        contentCell.setCellStyle(contentIntegerStyle);
                        contentCell.setCellValue(Integer.parseInt(dataObject.toString()));
                    } else if (dataObject instanceof Double) {
                        contentCell.setCellStyle(contentDoubleStyle);
                        contentCell.setCellValue(Double.parseDouble(dataObject.toString()));
                    } else if (dataObject instanceof Long && dataObject.toString().length() == 13) {
                        contentCell.setCellStyle(contentStyle);
                        contentCell.setCellValue(DateHelper.FormatDateTime(new Date(Long.parseLong(dataObject.toString()))));
                    } else if (dataObject instanceof Date) {
                        contentCell.setCellStyle(contentStyle);
                        contentCell.setCellValue(DateHelper.FormatDateTime((Date) dataObject));
                    } else if (dataObject instanceof Boolean) {
                        contentCell.setCellStyle(contentStyle);
                        contentCell.setCellValue((Boolean) dataObject);
                    } else {
                        contentCell.setCellStyle(contentStyle);
                        contentCell.setCellValue(dataObject.toString());
                    }
                } else {
                    contentCell.setCellStyle(contentStyle);
                    // 设置单元格内容为字符型
                    contentCell.setCellValue("");
                }

            }
        }
        try {
            HttpContext.Current.GetHttpResponse().reset();
            HttpContext.Current.GetHttpResponse().setCharacterEncoding(HttpConstant.UTF8_ENCODE);
            HttpContext.Current.GetHttpResponse().setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes("gbk"), "iso8859-1"));
            HttpContext.Current.GetHttpResponse().setHeader("Access-Control-Allow-Origin", "*");
            HttpContext.Current.GetHttpResponse().setContentType("application/octet-stream");
            OutputStream toClient = new BufferedOutputStream(HttpContext.Current.GetHttpResponse().getOutputStream());
            workBook.write(toClient);
            toClient.flush();
            toClient.close();


        } catch (IOException e) {
            throw new GlobalException(e.getMessage(), ErrorCode.PAGE_FILE_DOWNLOAD_FAILER);
        }
    }

    /**
     * 读取需要展示的列.
     *
     * @param fields fields
     * @return List
     */
    private static List<Field> getFieldsNeedToShow(Field[] fields) {
        List<Field> shownFields = new ArrayList<>();
        for (Field field : fields) {
            if (null != field.getDeclaredAnnotation(ExcelHeader.class)) {
                ExcelHeader excelHeader = field.getDeclaredAnnotation(ExcelHeader.class);
                if (!excelHeader.needShow()) {
                    continue;
                }
            }
            shownFields.add(field);
        }
        return shownFields;
    }

    /**
     * 读取excel中的中文列，匹配实体字段.
     *
     * @param fields fields
     * @return List
     */
    private static Map<String, String> GetAllFields(Field[] fields) {
        Map<String, String> map = new HashMap<>();
        for (Field field : fields) {
            if (null != field.getDeclaredAnnotation(ExcelHeader.class)) {
                ExcelHeader excelHeader = field.getDeclaredAnnotation(ExcelHeader.class);
                if (excelHeader != null && StringHelper.isNotBlank(excelHeader.value())) {
                    map.put(excelHeader.value(), field.getName());
                }
            }
        }
        return map;
    }

    /**
     * 单个excel导入.
     *
     * @param fileName 文件名
     * @param <T>      范型
     * @param clazz    类型
     * @return List
     */
    public static <T> List<T> ImportSingleExcel(String fileName, Class<T> clazz) throws Exception {
        String uploadPath = SystemConfigurationManager.getSingleton().GetUploadPath();
        File uploadFile = new File(uploadPath);
        if (!uploadFile.exists()) {
            uploadFile.mkdir();
        }
        File excelFile = new File(SystemConfigurationManager.getSingleton().GetUploadPath() + fileName);
        RequestContext context = new ServletRequestContext(HttpContext.Current.GetHttpRequest());
        DiskFileItemFactory factory = getDiskFileItemFactory(excelFile);
        ServletFileUpload upload = getServletFileUpload(factory);
        List<FileItem> fileItemList = upload.parseRequest(context);

        fileItemList.get(0).write(excelFile);

        InputStream inputStream = new FileInputStream(excelFile);

        return importExcel(inputStream, fileName, clazz);
    }

    /**
     * 多个文件导入.
     *
     * @param <T>   泛型
     * @param clazz 类型
     * @param file  文件扩展
     */
    public static <T> List<T> ImportMultiExcel(MultipartFile file, Class<T> clazz) throws Exception {
        InputStream inputStream = file.getInputStream();
        String fileName = file.getName();
        return importExcel(inputStream, fileName, clazz);

    }

    /**
     * excel导入.
     *
     * @param inputStream 文件名
     * @param <T>         范型
     * @param clazz       类型
     * @return List
     */
    private static <T> List<T> importExcel(InputStream inputStream, String fileName, Class<T> clazz) throws Exception {

        List<T> list = new ArrayList<T>();

        Workbook workbook = null;

        Sheet sheet = null;

        if (fileName.endsWith("xlsx")) {
            workbook = new XSSFWorkbook(inputStream);
        } else {
            workbook = new HSSFWorkbook(inputStream);
        }

        sheet = workbook.getSheetAt(0);

        // 行数。
        int rowNumbers = sheet.getLastRowNum() + 1;

        // Excel第一行。
        Row temp = sheet.getRow(1);

        if (temp == null) {
            return null;
        }

        CaseInsensitiveKeyMap caseInsensitiveKeyMap = new CaseInsensitiveKeyMap();

        // 列数
        int columnNumbers = temp.getLastCellNum();

        //先存储列名，用于后续的 赋值
        for (int i = 0; i < columnNumbers; i++) {
            String fieldName = StringHelper.toLowerCaseFirstOne(StringHelper.GetDBStringToCamelStyle(temp.getCell(i).toString().trim()));
            String index = String.valueOf(i);
            caseInsensitiveKeyMap.put(index, fieldName);
        }

        Field[] fields = clazz.getDeclaredFields();

        Map<String, String> fieldMap = GetAllFields(fields);

        if (fieldMap.size() == 0) {
            return null;
        }

        // 读数据。
        for (int rowIndex = 2; rowIndex < rowNumbers; rowIndex++) {
            Row row = sheet.getRow(rowIndex);

            T t = clazz.newInstance();

            for (int columnIndex = 0; columnIndex < columnNumbers; columnIndex++) {

                String columnName = caseInsensitiveKeyMap.get(String.valueOf(columnIndex)).toString();

                columnName = fieldMap.get(columnName);

                for (int fieldIndex = 0; fieldIndex < fields.length; fieldIndex++) {

                    fields[fieldIndex].setAccessible(true);

                    String fieldName = fields[fieldIndex].getName();

                    if (fieldName.equalsIgnoreCase(columnName)) {

                        Cell cellValue = row.getCell(columnIndex);

                        if (cellValue == null || cellValue.toString().length() == 0) {
                            continue;
                        }

                        ExcelTypeAdapter adapter = null;

                        //加载转换器
                        if (null != fields[fieldIndex].getDeclaredAnnotation(ExcelHeaderTransform.class)) {
                            ExcelHeaderTransform transform = fields[fieldIndex].getDeclaredAnnotation(ExcelHeaderTransform.class);

                            if (transform != null && transform.value() != null) {
                                Class<ExcelTypeAdapter> adapterClass = (Class<ExcelTypeAdapter>) transform.value();
                                adapter = adapterClass.newInstance();
                            }
                        }

                        //region 赋值
                        if (fields[fieldIndex].getType().equals(Boolean.class) && !"false".equals(cellValue.toString()) && !"true".equals(cellValue.toString())) {
                            Integer result = Integer.valueOf(cellValue.toString());
                            if (result > 0) {
                                fields[fieldIndex].set(t, true);
                            } else {
                                fields[fieldIndex].set(t, false);
                            }
                        } else if (fields[fieldIndex].getType().equals(Long.class)) {
                            fields[fieldIndex].set(t, Long.parseLong(cellValue.toString()));
                        } else if (fields[fieldIndex].getType().equals(Integer.class)) {
                            if (adapter != null) {
                                Object dst = adapter.read(cellValue.toString());
                                if (dst != null) {
                                    Integer in = new Integer(dst.toString());
                                    fields[fieldIndex].set(t, in);
                                } else {
                                    continue;
                                }
                            } else {
                                Integer in = new Integer(new Double(cellValue.toString()).intValue());
                                fields[fieldIndex].set(t, in);
                            }
                        } else if (fields[fieldIndex].getType().equals(Double.class)) {
                            fields[fieldIndex].set(t, Double.parseDouble(cellValue.toString()));
                        } else if (fields[fieldIndex].getType().equals(Date.class)) {
                            String dateString = cellValue.toString();
                            Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateString);
                            fields[fieldIndex].set(t, parse);
                        } else if (fields[fieldIndex].getType().equals(String.class)) {
                            String strValue = cellValue.toString();
                            fields[fieldIndex].set(t, strValue);
                        } else {
                            fields[fieldIndex].set(t, cellValue.toString());
                        }
                        //endregion

                        break;
                    }

                }
            }

            list.add(t);

        }

        return list;
    }

    /**
     * excel导入.
     *
     * @param factory factory
     * @return ServletFileUpload
     */
    private static ServletFileUpload getServletFileUpload(DiskFileItemFactory factory) {
        ServletFileUpload upload = new ServletFileUpload(factory);
        //监听文件上传的进度
        upload.setProgressListener(new ProgressListener() {
            @Override
            //pBytesRead：已读取到的文件大小
            //pContentLength：文件大小
            public void update(long pBytesRead, long pContentLength, int pItems) {
                System.out.println("总大小：" + pContentLength + "已上传：" + pBytesRead);
            }
        });
        //处理乱码问题
        upload.setHeaderEncoding("UTF-8");
        //设置单个文件的最大值
        upload.setFileSizeMax(ONE_THOUSAND_TWENTY_FOUR * ONE_THOUSAND_TWENTY_FOUR * HUNDRED);
        //设置总共能够上传文件的大小
        //1024=1kb*1024=1M*10=10M
        upload.setSizeMax(ONE_THOUSAND_TWENTY_FOUR * ONE_THOUSAND_TWENTY_FOUR * HUNDRED);
        return upload;
    }

    /**
     * getDiskFileItemFactory.
     *
     * @param file file
     * @return DiskFileItemFactory
     */
    private static DiskFileItemFactory getDiskFileItemFactory(File file) {
        DiskFileItemFactory factory = new DiskFileItemFactory();
        //通过这个工厂设置一个缓冲区，当上传的文件大于这个缓冲区的时候，将他放到临时文件中：
        factory.setSizeThreshold(ONE_THOUSAND_TWENTY_FOUR * ONE_THOUSAND_TWENTY_FOUR);
        factory.setRepository(file);
        return factory;
    }

    //endregion

    //region excel样式

    /**
     * 创建单元格表头样式.
     *
     * @param workbook 工作薄
     * @return CellStyle
     */
    private static CellStyle createCellHeadStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        // 设置边框样式
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        //设置对齐样式
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        // 生成字体
        Font font = workbook.createFont();
        // 表头样式
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
//        style.setFillForegroundColor((short) 25);
        style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
        font.setFontHeightInPoints(TWELVE);
        font.setBold(true);
        // 把字体应用到当前的样式
        style.setFont(font);
        return style;
    }

    /**
     * 创建单元格正文样式.
     *
     * @param workbook 工作薄
     * @return CellStyle
     */
    private static CellStyle createCellContentStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        // 设置边框样式
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        //设置对齐样式
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        // 生成字体
        Font font = workbook.createFont();
        // 正文样式
        style.setFillPattern(FillPatternType.NO_FILL);
        font.setBold(false);
        // 把字体应用到当前的样式
        style.setFont(font);
        return style;
    }

    /**
     * 单元格样式(Integer)列表.
     *
     * @param workbook 工作薄
     * @return CellStyle
     */
    private static CellStyle createCellContent4IntegerStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        // 设置边框样式
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        //设置对齐样式
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        // 生成字体
        Font font = workbook.createFont();
        // 正文样式
        style.setFillPattern(FillPatternType.NO_FILL);
        font.setBold(false);
        // 把字体应用到当前的样式
        style.setFont(font);
        //TODO
        //style.setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0"));//数据格式只显示整数
        return style;
    }

    /**
     * 单元格样式(Double)列表.
     *
     * @param workbook 工作薄
     * @return CellStyle
     */
    private static CellStyle createCellContent4DoubleStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        // 设置边框样式
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        //设置对齐样式
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        // 生成字体
        Font font = workbook.createFont();
        // 正文样式
        style.setFillPattern(FillPatternType.NO_FILL);
        font.setBold(false);
        // 把字体应用到当前的样式
        style.setFont(font);
        //TODO
        //style.setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0.00"));//保留两位小数点
        return style;
    }

    /**
     * 单元格样式列表.
     *
     * @param workbook 工作薄
     * @return Map
     */
    private static Map<String, CellStyle> styleMap(Workbook workbook) {
        Map<String, CellStyle> styleMap = new LinkedHashMap<>();
        styleMap.put("head", createCellHeadStyle(workbook));
        styleMap.put("content", createCellContentStyle(workbook));
        styleMap.put("integer", createCellContent4IntegerStyle(workbook));
        styleMap.put("double", createCellContent4DoubleStyle(workbook));
        return styleMap;
    }

    //endregion
}
