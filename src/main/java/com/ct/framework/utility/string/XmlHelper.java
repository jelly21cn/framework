package com.ct.framework.utility.string;

import com.ct.framework.utility.http.BasicHttpUtil;
import com.ct.framework.core.entity.RpcResponseEntity;
import com.ct.framework.utility.SerializeHelper;
import lombok.extern.slf4j.Slf4j;
import org.datanucleus.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage：Xml操作辅助类
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
@Slf4j
public final class XmlHelper {
    /**
     * 10.
     */
    private static final int TEN = 10 * 6;

    /**
     * 私有构造函数.
     */
    private XmlHelper() {
    }

    /**
     * 通过Http请求，访问负载均衡器，读取远程配置.
     *
     * @param condition 查询条件
     * @param url       url地址
     * @return NodeList
     */
    public static NodeList GetNodeListByHttp(String url, String condition) {
        NodeList nodeList = null;

        //创建一个DOM解析器工厂对象;
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

        //通过工厂对象创建DOM解析器
        DocumentBuilder documentBuilder;

        try {
            String responseString = BasicHttpUtil.get(url + "/config/get?" + condition, TEN);

            if (StringUtils.isEmpty(responseString)) {
                log.error("读取 配置服务器 内容为空");
                return null;
            }

            RpcResponseEntity rpcResponseEntity = SerializeHelper.JsonDeserialize(responseString, RpcResponseEntity.class);

            documentBuilder = documentBuilderFactory.newDocumentBuilder();

            String content = rpcResponseEntity.getContent().GetContent();

            InputSource is = new InputSource(new StringReader(content));

            Document doc = documentBuilder.parse(is);

            nodeList = doc.getDocumentElement().getChildNodes();

        } catch (Exception exception) {
            log.error("读取 配置服务器 失败:" + exception);
        }

        return nodeList;
    }

    /**
     * 读取xml文件，返回node list.
     *
     * @param file 文件路径
     * @return NodeList
     */
    public static NodeList GetNodeList(String file) {

        NodeList nodeList = null;

        //创建一个DOM解析器工厂对象;
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

        //通过工厂对象创建DOM解析器
        DocumentBuilder documentBuilder;

        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();

            ClassLoader classLoader = XmlHelper.class.getClassLoader();

            //InputStream resourceAsStream = classLoader.getResourceAsStream(file);

            InputStream resourceAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(file);

            Document doc = documentBuilder.parse(resourceAsStream);

            nodeList = doc.getDocumentElement().getChildNodes();

        } catch (Exception exception) {
            log.error("读取文件 [" + file + "] 失败:" + exception);
        }

        return nodeList;
    }

    /**
     * 返回xml node list节点的键值对.
     *
     * @param nodeList nodeList
     * @return Map
     */
    public static Map<String, String> GetMap(NodeList nodeList) {
        Map<String, String> map = new HashMap<>();

        for (int j = 0; j < nodeList.getLength(); j++) {

            Node childrenNode = nodeList.item(j);

            String childrenNodeName = childrenNode.getNodeName();

            if ("#text".equals(childrenNodeName)) {
                continue;
            }

            if ("#comment".equals(childrenNodeName)) {
                continue;
            }

            String key = childrenNode.getNodeName();
            String val = null;
            if (childrenNode.getFirstChild() != null) {
                val = childrenNode.getFirstChild().getNodeValue();
            }
            map.put(key, val);

        }

        return map;
    }

    /**
     * xml 解析.
     *
     * @param nodeList nodeList
     * @return List<String>
     */
    public static List<String> GetList(NodeList nodeList) {
        List<String> list = new ArrayList<>();

        for (int j = 0; j < nodeList.getLength(); j++) {

            Node childrenNode = nodeList.item(j);

            String childrenNodeName = childrenNode.getNodeName();

            if ("#text".equals(childrenNodeName)) {
                continue;
            }

            if ("#comment".equals(childrenNodeName)) {
                continue;
            }

            String val = childrenNode.getTextContent();

            list.add(val);
        }

        return list;
    }

    /**
     * 返回xml node节点的属性.
     *
     * @param node node
     * @return Map
     */
    public static Map<String, String> GetAttributes(Node node) {
        Map<String, String> map = new HashMap<>();
        int count = node.getAttributes().getLength();

        for (int i1 = 0; i1 < count; i1++) {

            Node attribute = node.getAttributes().item(i1);
            String name = attribute.getNodeName();
            String value = attribute.getNodeValue();
            map.put(name, value);
        }
        return map;
    }

    /**
     * 返回xml 第一个节点.
     *
     * @param xmlStr xmlStr
     * @return Node
     */
    public static Node GetFirstNode(String xmlStr) {
        try {
            //创建一个DOM解析器工厂对象;
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            //通过工厂对象创建DOM解析器
            DocumentBuilder documentBuilder;
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document doc = documentBuilder.parse(new ByteArrayInputStream(xmlStr.getBytes(StandardCharsets.UTF_8)));
            return doc.getFirstChild();
        } catch (Exception e) {
            log.error("解析xml字符串失败:{}", e.getMessage(), e);
            return null;
        }
    }

    /**
     * 返回xml文档.
     *
     * @param file file
     * @return Document
     */
    public static Document GetDocument(String file) {
        Document doc = null;

        //创建一个DOM解析器工厂对象;
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

        //通过工厂对象创建DOM解析器
        DocumentBuilder documentBuilder;

        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();

            ClassLoader classLoader = XmlHelper.class.getClassLoader();

            InputStream resourceAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(file);

            doc = documentBuilder.parse(resourceAsStream);
        } catch (Exception exception) {
            log.error("读取文件 [" + file + "] 失败:" + exception);
        }
        return doc;

    }

    /**
     * 更新xml文档.
     *
     * @param doc doc
     */
    public static void update(Document doc) throws Exception {

        Element documentElement = doc.getDocumentElement();
        NodeList propertyList = documentElement.getElementsByTagName("property");

        for (int i = 0; i < propertyList.getLength(); i++) {
            Node node = propertyList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element property = (Element) node;
                String nameValue = property.getElementsByTagName("name").item(0).getChildNodes().item(0).getNodeValue();
                if (nameValue.equals("yarn.scheduler.capacity.root.queues")) {
                    Element valueEle = (Element) property.getElementsByTagName("value").item(0);
                    String textContent = valueEle.getTextContent();
                    valueEle.setTextContent(textContent + ",tenant_43");
                }
            }
        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.transform(new DOMSource(doc), new StreamResult("new.xml"));

    }

}
