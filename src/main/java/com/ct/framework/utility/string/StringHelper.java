package com.ct.framework.utility.string;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.util.Base64;
import java.util.Random;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage：String操作辅助类
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public final class StringHelper {

    /**
     * 3.
     */
    private static final int THREE = 3;

    /**
     * 4.
     */
    private static final int FOUR = 4;

    /**
     * 8.
     */
    private static final int EIGHT = 8;

    /**
     * 8.
     */
    private static final int TEN = 10;

    /**
     * 62.
     */
    private static final int SIXTY_TWO = 62;

    /**
     * 0xf.
     */
    private static final char XF = 0xf;

    /**
     * 私有构造函数.
     */
    private StringHelper() {
    }

    /**
     * 首字母小写.
     *
     * @param s 入参
     * @return string
     */
    public static String toLowerCaseFirstOne(String s) {
        if (Character.isLowerCase(s.charAt(0))) {
            return s;
        } else {
            return (new StringBuilder()).append(Character.toLowerCase(s.charAt(0))).append(s.substring(1)).toString();
        }
    }

    /**
     * 首字母转大写.
     *
     * @param s 入参
     * @return string
     */
    public static String ToUpperCaseFirstOne(String s) {
        if (Character.isUpperCase(s.charAt(0))) {
            return s;
        } else {
            return (new StringBuilder()).append(Character.toUpperCase(s.charAt(0))).append(s.substring(1)).toString();
        }
    }

    /**
     * 是否空.
     *
     * @param cs 入参
     * @return boolean
     */
    public static boolean isNotBlank(final CharSequence cs) {
        return !isBlank(cs);
    }

    /**
     * 是否空.
     *
     * @param cs 入参
     * @return boolean
     */
    public static boolean isBlank(final CharSequence cs) {
        int strLen;
        if (cs == null || cs.length() == 0) {
            return true;
        }
        strLen = cs.length();
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(cs.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * like 语句.
     *
     * @param s 入参
     * @return boolean
     */
    public static String convertToLike(String s) {
        if (isBlank(s)) {
            return "";
        }
        return "%" + s + "%";
    }

    /**
     * 获取随机字符串.
     *
     * @param length 长度
     * @return String
     */
    public static String GetRandomString(int length) {
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(SIXTY_TWO);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }

    /**
     * MD5.
     *
     * @param s 入参
     * @return string
     */
    public static String MD5(String s) {

        char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

        try {
            byte[] btInput = s.getBytes();
            // 获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            // 使用指定的字节更新摘要
            mdInst.update(btInput);
            // 获得密文
            byte[] md = mdInst.digest();
            // 把密文转换成十六进制的字符串形式
            int j = md.length;
            char[] str = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> FOUR & XF];
                str[k++] = hexDigits[byte0 & XF];
            }
            return new String(str);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Base64 解密.
     *
     * @param msg 密文
     * @return String
     */
    public static String GetBase64Decoder(String msg) {
        byte[] byteArr = Base64.getDecoder().decode(msg);
        return new String(byteArr);
    }

    /**
     * Base64 加密.
     *
     * @param content 明文
     * @return String
     */
    public static String GetBase64Encoder(String content) {
        return Base64.getEncoder().encodeToString(content.getBytes());
    }

    /**
     * Throwable错误转为String.
     *
     * @param e 错误信息
     * @return string
     */
    public static String toString(Throwable e) {
        StringWriter stringWriter = new StringWriter();
        e.printStackTrace(new PrintWriter(stringWriter));
        String errorMsg = stringWriter.toString();
        return errorMsg;
    }

    /**
     * 根据文件扩展名获得MimeType.
     *
     * @param filePath 文件路径
     * @return string
     */
    public static String GetMimeType(String filePath) {
        return URLConnection.getFileNameMap().getContentTypeFor(filePath);
    }

    /**
     * 驼峰.
     *
     * @param str 如参
     * @return string
     */
    public static String GetDBStringToCamelStyle(String str) {
        if (str != null) {
            if (str.contains("_")) {
                str = str.toLowerCase();
                StringBuilder sb = new StringBuilder();
                sb.append(String.valueOf(str.charAt(0)).toUpperCase());
                for (int i = 1; i < str.length(); i++) {
                    char c = str.charAt(i);
                    if (c != '_') {
                        sb.append(c);
                    } else {
                        if (i + 1 < str.length()) {
                            sb.append(String.valueOf(str.charAt(i + 1)).toUpperCase());
                            i++;
                        }
                    }
                }
                return sb.toString();
            } else {
                String firstChar = String.valueOf(str.charAt(0)).toUpperCase();
                String otherChars = str.substring(1);
                return firstChar + otherChars;
            }
        }
        return null;
    }

    /**
     * 当前系统的 标准数据类型
     *
     * @param dataType 数据类型枚举
     * @return string
     */
    public static String GetDataTypeString(int dataType) {
        switch (dataType) {
            case 0:
                return "INTEGER";
            case 1:
                return "SMALLINT";
            case 2:
                return "DECIMAL";
            case 3:
                return "NUMERIC";
            case 4:
                return "DATETIME";
            case 5:
                return "DATE";
            case 6:
                return "TIMESTAMP";
            case 7:
                return "TIME";
            case 8:
                return "CHAR";
            case 9:
                return "VARCHAR";
            case 10:
                return "BINARY";
            case 11:
                return "VARBINARY";
            case 12:
                return "BLOB";
            case 13:
                return "TEXT";
            case 20:
                return "枚举";
            case 99:
                return "维度表";
            default:
                return "Unknow";
        }
    }
}
