package com.ct.framework.utility.string;

import java.util.regex.Pattern;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   沈剑峰
 * Create Date:  01/06/2021
 * Usage：正则
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public final class PatternHelper {
    /**
     * 私有构造函数.
     */
    private PatternHelper() {
    }

    /**
     * 日期+时间格式正则.
     */
    private static final String DATETIME_REGEX = "^(((20[0-3][0-9]-(0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|"
            +
            "(20[0-3][0-9]-(0[2469]|11)-(0[1-9]|[12][0-9]|30))) (20|21|22|23|[0-1][0-9]):[0-5][0-9]:[0-5][0-9])$";
    /**
     * Int.
     */
    private static final String INT_REGEX = "^-?[0-9]+$";
    /**
     * 手机号码正则.
     */
    private static final String PHONE_REGEX = "^((13[0-9])|(14(0|[5-7]|9))|(15([0-3]|[5-9]))|(16(2|[5-7]))|(17[0-8])|(18[0-9])|(19([0-3]|[5-9])))\\d{8}$";
    /**
     * 邮箱地址正则.
     */
    private static final String EMAIL_REGEX = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";

    /**
     * 判断是否为yyyy-MM-dd HH:mm:ss 格式的字符串.
     *
     * @param datetime 时间字符串
     * @return boolean
     */
    public static boolean IsDateTime(String datetime) {
        Pattern p = Pattern.compile(DATETIME_REGEX);
        return p.matcher(datetime).matches();
    }

    /**
     * 判断是否 int型.
     *
     * @param param 入参
     * @return boolean
     */
    public static boolean IsInt(String param) {
        Pattern p = Pattern.compile(INT_REGEX);
        return p.matcher(param).matches();
    }

    /**
     * 判断是否手机号是否符合规范.
     *
     * @param phone 手机号
     * @return boolean
     */
    public static boolean checkPhone(String phone) {
        Pattern p = Pattern.compile(PHONE_REGEX);
        return p.matcher(phone).matches();
    }

    /**
     * 判断是否邮箱是否符合规范.
     *
     * @param email 邮箱
     * @return boolean
     */
    public static boolean checkEmail(String email) {
        Pattern p = Pattern.compile(EMAIL_REGEX);
        return p.matcher(email).matches();
    }

    /**
     * 判断文件名.
     *
     * @param filename 文件名
     * @return boolean
     */
    public static boolean checkFileName(String filename) {
        if (!filename.matches("[^\\\\{}\\^`\\[\\]<>#~%|]+")) {
            return false;
        }

        return true;
    }
}
