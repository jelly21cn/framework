package com.ct.framework.utility.string;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/*****************************************************************
 * Copyright (C) xxx Corporation. All rights reserved.
 *
 * Author:   xxx
 * Create Date:  01/06/2021
 * Usage：密码
 *
 * RevisionHistory
 * Date                 Author                     Description
 * 2021-6-1             沈剑峰                         新增
 * @author xxx沈剑峰
 *****************************************************************/
public final class PasswordHelper {

    /**
     * 私有构造函数.
     */
    private PasswordHelper() {
    }

    /**
     * 8.
     */
    private static final int EIGHT = 8;

    /**
     * 密码加盐长度.
     */
    private static final int DEFAULT_SALT_LENGTH = 10;
    /**
     * 密码加盐.
     */
    public static final String DEFAULT_SALT_KEY = "salt";
    /**
     * 密码.
     */
    public static final String DEFAULT_PASSWORD_KEY = "password";

    /**
     * 密码字符串.
     *
     * @param password 明文密码
     * @return Map
     */
    public static Map<String, String> GeneratePassword(String password) {
        Map<String, String> result = new HashMap<>();
        String salt = GetSalt(DEFAULT_SALT_LENGTH);
        String passwordDb = GetPasswordMd5(password, salt);
        result.put(DEFAULT_SALT_KEY, salt);
        result.put(DEFAULT_PASSWORD_KEY, passwordDb);
        return result;
    }

    /**
     * 获取盐值.
     *
     * @param count 长度
     * @return string
     */
    private static String GetSalt(int count) {
        return RandomStringUtils.random(count, true, true);
    }

    /**
     * 密码字符串.
     *
     * @param password 密码
     * @param salt     盐值
     * @return string
     */
    private static String GetPasswordMd5(String password, String salt) {
        String passwordMd5 = DigestUtils.md5DigestAsHex(password.getBytes(StandardCharsets.UTF_8));
        return DigestUtils.md5DigestAsHex((passwordMd5 + salt).getBytes(StandardCharsets.UTF_8));
    }

    /**
     * 获取随机密码.
     *
     * @param length 长度
     * @return String
     */
    public static String GetRandomPassword(int length) {
        String result = null;
        while (length >= EIGHT) {
            result = MakeRandomPassword(length);
            if (result.matches(".*[a-z]{1,}.*") && result.matches(".*[A-Z]{1,}.*") && result.matches(".*\\d{1,}.*") && result.matches(".*[~!@#$%^&*\\.?]{1,}.*")) {
                return result;
            }
            result = MakeRandomPassword(length);
        }
        return "长度不得少于8位!";
    }

    /**
     * 生成密码.
     *
     * @param length 长度
     * @return string
     */
    private static String MakeRandomPassword(int length) {
        char[] charr = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890~!@#$%^&*.?".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random r = new Random();
        for (int x = 0; x < length; ++x) {
            sb.append(charr[r.nextInt(charr.length)]);
        }
        return sb.toString();
    }
}
